package com.mayaswell.orbital;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.RectF;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.Impulse;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.orbital.OrbitalLaser.Global;

public class Slice  implements Controllable {
	public int startFrame = 0;
	public float position = 0;
	public int ind = 0;
	
	private int group=-1;

	public Direction direction = Direction.FORWARD;
	public float pan=0;
	public float gain=1;
	public float tune=0;
	public int stutter=1;
	public short fxBus = 0;
	public ArrayList<LFO> lfo = new ArrayList<LFO>();
	public ArrayList<Envelope> envelope = new ArrayList<Envelope>();
	public ArrayList<Impulse> impulse = new ArrayList<Impulse>();

	public int type = 0;
	protected RectF frameRect = new RectF();
	protected RectF posRect = new RectF();
	public float frameX = 0;
	public int posX = 0;
	public int label = -1;
	public boolean enabled = true;
	public boolean selected = false;

	public Slice(int i, long l, float p)
	{
		ind = i;
		startFrame = (int) l;
		position = p;
	}

	public Slice()
	{
		this(0, 0, 0);
	}

	public Slice clone()
	{
		Slice s = new Slice();
		s.startFrame = startFrame;
		s.position = position;

		s.gain = gain;
		s.pan = pan;
		s.tune = tune;
		s.direction = direction;
		s.stutter = stutter;
		s.fxBus = fxBus;
		s.group = group;
		for (LFO i: lfo) {
			s.lfo.add(i.clone());
		}
		for (Envelope i: envelope) {
			s.envelope.add(i.clone());
		}
		for (Impulse i: impulse) {
			s.impulse.add(i.clone());
		}

		s.ind = ind;
		s.type = type;
		s.frameRect = new RectF(frameRect);
		s.posRect = new RectF(posRect);
		s.frameX = frameX;
		s.posX = posX;
		s.label = label;
		s.enabled = enabled;
		s.selected = selected;
		return s;
	}

	public void setFrameRect(float l, float t, float r, float b)
	{
		frameRect.set(l, t, r, b);
	}

	public RectF getFrameRect()
	{
		return frameRect;
	}

	public RectF getPositionedFrameRect()
	{
		return new RectF(frameX+frameRect.left, frameRect.top, frameX+frameRect.right, frameRect.bottom);
	}

	public void setPosRect(float l, float t, float r, float b)
	{
		posRect.set(l, t, r, b);
	}

	public RectF getPosRect()
	{
		return posRect;
	}

	public void setDirection(Direction d) {
		direction = d;
	}

	public void setTune(float v) {
		tune = v;
	}

	public void setGain(float v) {
		gain = v;
	}

	public void setPan(float v) {
		pan = v;
	}

	public void setStutter(int v) {
		stutter = v;
	}

	/*
	 * lfo entry points
	 */
	private LFO ensureExistsLFO(int xyci) {
		if (xyci < 0 || xyci >= Global.LFO_PER_WRECK) return null;
		for (int i = lfo.size(); i<=xyci; i++) {
			lfo.add(OrbitalLaser.getOL().newLFO()); // TODO XXX watch defaults
		}
		return lfo.get(xyci);
	}

	private ModulationTarget ensureExistsLFOTarget(int xyci, int xycj) {
		if (xycj < 0 || xycj >= Global.MAX_LFO_TGT) return null;
		LFO l = ensureExistsLFO(xyci);
		if (l.target == null) l.target = new ArrayList<ModulationTarget>();
		for (int i=l.target.size(); i<=xycj; i++) {
			l.target.add(new ModulationTarget());
		}
		return lfo.get(xyci).target.get(xycj);
	}

	public boolean setLFORate(int xyci, float amt)
	{
		LFO l = ensureExistsLFO(xyci);
		if (l == null) return false;
		l.rate = amt;
		return true;
	}

	public boolean setLFOPhase(int xyci, float amt)
	{
		LFO l = ensureExistsLFO(xyci);
		if (l == null) return false;
		l.phase = amt;
		return true;
	}

	public boolean setLFOLock(int xyci, boolean amt)
	{
		LFO l = ensureExistsLFO(xyci);
		if (l == null) return false;
		l.tempoLock = amt;
		return true;
	}

	public boolean setLFOReset(int xyci, ResetMode amt)
	{
		LFO l = ensureExistsLFO(xyci);
		if (l == null) return false;
		l.trigger = amt;
		return true;
	}

	public boolean setLFOWave(int xyci, LFWave amt)
	{
		LFO l = ensureExistsLFO(xyci);
		if (l == null) return false;
		l.waveform = amt;
		return true;
	}

	public boolean setLFOTarget(int xyci, int xycj, ICControl target, float amt)
	{
		ModulationTarget m = ensureExistsLFOTarget(xyci, xycj);
		if (m== null) return false;
		m.target = target;
		m.amount = amt;
		return true;
	}

	public boolean setLFOTargetAmt(int xyci, int xycj, float amt)
	{
		ModulationTarget m = ensureExistsLFOTarget(xyci, xycj);
		if (m== null) return false;
		m.amount = amt;
		return true;
	}



	 /*
	  * envelope entry points
	  */
	 private Envelope ensureExistsEnv(int xyci) {
		if (xyci < 0 || xyci >= Global.ENV_PER_WRECK) return null;
		if (envelope == null) envelope = new ArrayList<Envelope>();
		for (int i = envelope.size(); i<=xyci; i++) {
			envelope.add(OrbitalLaser.getOL().newEnv());
		}
		return envelope.get(xyci);
	}

	 private ModulationTarget ensureExistsEnvTarget(int xyci, int xycj) {
		 if (xycj < 0 || xycj >= Global.MAX_ENV_TGT) return null;
		 Envelope l = ensureExistsEnv(xyci);
		 if (l == null) return null;
		 if (l.target == null) l.target = new ArrayList<ModulationTarget>();
		 for (int i=l.target.size(); i<=xycj; i++) {
			 l.target.add(new ModulationTarget());
		 }
		 return envelope.get(xyci).target.get(xycj);
	 }
	 
	 public boolean setEnvelopeLock(int xyci, boolean amt)
	 {
		 Envelope l = ensureExistsEnv(xyci);
		 if (l == null) return false;
		 l.tempoLock = amt;
		 return true;
	 }

	 public boolean setEnvelopeReset(int xyci, ResetMode amt)
	 {
		 Envelope l = ensureExistsEnv(xyci);
		 if (l == null) return false;
		 l.trigger = amt;
		 return true;
	 }

	 public boolean setEnvelopeAttack(int xyci, float t)
	 {
		 Envelope l = ensureExistsEnv(xyci);
		 if (l == null) return false;
		 l.attackT = t;
		 return true;
	 }

	 public boolean setEnvelopeDecay(int xyci, float t)
	 {
		 Envelope l = ensureExistsEnv(xyci);
		 if (l == null) return false;
		 l.decayT = t;
		 return true;
	 }

	 public boolean setEnvelopeSustain(int xyci, float t)
	 {
		 Envelope l = ensureExistsEnv(xyci);
		 if (l == null) return false;
		 l.sustainT = t;
		 return true;
	 }

	 public boolean setEnvelopeSustainLevel(int xyci, float l)
	 {
		 Envelope e = ensureExistsEnv(xyci);
		 if (e == null) return false;
		 e.sustainL = l;
		 return true;
	 }

	 public boolean setEnvelopeRelease(int xyci, float t)
	 {
		 Envelope e = ensureExistsEnv(xyci);
		 if (e == null) return false;
		 e.releaseT = t;
		 return true;
	 }

	 public boolean setEnvelopeTarget(int xyci, int xycj, ICControl target, float amt)
	 {
		 ModulationTarget m = ensureExistsEnvTarget(xyci, xycj);
		 if (m == null) return false;
		 m.target = target;
		 m.amount = amt;
		 return true;
	 }

	 public boolean setEnvelopeTargetAmt(int xyci, int xycj, float amt)
	 {
		 ModulationTarget m = ensureExistsEnvTarget(xyci, xycj);
		 if (m == null) return false;
		 m.amount = amt;
		 return true;
	 }

	 /**
	  * controllable methods
	  */
	 @Override
	 public ControlsAdapter getInterfaceTgtAdapter() {
		 return null;
	 }

	 @Override
	 public int controllableId() {
		 return ind;
	 }
	 @Override
	 public String controllableType() {
		 return "Slice";
	 }
	 @Override
	 public float floatValue(Control cc) {
		 switch (cc.ccontrolId()) {
		 case CControl.GAIN: return gain;
		 case CControl.PAN: return pan;
		 case CControl.TUNE: return tune;
		 /*
			case CControl.LOOP_START: {
				return nTotalFrames == 0? 0: (((float)state.loopStart)/nTotalFrames);
			}
			case CControl.LOOP_LENGTH: {
				return nTotalFrames == 0? 0: (((float)state.loopLength)/nTotalFrames);
			}
		  */
		 /*
			case CControl.FLT_CUTOFF: return filterFrequency;
			case CControl.FLT_ENVMOD: return filterEnvMod;
			case CControl.FLT_RESONANCE: return filterResonance;
		  */
		 default: {
			 if (cc.ccontrolId() >= CControl.envIdBase(0) && cc.ccontrolId() < CControl.envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				 int envId = CControl.env4Id(cc.ccontrolId());
				 int param = CControl.envParam4Id(cc.ccontrolId());
				 if (param == 0) {
					 if (envId<envelope.size()) { return envelope.get(envId).attackT; }
				 } else if (param == 1) { // decay
					 if (envId<envelope.size()) {	return envelope.get(envId).decayT; }
				 } else if (param == 2) {// sustain
					 if (envId<envelope.size()) {	return envelope.get(envId).sustainT; } 
				 } else if (param == 3) {// sustain level
					 if (envId<envelope.size()) {	return envelope.get(envId).sustainL; } 
				 } else if (param == 4) { // release
					 if (envId<envelope.size()) {	return envelope.get(envId).releaseT; }
				 } else {// depth(param-5);
					 if (envId<envelope.size() && (param-5)<envelope.get(envId).target.size()) { 
						 return envelope.get(envId).target.get(param-5).amount; 
					 }
				 }
			 } else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK)) {
				 int lfoId = CControl.lfo4Id(cc.ccontrolId());
				 int param = CControl.lfoParam4Id(cc.ccontrolId());
				 if (param == 0) { // rate
					 if (lfoId<lfo.size()) { return lfo.get(lfoId).rate; }
				 } else if (param == 1) { // phase
					 if (lfoId<lfo.size()) { return lfo.get(lfoId).phase; } 
				 } else {// depth(param-2);
					 if (lfoId<lfo.size() && (param-2) < lfo.get(lfoId).target.size()) { 
						 return lfo.get(lfoId).target.get(param-2).amount;
					 }
				 }
			 } else if (cc.ccontrolId() >= CControl.busIdBase(0) && cc.ccontrolId() < CControl.busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK)) {
				 int bus = CControl.bus4Id(cc.ccontrolId());
				 int param = CControl.busParam4Id(cc.ccontrolId());
				 /*
					if (param == 0)	{
						Send bs = state.send4bus(bus);
						if (bs == null) {
							Bus b = BusMixer.getBus(bus);
							if (b != null) {
								bs = new Send(b, 1);
								state.addSend(bs);
								if (bus == 1) {
									bs.set(1.0f, false);
								} else {
									bs.set(0.0f, false);
								}
							}
						}
						return bs.gain;
					}
				  */
			 }
		 }
		 }
		 return 0;
	 }

	 @Override
	 public void setValue(Control cc, float v) {
		 switch (cc.ccontrolId()) {
		 case CControl.GAIN: {
			 setGain(v);
			 break;
		 }
		 case CControl.PAN: {
			 setPan(v);
			 break;
		 }
		 case CControl.TUNE: {
			 setTune(v);
			 break;
		 }
		 /*
			case CControl.LOOP_START: {
				setLoopStart((long) (v * nTotalFrames));
				break;
			}
			case CControl.LOOP_LENGTH: {
				setLoopLength((long) (v * nTotalFrames));
				break;
			}*/
		 /*
			case CControl.FLT_CUTOFF: {
				setFilterFrequency(v);
				break;
			}
			case CControl.FLT_ENVMOD: {
				setFilterEnvMod(v);
				break;
			}
			case CControl.FLT_RESONANCE: {
				setFilterResonance(v);
				break;
			}*/
		 default:
			 if (cc.ccontrolId() >= CControl.envIdBase(0) && cc.ccontrolId() < CControl.envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				 int envId = CControl.env4Id(cc.ccontrolId());
				 int param = CControl.envParam4Id(cc.ccontrolId());
				 if (param == 0) { setEnvelopeAttack(envId, v); }
				 else if (param == 1) { setEnvelopeDecay(envId, v); } // decay
				 else if (param == 2) { setEnvelopeSustain(envId, v); } // sustain
				 else if (param == 3) { setEnvelopeSustainLevel(envId, v); } // sustain level
				 else if (param == 4) { setEnvelopeRelease(envId, v); } // sustain release
				 else { setEnvelopeTargetAmt(envId, param-5, v);} // depth(param-5);
			 } else if (cc.ccontrolId() >= CControl.lfoIdBase(0) && cc.ccontrolId() < CControl.lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK)) {
				 int lfoId = CControl.lfo4Id(cc.ccontrolId());
				 int param = CControl.lfoParam4Id(cc.ccontrolId());
				 if (param == 0) { setLFORate(lfoId, v); } // rate
				 if (param == 1) { setLFOPhase(lfoId, v); } // phase
				 else { setLFOTargetAmt(lfoId, param-2, v);} // depth(param-2);
			 } else if (cc.ccontrolId() >= CControl.busIdBase(0) && cc.ccontrolId() < CControl.busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK)) {
				 int bus = CControl.bus4Id(cc.ccontrolId());
				 int param = CControl.busParam4Id(cc.ccontrolId());
				 /*
					if (param == 0)	{
						Send bs = state.send4bus(bus);
						if (bs == null) {
							Bus b = BusMixer.getBus(bus);
							if (b != null) {
								bs = new Send(b, 1);
								state.addSend(bs);
								if (bus == 1) {
									bs.set(1.0f, false);
								} else {
									bs.set(0.0f, false);
								}
								setSend(bs.bus, v, bs.mute);
							}
						}
						setSendGain(bs.bus, v);
					}*/
			 }
		 }
	 }
	 
	 @Override
	 public String abbrevName() {
		 return "S"+Integer.toString(ind);
	 }

	@Override
	public void setupControlsAdapter(Context c, int tvrid, int ddrid) {
	}

	public boolean setFXBus(short b) {
		fxBus = b;
		return true;
	}

	public LFO getLFO(int xyci) {
		if (xyci >= 0 && xyci < lfo.size()) {
			return lfo.get(xyci);
		}
		return null;
	}

	public Envelope getEnv(int xyci) {
		if (xyci >= 0 && xyci < envelope.size()) {
			return envelope.get(xyci);
		}
		return null;
	}

	public LFO cloneLFO(int xyci, LFO src) {
		if (src == null) {
			return null;
		}
		LFO l = src.clone();
		ensureExistsLFO(xyci);
		lfo.set(xyci, l);
		return l;
	}

	public Envelope cloneEnv(int xyci, Envelope src) {
		if (src == null) {
			return null;
		}
		Envelope l = src.clone();
		ensureExistsEnv(xyci);
		envelope.set(xyci, l);
		return l;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public boolean hasNonDefaultParams() {
		return gain != 1 || pan != 0 || direction != Direction.FORWARD || tune != 0 || fxBus != 0 || hasValidLFO() || hasValidEnvelope();
	}

	protected boolean hasValidEnvelope() {
		if (envelope.size() == 0) return false;
		for (Envelope e: envelope) {
			for (ModulationTarget m: e.target) {
				if (m.target.ccontrolId() != CControl.NOTHING) {
					return true;
				}
			}
		}
		return false;
	}

	protected boolean hasValidLFO() {
		if (lfo.size() == 0) return false;
		for (LFO e: lfo) {
			for (ModulationTarget m: e.target) {
				if (m.target.ccontrolId() != CControl.NOTHING) {
					return true;
				}
			}
		}
		return false;
	}

}
