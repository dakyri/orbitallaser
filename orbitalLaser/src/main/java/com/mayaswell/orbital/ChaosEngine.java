package com.mayaswell.orbital;

import java.util.Random;

import android.util.Log;

import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.orbital.util.MappedProbability;

public class ChaosEngine extends WreckerEngine {
	
	private float currentGain = 0;
	private double currentDuration = 0;
	private float nextScheduledPosition = -1;

	private double beatTol = 0.0001;
	private int stutterMod = 0;
	private float pitchMod = 0;
	private short altFxBus = -1;
	private Direction currentDirection = null;
	
	public ChaosEngine(Wrecker w) {
		super(w);
	}


	@Override
	public float currentSliceGain() {
		return currentGain;
	}

	@Override
	public float currentSliceTune() {
		return wrecker.currentSlice.tune + pitchMod;
	}

	@Override
	public short currentSliceFxBus() {
		return altFxBus >= 0? altFxBus: wrecker.currentSlice.fxBus;
	}

	@Override
	public Direction currentSliceDirection() {
		return currentDirection!=null? currentDirection : wrecker.currentSlice.direction;
	}

	@Override
	public int currentStutter() {
		return wrecker.currentSlice.stutter + stutterMod;
	}


	@Override
	public float currentSliceBeatDuration() {
		return (float) currentDuration;
	}

	@Override
	public int getFirstSlice() {
		// maybe not quite so rigidly XXX 
//		resetAutomaton();
		return nextChaoticSlice();
	}
	@Override
	public int getNextSlice() {
		// maybe not quite so rigidly XXX 
//		resetAutomaton();
		return nextChaoticSlice();
	}

	
	@Override
	public boolean isFirstSlice() {
		return Math.abs(wrecker.currentBeat) <= beatTol;
	}


	@Override
	public boolean initialize() {
		currentGain = 0;
		currentDuration = 0;
		nextScheduledPosition = 0;

		return true;
	}
	
	@Override
	public boolean atLoopEnd() {
		Log.d("chaos", String.format("%g > %d test end", wrecker.currentBeat, wrecker.nBeat));
		return wrecker.currentBeat+beatTol > wrecker.nBeat;
	}
	
	private int nextChaoticSlice() {
		return getSliceIndexAt(wrecker.currentSliceIndex, wrecker.currentBeat);
	}

	private boolean roll(double p) {
		double roll = Math.random();
//		Log.d("chaos", String.format("roll %g %g", roll, p));
		return roll < p;
	}

	private int getSliceIndexAt(int previousSliceIndex, double position) {
		double currentPosition = position;
		if (nextScheduledPosition  > 0) {
			position = nextScheduledPosition;
		}
		int [] seq = new int[]{1,2,3,4};
		shuffleArray(seq);
		float p = 0;
		int unchaosedInd = wrecker.getSliceAt((float) position);
		int choiceInd = unchaosedInd;
		Slice choice = null;
		altFxBus  = -1;
		boolean selected = false;
		for (int i: seq) {
			switch (i) {
			case 1:
				if ((p=getPatternShiftChance((float) position)) > 0 && roll(p)) {
//					pattern shift, keeping the main grid, and fxbus assignment
					Slice choice_0 = wrecker.getSlice(unchaosedInd);
					if (choice_0 != null) {
						altFxBus = choice_0.fxBus;
					}
					choiceInd = wrecker.randomSlice();
					Slice choice_1 = wrecker.getSlice(unchaosedInd+1);
					nextScheduledPosition = choice_1.position;
					currentDuration = nextScheduledPosition - currentPosition;
					selected = true;
				}
				break;
			case 2:
				if ((p=getPatternGridShiftChance((float) position)) > 0 && roll(p)) {
//					pattern shift, using the duration and fx bus of the choice
					choiceInd = wrecker.randomSlice();
					choice = wrecker.getSlice(choiceInd);			
					Slice choice_1 = wrecker.getSlice(choiceInd+1);
					currentDuration = choice_1.position-choice.position;
					nextScheduledPosition += currentDuration;
					selected = true;
				}
				break;
			case 3:
				if ((p=getGroupShiftChance((float) position)) > 0 && roll(p)) {
//					pattern shift within group;
					Slice unchaosed = wrecker.getSlice(unchaosedInd);
					choice = wrecker.randomGroupSlice(unchaosed.getGroup());
					Slice unchaosed_1 = wrecker.getSlice(unchaosedInd+1);
					nextScheduledPosition = unchaosed_1.position;
					currentDuration = nextScheduledPosition - currentPosition;
					selected = true;
				}
				break;
			case 4:
				if ((p=getRepeatPreviousChance((float) position)) > 0 && roll(p)) {
//					repeat previous note;
					if (previousSliceIndex >= 0) {
						choiceInd = previousSliceIndex;
						choice = wrecker.getSlice(choiceInd);
						Slice unchaosed_1 = wrecker.getSlice(choiceInd+1);
						if (choice != null && unchaosed_1 != null) {
							currentDuration = unchaosed_1.position-choice.position;
						}
						nextScheduledPosition += currentDuration;
						selected = true;
					}
				}
				break;
			}
			if (selected) {
				break;
			}
		}
		if (!selected) {
			Slice unchaosed_1 = wrecker.getSlice(unchaosedInd+1);
			nextScheduledPosition = unchaosed_1.position;
			currentDuration = nextScheduledPosition - currentPosition;
		}
		// nextScheduledPosition is relative to an idealized grid
		// at this point currentDuration is relative to the idealized grid
		
		// futz with duration to give some stretch to the grid
		
		if (choice == null && choiceInd >= 0) {
			choice = wrecker.getSlice(choiceInd);
		}
		if (choice != null) {
			currentGain = choice.gain;
		} else {
			currentGain = 1;
		}
		stutterMod = 0;
		pitchMod = 0;
		currentDirection = null;

		if ((p=getDynamicModChance((float) position)) > 0 && roll(p)) {
			// futz around with gain
			currentGain *= getDynamicAdjustment();
		}
		
		if ((p=getDurationModChance((float) position)) > 0 && roll(p)) {
			// futz around with duration
			currentDuration *= getDurationAdjustment();
		}
		
		if ((p=getStutterModChance((float) position)) > 0 && roll(p)) {
			// futz around with stutter
			stutterMod = getStutterAdjustment();
		}
		
		if ((p=getPitchModChance((float) position)) > 0 && roll(p)) {
			// futz around with pitch
			pitchMod = getPitchAdjustment();
		}
		
		if ((p=getReverseChance((float) position)) > 0 && roll(p)) {
			// futz around with direction
			if (choice != null) {
				currentDirection = choice.direction.reverse();
			}
		}
		
		return choiceInd;
	}

	/**
	 * @return a multiplying factor for gain adjustment
	 */
	private float getDynamicAdjustment() {
		if (state != null && state.dynamicModChance != null) {
			float a =  state.dynamicModChance.getAdjustment();
//			Log.d("chaos", String.format("dynamic %g adjust", a));
			return a;
		}
		return 1;
	}

	/**
	 * @return a multiplying factor for duration adjustment
	 */
	private float getDurationAdjustment() {
		if (state != null && state.durationModChance != null) {
			return state.durationModChance.getAdjustment();
		}
		return 1;
	}

	/**
	 * @return an additive factor for stutter adjustment
	 */
	private int getStutterAdjustment() {
		if (state != null && state.stutterModChance != null) {
			int a = (int) Math.round(state.stutterModChance.getAdjustment());
//			Log.d("chaos", String.format("stutter %d adjust", a));
			return a;
		}
		return 0;
	}

	/**
	 * @return an additive factor for stutter adjustment
	 */
	private float getPitchAdjustment() {
		if (state != null && state.stutterModChance != null) {
			return state.pitchModChance.getAdjustment();
		}
		return 0;
	}

	private float getPatternShiftChance(float position) {
		if (state != null && state.patternShiftChance != null) {
			float pr = state.patternShiftChance.getPr(position);
//			Log.d("chaos", String.format("%g %g", position, pr));
			return pr;
		}
		return 0;
	}

	private float getPatternGridShiftChance(float position) {
		if (state != null && state.patternGridShiftChance != null) {
			return state.patternGridShiftChance.getPr(position);
		}
		return 0;
	}

	private float getGroupShiftChance(float position) {
		if (state != null && state.groupShiftChance != null) {
			return state.groupShiftChance.getPr(position);
		}
		return 0;
	}

	private float getRepeatPreviousChance(float position) {
		if (state != null && state.repeatPreviousChance != null) {
			return state.repeatPreviousChance.getPr(position);
		}
		return 0;
	}

	private float getDynamicModChance(float position) {
		if (state != null && state.dynamicModChance != null) {
			return state.dynamicModChance.getPr(position);
		}
		return 0;
	}

	private float getDurationModChance(float position) {
		if (state != null && state.durationModChance != null) {
			return state.durationModChance.getPr(position);
		}
		return 0;
	}

	private float getStutterModChance(float position) {
		if (state != null && state.stutterModChance != null) {
			return state.stutterModChance.getPr(position);
		}
		return 0;
	}


	private float getPitchModChance(float position) {
		if (state != null && state.pitchModChance != null) {
			return state.pitchModChance.getPr(position);
		}
		return 0;
	}

	private float getReverseChance(float position) {
		if (state != null && state.reverseChance != null) {
			return state.reverseChance.getPr(position);
		}
		return 0;
	}

	private void shuffleArray(int[] ar)
	{
		Random rnd = new Random();
		for (int i = ar.length - 1; i > 0; i--) {
			int index = rnd.nextInt(i + 1);
			int a = ar[index];
			ar[index] = ar[i];
			ar[i] = a;
		}
	}
}