package com.mayaswell.orbital;

import java.util.ArrayList;

import com.mayaswell.orbital.SliceGroups.SliceGroup;

public class SliceGroups {
	public class SliceGroup {
		private int id;
		private String name;
		private ArrayList<Slice> slice;

		public SliceGroup(int i) {
			this(i, "");
		}
		
		public SliceGroup(int i, String nm) {
			id = i;
			name = nm;
			slice = new ArrayList<Slice>();
		}

		public void addSlice(Slice s) {
			if (!findSlice(s)) {
				slice.add(s);
			}
		}

		public boolean findSlice(Slice s) {
			return slice.indexOf(s) >= 0;
		}

		public void removeSlice(Slice s) {
			slice.remove(s);
		}

		public int countSlices() {
			return slice.size();
		}

		public Slice getSlice(int i) {
			return (i>=0 && i<slice.size())?slice.get(i):null;
		}

		public void clear() {
			slice.clear();
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		
		public String toString() {
			return Integer.toString(id);
		}
	}
	
	private ArrayList<SliceGroup> groups;
	
	public SliceGroups(int n) {
		groups = new ArrayList<SliceGroup>();
		for (int i=0; i<n; i++) {
			groups.add(new SliceGroup(i));
		}
	}
	
	public void clear() {
		for (SliceGroup sg: groups) {
			sg.clear();
		}
	}
	
	public void setTo(ArrayList<Slice> slices) {
		clear();
		for (Slice s: slices) {
			int g;
			if ((g=s.getGroup()) >= 0 && g < groups.size()) {
				groups.get(g).addSlice(s);
			}
		}
	}
	
	public boolean setGroup(Slice s, int newg) {
		int g = s.getGroup();
		if (g >= 0 && g < groups.size()) {
			groups.get(g).removeSlice(s);
		}
		if (newg >= 0 && newg < groups.size()) {
			groups.get(newg).addSlice(s);
			s.setGroup(newg);
		} else {
			s.setGroup(-1);
		}
		return true;
	}

	public SliceGroup getGroup(int g) {
		if (g >= 0 && g < groups.size()) {
			return groups.get(g);
		}
		return null;
	}
	
	public int size() {
		return groups.size();
	}

	public static int getGroupColor(int gid) {
		int c = 0xff444444;
		if (gid >= 0) {
			if (gid >= OrbitalLaser.regginbow.length) {
				gid = OrbitalLaser.regginbow.length-1;
			}
			c = OrbitalLaser.regginbow[gid];
		}
		return c;
	}

	public Slice selectSlice(SliceCell sc) {
		if (sc != null) {
			return selectSlice(sc.getId());
		}
		return null;
	}

	protected Slice selectSlice(int gid) {
		if (gid >= 0 && gid < groups.size()) {
			SliceGroup g = groups.get(gid);
			int ind = (int) Math.floor(Math.random()*g.countSlices());
			Slice s = g.getSlice(ind);
			return s;
		}
		return null;
	}
}
