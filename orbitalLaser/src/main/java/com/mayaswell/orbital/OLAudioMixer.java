package com.mayaswell.orbital;

import java.io.File;
import java.io.IOException;
import java.util.AbstractQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.mayaswell.audio.file.AudioFileException;
import com.mayaswell.audio.AudioMixer;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.file.WavFile;
import com.mayaswell.audio.file.AudioFile.Type;
import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.util.EventID;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Message;
import android.util.Log;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subjects.PublishSubject;

public class OLAudioMixer extends AudioMixer
{	
	protected Wrecker wrecker = null;
	
	public OLAudioMixer(int n)
	{
		super();
	}
	
	public void addWrecker(Wrecker w)
	{
		wrecker = w;
	}
	
	public boolean play()
	{
		Thread t; 
		
		if (isPlaying) {
			return false;
		}
		
		t = new Thread() {
			public void run() {
				isPlaying = true;
				
				setPriority(Thread.MAX_PRIORITY);

				AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, dfltSampleRate, 
							outChannelFormat, 
							AudioFormat.ENCODING_PCM_16BIT, 
						 	outBufSize, 
						 	AudioTrack.MODE_STREAM);
			
				int nFramePerBuf = outBufSize/nOutChannels;
				//				float busBuffer[] = new float[outBufSize];
				short outBuffer[] = new short[outBufSize];

				audioTrack.play();
				
				while(isPlaying){
					MasterBus.zeroBuffers(nFramePerBuf);
					if (wrecker != null && wrecker.isPlaying()) {
						wrecker.playCumulative(MasterBus.getNativePointer(), nFramePerBuf, (short) 2);
					}
					MasterBus.mix(outBuffer, nFramePerBuf, (short)nOutChannels);
					if (exportState > 0 && exportState < 3) {
						xStashCount+=(exportEndOffset-exportStartOffset)/nOutChannels;
						Log.d("render", "stash "+nFramePerBuf +","+Integer.toString((exportEndOffset-exportStartOffset))+"," + xStashCount+","+exportState);
						short[] xBuffer = new short[exportEndOffset-exportStartOffset];
						for (int i=exportStartOffset; i<xBuffer.length; i++) {
							xBuffer[i] = outBuffer[i];
						
						}
						synchronized (exportDataQueue) {
							exportDataQueue.add(xBuffer);		
							exportStartOffset = 0;
							exportEndOffset = outBufSize;
							if (exportState == 2) {
								exportState = 3;
							}
						}
					}
					audioTrack.write(outBuffer, 0, outBufSize);
				}
				
				/*if (dumpFile != null)
					try {
						dumpFile.close();
					} catch (IOException e) {
				}*/
				audioTrack.stop();
				audioTrack.release();
			}
		};
		
		t.start();
		return true;
	}
/*	
	public synchronized boolean startPad(PadSample p)
	{
	....
		return true;
	}
	
	public synchronized void stopPad(PadSample p)
	{
...
	}
	
	public boolean isPlaying(PadSample p)
	{
...
		return false;
	}
*/	
	public void stop()
	{
		isPlaying = false;
	}
	
	private volatile int exportState = 0; /** where we are in the export thing. 0=not, 1=done, 2=got last buffers from source, 3=grabbed all and flushing */
	private volatile int exportStartOffset = 0;
	private volatile int exportEndOffset = 0;
	private AbstractQueue<Object> exportDataQueue=null;
	private volatile int xStashCount;
	private volatile int xWriteCount;
	private PublishSubject<AudioFile> exportSubject = PublishSubject.create();;

	public boolean isPlaying(Wrecker p)
	{
		return isPlaying;
	}

	public synchronized void stopWreckage(Wrecker w) {
	}

	public synchronized boolean startWreckage(Wrecker w) {
		w.fire();
		return true;
	}
	
	public Subscription monitorExport(Observer<AudioFile> o) {
		return exportSubject.observeOn(AndroidSchedulers.mainThread()).subscribe(o);
	}

 	/**
	 * this may be called asynchronously with the audio thread and kicks off the file writer
	 */
	@Override
	public boolean startExport(SamplePlayer s, long frameOffset, File to, AudioFile.Type type) {
		if (exportState > 0) {
			return false;
		}
		exportState = 1;
		exportStartOffset = (int) (frameOffset*nOutChannels);
		exportEndOffset = outBufSize;
		exportDataQueue = new ConcurrentLinkedQueue<Object>();
		xStashCount = 0;
		xWriteCount = 0;
		final File finalto = to;
		Thread t; 
		t = new Thread() {
			public void run() {
				WavFile outFile;
				try {
					outFile = WavFile.newWavFile(finalto, nOutChannels, Long.MAX_VALUE, 16, dfltSampleRate);
				} catch (IOException e) {
					exportSubject.onError(new Exception("Catastrophe! Some kind of IO exception, creating record file "+finalto.getPath()));
					return;
				} catch (AudioFileException e) {
					exportSubject.onError(new Exception("How Bizzare! An audio file exception, creating record file "+finalto.getPath()));
					return;
				}
				
				boolean stillGoing=false;
				do {				
					try {
						Object o = exportDataQueue.poll();
						if (o != null) {
							short [] buf = (short[]) o;
							outFile.writeFrames(buf, buf.length/nOutChannels);
							xWriteCount+=(buf.length/nOutChannels);
							Log.d("render", "write "+Integer.toString(buf.length/nOutChannels)+"," + xWriteCount+","+exportState);
						} else {
							synchronized(exportDataQueue) {
								if (exportState == 3) { // we're done!!!!
									Log.d("render", "not found buffer, done flush "+"," + xWriteCount+","+exportState + ","+exportDataQueue.size());
									exportState = 0;
								}
							}
							Log.d("render", "not found buffer, sleeping "+"," + xWriteCount+","+exportState + ","+exportDataQueue.size());
							sleep(500);
						}
					} catch (InterruptedException e) {
						Log.d("render", "export thread sleep is interrupted");
						break;
					} catch (ClassCastException e) {
						Log.d("render", "export ClassCastException");
						exportSubject.onError(new Exception("Catastrophe! Some kind of IO exception, writing record file "+finalto.getPath()));
						break;
					} catch (IOException e) {
						Log.d("render", "export IOException");
						exportSubject.onError(new Exception("Catastrophe! Some kind of IO exception, writing record file "+finalto.getPath()));
						break;
					} catch (AudioFileException e) {
						Log.d("render", "export IOException");
						exportSubject.onError(new Exception("How Bizzare! An audio file exception, writing record file "+finalto.getPath()));
						break;
					}
					synchronized (exportDataQueue) {
						stillGoing = (exportState > 0);
					}
					Log.d("render", "loop "+ xWriteCount+","+exportState + ","+exportDataQueue.size());
				} while (stillGoing);
				Log.d("render", "closing");
				try {
					outFile.close();
				} catch (IOException e) {
					exportSubject.onError(
							new Exception(
									"Catastrophe! Some kind of IO exception, finalizing record file "+finalto.getPath()));
				}
				exportSubject.onNext(outFile);
				exportSubject.onCompleted();
				outFile = null;
			}
		};
		t.start();
		return true;
	}

	/**
	 * this is assumed called synchronously with the main audio out thread
	 */
	public boolean stopExport(SamplePlayer s, long frameOffset) {
		synchronized (exportDataQueue) {
			exportStartOffset = 0;
			exportEndOffset = (int) (frameOffset*nOutChannels);
			exportState = 2;
			Log.d("render", "stop "+exportStartOffset+","+exportEndOffset);
		}
		return true;
	}
}
