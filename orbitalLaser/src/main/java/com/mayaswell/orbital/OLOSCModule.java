package com.mayaswell.orbital;

import java.util.Date;

import android.util.Log;

import com.illposed.osc.OSCListener;
import com.illposed.osc.OSCMessage;
import com.mayaswell.audio.OSCModule;

public class OLOSCModule extends OSCModule {

	protected OLGlobalListener globalListener = null;
	protected OrbitalLaser orbital = null;
	
	public class OLGlobalListener implements OSCListener
	{
		public OLGlobalListener() {
		}

		@Override
		public void acceptMessage(Date date, OSCMessage msg) {
			String path=msg.getAddress();
			String leaves[] = path.split("/");
			/*
			 * string starts with a '/' so leaves[0] == '' ...
			 */
			Object [] args = msg.getArguments();
			if (!leaves[1].equals("ol")) {
				return;
			}
			if (leaves.length <= 3) { /** "/ol/*" */
				if (args.length < 1) {
					return;
				}
				float p;
				if (args.length > 0 && args[0] instanceof Float) {
					p = ((Float)args[0]).floatValue();
				} else {
					return;
				}
				if (leaves[2].equals("tempo")) {
					orbital.setTempo(p); // TODO doesn't update interface
				}
			}
		}
	}

	public OLOSCModule(OrbitalLaser ol) {
		orbital = ol;
		globalListener = new OLGlobalListener();
		listeners.put("/ol/.*", globalListener);
		Log.d("OSC", "ip = "+getIPAddress(true));
	}

}
