package com.mayaswell.orbital;

import java.util.ArrayList;

import android.util.Log;

public class MasterBus
{
//	protected ArrayList<Bus> bus = null;
	private static long bmxPointer = 0;
	private static MasterBus instance = null;
	
	private static native long allocBmx(int bs);
	private static native void deleteBmx(long ptr);
	private static native void setBmxBufsize(long ptr, int bufsize);
	public static native void globalBmxInit();
	public static native void globalBmxCleanup();
	
//	private static native void setBmxTempo(long ptr, float tempo);
		
	private static native void zeroBmxBuffers(long ptr, int nFramePerBuf);
	private static native void bmxMix(long ptr, int nActive, short[] outBuffer, int nFramePerBuf, short nOutChannels);
	
	private MasterBus(int nb, int bufSize)
	{
		bmxPointer = allocBmx(bufSize);
	}
	
	public static MasterBus build(int nb, int bufSize)
	{
		if (instance == null) {
			instance = new MasterBus(nb, bufSize);
		}
		return instance;
	}
	
	public static MasterBus getInstance()
	{
		if (instance == null) {
			Log.d("busmix", "bus mixer unconstructed before access");
		}
		return instance;
	}
	public static long getNativePointer()
	{
		return bmxPointer;
	}
	
	public void setBufsize(int bufsize)
	{
		setBmxBufsize(bmxPointer, bufsize);
	}
	
	public static void zeroBuffers(int nFramePerBuf)
	{
		zeroBmxBuffers(bmxPointer, nFramePerBuf);	
	}
	
	public static void mix(short[] outBuffer, int nFramePerBuf, short nOutChannels) {
		bmxMix(bmxPointer, 1, outBuffer, nFramePerBuf, nOutChannels);
	}

}
