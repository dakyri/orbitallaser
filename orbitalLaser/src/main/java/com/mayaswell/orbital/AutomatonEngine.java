package com.mayaswell.orbital;


public class AutomatonEngine extends WreckerEngine {
	protected SliceCell currentSliceCell = null;

	public AutomatonEngine(Wrecker w) {
		super(w);
	}

	@Override
	public float currentSliceGain() {
		float g = wrecker.currentSlice.gain;
		if (currentSliceCell != null) {
			g *= currentSliceCell.getGain();
		}
		return g;
	}


	@Override
	public float currentSliceBeatDuration() {
		if (currentSliceCell != null) {
			return currentSliceCell.getDuration();
		}
		return super.currentSliceBeatDuration();
	}

	@Override
	public int getFirstSlice() {
		// maybe not quite so rigidly XXX 
//		resetAutomaton();
		return nextAutoSlice();
	}
	

	@Override
	public int getNextSlice() {
		// maybe not quite so rigidly XXX 
//		resetAutomaton();
		return nextAutoSlice();
	}

	@Override
	public boolean isFirstSlice() {
		return wrecker.currentBeat <= 0;
	}

	@Override
	public boolean atLoopEnd() {
		return wrecker.currentBeat >= wrecker.nBeat; // XXX but probably should have a tolerance on this
	}
	
	protected int nextAutoSlice() {
		AlgoRhythm a = wrecker.algorithm;
		if (a == null || wrecker.state == null) return 0;
		currentSliceCell = a.next();
		Slice s = wrecker.groups.selectSlice(currentSliceCell);
		if (s == null) return 0; // XXX no slice should not be 0
		return wrecker.state.findSlice(s);
	}
		

}
