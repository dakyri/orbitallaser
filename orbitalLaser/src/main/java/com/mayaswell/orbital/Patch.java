package com.mayaswell.orbital;

import java.io.File;
import java.util.ArrayList;

import android.util.Log;

import com.mayaswell.util.AbstractPatch;

public class Patch extends AbstractPatch {
	public float tempo = 120;
	public WreckageState state = null;
	
	public Patch(String name, boolean doInit) {
		this.name = name;
		if (doInit) {
			state = new WreckageState();
		}
	}
	
	public Patch(String name, float tempo, WreckageState state) {
		this.name = name;
		this.tempo = tempo;
		this.state = state;
	}
	
	public Patch()
	{
		this("default", true);
	}

	public Patch clone() {
		Patch p = new Patch();
		p.setTempo(tempo);
		p.state = state.clone();
		p.name = name;
		return p;
	}

	public void setTempo(float v) {
		tempo = v;
	}

	public boolean getMissingFiles(ArrayList<String> fl) {
		boolean b = false;
		if (state != null && state.path != null && !state.path.equals("")) {
			File f = new File(state.path);
			if (!f.exists()) {
				if (!fl.contains(state.path)) {
					fl.add(state.path);
				}
				b = true;
			}
		}
		return b;
	}
	
	public boolean fixFilePath(String oldPath, String newPath) {
		boolean b = false;
		if (oldPath != null && newPath != null && state != null && state.path != null) {
			if (state.path.equals(oldPath)) {
				state.path = newPath;
				b = true;
			}
		}
		return b;
	}
}
