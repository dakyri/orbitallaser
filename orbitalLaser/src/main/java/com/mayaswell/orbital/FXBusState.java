package com.mayaswell.orbital;

import java.util.ArrayList;

import android.util.Log;

import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXState;

public class FXBusState {
	public int id;
	public String name;
	public ArrayList<FXState> fx = new ArrayList<FXState>();
	public float gain = 1;
	public float pan = 0;
	public boolean enable = true;
	
	public FXBusState(int i, String nm)
	{
		id = i;
		name = nm;
	}
	
	public FXBusState clone()
	{
		FXBusState fxbs = new FXBusState(id, new String(name));
		fxbs.gain = gain;
		fxbs.pan = pan;
		fxbs.enable = enable;
		for (FXState f: fx) {
			fxbs.fx.add(f.clone());
		}
		return fxbs;
	}

	/**
	 * 
	 * @param xi
	 * @param f
	 * @return false if can't set type, either because it's the same, or because other invalidity
	 */
	public boolean setFXstate(int xi, FX f)
	{
		Log.d("FXBusState", String.format("setFXstate %d %s", xi, f!=null?f.toString():""));
		if (f == null) {
			return false;
		}
		if (fx == null) fx = new ArrayList<FXState>();
		while (fx.size() <= xi) {
			fx.add(new FXState(FX.unityFX, true));
		}
		FXState fxs = fx.get(xi);
		if (fxs == null) {
			return false;
		}
		if (fxs.getFx().getId() == f.getId()) {
			Log.d("FXBusState", String.format("already is %s", fxs.getFx().toString()));
			return false;
		}
		Log.d("FXBusState", String.format("setting from current fx %s", fxs.getFx().toString()));
		fxs.setType(f);
		return true;
	}

	public boolean setFXEnable(int xi, boolean enable)
	{
		if (fx == null) fx = new ArrayList<FXState>();
		while (fx.size() <= xi) {
			fx.add(new FXState(FX.unityFX, true));
		}
		fx.get(xi).setEnable(enable);
		return true;
	}
	public boolean getFXEnable(int fxId) {
		if (fxId < 0 || fxId >= fx.size()) {
			return false;
		}
		return fx.get(fxId).getEnable();
	}

	public boolean setFXparam(int xi, int yi, float p)
	{
		if (fx == null) fx = new ArrayList<FXState>();
		while (fx.size() <= xi) {
			fx.add(new FXState(FX.unityFX, true));
		}
		FXState fss = fx.get(xi);
		if (fss.countParams() <= yi) {
			return false;
		}
		fss.param(yi, p);
		return true;
	}

	public boolean setEnable(boolean en) {
		enable = en;
		return true;
	}

	public boolean setPan(float amt) {
		pan = amt;
		return true;
	}

	public boolean setGain(float amt) {
		gain = amt;
		return true;
	}

	public FX getFX(int fxId) {
		if (fxId < 0 || fxId >= fx.size()) {
			return null;
		}
		FXState f = fx.get(fxId);
		if (f == null) {
			return null;
		}
		return f.getFx();
	}


}
