package com.mayaswell.orbital;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import android.content.Context;
import android.graphics.Path;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.audio.file.AudioFile.Type;
import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXParam;
import com.mayaswell.audio.FXState;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Bufferator.SampleChunkInfo;
import com.mayaswell.audio.Bufferator.SampleGfxInfo;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.ControlsAdapter.ControlsFilter;
import com.mayaswell.audio.ControlsAdapter;
import com.mayaswell.audio.Metric;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ModulationTarget;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.orbital.OrbitalLaser.Global;
import com.mayaswell.orbital.SliceGroups.SliceGroup;
import com.mayaswell.orbital.Slice;
import com.mayaswell.util.Automaton;
import com.mayaswell.widget.MenuData;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Func1;

public class Wrecker extends SamplePlayer implements Controllable, Envelope.Host, LFO.Host {

	private final Wrecker self;

	public interface Listener {
		void playStarted(Wrecker wrecker);
		void playComplete(Wrecker wrecker);
		void setSampleData(Wrecker wrecker);
		void analysisComplete(Wrecker wrecker);
		void nodeChanged(Wrecker wrecker);
		void cellChanged(Wrecker wrecker);
	}
	private Listener listener = null;
	public void setListener(Listener l) {
		listener = l;
	}

	public enum PlayMode {
		VANILLA, PATTERNATOR, AUTOMATOR, CHAOTICOR, RANDOMATOR;
		@Override
		public String toString()
		{
			switch (this) {
			case VANILLA: return "vanilla";
			case PATTERNATOR: return "pattern";
			case RANDOMATOR: return "random";
			case CHAOTICOR: return "chaos";
			case AUTOMATOR: return "automaton";
			}
			return super.toString();
		}
		
		public static PlayMode parse(String s)
		{
			if (s.equals("vanilla")) return VANILLA;
			if (s.equals("pattern")) return PATTERNATOR;
			if (s.equals("random")) return RANDOMATOR;
			if (s.equals("chaos")) return CHAOTICOR;
			if (s.equals("automaton")) return AUTOMATOR;
			return PlayMode.valueOf(s); 
		}		
	}
	
	private static final int minStutterFrames = 10;

	public static ArrayAdapter<Direction> directionAdapter = null;
	public static ArrayAdapter<MenuData<SliceGroup>> groupAdapter = null;
	public static ArrayAdapter<MenuData<SliceGroup>> nnGroupAdapter = null;
	public static ArrayAdapter<MenuData<Slice>> sliceAdapter = null;
	
	private ControlsAdapter lfoTgtAdapter = null;
	private ControlsAdapter envTgtAdapter = null;

	protected long cWreckerPointer = 0;
	
	protected int id = 0;
	protected String name = ""; // the fixed name, visible in lists of targets for controllers
	
	protected WreckageState state = new WreckageState();
	
	protected final OrbitalLaser orbital;

	protected double currentBeat=0;
	
	protected Slice currentSlice=null;
	protected int currentSliceFrame = 0;
	protected int currentSliceIndex = -1;
	protected double dataTempo = 120;
	protected int currentDataFrame = 0;
	protected int displayedBus = 0;
	private short currentSliceBus = 0;

	public int nBeat = 4; /** number of beats total in the sample being sliced */
	public Metric metric = new Metric(4);
	public int gridQuant = 2;

	private int nStutterFrames;

	private int nextStutterFrame;
	private int thisSliceStartFrame;

	private boolean startExporting = false;
	private int exportCount = 0;
	private File exportFile = null;

	private boolean isExporting;

	private float baseTempo = 120;
	private long trueLength = 0;
	private float baseDuration = 0;
	
	protected AlgoRhythm algorithm=null;

	protected SliceGroups groups = null;

	public int lastCellItemInd = -1;
	public int currentCellItemInd = -1;
	public int lastNodeId = -1;
	public int currentNodeId = -1;
	
	protected LinearEngine linearEngine;
	protected PatternEngine patternEngine;
	protected RandomEngine randomEngine;
	protected AutomatonEngine automatonEngine;
	protected ChaosEngine chaosEngine;
	protected IWreckerEngine njin;

	/*
	 * native hooks
	 */
	private static native long allocWrecker(
			int id, int nEnv, int nEnvTgt, int nLfo, int nLfoTgt,
			int maxFXChain, int maxFXPerChain);
	private static native void deleteWrecker(long ptr);
	private static native void setWreckerBufsize(long ptr, int bufsize);
	public static native void globalWreckerInit();
	public static native void globalWreckerCleanup();
	
	private static native void setWreckerSampleInfo(long ptr, long nframe, short nc);
	private static native void setWreckerGain(long ptr, float gain);
	private static native void setWreckerPan(long ptr, float pan);
	private static native void setWreckerTune(long ptr, float tune);
	
	private static native int playChunkMixer(long ptr, long mxPtr,
			int buffInd, int nIterSliceFrames, int nIterDataFrames, short nOutChannels, int currentDataFrame,
			float[] chunkData, int chunkStart, int chunkLen, float nextL, float nextR, int direction, short fxBus);
	
	private static native int getNextDataFrame(long ptr);
	
	private static native boolean setWreckerNLFO(long cWreckerPointer2, int size);
	private static native boolean setWreckerLFO(long cWreckerPointer2, int i, byte code,
			byte code2, boolean tempoLock, float rate, float phase);
	private static native boolean setWreckerLFOTgt(long cWreckerPointer2, int i, int j, int k, float amount);
	private static native boolean setWreckerNLFOTgt(long cWreckerPointer2, int i, int j);
	private static native boolean resetWreckerLFO(long cWreckerPointer2, int i);
	private static native float getWreckerLFO(long cWreckerPointer2, int i);
	private static native boolean setWreckerNEnv(long cWreckerPointer2, int size);
	private static native boolean setWreckerEnv(long cWreckerPointer2, int i, byte code,
			boolean tempoLock, float attackT, float decayT, float sustainT,
			float sustainL, float releaseT);
	private static native boolean setWreckerEnvTgt(long cWreckerPointer2, int i, int j, int k, float amount);
	private static native boolean setWreckerNEnvTgt(long cWreckerPointer2, int i, int j);
	private static native boolean resetWreckerEnv(long cWreckerPointer2, int i);
	private static native float getWreckerEnv(long cWreckerPointer2, int i);
	private static native boolean setWreckerTempo(long cWreckerPointer2, float t);
	
	private static native boolean setBusGain(long cWreckerPointer, int whichBus, float g);
	private static native boolean setBusPan(long cWreckerPointer, int whichBus, float p);
	private static native boolean setBusEnable(long cWreckerPointer, int whichBus, boolean e);
	private static native boolean setFXType(long cWreckerPointer, int whichBus, int whichFX, int type, int nParams, float [] params);
	private static native boolean setFXEnable(long cWreckerPointer, int whichBus, int whichFX, boolean en);
	private static native boolean setFXParam(long cWreckerPointer, int whichBus, int whichFX, int whichParam, float val);
	private static native boolean setFXParams(long cWreckerPointer, int whichBus, int whichFX, int nParams, float [] sparams);
	
	private static native void reloopWrecker(long ptr);
	private static native void stopWrecker(long ptr);
	private static native void fireWrecker(long ptr);

	public Wrecker(String name, int id, OrbitalLaser orbital)
	{
		super();
		this.id = id;
		this.name = name;
		this.orbital = orbital;
		groups = new SliceGroups(8);
		cWreckerPointer  = allocWrecker(id, Global.ENV_PER_WRECK, Global.MAX_ENV_TGT, Global.LFO_PER_WRECK, Global.MAX_LFO_TGT,
				Global.MAX_BUS_PER_WRECK, Global.MAX_BUS_FX);
		setNBeat(4);
		setDisplayedBus(0);
		
		linearEngine = new LinearEngine(this);
		patternEngine = new PatternEngine(this);
		randomEngine = new RandomEngine(this);
		automatonEngine = new AutomatonEngine(this);
		chaosEngine = new ChaosEngine(this);
		njin = linearEngine;
		self = this;
	}
	
	/**
	 * current way of doing things ...
	 * @param busMixPointer
	 * @param nFrames
	 * @param nOutChannels
	 */
	public int playCumulative(long busMixPointer, int nFrames, short nOutChannels)
	{
//		Log.d("Wrecker", String.format("playCumulative, wreck %d to play %d %d", id, nFrames, nOutChannels));
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
//			Log.d("player", "bail with null chunk");
			return 0;
		}
		if (!isPlaying()) {
//			Log.d("player", "tried to play, but not really playing");
			return 0;
		}
		
//		Log.d("Wrecker", String.format("playCumulative, wreck %d read to play %d %d", id, nFrames, nOutChannels));
		int nFramesOut = 0;
		int ll = ((int)state.regionLength());
		if (ll < 2) ll = 2;
		int sf = ((int)state.regionStart());
		int buffInd = 0;
//		Log.d("Wrecker", String.format("playCumulative, in loop to play %d %d", id, nFramesOut, nOutChannels));
		checkExportStart(0);
		while (nFramesOut < nFrames) {
			int nFramesRemaining = nFrames-nFramesOut;
			double framesPerBeat = orbital.getFramesPerBeat();
			currentBeat = currentLoopFrame / framesPerBeat;
			int nextSliceStartFrame = thisSliceStartFrame+(int) (njin.currentSliceBeatDuration()*framesPerBeat);
			int nIterSliceFrames = nextSliceStartFrame-currentLoopFrame;
			if (nIterSliceFrames > nFramesRemaining) {
				nIterSliceFrames = nFramesRemaining;
			}
			if (njin.currentStutter() > 1) {
				if (nIterSliceFrames > nextStutterFrame-currentLoopFrame) {
					nIterSliceFrames = nextStutterFrame-currentLoopFrame;
				}
			}
			
			int nIterDataFrames = (currentDirection >= 0)?
					state.getSlice(currentSliceIndex+1).startFrame-currentDataFrame:
					currentDataFrame - state.getSlice(currentSliceIndex).startFrame;
			if (nIterSliceFrames > 0) {
				int scid = 0;
				int scistart = 0;
				int scilength = 0;
				float [] scidata = null;
				SampleChunkInfo sci = csf.getChunkFor(currentDataFrame);
				float nextL = 0;
				float nextR = 0;
				if (sci != null) {
					scid = sci.id;
					scilength = sci.nFrames;
					scidata = sci.data;
					scistart = sci.startFrame;
					if (njin.currentSliceTune() != 0) { // TODO also check moduators
						if (currentDirection >= 0) {
							SampleChunkInfo scnxt = null;
							if (sci.nextChunkStartFrame() >= sf + ll) {
								scnxt = csf.getChunkFor(sf);
							} else {
								scnxt = csf.getChunk(scid+1);
							}
							if (scnxt != null) {
								nextL = scnxt.data[0];
								if (nChannels > 1) {
									nextR = scnxt.data[1];
								}
							}
						} else { // backwards .
							SampleChunkInfo scnxt = null;
							if (sci.prevChunkEndFrame() < sf) {
								scnxt = csf.getChunkFor(sf);
							} else {
								scnxt = csf.getChunk(scid-1);
							}
							if (scnxt != null) {
								if (nChannels > 1) {
									nextL = scnxt.data[scnxt.nFrames-2];
									nextR = scnxt.data[scnxt.nFrames-1];
								} else {
									nextL = scnxt.data[scnxt.nFrames-1];
								}
							}
						}
					}
//					Log.d("wrecker", String.format("pad %d got %d chunk %d len %d @ %d data[0] %g path %d %s", id, currentDataFrame, scid, scilength, scistart, scidata[0], csf.refCount, csf.path));
				} else {
					scid = Bufferator.chunk4Frame(currentDataFrame);
//					Log.d("wrecker", String.format("pad %d failed to find %d chunk %d path %s", id, currentDataFrame, scid, csf.path));
				}
				int nIterOutFrames = playChunkMixer(
						cWreckerPointer, busMixPointer, buffInd, nIterSliceFrames, nIterDataFrames, nOutChannels, currentDataFrame,
						scidata, scistart, scilength, nextL, nextR, currentDirection, currentSliceBus);
				if (scid >= 0) {
					csf.requestChunk(scid, 1); // will already be loaded, but we'll make sure it's there
					if (currentDirection >= 0) {
						csf.requestChunk(scid+1, 1);
						csf.requestChunk(scid+2, 1);
					} else {
						csf.requestChunk(scid-1, 1);
						csf.requestChunk(scid-2, 1);
					}
				}
				buffInd += nOutChannels*nIterOutFrames;
				currentLoopFrame += nIterOutFrames;
				currentSliceFrame += nIterOutFrames;
				currentDataFrame = getNextDataFrame(cWreckerPointer);
				currentBeat = currentLoopFrame / framesPerBeat;
	 			nFramesOut += nIterOutFrames;
//				Log.d("player", String.format("pad %d played %d %d requested %d loop %d data %d end %d", id, nIterOutFrames, nFramesOut, nIterFrames, currentLoopFrame, getNextDataFrame(cPadPointer), ll));
			} else {
				if (!isPlaying()) {
					break;
				}
			}
			if (currentLoopFrame >= nextSliceStartFrame) {
				Log.d("wrecker", "loop frame >= next slice "+nextSliceStartFrame);
				if (njin.atLoopEnd()) {
					if (isExporting) {
						if (--exportCount <= 0 || playState == State.STOPPED) {
							stopExport(buffInd/nOutChannels);
						}
					}		
					onLoopEnd();
				} else {
					setSlice(njin.getNextSlice());
					startSlice();
				}
			} else if (njin.currentStutter() > 1) {
				if (currentLoopFrame >= nextStutterFrame) {
					doStutter();
				}
			}
		}
		
		return nFrames;
	}
	
	private void doStutter() {
		switch(njin.currentSliceDirection()) {
		case BACKWARD:
			currentDataFrame = state.getSlice(currentSliceIndex+1).startFrame-1;
			break;
		case FORWARD:
			currentDataFrame = currentSlice.startFrame;
			break;
		case FWDBACK:
			currentDirection = -currentDirection;
			break;
		case BACKFWD:
			currentDirection = -currentDirection;
			break;
		}
		nextStutterFrame = currentLoopFrame + nStutterFrames;
	}
	
	private void startSlice() {
		currentSliceFrame = 0;
		thisSliceStartFrame = currentLoopFrame;
		if (currentSlice == null) {
			return;
		}
		if (njin.currentSliceDirection() == Direction.BACKWARD || njin.currentSliceDirection() == Direction.BACKFWD) {
			currentDirection = -1;
		} else {
			currentDirection = 1;
		}
		currentDataFrame = (currentDirection >= 0)?
				currentSlice.startFrame : state.getSlice(currentSliceIndex+1).startFrame-1;
		double framesPerBeat = orbital.getFramesPerBeat();
		if (njin.currentStutter() > 1) {
			nStutterFrames = (int) (framesPerBeat * njin.currentSliceBeatDuration() / (njin.currentStutter()));
			if (nStutterFrames < minStutterFrames) {
				nStutterFrames = minStutterFrames;
			}
			nextStutterFrame = currentLoopFrame + nStutterFrames;
		} else {
			nextStutterFrame = (int) (state.getSlice(currentSliceIndex+1).position * framesPerBeat);
		}
		setWreckerTune(cWreckerPointer, njin.currentSliceTune());
		setWreckerGain(cWreckerPointer, njin.currentSliceGain());
		setWreckerPan(cWreckerPointer, currentSlice.pan);
		ArrayList<LFO> ll = currentSlice.lfo;
		boolean ifs = njin.isFirstSlice();
		if (ll != null) {
			
			setWreckerNLFO(cWreckerPointer, ll.size());
			for (int i=0; i<ll.size(); i++) {
				LFO l = ll.get(i);
				if (globalResetMode(l.trigger)) {
					if (ifs) {
						l = getSlice(0).getLFO(i);
					} else {
						continue;
					}
				}
				Log.d("wreckker", "setting lfos "+i+", "+l.rate+", "+l.trigger);
				if (l.trigger == ResetMode.ONATTACK || l.trigger == ResetMode.ONSLICE || (ifs && l.trigger == ResetMode.ONLOOP)) {
					resetWreckerLFO(cWreckerPointer, i);
				}
				if (!globalResetMode(l.trigger) || ifs) {
					setWreckerLFO(cWreckerPointer, i, l.waveform.code(), l.trigger.code(), l.tempoLock, l.rate, l.phase);
					ArrayList<ModulationTarget> t = l.target;
					if (t != null) {
						Log.d("wreckker", "setting lfo tgs "+t.size());
						setWreckerNLFOTgt(cWreckerPointer, i, t.size());
						for (int j=0; j<t.size(); j++) {
							Log.d("wreckker", "setting lfo tgs ");
							ModulationTarget tj = t.get(j);
							setWreckerLFOTgt(cWreckerPointer, i, j, tj.target.ccontrolId(), tj.amount);
						}
					}
				}
			}
		}
		ArrayList<Envelope> el = currentSlice.envelope;
		if (el != null) {
			Log.d("wreckker", "setting envs");
			setWreckerNEnv(cWreckerPointer, el.size());
			for (int i=0; i<el.size(); i++) {
				Envelope e=el.get(i);
				ArrayList<ModulationTarget> t = e.target;
				if (globalResetMode(e.trigger)) {
					if (ifs) {
						e = getSlice(0).getEnv(i);
					} else {
						continue;
					}
				}
				if (e.trigger == ResetMode.ONATTACK || e.trigger == ResetMode.ONSLICE || (ifs && e.trigger == ResetMode.ONLOOP)) {
					resetWreckerEnv(cWreckerPointer, i);
				}
				if (!globalResetMode(e.trigger) || ifs) {
					setWreckerEnv(cWreckerPointer, i, e.trigger.code(), e.tempoLock, e.attackT, e.decayT, e.sustainT, e.sustainL, e.releaseT);
					if (t != null) {
						setWreckerNEnvTgt(cWreckerPointer, i, t.size());
						for (int j=0; j<t.size(); j++) {
							ModulationTarget tj = t.get(j);
							setWreckerEnvTgt(cWreckerPointer, i, j, tj.target.ccontrolId(), tj.amount);
						}
	
					}
				}
			}
		}
		currentSliceBus  = njin.currentSliceFxBus();
//		if (currentSlice.direction == Direction.FWDBACK.code() || currentSlice.direction == Direction.BACKFWD.code()) {
//			currentDirection = -currentDirection;
//		}
	}
	private void setSlice(int i)
	{
		Log.d("wrecker", "set slice "+i);
		currentSlice = state.getSlice(i);
		if (currentSlice == null) {
			return;
		}
		currentSliceIndex = i;
	}
	
	public void setPlayMode(PlayMode m) {
		if (state.playMode == m) {
			return;
		}
		switch (state.playMode) {
		default:
			break;
		}
		state.playMode = m;
		setPlayMode();
	}
	
	protected void setPlayMode() {
		switch (state.playMode) {
		case CHAOTICOR: {
			njin = chaosEngine;
			break;
		}
		case AUTOMATOR: {
			if (algorithm == null) {
				state.playMode = PlayMode.VANILLA;
				njin = linearEngine;
				return;
			}
			njin = automatonEngine;
			break;
		}
		case PATTERNATOR: {
			njin = patternEngine;
			break;
		}
		case RANDOMATOR:
			njin = randomEngine;
			break;
		case VANILLA:
		default:
			njin = linearEngine;
			break;
		}
		njin.setState(state);
		njin.initialize();
	}
	
	private void resetAutomaton() {
		if (algorithm != null) {
			algorithm.reset();
		} else {
			setPlayMode(PlayMode.VANILLA);
		}
	}
	
	private void onLoopEnd() {
//		Log.d("player", "end of the loop "+currentLoopFrame + " ... "+ currentSlice.direction);
		++currentLoopCount;
		OrbitalLaser.getOL().onLoopEnd(this);
		if (state.loopCount > 0 && currentLoopCount >= state.loopCount) {
			stop(true);
		}
// reset these variables for the start of the loop ... assume we have done all appropriate loop end checks involving them first
		currentLoopFrame = 0;
		currentBeat = 0;
		setSlice(njin.getFirstSlice());
		startSlice();
		int lsb = Bufferator.chunk4Frame((int) currentDataFrame);
		currentSampleInfo.requestChunk(lsb, 1);
		currentSampleInfo.requestChunk(lsb+1, 1);
		reloopWrecker(cWreckerPointer);
		loopStart();
	}
		
	private void loopStart() {
	}
	
	protected void checkExportStart(int buffInd) {
		if (startExporting) {
			startExporting = false;
			startExport(0);
		}
	}

	private boolean startExport(int froffset) {
		isExporting = true;
		orbital.getCurrentMixer().startExport(this, froffset, exportFile, AudioFile.Type.WAV_16_STEREO);
		return true;
	}
	
	private boolean stopExport(int froffset)
	{
		if (isExporting) {
			orbital.getCurrentMixer().stopExport(this, froffset);
		}
		isExporting = false;
		return true;
	}
	
	public boolean exportLoops(File sff, int n, Observer<AudioFile> o) {
		if (isExporting) {
			return false;
		}
		orbital.getCurrentMixer().monitorExport(o);
		isExporting = true;
		exportFile = sff;
		exportCount = n;
		startExporting = true;
		return true;
	}
	
	/**
	 * initial buffer requests are made inside the load routine in {@link com.mayaswell.audio.Bufferator}
	 * @see com.mayaswell.audio.Bufferator.SampleInfo#setSampleData()
	 * @param path
	 * @param setLoopBounds
	 * @return
	 */
	public boolean setSamplePath(String path, boolean setLoopBounds)
	{
		if (path == null || path.equals("")) {
			Bufferator.free(currentSampleInfo);
			currentSampleInfo = null;
			state.path = null;
			
			setSampleInfo(0, (short) 0);
			if (listener != null) listener.setSampleData(this);
			return true;
		}

		state.path = path;
		if (currentSampleInfo != null && currentSampleInfo.path != null && currentSampleInfo.path.equals(path)) {
			return true;
		}
		
		Bufferator.free(currentSampleInfo);
		currentSampleInfo = null;
		SampleInfo inf = Bufferator.load(path);
		
		if (inf == null) {
			return false;
		}
		currentSampleInfo = inf;
		setSampleInfo(currentSampleInfo.nTotalFrames, currentSampleInfo.nChannels);

		if (listener != null) listener.setSampleData(this);

		if (setLoopBounds) {
			clearMarkers();
			setRegionStart(0);
			setRegionEnd(nTotalFrames);
		}
		return true;
	}

	private void setSampleInfo(int ntf, short nc)
	{
		nTotalFrames = ntf;
		nChannels = nc;
		setWreckerSampleInfo(cWreckerPointer, ntf, nc);
	}
	
	@Override
	public boolean isPlaying()
	{
		return playState == State.PLAYING;
	}

	public boolean isPaused()
	{
		return playState == State.PAUSED;
	}


	@Override
	public SampleInfo getSampleInfo() {
		return currentSampleInfo;
	}

	@Override
	public long getRegionStart() {
		return state.regionStart();
	}

	@Override
	public long getRegionLength() {
		return state.regionLength();
	}
	
	public long getRegionEnd() {
		return state.getRegionEnd();
	}
	public void setRegionStart(long v) {
		Log.d("wrecker", "set region start "+v);
		state.setRegionStart(v);
	}
	
	public void setRegionEnd(long v) {
		Log.d("wrecker", "set region end "+v);
		state.setRegionEnd(v);
	}
	
	private void clearMarkers() {
		state.clearMarkers();
	}
	
	public boolean fire() {
		SampleInfo csf = currentSampleInfo;
		if (csf == null || csf.sampleChunk == null) {
			return false;
		}
		if (nTotalFrames == 0) return false;
		currentLoopFrame = 0;
		currentLoopCount = 0;
		currentBeat = 0;
		njin.initialize();
		setSlice(njin.getFirstSlice());
		startSlice();
		playState = State.PLAYING;
		fireWrecker(cWreckerPointer);
		loopStart();
		return true;
	}

	/**
	 * @return
	 */
	public boolean stop(boolean andTriggerEvent)
	{
//		Log.d("padsample", String.format("stop triggered"));
		if (andTriggerEvent && listener != null) listener.playComplete(this);
		playState = State.STOPPED;
		currentLoopCount = 0;
		stopWrecker(cWreckerPointer);
		stopExport(0);
		return true;
	}
	
	@Override
	public long getFrameCount() {
		return nTotalFrames;
	}
	@Override
	public float getCurrentPosition() {
		int l = state.regionLength();
		return l!=0? (((float)(currentDataFrame-state.regionStart()))/state.regionLength()) : 0;
	}
	
	@Override
	public Observable<Path[]> getSamplePaths(final int w, final int h)
	{
		final SampleInfo csi = currentSampleInfo;
		SampleGfxInfo gfc = null;
		if (csi != null) {
			return csi.getMinMax(w).map(new Func1<SampleGfxInfo,Path[]>() {
				@Override
				public Path[] call(SampleGfxInfo gfc) {
					Log.d("padsample", "got minimaxa "+gfc.length+", "+gfc.dataMax[0].length+" for "+w+", "+h);
					Path[] drawPath = new Path[csi.nChannels];

					float zp[] = new float[csi.nChannels];
					float ch = h/nChannels;
					for (int k=0; k<nChannels; k++) {
						zp[k] = (ch*k+(ch/2));
						drawPath[k] = new Path();
						drawPath[k].moveTo(0, zp[k]);
						for (int j=0; j<gfc.dataMax[k].length; j++) {
							drawPath[k].lineTo(j, zp[k]+(ch*gfc.dataMax[k][j]/2));
						}
						drawPath[k].lineTo(gfc.dataMin[k].length, zp[k]);
						for (int j=gfc.dataMin[k].length-1; j>=0; j--) {
							drawPath[k].lineTo(j, zp[k]+(ch*gfc.dataMin[k][j]/2));
						}
						drawPath[k].close();
					}
					return drawPath;
				}
			});
		} else {
			return Observable.create(new Observable.OnSubscribe<Path[]>() {
				@Override
				public void call(Subscriber<? super Path[]> subscriber) {
					subscriber.onNext(null);
					subscriber.onCompleted();
				}
			});
		}
	}

	@Override
	public float proportion2secs(float tL) {
		if (nTotalFrames == 0) return 0f;
		return (tL * nTotalFrames)/OLAudioMixer.sampleRate;
	}
	
	public WreckageState getState() {
		return state;
	}
	
	public void setState(WreckageState s)
	{
		state = s;
		groups.setTo(state.getSlices());
		setSamplePath(s.path, false);
		setNBeat(s.nDataBeats);
		setAlgorithm(s.algorithmId);
		setPlayMode();
		if (njin != null) {
			njin.setState(s);
		}
		for (FXBusState fxb: state.getBuses()) {
			setFXBusState(fxb);
		}
		buildSliceAdapter();
	}
	
	public void analyseGrid(int nslice) {
		long sliceLength = getRegionLength()/nslice;
		float posLength = ((float)nBeat) / nslice;
		ArrayList<Slice> slices = new ArrayList<Slice>();
		long mki = getRegionStart();
		float posi = 0;
		slices.add(new Slice(0, mki, 0));
		for (int i=1; i<nslice; i++) {
			mki += sliceLength;
			posi += posLength;
			slices.add(new Slice(i, mki, posi));
		}
		slices.add(new Slice(nslice, getRegionEnd(), nBeat));
		setSlices(slices);
		setAnalysisInfo();
		buildSliceAdapter();
		if (listener != null) listener.analysisComplete(this);
	}
	
	public void setAnalysisInfo() {
		trueLength = getRegionLength();
		baseDuration = ((float)trueLength) / OLAudioMixer.sampleRate;
		baseTempo  = (trueLength > 0? ((float)60 * nBeat * OLAudioMixer.sampleRate)/trueLength : 0);
	}
	
	public long getTrueLength() {
		return trueLength;
	}
	
	public float getBaseDuration() {
		return baseDuration;
	}
	
	public float getBaseTempo() {
		return baseTempo;
	}
	
	public boolean moveRegionTo(long newStart) {
		if (newStart == getRegionStart()) {
			return true;
		}
		if (newStart < 0) {
			return false;
		}
		int delta = (int) (newStart-getRegionStart());
		if (getRegionEnd()+delta > nTotalFrames) {
			delta = (int) (nTotalFrames - getRegionEnd());
		}
		for (Slice s: state.getSlices()) {
			s.startFrame += delta;
		}
		return true;
	}
	public boolean scaleRegion(long newStart, long newEnd) {
		long sl = getRegionStart();
		if (newStart == sl && newEnd == getRegionEnd()) {
			return true;
		}
		if (newStart < 0) {
			return false;
		}
		if (newEnd <= newStart+state.getSlices().size()) {
			return false;
		}
		if (newEnd > nTotalFrames) {
			return false;
		}
		long newLength = newEnd - newStart;
		float scaleF = ((float)newLength)/((float) getRegionLength());
		for (int i=1; i<state.getSlices().size()-1; i++) {
			Slice s = state.getSlice(i);
			s.startFrame = (int) (newStart+((float)scaleF*(s.startFrame-((int)sl))));
		}
		state.setSlicePosition(0, (int) newStart);
		state.setSlicePosition(state.getSlices().size()-1, (int) newEnd);
		return true;
	}
	
	private void setSlices(ArrayList<Slice> slices) {
		state.setSlices(slices);
	}
	public void setNBeat(int v) {
		nBeat = v;
		setAnalysisInfo();
		if (state != null) {
			state.nDataBeats = v;
			Slice s = state.endMarker();
			if (s != null) {
				s.position = v;
			}
		}
	}
	
	public void setMeter(int v) {
		metric.meter = v;
	}
	
	/* *********************************************************
	 * controllable entry points
	 * *********************************************************/
	@Override
	public void setupControlsAdapter(Context c, int tvrid, int ddrid) {
		directionAdapter =  new ArrayAdapter<Direction>(c, tvrid);
		directionAdapter.setDropDownViewResource(ddrid);
		directionAdapter.add(Direction.FORWARD);
		directionAdapter.add(Direction.BACKWARD);
		directionAdapter.add(Direction.FWDBACK);
		directionAdapter.add(Direction.BACKFWD);
		
		lfoTgtAdapter = new ControlsAdapter(c, tvrid);
		lfoTgtAdapter.setDropDownViewResource(ddrid);
		lfoTgtAdapter.add(new CControl(CControl.NOTHING));
		lfoTgtAdapter.add(new CControl(CControl.GAIN));
		lfoTgtAdapter.add(new CControl(CControl.PAN));
		lfoTgtAdapter.add(new CControl(CControl.TUNE));
		for (int i=0; i<Global.LFO_PER_WRECK; i++) {
			lfoTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
//			interfaceTgtAdapter.add(new CControl(CControl.lfoPhaseId(i)));
			for (int j=0; j<Global.MAX_LFO_TGT; j++) {
//				lfoTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<Global.ENV_PER_WRECK; i++) {
			for (int j=0; j<Global.MAX_ENV_TGT; j++) {
				lfoTgtAdapter.add(new CControl(CControl.envTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<Global.MAX_BUS_PER_WRECK; i++) {
			lfoTgtAdapter.add(new CControl(CControl.busGainId(i)));
			lfoTgtAdapter.add(new CControl(CControl.busPanId(i)));
			for (int j=0; j<Global.MAX_BUS_FX; j++) {
				for (int k=0; k<Global.MAX_FX_PARAM; k++) {
					CControl cc = new CControl(CControl.fxParamId(i, j, k));
					lfoTgtAdapter.add(cc);
				}
			}
		}
		lfoTgtAdapter.setFilter(inUseModulatorFilter);
		lfoTgtAdapter.refilter();

		envTgtAdapter = new ControlsAdapter(c, tvrid);
		envTgtAdapter.setDropDownViewResource(ddrid);
		envTgtAdapter.add(new CControl(CControl.NOTHING));
		envTgtAdapter.add(new CControl(CControl.GAIN));
		envTgtAdapter.add(new CControl(CControl.PAN));
		envTgtAdapter.add(new CControl(CControl.TUNE));
		for (int i=0; i<Global.LFO_PER_WRECK; i++) {
			envTgtAdapter.add(new CControl(CControl.lfoRateId(i)));
			for (int j=0; j<Global.MAX_LFO_TGT; j++) {
				envTgtAdapter.add(new CControl(CControl.lfoTargetDepthId(i, j)));
			}
		}
		for (int i=0; i<Global.MAX_BUS_PER_WRECK; i++) {
			envTgtAdapter.add(new CControl(CControl.busGainId(i)));
			envTgtAdapter.add(new CControl(CControl.busPanId(i)));
			for (int j=0; j<Global.MAX_BUS_FX; j++) {
				for (int k=0; k<Global.MAX_FX_PARAM; k++) {
					envTgtAdapter.add(new CControl(CControl.fxParamId(i,  j, k)));
				}
			}
		}
		envTgtAdapter.setFilter(inUseModulatorFilter);
		envTgtAdapter.refilter();

		groupAdapter = new ArrayAdapter<MenuData<SliceGroup>>(c, R.layout.mw_spinner_item);
		groupAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		groupAdapter.add(new MenuData<SliceGroup>(null, -1, " "));
		for (int i=0; i<groups.size(); i++) {
			SliceGroup g = groups.getGroup(i);
			groupAdapter.add(new MenuData<SliceGroup>(g, g.getId(), Integer.toString(g.getId())));
		}
		nnGroupAdapter = new ArrayAdapter<MenuData<SliceGroup>>(c, R.layout.mw_spinner_item);
		nnGroupAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		for (int i=0; i<groups.size(); i++) {
			SliceGroup g = groups.getGroup(i);
			nnGroupAdapter .add(new MenuData<SliceGroup>(g, g.getId(), Integer.toString(g.getId())));
		}
		
		sliceAdapter = new ArrayAdapter<MenuData<Slice>>(c, R.layout.mw_spinner_item);
		sliceAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		buildSliceAdapter();
	}
	
	protected void buildSliceAdapter() {
		if (state == null) {
			return;
		}
		sliceAdapter.clear();
		for (int i=0; i<state.sliceCount(); i++) {
			Slice g = state.getSlice(i);
			sliceAdapter.add(new MenuData<Slice>(g, g.ind, Integer.toString(g.ind+1)));
		}
		
	}
	private Slice lastSelectedSlice=null;
	
	private ControlsFilter inUseModulatorFilter = new ControlsFilter() {

		@Override
		public boolean isAvailable(Control c) {
			CControl cc = (CControl) c;
			if (cc.isEnvControl()) {
				if (lastSelectedSlice == null) return false;
				ArrayList<Envelope> el = lastSelectedSlice.envelope;
				int env = cc.env4Id();
				int param = cc.envParam4Id();
				if (env < el.size()) {
					if (el.get(env).target.size() == 0) return false;
					if (el.get(env).target.size() == 1 && el.get(env).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param >= 5) {
						if (param-5 >= el.get(env).target.size()) return false;
						if (el.get(env).target.get(param-5).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}
				return false;
			} else if (cc.isLFOControl()) {
				if (lastSelectedSlice == null) return false;
				ArrayList<LFO> ll = lastSelectedSlice.lfo;
				int lfo = cc.lfo4Id();
				int param = cc.lfoParam4Id();
				if (lfo < ll.size()) {
					if (ll.get(lfo).target.size() == 0) return false;
					if (ll.get(lfo).target.size() == 1 && ll.get(lfo).target.get(0).target.ccontrolId() == CControl.NOTHING) return false;
					if (param == 1) {
						return false;
					} else if (param >= 2) {
						if (param-2 >= ll.get(lfo).target.size()) return false;
						if (ll.get(lfo).target.get(param-2).target.ccontrolId() == CControl.NOTHING) return false;
					}
					return true;
				}
				return false;
			} else if (cc.isBusControl()) {
				int bid = cc.bus4Id();
				for (Slice s: state.getSlices()) {
					if (s.fxBus == bid) {
						return true;
					}
				}
				return false;
			} else if (cc.isFXControl()) {
				int fid = cc.fx4fxId();
				int bid = cc.bus4fxId();
				int pid = cc.param4fxId();
				FXBusState b = state.getBus(bid);
				if (b == null) return false;
				FX f = b.getFX(fid);
				if (f == null || f.getId() == FX.UNITY) {
					return false;
				}
				if (f.countParams() < pid) {
					return false;
				}
				return true;
			}
			return true;
		}
	};

	@Override
	public ControlsAdapter getInterfaceTgtAdapter() {
		return null;
	}
	
	@Override
	public int controllableId() {
		return id;
	}
	@Override
	public String controllableType() {
		return "Wrecker";
	}
	
	@Override
	public float floatValue(Control cc) {
		switch (cc.ccontrolId()) {
		case CControl.GAIN: {
			return 0;
		}
		default: {
			break;
		}
		}
		return 0;
	}
	
	@Override
	public void setValue(Control cc, float v) {
		switch (cc.ccontrolId()) {
		case CControl.GAIN: {
			break;
		}
		default: {
			break;
		}
		}
	}
	
	@Override
	public String abbrevName() {
		return "W"+id;
	}
	public void setBufSize(int outBufsize) {
		setWreckerBufsize(cWreckerPointer, outBufsize);
	}
	public boolean setSlicePosition(Slice slice, float v) {
		if (v < 0 || v > nBeat) {
			return false;
		}
		int i = state.getSlices().indexOf(slice);
		if (i < 0) {
			return false;
		}
		if (i > 0 && state.getSlice(i-1).position >= v) {
			return false;
		}
		if (i < state.sliceCount()-1 && state.getSlice(i+1).position <= v) {
			return false;
		}
		slice.position = v;
		return true;
	}
	
	public boolean setSliceStart(Slice slice, long v) {
		if (v < 0 || v > nTotalFrames) {
			return false;
		}
		int i = state.getSlices().indexOf(slice);
		if (i < 0) {
			return false;
		}
		if (i > 0 && state.getSlice(i-1).startFrame >= v) {
			return false;
		}
		if (i < state.sliceCount()-1 && state.getSlice(i+1).startFrame <= v) {
			return false;
		}
		slice.startFrame = (int) v;
		return true;
	}

	/* ********************************************
	 * FXBus entry points 
	 * ********************************************/
	/**
	 * 
	 * @return
	 */
	public int getDisplayedBus() {
		return displayedBus ;
	}

	public void setDisplayedBus(int b) {
		displayedBus = b;
		
	}
	public FXBusState getBusState(int i) {
		if (state == null) {
			return null;
		}
		if (i >= 0 && i < Global.MAX_BUS_PER_WRECK) {
			FXBusState b = state.getBus(i);
			if (b == null) {
				b = state.addBus(i);
			}
			return b;
		}
		return null;
	}

	public FXBusState getDisplayedBusState() {
		return getBusState(displayedBus);
	}
	public boolean setNFX(FXBusState fxBus,int n)
	{
		return false;
	}
	
	public boolean setFXBusState(FXBusState fxBus)
	{
		int i=0;
		if (fxBus != null) {
			setBusGain(cWreckerPointer, fxBus.id, fxBus.gain);
			setBusPan(cWreckerPointer, fxBus.id, fxBus.pan);
			setBusEnable(cWreckerPointer, fxBus.id, fxBus.enable);
			for (FXState f: fxBus.fx) {
				if (f.getFx() != null) {
					int j=0;
					float [] param = new float[f.getFx().params().size()];
					for (float pi: f.getParams()) {
						if (j > param.length) {
							break;
						}
						param[j++] = pi;
					}
					setFXType(cWreckerPointer, fxBus.id, i, f.getFx().getId(), param.length, param);
					setFXEnable(cWreckerPointer, fxBus.id, i, f.getEnable());
				} else {
					Log.d("bus", "fx list get fx null");
				}
				i++;
			}
		}
		for (; i<OrbitalLaser.Global.MAX_BUS_FX; i++) {
			setFXType(cWreckerPointer, fxBus.id, i, FX.UNITY, 0, null);
			setFXEnable(cWreckerPointer, fxBus.id, i, false);
		}
		refilterAdapters();
		return true;
	}
	
	public boolean setFX(FXBusState fxb, int fxId, FX fx) {
		if (fxb == null|| state == null) {
			return false;
		}
		boolean b = fxb.setFXstate(fxId, fx);
		if (b) {
			float [] param = new float[fx.params().size()];
			int i=0;
			for (FXParam v: fx.params()) {
				param[i++] = v.getCenterValue();
			}
			setFXType(cWreckerPointer, fxb.id, fxId, fx.getId(), param.length, param);
			setFXEnable(cWreckerPointer, fxb.id, fxId, fxb.getFXEnable(fxId));
			refilterAdapters();
		}
		return b;
	}
	protected void refilterAdapters() {
		OrbitalLaser.ol.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (lfoTgtAdapter != null) lfoTgtAdapter.refilter();
				if (envTgtAdapter != null) envTgtAdapter.refilter();
			}
		});
	}
	
	public boolean setFXparam(FXBusState fxb, int fxId, int paramId, float amt) {
		if (fxb == null || state == null) {
			return false;
		}
		boolean b = fxb.setFXparam(fxId, paramId, amt);
		setFXParam(cWreckerPointer, fxb.id, fxId, paramId, amt);
		return b;
	}
	
	public boolean setFXenable(FXBusState fxb, int fxId, boolean en) {
		if (fxb == null || state == null) {
			return false;
		}
		boolean b = fxb.setFXEnable(fxId, en);
		setFXEnable(cWreckerPointer, fxb.id, fxId, en);
		return b;
	}
	
	public boolean setFXBusGain(FXBusState fxb, float amt) {
		if (fxb == null || state == null) {
			return false;
		}
		boolean b = fxb.setGain(amt);
		setBusGain(cWreckerPointer, fxb.id, amt);
		return b;
		
	}
	public boolean setFXBusPan(FXBusState fxb, float amt) {
		if (fxb == null || state == null) {
			return false;
		}
		boolean b = fxb.setPan(amt);
		setBusPan(cWreckerPointer, fxb.id, amt);	
		return b;
	}
	public boolean setFXBusEnable(FXBusState fxb, boolean en) {
		if (fxb == null || state == null) {
			return false;
		}
		boolean b = fxb.setEnable(en);
		setBusEnable(cWreckerPointer, fxb.id, en);
		return b;
	}

	/* ******************************************
	 * 	lfo entry points
	 * ******************************************/
	 @Override
	public boolean setLFOList(ArrayList<LFO> lfo)
	{
		setWreckerNLFO(cWreckerPointer, lfo.size());
		int i=0;
		for (LFO l: lfo) {
			if (l.countValidTargets() > 0) {
				setWreckerLFO(cWreckerPointer, i, l.waveform.code(), l.trigger.code(), l.tempoLock, l.rate, l.phase);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null/* && mt.target.getTypeCode() != CControl.NOTHING*/) {
						setWreckerLFOTgt(cWreckerPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setWreckerNLFOTgt(cWreckerPointer, i, j);
				i++;
			}
		}
		return true;
	}
	
	 @Override
	 public boolean setNLFO(int n) {
		 return true;
	 }

	 @Override
	 public boolean setNLFOTarget(int xyci, int n) {
		 return true;
	 }

	 @Override
	 public int maxLFO() {
		 return Global.LFO_PER_WRECK;
	 }

	 @Override
	 public int maxLFOTgt() {
		 return Global.MAX_LFO_TGT;
	 }

	 @Override
	 public ControlsAdapter getLFOTgtAdapter() {
		 return lfoTgtAdapter;
	 }

	 @Override
	 public ArrayList<LFO> lfos() {
		 return lastSelectedSlice != null? lastSelectedSlice.lfo:null;
	 }

	@Override
	public boolean setLFORate(int xyci, float amt) {
		Collection<Slice> sa = sliceCollectionForLFO(xyci);
		for (Slice si: sa) {
			si.setLFORate(xyci,  amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	
	@Override
	public boolean setLFOPhase(int xyci, float amt) {
		Collection<Slice> sa = sliceCollectionForLFO(xyci);
		for (Slice si: sa) {
			si.setLFOPhase(xyci,  amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setLFOLock(int xyci, boolean amt) {
		Collection<Slice> sa = sliceCollectionForLFO(xyci);
		for (Slice si: sa) {
			si.setLFOLock(xyci,  amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	
	private boolean globalResetMode(ResetMode rm)
	{
		return rm == ResetMode.ONFIRE || rm == ResetMode.ONLOOP;
	}
	
	@Override
	public boolean setLFOReset(int xyci, ResetMode rm) {
		Collection<Slice> sa;
		if (globalResetMode(rm)) {
			sa = state.getSlices();
			if (lastSelectedSlice != null && lastSelectedSlice.ind != 0) {
				Slice first = state.getSlice(0);
				first.cloneLFO(xyci, lastSelectedSlice.getLFO(xyci));
			}
		} else {
			sa = sliceCollectionForLFO(xyci);
		}
		for (Slice si: sa) {
			si.setLFOReset(xyci,  rm);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setLFOWave(int xyci, LFWave amt) {
		Collection<Slice> sa = sliceCollectionForLFO(xyci);
		for (Slice si: sa) {
			si.setLFOWave(xyci,  amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}

	@Override
	public boolean setLFOTarget(int xyci, int xycj, ICControl target, float amt) {
		Collection<Slice> sa = sliceCollectionForLFO(xyci);
		for (Slice si: sa) {
			si.setLFOTarget(xyci,  xycj, target, amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setLFOTargetAmt(int xyci, int xycj, float amt) {
		Collection<Slice> sa = sliceCollectionForLFO(xyci);
		for (Slice si: sa) {
			si.setLFOTargetAmt(xyci,  xycj, amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	
	/* **********************************************************************
	 * 	envelope entry points
	 * **********************************************************************/
	 @Override
	public boolean setEnvList(ArrayList<Envelope> envelope)
	{
		setWreckerNEnv(cWreckerPointer, envelope.size());
		int i=0;
		for (Envelope l: envelope) {
			if (l.countValidTargets() > 0) {
				setWreckerEnv(cWreckerPointer, i++, l.trigger.code(), l.tempoLock, l.attackT, l.decayT, l.sustainT, l.sustainL, l.releaseT);
				int j=0;
				for (ModulationTarget mt: l.target) {
					if (mt != null/* && mt.target.getTypeCode() != CControl.NOTHING*/) { // need to set the NOTHING value too
						setWreckerEnvTgt(cWreckerPointer, i, j++, mt.target!=null?mt.target.ccontrolId():CControl.NOTHING, mt.amount);
					}
				}
				setWreckerNLFOTgt(cWreckerPointer, i, j);
				i++;
			}
		}
		return true;
	}
		
	@Override  
	public boolean setNEnvelope(int n) {
		 return true;
	}

	@Override
	public boolean setNEnvelopeTarget(int xyci, int n) {
		return true;
	}

	@Override
	public int maxEnv() {
		return Global.ENV_PER_WRECK;
	}

	@Override 
	public int maxEnvTgt() {
		return Global.MAX_ENV_TGT;
	}

	@Override
	public ControlsAdapter getEnvTgtAdapter() {
		return envTgtAdapter;
	}

	@Override
	public ArrayList<Envelope> envs() {
		return lastSelectedSlice != null? lastSelectedSlice.envelope:null;
	}
	@Override
	public boolean setEnvelopeLock(int xyci, boolean amt) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeLock(xyci,  amt);
			if (si == currentSlice) {

			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeReset(int xyci, ResetMode rm) {
		 Collection<Slice> sa;
		 if (globalResetMode(rm)) {
			sa = state.getSlices();
			if (lastSelectedSlice != null && lastSelectedSlice.ind != 0) {
				Slice first = state.getSlice(0);
				first.cloneEnv(xyci, lastSelectedSlice.getEnv(xyci));
			}
		} else {
			sa = sliceCollectionForLFO(xyci);
		}
		for (Slice si: sa) {
			si.setEnvelopeReset(xyci,  rm);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeAttack(int xyci, float t) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeAttack(xyci,  t);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeDecay(int xyci, float t) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeDecay(xyci,  t);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeSustain(int xyci, float t) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeSustain(xyci,  t);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeSustainLevel(int xyci, float l) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeSustainLevel(xyci,  l);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeRelease(int xyci, float t) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeRelease(xyci,  t);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeTarget(int xyci, int xycj, ICControl target, float amt) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeTarget(xyci,  xycj, target, amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}
	@Override
	public boolean setEnvelopeTargetAmt(int xyci, int xycj, float amt) {
		Collection<Slice> sa = sliceCollectionForEnv(xyci);
		for (Slice si: sa) {
			si.setEnvelopeTargetAmt(xyci, xycj, amt);
			if (si == currentSlice) {
				
			}
		}
		return true;
	}


	/* **********************************************************************
	 * 	slice parameter entry points
	 * **********************************************************************/

	public void setLastSelectedSlice(Slice m) {
		lastSelectedSlice = m;
	}

	public Collection<Slice> selectedSlices()
	{
		ArrayList<Slice> sa = new ArrayList<Slice>();
		if (state != null) {
			for (Slice s: state.getSlices()) {
				if (s.selected) {
					sa.add(s);
				}
			}
		}
		return sa;
	}
	
	private Collection<Slice> sliceCollectionForLFO(int xyci) {
		LFO l;
		if (currentSlice != null && (l=currentSlice.getLFO(xyci)) != null && (l.trigger == ResetMode.ONFIRE || l.trigger == ResetMode.ONLOOP)) {
			return state.getSlices();
		}
		return selectedSlices();
	}
	
	private Collection<Slice> sliceCollectionForEnv(int xyci) {
		Envelope l;
		if (currentSlice != null && (l=currentSlice.getEnv(xyci)) != null && (l.trigger == ResetMode.ONFIRE || l.trigger == ResetMode.ONLOOP)) {
			return state.getSlices();
		}
		return selectedSlices();
	}
	
	public boolean setFXBus(Slice slice, short b) {
		for (Slice si: selectedSlices()) {
			si.setFXBus(b);
			if (si == slice) {
				
			}
		}
		return false;
	}
	public void setTune(Slice slice, float v) {
		for (Slice si: selectedSlices()) {
			si.setTune(v);
			if (si == currentSlice) {
				
			}
		}
	}
	public void setPan(Slice slice, float v) {
		for (Slice si: selectedSlices()) {
			si.setPan(v);
			if (si == currentSlice) {
				
			}
		}
	}
	public void setGain(Slice slice, float v) {
		for (Slice si: selectedSlices()) {
			si.setGain(v);
			if (si == currentSlice) {
				
			}
		}
	}
	public void setStutter(Slice slice, int v) {
		for (Slice si: selectedSlices()) {
			si.setStutter(v);
			if (si == currentSlice) {
				
			}
		}
	}
	public void setDirection(Slice slice, Direction d) {
		for (Slice si: selectedSlices()) {
			si.setDirection(d);
			if (si == currentSlice) {
				
			}
		}
	}
	public void setTempo(float t) {
		setWreckerTempo(cWreckerPointer, t);
	}
	
	@Override
	public boolean alwaysLockedTempo()
	{
		return false;
	}
	public int countSlices() {
		return state != null? state.sliceCount():2;
	}
	public boolean splitSlice(Slice s) {
		int i=0;
		boolean succ = false;
		for (Slice si: state.getSlices()) {
			if (si == s) {
				if (i >= state.sliceCount()) {
					return false;
				}
				Slice sn = s.clone();
				Slice sip1 = state.getSlice(i+1);
				state.insSlice(i+1, sn);
				sn.position = (s.position+sip1.position)/2.0f;
				sn.startFrame = (s.startFrame+sip1.startFrame)/2;
				succ = true;
				break;
			}
			i++;
		}
		if (!succ) {
			return false;
		}
		Log.d("split", state.getSlices().size()+" success. this many slices now!!");
		i = 0;
		for (Slice si: state.getSlices()) {
			si.ind = i++;
		}
		buildSliceAdapter();
		return true;
	}
	public boolean mergeSlice(Slice s) {
		int i=0;
		boolean succ = false;
		for (Slice si: state.getSlices()) {
			if (si == s) {
				if (i >= state.sliceCount()-1) {
					return false;
				}
				state.delSlice(i+1);
				succ = true;
				break;
			}
			i++;
		}
		if (!succ) return false;
		i = 0;
		for (Slice si: state.getSlices()) {
			si.ind = i++;
		}
		buildSliceAdapter();
		return true;
	}

	public Slice getSlice(int i) {
		if (state == null) return null;
		return state.getSlice(i);
	}
	public int getSliceAt(float p) {
		if (state == null) return -1;
		return state.getSliceAt(p);
	}
	public int sliceCount() {
		if (state == null) return 0;
		return state.sliceCount();
	}
	public void setPattern(ArrayList<SliceCell> p) {
		state.setPattern(p);
	}
	public boolean setGroup(Slice s, int g) {
		if (groups == null) return false;
		return groups.setGroup(s, g);
	}
	public SliceGroup getGroup(int ind) {
		if (groups == null) return null;
		return groups.getGroup(ind);
	}
	public void setAlgorithm(int ai) {
		AlgoRhythm a = OrbitalLaser.ol.getBank().getAlgorithm(ai);
		state.algorithmId = ai;
		if (a == null) {
			if (state.playMode == PlayMode.AUTOMATOR) {
				setPlayMode(PlayMode.VANILLA);
			}
		}
		if (algorithm != null) {
			algorithm.setListener(null);
		}
		algorithm = a;
		if (algorithm != null) {
			algorithm.setListener(new AlgoRhythm.Listener() {
	
				@Override
				public void onCellChange(int i) {
					lastCellItemInd = currentCellItemInd;
					currentCellItemInd = i;
					if (listener != null) listener.cellChanged(self);
				}
	
				@Override
				public void onNodeChange(int i) {
					lastNodeId = currentNodeId;
					currentNodeId = i;
					if (listener != null) listener.nodeChanged(self);
				}
			});
		}
	}
	
	public void setGridQuant(int v) {
		gridQuant = v;
	}
	public AlgoRhythm getAlgorithm() {
		return algorithm;
	}
	
	public void resetCurrentSlice() {
		startSlice();
	}
	public int getCurrentAlgorithmId() {
		return state != null? state.algorithmId: -1;
	}
	
	public int randomSlice() {
		if (state == null) return -1;
		return (int) Math.floor(Math.random()*state.sliceCount());
	}
	
	public Slice randomGroupSlice(int grp) {
		return groups.selectSlice(grp);
	}
	public long getMeter() {
		return metric != null? metric.meter: 4;
	}
	public CharSequence getBeatCounterString() {
		return metric.getString(currentBeat+currentLoopCount*nBeat);
	}
}
