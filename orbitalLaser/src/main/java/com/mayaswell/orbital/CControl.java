package com.mayaswell.orbital;

import com.mayaswell.audio.Control;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.ICControl;
import com.mayaswell.audio.LFO;
import com.mayaswell.orbital.OrbitalLaser.Global;

public class CControl extends Control {
	public static final int SLICE_START = 1;
	public static final int SLICE_POSITION = 2;
	public static final int GAIN = 3;
	public static final int PAN = 4;
	public static final int TUNE = 5;
	public static final int STUTTER = 6;
	public static final int BUS_SELECT = 7;
	public static final int FLT_CUTOFF = 8; // maybe one day
	public static final int FLT_ENVMOD = 9;
	public static final int FLT_RESONANCE = 10;
	public static final int ENV_BASE= 30;
//OrbitalLaser.ENV_PER_PAD *
//	attackT 
//	decayT 
//	sustainT
//	sustainL
//	releaseT 
//		+ OrbitalLaser.MAX_ENV_TGT * target
	public static int envParam4Id(int i) { return (i-ENV_BASE)%(Global.MAX_ENV_TGT+5); }
	public static int env4Id(int i) { return (i-ENV_BASE)/(Global.MAX_ENV_TGT+5); }
	public static int envIdBase(int i) { return ENV_BASE+i*(Global.MAX_ENV_TGT+5); }
	public static int envAttackTimeId(int i) { return 0+envIdBase(i); }
	public static int envDecayTimeId(int i) { return 1+envIdBase(i); }
	public static int envSustainTimeId(int i) { return 2+envIdBase(i); }
	public static int envSustainLevelId(int i) { return 3+envIdBase(i); }
	public static int envReleaseTimeId(int i) { return 4+envIdBase(i); }
	public static int envTargetDepthId(int i, int j) { return j+5+envIdBase(i); }
//OrbitalLaser.LFO_PER_PAD *
//	rate 
//	phase 
//		+ OrbitalLaser.MAX_LFO_TGT * target
	public static int lfoParam4Id(int i) { return (i-envIdBase(Global.ENV_PER_WRECK))%(Global.MAX_LFO_TGT+2); }
	public static int lfo4Id(int i) { return (i-envIdBase(Global.ENV_PER_WRECK))/(Global.MAX_LFO_TGT+2); }
	public static int lfoIdBase(int i) { return envIdBase(Global.ENV_PER_WRECK)+i*(Global.MAX_LFO_TGT+2); }
	public static int lfoRateId(int i) { return 0+lfoIdBase(i); }
	public static int lfoPhaseId(int i) { return 1+lfoIdBase(i); }
	public static int lfoTargetDepthId(int i, int j) { return j+2+lfoIdBase(i); }
//OrbitalLaser.MAX_BUS_PER_WRECK *
//	gain + pan + enable 
	public static int busIdBase(int i) { return (lfoIdBase(Global.LFO_PER_WRECK) + 3*i); }
	public static int busGainId(int i) { return (busIdBase(i) + 0); }
	public static int busPanId(int i) { return (busIdBase(i) + 1); }
	public static int busEnableId(int i) { return (busIdBase(i) + 2); }
	public static int bus4Id(int id) { return (id-busIdBase(0))/3; }
	public static int busParam4Id(int id) { return (id-busIdBase(0))%3; }
	
//OrbitalLaser.MAX_BUS_PER_WRECK * OrbitalLaser.MAX_BUS_FX *
//	 enable + param 0-MAX_FX_PARAM
	//params are 0 enable, 1-6 floating point parameters
	public static int fxIdBase(int b, int i) { return busIdBase(Global.MAX_BUS_PER_WRECK)+(b*Global.MAX_BUS_FX+i)*(Global.MAX_FX_PARAM+1); }
	public static int fxEnableId(int b, int i) { return 0+fxIdBase(b, i); }
	public static int fxParamId(int b, int i, int j) { return 1+j+fxIdBase(b, i); }
	public static int param4fxId(int id) { return ((id-busIdBase(Global.MAX_BUS_PER_WRECK))%(Global.MAX_FX_PARAM+1)); }
	public static int bfx4Id(int id) { return (id-busIdBase(Global.MAX_BUS_PER_WRECK))/(Global.MAX_FX_PARAM+1); }
	public static int bus4fxId(int id) { return bfx4Id(id)/Global.MAX_BUS_FX; }
	public static int fx4fxId(int id) { return bfx4Id(id)%Global.MAX_BUS_FX; }
	
	public CControl(int i)
	{
		typeCode = i;
		id = 0;
	}
	
	public CControl(String s)
	{
		typeCode = string2code(s);
		id = 0;
	}
	
	/**
	 */
	@Override
	public String fullName()
	{
		switch (typeCode) {
		case GAIN: return "gain";
		case SLICE_START: return "slice start";
		case SLICE_POSITION: return "slice position";
		case PAN: return "pan";
		case TUNE: return "tune";
		case STUTTER: return "stutter";
		case BUS_SELECT: return "bus select";
		case FLT_CUTOFF: return "cutoff";
		case FLT_ENVMOD: return "env mod";
		case FLT_RESONANCE: return "resonance";
		default: {
			if (typeCode >= envIdBase(0) && typeCode < envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				int env = env4Id(typeCode)+1;
				int param = envParam4Id(typeCode);
				if (param == 0) return "env"+Integer.toString(env)+" attack";
				if (param == 1) return "env"+Integer.toString(env)+" decay";
				if (param == 2) return "env"+Integer.toString(env)+" sustain";
				if (param == 3) return "env"+Integer.toString(env)+" sustain level";
				if (param == 4) return "env"+Integer.toString(env)+" sustain release";
				return "env"+Integer.toString(env)+" depth"+Integer.toString(param-5+1);
			} else if (typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(Global.LFO_PER_WRECK)) {
				int lfo = lfo4Id(typeCode)+1;
				int param = lfoParam4Id(typeCode);
				if (param == 0) return "lfo"+Integer.toString(lfo)+" rate";
				if (param == 1) return "lfo"+Integer.toString(lfo)+" phase";
				return "lfo"+Integer.toString(lfo)+" depth"+Integer.toString(param-2+1);
			} else if (typeCode >= fxIdBase(0,0) && typeCode < fxIdBase(Global.MAX_BUS_PER_WRECK, 0)) {
				int fx = fx4fxId(typeCode)+1;
				int bus = bus4fxId(typeCode)+1;
				int param = param4fxId(typeCode); // param 0 is enable
				if (param == 0) return "bus"+bus+" fx"+Integer.toString(fx)+" enable";
				return "bus"+bus+"fx"+Integer.toString(fx)+" param"+Integer.toString(param);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(Global.MAX_BUS_PER_WRECK)) {
				int bus = bus4Id(typeCode)+1;
				int param = busParam4Id(typeCode);
				if (param == 0) return "bus"+Integer.toString(bus)+" gain";
				if (param == 1) return "bus"+Integer.toString(bus)+" pan";
				if (param == 2) return "bus"+Integer.toString(bus)+" enable";
				return "bus"+Integer.toString(bus)+" param "+param;
			}
			break;
		}
		}
		return "none";
	}
	
	/**
	 */
	@Override
	public String abbrevName() {
		switch (typeCode) {
		case GAIN: return "gain";
		case SLICE_START: return "start";
		case SLICE_POSITION: return "position";
		case PAN: return "pan";
		case TUNE: return "tune";
		case STUTTER: return "stutter";
		case BUS_SELECT: return "bus";
		case FLT_CUTOFF: return "cutoff";
		case FLT_ENVMOD: return "env mod";
		case FLT_RESONANCE: return "resonance";
		default: {
			if (typeCode >= envIdBase(0) && typeCode < envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				int env = env4Id(typeCode)+1;
				int param = envParam4Id(typeCode);
				if (param == 0) return "env"+Integer.toString(env)+" att";
				if (param == 1) return "env"+Integer.toString(env)+" dec";
				if (param == 2) return "env"+Integer.toString(env)+" sus";
				if (param == 3) return "env"+Integer.toString(env)+" susL";
				if (param == 4) return "env"+Integer.toString(env)+" rel";
				return "env"+Integer.toString(env)+" amt"+Integer.toString(param-5+1);
			} else if (typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK)) {
				int lfo = lfo4Id(typeCode)+1;
				int param = lfoParam4Id(typeCode);
				if (param == 0) return "lfo"+Integer.toString(lfo)+" rate";
				if (param == 1) return "lfo"+Integer.toString(lfo)+" phs";
				return "lfo"+Integer.toString(lfo)+" amt"+Integer.toString(param-2+1);
			} else if (typeCode >= fxIdBase(0,0) && typeCode < fxIdBase(Global.MAX_BUS_PER_WRECK, 0)) {
				int fx = fx4fxId(typeCode)+1;
				int bus = bus4fxId(typeCode)+1;
				int param = param4fxId(typeCode); 
				if (param == 0) return "fx"+Integer.toString(fx)+" en";
				return "b" + bus + " fx"+Integer.toString(fx)+" p"+Integer.toString(param);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK)) {
				int bus = bus4Id(typeCode)+1;
				int param = busParam4Id(typeCode);
				if (param == 0) return "b"+Integer.toString(bus)+"gain";
				if (param == 1) return "b"+Integer.toString(bus)+"pan";
				if (param == 2) return "b"+Integer.toString(bus)+"enable";
				return "b"+Integer.toString(bus)+"p"+param;
			}
			break;
		}
		}
		return "none";
	}

	public String toString()
	{
		return fullName();
	}
	
	/**
	 *  converts a float param value with an arbitrary range into a normalized parameter, from [0,1]
	 * perhaps this should be refactored into PadSample, as some params (ie sample ranges) can't be arbitrarily normalized out of context
	 * from the parent audio object ... perhaps most more interesting parameters ...
	 * @param v
	 * @return the normalized value from [0,1]
	 */
	public float normalize(float v)
	{
		float nv=0;
		switch (typeCode) {
		case PAN: {
			nv = (v+1)/2;
			break;
		}
		case TUNE: {
			nv = (v+Controllable.TUNE_RANGE_SEMITONES)/(2*Controllable.TUNE_RANGE_SEMITONES);
			break;
		}
		default: {
			if (typeCode >= CControl.envIdBase(0) && typeCode < CControl.envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				int env = CControl.env4Id(typeCode);
				int param = CControl.envParam4Id(typeCode);
				if (param == 0) { nv = v/Envelope.NormalizedBeatPerSegment; } // attack
				else if (param == 1) { nv = v/Envelope.NormalizedBeatPerSegment; } // decay
				else if (param == 2) { nv = v/Envelope.NormalizedBeatPerSegment; } // sustain
				else if (param == 3) { nv = v; } // sustain level
				else if (param == 4) { nv = v/Envelope.NormalizedBeatPerSegment; } // release
				else { nv = (v+1)/2; } // depth(param-5);
			} else if (typeCode >= CControl.lfoIdBase(0) && typeCode < CControl.lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK)) {
				int lfo = CControl.lfo4Id(typeCode);
				int param = CControl.lfoParam4Id(typeCode);
				if (param == 0) { // rate
					float minl = (float) Math.log(LFO.NormalizedMinRate);
					float maxl = (float) Math.log(LFO.NormalizedMaxRate);
					nv = ((float) Math.log(v)-minl)/(maxl-minl);
				} else if (param == 1) { nv = v; } // phase
				else { nv = (v+1)/2; } // depth(param-2);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK)) {
				int bus = bus4Id(typeCode)+1;
				int param = busParam4Id(typeCode);
				if (param == 0) { // gain
					nv = v/2; // bus send is over unity ... maybe do same for main gain or not TODO ro
				} else if (param == 1) { // pan
					nv = (v+1)/2;
				} else if (param == 2) { // enable
					nv = v;
				} else { // !!!!
					
				}
			} else {
				nv = v;
				break;
			}
		}
		}
		if (nv < 0) nv = 0;
		else if (nv > 1) nv = 1;
		return nv;
	}
	
	/**
	 *  converts a float normalized param value from [0,1] into an ordinary arbitrary range float. 
	 * @param nv
	 * @return the regular param
	 */
	public float denormalize(float nv)
	{
		float v=0;
		if (nv < 0) nv = 0;
		else if (nv > 1) nv = 1;
		switch (typeCode) {
		case PAN: {
			v = 2*nv -1;
			break;
		}
		case TUNE: {
			v = Controllable.TUNE_RANGE_SEMITONES*(2*nv -1);
			break;
		}
		default: {
			if (typeCode >= CControl.envIdBase(0) && typeCode < CControl.envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				int env = CControl.env4Id(typeCode);
				int param = CControl.envParam4Id(typeCode);
				if (param == 0) { v = nv*Envelope.NormalizedBeatPerSegment; } // attack
				else if (param == 1) { v = nv*Envelope.NormalizedBeatPerSegment;  } // decay
				else if (param == 2) { v = nv*Envelope.NormalizedBeatPerSegment;  } // sustain
				else if (param == 3) { v = nv; } // sustain level
				else if (param == 4) { v = nv*Envelope.NormalizedBeatPerSegment; } // release
				else { v = 2*nv -1;} // depth(param-5);
			} else if (typeCode >= CControl.lfoIdBase(0) && typeCode < CControl.lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK)) {
				int lfo = CControl.lfo4Id(typeCode);
				int param = CControl.lfoParam4Id(typeCode);
				if (param == 0) { // rate
					float minl = (float) Math.log(LFO.NormalizedMinRate);
					float maxl = (float) Math.log(LFO.NormalizedMaxRate);
					float logv = minl + (maxl-minl)*nv;
					v = (float) Math.exp(logv);
				} else if (param == 1) { v = nv; } // phase 
				else { v = 2*nv -1; } // depth(param-2);
			} else if (typeCode >= busIdBase(0) && typeCode < busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK)) {
				int bus = bus4Id(typeCode)+1;
				int param = busParam4Id(typeCode);
				if (param == 0) { // gain
					v = 2*nv; // bus send is over unity ... maybe do same for main gain or not TODO ro
				} else if (param == 1) { // pan
					v = 2*nv -1;
				} else if (param == 2) { // enable
					nv = v;
				} else { // !!!!
					
				}
				
			} else {
				v = nv;
				break;
			}
		}
		}
		return v;
	}

	/* (non-Javadoc)
	 * @see com.mayaswell.OrbitalLaser.ICControl#ccontrolId()
	 */
	@Override
	public int ccontrolId()
	{
		return typeCode;
	}
/* (non-Javadoc)
* @see com.mayaswell.OrbitalLaser.ICControl#ccontrolName()
*/
	@Override
	public String ccontrolName()
	{
		return code2string(typeCode);
	}

	public int instanceId()
	{
		return 0;
	}
	
	public static String code2string(int tc)
	{
		switch (tc) {
		case SLICE_START: return "sliceStart";
		case SLICE_POSITION: return "slicePosition";
		case STUTTER: return "stutter";
		case BUS_SELECT: return "busSelect";
		case GAIN: return "gain";
		case PAN: return "pan";
		case FLT_CUTOFF: return "fltCutoff";
		case FLT_ENVMOD: return "fltEnvmod";
		case FLT_RESONANCE: return "fltResonance";
		case TUNE: return "tune";
		default: {
			if (tc >= envIdBase(0) && tc < envIdBase(OrbitalLaser.Global.ENV_PER_WRECK)) {
				int env = env4Id(tc);
				int param = envParam4Id(tc);
				if (param == 0) return "env"+Integer.toString(env)+" attack";
				if (param == 1) return "env"+Integer.toString(env)+" decay";
				if (param == 2) return "env"+Integer.toString(env)+" sustain";
				if (param == 3) return "env"+Integer.toString(env)+" sustainLevel";
				if (param == 4) return "env"+Integer.toString(env)+" release";
				return "env"+Integer.toString(env)+" depth"+Integer.toString(param-5);
			} else if (tc >= lfoIdBase(0) && tc < lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK)) {
				int lfo = lfo4Id(tc);
				int param = lfoParam4Id(tc);
				if (param == 0) return "lfo"+Integer.toString(lfo)+" rate";
				if (param == 1) return "lfo"+Integer.toString(lfo)+" phase";
				return "lfo"+Integer.toString(lfo)+" depth"+Integer.toString(param-2);
			} else if (tc >= fxIdBase(0,0) && tc < fxIdBase(Global.MAX_BUS_PER_WRECK, 0)) {
				int fx = fx4fxId(tc);
				int bus = bus4fxId(tc);
				int param = param4fxId(tc); 
				if (param == 0) return "bus"+bus+" fx"+Integer.toString(fx)+" enable";
				return "bus"+bus+" fx"+Integer.toString(fx)+" param"+Integer.toString(param-1);
			} else if (tc >= busIdBase(0) && tc < busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK)) {
				int bus = bus4Id(tc);
				int param = busParam4Id(tc);
				if (param == 0) return "bus"+Integer.toString(bus)+" gain";
				if (param == 1) return "bus"+Integer.toString(bus)+" pan";
				if (param == 2) return "bus"+Integer.toString(bus)+" enable";
				return "bus"+Integer.toString(bus)+" param"+param;
			}
			break;
		}
		}
		return "nothing";
	}

	public static int string2code(String type)
	{
		if (type.equals("gain")) {
			return GAIN;
		} else if (type.equals("pan")) {
			return PAN;
		} else if (type.equals("sliceStart")) {
			return SLICE_START;
		} else if (type.equals("slicePosition")) {
			return SLICE_POSITION;
		} else if (type.equals("fltCutoff")) {
			return FLT_CUTOFF;
		} else if (type.equals("fltEnvmod")) {
			return FLT_ENVMOD;
		} else if (type.equals("fltResonance")) {
			return FLT_RESONANCE;
		} else if (type.equals("tune")) {
			return TUNE;
		} else if (type.equals("busSelect")) {
			return BUS_SELECT;
		} else if (type.equals("stutter")) {
			return STUTTER;
		} else {
			String [] scw = type.split(" ");
			if (scw.length >= 2) {
				if (scw[0].startsWith("lfo")) {
					int li = Integer.parseInt(scw[0].substring(3));
					if (scw[1].equals("rate")) {
						return lfoRateId(li);
					} else if (scw[1].equals("phase")) {							
						return lfoPhaseId(li);
					} else if (scw[1].startsWith("depth")) {
						int di = Integer.parseInt(scw[1].substring(5));
						return lfoTargetDepthId(li, di);
					} else {
					}
				} else if (scw[0].startsWith("env")) {
					int li = Integer.parseInt(scw[0].substring(3));
					if (scw[1].equals("attack")) {
						return envAttackTimeId(li);
					} else if (scw[1].equals("decay")) {
						return envDecayTimeId(li);
					} else if (scw[1].equals("sustain")) {
						return envSustainTimeId(li);
					} else if (scw[1].equals("sustainLevel")) {
						return envSustainLevelId(li);
					} else if (scw[1].equals("release")) {						
						return envReleaseTimeId(li);
					} else if (scw[1].startsWith("depth")) {
						int di = Integer.parseInt(scw[1].substring(5));
						return envTargetDepthId(li, di);
					} else {
					}
				} else if (scw[0].startsWith("bus")) {
					int li = Integer.parseInt(scw[0].substring(3));
					if (scw[1].equals("gain")) {
						return busGainId(li);
					} else if (scw[1].equals("pan")) {
						return busPanId(li);
					} else if (scw[1].equals("enable")) {
						return busEnableId(li);
					} else if (scw[1].startsWith("fx")) {
						int fi = Integer.parseInt(scw[1].substring(2));
						if (scw.length > 2) {
							if (scw[2].startsWith("param")) {
								int di = Integer.parseInt(scw[2].substring(5));
								return fxParamId(li, fi, di);
							} else if (scw[2].startsWith("enable")) {
								return fxEnableId(li, fi);
							} else {
								
							}
						}
					} else {
						
					}
				}
			}
		}
		return NOTHING;
	}
	public boolean isEnvControl() {
		return typeCode >= envIdBase(0) && typeCode < envIdBase(OrbitalLaser.Global.ENV_PER_WRECK);
	}
	public int env4Id() {
		return env4Id(typeCode);
	}
	public int envParam4Id() {
		return envParam4Id(typeCode);
	}
	public boolean isLFOControl() {
		return typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(OrbitalLaser.Global.LFO_PER_WRECK);
	}
	public int lfo4Id() {
		return lfo4Id(typeCode);
	}
	public int lfoParam4Id() {
		return lfoParam4Id(typeCode);
	}
	public int param4fxId() {
		return param4fxId(typeCode);
	}
	public int bus4Id() {
		return bus4Id(typeCode);
	}
	public int bus4fxId() {
		return bus4fxId(typeCode);
	}
	public int fx4fxId() {
		return fx4fxId(typeCode);
	}
	public boolean isFXControl() {
		return typeCode >= fxIdBase(0,0) && typeCode < fxIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK, 0);
	}
	public boolean isBusControl() {
		return typeCode >= busIdBase(0) && typeCode < busIdBase(OrbitalLaser.Global.MAX_BUS_PER_WRECK);
	}
	@Override
	public float defaultValue() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public int valueType() {
		return TYPE_FLOAT;
	}
}
