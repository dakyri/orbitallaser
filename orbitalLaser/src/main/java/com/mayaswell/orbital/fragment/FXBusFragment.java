package com.mayaswell.orbital.fragment;

import java.util.ArrayList;

import com.mayaswell.orbital.CControl;
import com.mayaswell.orbital.FXBusState;
import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.OrbitalLaser.Global;
import com.mayaswell.orbital.widget.FXBusMaster;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXState;
import com.mayaswell.fragment.SubPanelFragment;
import com.mayaswell.widget.FXControl;
import com.mayaswell.widget.FXControl.FXControlListener;
import com.mayaswell.widget.BaseControl;
import com.mayaswell.widget.DisplayMode;
import com.mayaswell.widget.LFOControl;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

public class FXBusFragment extends SubPanelFragment {

	private ArrayList<FXControl> fxControl = new ArrayList<FXControl>();
	private RelativeLayout subPanelHolder = null;
	private FXBusMaster fxBusMaster = null;
	protected OrbitalLaser ol = null;
	protected Wrecker wreckage = null;
		
	public FXBusFragment() {
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		ol = (OrbitalLaser) activity;
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	
	private FXBusMaster.Listener fxMasterListener = null;
	
	public void setFXMasterListener(FXBusMaster.Listener l) {
		fxMasterListener = l;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.fx_bus_fragment, container, true);
		
		subPanelHolder = (RelativeLayout) v.findViewById(R.id.subPanelHolder);
		fxBusMaster  = (FXBusMaster) v.findViewById(R.id.fxBusMaster);
		
		fxBusMaster.setListener(new FXBusMaster.Listener() {

			@Override
			public void onGainChanged(FXBusState b, float amt) {
				if (fxMasterListener != null) {
					fxMasterListener.onGainChanged(b, amt);
				}
				if (wreckage != null) {
					wreckage.setFXBusGain(b, amt);
				}
			}

			@Override
			public void onPanChanged(FXBusState b, float amt) {
				if (fxMasterListener != null) {
					fxMasterListener.onPanChanged(b, amt);
				}
				if (wreckage != null) {
					wreckage.setFXBusPan(b, amt);
				}
			}

			@Override
			public void onEnableChanged(FXBusState b, boolean e) {
				if (fxMasterListener != null) {
					fxMasterListener.onEnableChanged(b, e);
				}
				if (wreckage != null) {
					wreckage.setFXBusEnable(b, e);
				}
			}

			@Override
			public void onNameChanged(FXBusState b, String s) {
				if (fxMasterListener != null) {
					fxMasterListener.onNameChanged(b, s);
				}
			}
			
		});
		

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		FXControl fc = addFXControl(fxControl.size(), ol.getFXAdapter(null), fxControl);
		layoutEditorControls();
	}
	
	@Override
	public void layoutEditorControls() {
		layoutEditorControls(subPanelHolder, 1, fxBusMaster.getId());
	}
	
	private void setFXListener(FXControl p, final int xyci)
	{
		p.setListener(new FXControlListener() {
			@Override
			public void onEnableChanged(FXControl fpc, int fxId, boolean isChecked)
			{
				if (wreckage != null) {
					FXBusState fxBus = wreckage.getDisplayedBusState();
					wreckage.setFXenable(fxBus, fxId, isChecked);
				}
			}

			@Override
			public void onParamChanged(FXControl fpc, int fxId, int paramId, float amt)
			{
				if (wreckage != null) {
					FXBusState fxBus = wreckage.getDisplayedBusState();
					wreckage.setFXparam(fxBus, fxId, paramId, amt);
					ol.ccontrolUpdate(wreckage, CControl.fxParamId(fxBus.id, fxId, paramId), amt, Controllable.Controller.INTERFACE);
				}
			}

			@Override
			public void onFXChanged(FXControl fpc, int fxId, FX fx)
			{
				if (wreckage != null) {
					FXBusState fxBus = wreckage.getDisplayedBusState();
					wreckage.setFX(fxBus, fxId,  fx);
					fpc.setDisplay(DisplayMode.SHOW_PARAM_CTLS);
					boolean hasFree=false;
					if (fxControl.size() == 0) {
						hasFree = false;
					} else {
						FX f2 = fxControl.get(fxControl.size()-1).selectedFX();
						Log.d("onFXChanged", String.format("last has fx %s", f2.toString()));
						if (f2.getId() == FX.UNITY) {
							hasFree = true;
						}
					}
					Log.d("onFXChanged", String.format("fx is %s has free slot %b", fx.toString(), hasFree));

					if (!hasFree && fxControl.size() <Global.MAX_BUS_FX) {
						FXControl fc = addFXControl(fxControl.size(), ol.getFXAdapter(null), fxControl);
						layoutEditorControls();
					}
				}
			}

		});
	}
	
	public FXControl addFXControl(int fxInd, ArrayAdapter<FX> erma, ArrayList<FXControl> list)
	{
		int mid = 1;
		BaseControl sxy0 = null;
//		RelativeLayout subPanelHolder = (RelativeLayout) getView();
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		
		if (subPanelHolder.getChildCount() <= 0) {
			relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
		} else {
			sxy0 = (BaseControl) subPanelHolder.getChildAt((subPanelHolder.getChildCount()-1));
			mid = sxy0.getId()+1;
			relativeParams.addRule(RelativeLayout.BELOW, sxy0.getId());
		}
		FXControl snsx = new FXControl(getActivity());
		snsx.setName("FX"+Integer.toString(fxInd+1));
		snsx.setAdapter(erma);
		snsx.setGravity(Gravity.CENTER_VERTICAL);
		snsx.setBackgroundResource(R.drawable.label_text_bg);
		snsx.setFxId(fxInd);
		int pxpd = getResources().getDimensionPixelSize(R.dimen.subElementPadding);
		snsx.setPadding(pxpd, pxpd, pxpd, pxpd);
		snsx.setId(mid);
		snsx.setDisplay(DisplayMode.SHOW_PARAM_CTLS);
		
		list.add(snsx);
		subPanelHolder.addView(snsx, relativeParams);
		
		setFXListener(snsx, fxInd);
		Log.d("fxbusfrag", "addedd conrol");
		return snsx;
	}
	
	public void setFXEditor(ArrayList<FXState> fx) 
	{
		if (fx == null) {
			return;
		}
		Log.d("setFXEditor", String.format("%d fx", fx.size()));
		int pxi = 0;
		for (FXState xsp: fx) {
			if (pxi >= Global.MAX_BUS_FX) {
				break;
			}
			Log.d("setFXEditor", String.format("fx ind  %d, n controls %d fxcontrol %s", pxi, fxControl.size(), xsp.getFx() != null?xsp.getFx().toString():""));
			if (pxi >= fxControl.size()) {
				Log.d("setFXEditor", String.format("add fx control here"));
				addFXControl(fxControl.size(), ol.getFXAdapter(null), fxControl);
				layoutEditorControls();
			}
			FXControl lc = fxControl.get(pxi);
			lc.setFXState(xsp);
			pxi++;
		}
		for (; pxi<fxControl.size(); pxi++) {
			FXControl lc = fxControl.get(pxi);
			lc.setFXState(null);
		}
	}
	
	private void delFXControl(FXControl p)
	{
		int pxi = fxControl.indexOf(p);
		fxControl.remove(pxi);
		subPanelHolder.removeView(p);
	}

	private FXControl getFXControl(int env) {
		return fxControl.get(env);
	}

	public void setToWrecker(Wrecker sps) {
		wreckage = sps;
		if (sps == null || getView() == null) return;
		WreckageState s = sps.getState();
		setFXBusState(wreckage.getDisplayedBus(), wreckage.getDisplayedBusState());
	}

	public void setFXParam(int fx, int i, float v) {
		FXControl l = getFXControl(fx);
		if (l != null) l.setFXParamValue(i, v);
	}

	public void setFXBusState(int i, FXBusState fxState) {
		if (fxBusMaster != null) {
			fxBusMaster.setBusState(i, fxState);	
		}
		if (fxState != null) {
			setFXEditor(fxState.fx);
		} else {
			setFXEditor(null);
		}
	}

	public void setBusName(int bus, String v) {
		if (fxBusMaster != null && fxBusMaster.showingBus(bus)) {
			fxBusMaster.setBusName(v);
		}
	}
	public void setBusGain(int bus, float v) {
		if (fxBusMaster != null && fxBusMaster.showingBus(bus)) {
			fxBusMaster.setBusGain(v);
		}
	}
	public void setBusPan(int bus, float v) {
		if (fxBusMaster != null && fxBusMaster.showingBus(bus)) {
			fxBusMaster.setBusPan(v);
		}
	}
	

}
