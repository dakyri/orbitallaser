package com.mayaswell.orbital.fragment;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mayaswell.util.Automaton;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.widget.InfinIntControl;
import com.mayaswell.widget.MenuControl;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.InfinIntControl.InfiniteLongListener;
import com.mayaswell.widget.MenuControl.MenuControlListener;
import com.mayaswell.widget.TeaPot.TeaPotListener;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.orbital.AlgoRhythm;
import com.mayaswell.orbital.Bank;
import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.SliceCell;
import com.mayaswell.orbital.SliceGroups;
import com.mayaswell.orbital.SliceGroups.SliceGroup;
import com.mayaswell.orbital.Wrecker.PlayMode;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.orbital.fragment.AlgorithmEditorFragment.AlgMode;
import com.mayaswell.orbital.widget.AlgoRhythmView;
import com.mayaswell.orbital.widget.GroupView;
import com.mayaswell.orbital.widget.PatternView;
import com.mayaswell.orbital.widget.TrashCanView;

@SuppressLint("DefaultLocale")
public class AlgorithmEditorFragment extends OLFragment {
	public interface Listener {
		void algSelected(AlgoRhythm p);
		void modeSelected(PlayMode p);
		void algChanged(AlgoRhythm p);
		void patternChanged(ArrayList<SliceCell> p);
		void editorClosed();
		void algDeleted(AlgoRhythm s);
		void algCreated(AlgoRhythm s);
		void algNameChanged(AlgoRhythm s);
		void groupChanged(Slice slice, int og, int id);
		void onError(String msg);
	}

	private Listener listener = null;
	private AlgoRhythmView algorithmEditor = null;
	private MenuControl algSelector = null;
	private ArrayAdapter<AlgoRhythm> algSelectAdapter;
	private Bank currentBank = null;
	private AlgoRhythm algorithm = null;
	private EditText algNameEditView = null;
	private Button closeButton = null;
	private GroupView groupEditor = null;
	private PatternView patternEditor = null;
	private MenuControl cellGroupSelect = null;
	private MenuControl cellSliceSelect = null;
	private InfinIntControl cnxWeightControl = null;
	private TeaPot cellLengthControl = null;
	private TeaPot cellGainControl = null;
	private RelativeLayout detailView = null;
	private TextView detailLabel = null;
	private Automaton<SliceCell>.NodeConnection selectCnx = null;
	private SliceCell selectCell = null;
	private Automaton<SliceCell>.Node selectNode = null;
	private TrashCanView trashCan = null;
	private boolean isAlgorithm = false;
	private TextView patternLabel = null;
	private WreckageState wreckageState = null;
	
	public enum AlgMode {
		PATTERN, EDIT, PLAY_LOOSE, PLAY_FREE, PLAY_WILD;
		@Override
		public String toString()
		{
			switch (this) {
			case PATTERN: return "pattern";
			case EDIT: return "edit";
			case PLAY_LOOSE: return "loose";
			case PLAY_FREE: return "free";
			case PLAY_WILD: return "wild";
			}
			return super.toString();
		}
		
		public static AlgMode parse(String s)
		{
			if (s.equals("pattern")) return PATTERN;
			if (s.equals("edit")) return EDIT;
			if (s.equals("loose")) return PLAY_LOOSE;
			if (s.equals("free")) return PLAY_FREE;
			if (s.equals("wild")) return PLAY_WILD;
			return EDIT;
		}
	}
	
	private AlgMode mode = AlgMode.EDIT;
	private ArrayAdapter<AlgMode> modeAdapter = null;
	private Spinner modeSelector = null ;
	private Wrecker wrecker = null;

	public AlgorithmEditorFragment() {
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.algorithm_editor_fragment, container, true);
		algSelector = (MenuControl) v.findViewById(R.id.algSelector);
		algNameEditView = (EditText) v.findViewById(R.id.algNameEditView);
		closeButton  = (Button) v.findViewById(R.id.cancelButton);
		groupEditor  = (GroupView) v.findViewById(R.id.groupsView);
		patternEditor  = (PatternView) v.findViewById(R.id.patternView);
		algorithmEditor = (AlgoRhythmView) v.findViewById(R.id.algView);
		trashCan = (TrashCanView) v.findViewById(R.id.trashCan);
		patternLabel = (TextView) v.findViewById(R.id.patternLabel);		
		modeSelector = (Spinner) v.findViewById(R.id.modeSelector);

		detailView  = (RelativeLayout) v.findViewById(R.id.detailView);
		if (detailView != null) {
			detailLabel  = (TextView) detailView.findViewById(R.id.mwCtlLabel);
			cellSliceSelect = (MenuControl) detailView.findViewById(R.id.cellSliceSelect);
			cellGroupSelect = (MenuControl) detailView.findViewById(R.id.cellGroupSelect);
			cnxWeightControl = (InfinIntControl) detailView.findViewById(R.id.cnxWeightControl);
			cellLengthControl = (TeaPot) detailView.findViewById(R.id.cellLengthControl);
			cellGainControl = (TeaPot) detailView.findViewById(R.id.cellGainControl);
		}
		
		algSelectAdapter = new ArrayAdapter<AlgoRhythm>(getActivity(), R.layout.mw_spinner_item);
		algSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		if (algSelector != null) {
			algSelector.setAdapter(algSelectAdapter);
			algSelector.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					return true;
				}
				
			});
			
			algSelector.setValueListener(new MenuControlListener() {	
				@Override
				public void onValueChanged(int position) {
					AlgoRhythm b = algSelectAdapter.getItem(position);
					if (listener != null) {
						listener.algSelected(b);
					}
					displayAlgoRhythm(b);
				}
			});
		}
		
		if (algNameEditView != null) {
			algNameEditView.addTextChangedListener(new TextWatcher() {
				@Override	public void afterTextChanged(Editable s) { }
				@Override	public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
				@Override	public void onTextChanged(CharSequence s, int start, int before, int count) { }
			});
			algNameEditView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) {				
						onAlgNameChanged();
						return true;
					}
					return false;
				}			
			});
			algNameEditView.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						onAlgNameChanged();
					}
				}

			});
		}
		
		if (closeButton != null) {
			closeButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					if (listener != null) {
						listener.editorClosed();
					}
				}
				
			});
		}
		
		if (patternEditor != null) {
			patternEditor.setListener(new PatternView.Listener() {			
				@Override
				public void cellSelected(int g, SliceCell patternSliceCell) {
					showPatternCell(g, patternSliceCell);
				}

				@Override
				public void cellAdded(SliceCell sc) {
					setPatternLabel();
				}

				@Override
				public void cellDeleted(SliceCell sc) {
					setPatternLabel();
				}

				@Override
				public void patternSet(ArrayList<SliceCell> sca) {
					setPatternLabel();
				}
			});
		}
		
		if (algorithmEditor != null) {
			algorithmEditor.setRegginbow(OrbitalLaser.regginbow);
			algorithmEditor.setListener(new AlgoRhythmView.Listener() {
				
				@Override
				public AlgoRhythm getCurrentAlgorithm() {
					if (algorithm != null) {
						return algorithm;
					}
					newAlgorithm();
					return algorithm;
				}

				@Override
				public void nodeSelected(int nid) {
					showNode(nid);
				}

				@Override
				public void nodePlayed(int nid) {
					showNode(nid);
					if (algorithmEditor == null) {
						return;
					}
					AlgoRhythm a = algorithmEditor.getAlgoRythm();
					if (a != null) {
						int inid = a.getCurrent().getId();
						switch (mode) {
						case PLAY_LOOSE:
							a.setNextNode4Id(nid);
							break;
						case PLAY_FREE:
							a.setCurrentNode4Id(nid);
							a.resetCurrentNodeItem();
							algorithmEditor.currenNodeChanged(a, inid, nid);
							break;
						case PLAY_WILD:
							a.setCurrentNode4Id(nid);
							a.resetCurrentNodeItem();
							if (wrecker != null) {
								wrecker.resetCurrentSlice();
							}
							algorithmEditor.currenNodeChanged(a, inid, nid);
							break;
						case EDIT: break;
						default: break;
						}
					}
				}

				@Override
				public void cellSelected(int nid, int cdid) {
					showCell(nid, cdid);
				}

				@Override
				public void cnxSelected(int nid, int cnxid) {
					showCnx(nid, cnxid);
				}
			});
		}
		
		if (cellGroupSelect != null) {
			cellGroupSelect.setAdapter(Wrecker.nnGroupAdapter);
			cellGroupSelect.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					if (selectCell != null) {
						MenuData<SliceGroup> d = Wrecker.nnGroupAdapter.getItem(position);
						if (d.getData() >= 0) {
							selectCell.setGroup(d.getData());
							if (isAlgorithm) {
								if (algorithmEditor != null) algorithmEditor.invalidateCell(selectCell);
								if (listener != null) {
									listener.algChanged(algorithm);
								}
							} else {
								if (patternEditor != null) patternEditor.invalidate();
							}
						}
					}
				}
			});
		}
			
		if (cellSliceSelect != null) {
			cellSliceSelect.setAdapter(Wrecker.sliceAdapter);
			cellSliceSelect.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					if (selectCell != null) {
						MenuData<Slice> d = Wrecker.sliceAdapter.getItem(position);
						if (d.getData() >= 0) {
							selectCell.setGroup(d.getData());
							if (isAlgorithm) {
								if (algorithmEditor != null) algorithmEditor.invalidateCell(selectCell);
								if (listener != null) {
									listener.algChanged(algorithm);
								}
							} else {
								if (patternEditor != null) patternEditor.invalidate();
							}
						}
					}
				}
			});
		}
		if (cellLengthControl != null) {
			cellLengthControl.setPotListener(new TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					if (selectCell != null) {
						selectCell.setDuration(v);
						if (isAlgorithm) {
							if (algorithmEditor != null) algorithmEditor.invalidateCell(selectCell);
							if (listener != null) {
								listener.algChanged(algorithm);
							}
						} else {
							if (patternEditor != null) {
								patternEditor.invalidateCell();
								setPatternLabel();
							}
						}
					}
				}
			});
		}
		
		if (cellGainControl != null) {
			cellGainControl.setPotListener(new TeaPotListener() {
				@Override
				public void onValueChanged(float v) {
					if (selectCell != null) {
						selectCell.setGain(v);
						if (isAlgorithm) {
							if (algorithmEditor != null) algorithmEditor.invalidateCell(selectCell);
							if (listener != null) {
								listener.algChanged(algorithm);
							}
						} else {
							if (patternEditor != null) {
								patternEditor.invalidateCell();
							}
						}
					}
				}
			});
		}
		
		if (cnxWeightControl != null) {
			cnxWeightControl.setPotListener(new InfiniteLongListener() {
				@Override
				public void onValueChanged(long v) {
					if (selectCnx != null) {
						selectCnx.setWeight((int) v);
						if (algorithmEditor != null) algorithmEditor.invalidateCnx(selectCnx);
						if (listener != null) listener.algChanged(algorithm);
					}
				}
			});
		}
		
		groupEditor.setListener(new GroupView.Listener() {
			
			@Override
			public void groupChanged(Slice slice, int og, int id) {
				if (listener != null) {
					listener.groupChanged(slice, og, id);
				}
			}
		});
		
		if (modeSelector != null) {
			modeSelector.setAdapter(modeAdapter);
			modeSelector.setOnItemSelectedListener(new OnItemSelectedListener() 
			{	
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) 
				{
					AlgMode b = modeAdapter.getItem(position);
					setMode(b);
				}
	
				@Override
				public void onNothingSelected(AdapterView<?> parentView) 
				{
				}
			});
		}
		
		hideDetailView();
		return v;
	}
	
	protected void setPatternLabel() {
		if (patternLabel != null && patternEditor != null) {
			float l = patternEditor.getDuration();
			String ls = String.format("%.3f", l);
			patternLabel.setText(Html.fromHtml("Pattern<br>"+"<small>"+ls+"</small>"));
		}
	}

	protected void showNode(int nid) {
		if (detailView == null) return;
		AlgoRhythm.Node n;
		if (algorithm == null || (n=algorithm.getNode(nid)) == null) {
			hideDetailView();
			return;
		}
		if (detailLabel != null) {
			detailLabel.setText("node "+nid);
		}
		if (cnxWeightControl != null) cnxWeightControl.setVisibility(View.GONE);
		if (cellSliceSelect != null) cellSliceSelect.setVisibility(View.GONE);
		if (cellGroupSelect != null) cellGroupSelect.setVisibility(View.GONE);
		if (cellLengthControl != null) cellLengthControl.setVisibility(View.GONE);
		if (cellGainControl != null) cellGainControl.setVisibility(View.GONE);
		detailView.setVisibility(View.VISIBLE);
	}

	protected void showCnx(int nid, int cnxid) {
		if (detailView == null) return;
		AlgoRhythm.Node n;
		AlgoRhythm.NodeConnection nc;
		if (algorithm == null || (n=algorithm.getNode(nid)) == null || (nc=n.getCnx(cnxid)) == null) {
			hideDetailView();
			return;
		}
		selectCnx = nc;
		selectCell = null;
		selectNode = n;
		if (detailLabel != null) {
			detailLabel.setText("cnx "+nid+"/"+cnxid);
		}
		if (cnxWeightControl != null) {
			cnxWeightControl.setValue(nc.getWeight());
			cnxWeightControl.setVisibility(View.VISIBLE);
		}
		if (cellSliceSelect != null) cellSliceSelect.setVisibility(View.GONE);
		if (cellGroupSelect != null) cellGroupSelect.setVisibility(View.GONE);
		if (cellLengthControl != null) cellLengthControl.setVisibility(View.GONE);
		if (cellGainControl != null) cellGainControl.setVisibility(View.GONE);
		detailView.setVisibility(View.VISIBLE);
	}

	protected void showCell(int nid, int cdid) {
		if (detailView == null) return;
		AlgoRhythm.Node n;
		SliceCell sc;
		if (algorithm == null || (n=algorithm.getNode(nid)) == null || (sc=n.getCellData(cdid)) == null) {
			hideDetailView();
			return;
		}
		selectCnx = null;
		selectNode = n;
		showCellDetails("cell "+nid+"/"+cdid, sc, true);
	}

	protected void showPatternCell(int cdid, SliceCell sc) {
		if (detailView == null) return;
		selectCnx = null;
		selectNode = null;
		showCellDetails("pat/"+cdid, sc, false);
	}

	private void showCellDetails(String label, SliceCell sc, boolean isAlg) {
		selectCell = sc;
		if (selectCell == null) {
			if (cellGroupSelect != null) {
				cellGroupSelect.setVisibility(View.GONE);
			}
			if (cnxWeightControl != null) {
				cnxWeightControl.setVisibility(View.GONE);
			}
			if (cellLengthControl != null) {
				cellLengthControl.setVisibility(View.GONE);
			}
			if (cellGainControl != null) {
				cellGainControl.setVisibility(View.GONE);
			}
			return;
		}
		isAlgorithm  = isAlg;
		if (detailLabel != null) {
			detailLabel.setText(label);
		}
		if (cnxWeightControl != null) cnxWeightControl.setVisibility(View.GONE);
		if (isAlg) {
			if (cellSliceSelect != null) cellSliceSelect.setVisibility(View.GONE);
			if (cellGroupSelect != null) {
				cellGroupSelect.setSelection(sc.getId());
				cellGroupSelect.setVisibility(View.VISIBLE);
			}
		} else {
			if (cellSliceSelect != null) cellSliceSelect.setVisibility(View.VISIBLE);
			if (cellGroupSelect != null) {
				cellGroupSelect.setVisibility(View.GONE);
			}
		}
		if (cellLengthControl != null) {
			cellLengthControl.setValue(sc.getDuration());
			cellLengthControl.setVisibility(View.VISIBLE);
		}
		if (cellGainControl != null) {
			cellGainControl.setValue(sc.getGain());
			cellGainControl.setVisibility(View.VISIBLE);
		}
		detailView.setVisibility(View.VISIBLE);
	}

	private void hideDetailView() {
		selectCnx = null;
		selectCell = null;
		selectNode = null;
		if (detailView != null) {
			detailView.setVisibility(View.GONE);
		}
	}

	private void onAlgNameChanged() {
		algNameEditView.setVisibility(View.GONE);
		if (algNameEditView.length() > 0 && algorithm != null) {
			
		}
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(algNameEditView.getWindowToken(), 0);//InputMethodManager.HIDE_NOT_ALWAYS
		try {
			if (algNameEditView.length() > 0) {
				String pnm=new String(algNameEditView.getText().toString());
				algorithm.setName(pnm);
			}
		} catch (IndexOutOfBoundsException e) {
 		
 		}
		if (algorithm != null) {
			
		}
	}
	
	private void displayAlgoRhythm(AlgoRhythm b) {
		algorithmEditor.setTo(b);
	}

	public void setListener(Listener l) {
		listener  = l;
	}

	public void setToBank(Bank b) {
		currentBank  = b;
		if (currentBank != null) {
			currentBank.setAlgorithmAdapterItems(algSelectAdapter);
			if (algSelectAdapter.getCount() > 0) {
				algorithm = algSelectAdapter.getItem(0);
				algSelector.setSelection(0);
			} else {
				algorithm = null;
			}
			algorithmEditor.setTo(algorithm);
		}
	}
	
	public void setToWrecker(Wrecker w) {
		wrecker  = w;
		wreckageState  = w.getState();
		groupEditor.setTo(w);
		patternEditor.setTo(w.getState());
		algorithmEditor.setTo(w);
		trashCan .setTo(w);
	}

	public int delAlgorithm() {
		if (currentBank != null && algorithm != null) {
			if (currentBank.numAlgorithms() > 1) {
				int csi = currentBank.find(algorithm);
				if (csi < 0) {
					return -1;
				}
				csi = currentBank.deleteAlgorithm(csi);
				if (csi >= 0) {
					setCurrentAlgorithm(currentBank.getAlgorithm(csi));
					currentBank.setAlgorithmAdapterItems(algSelectAdapter);
					if (listener != null) {
						listener.algDeleted(algorithm);
					}
				}
				return csi;
			}
		}
		return -1;
	}

	public void branchAlgorithm() {
		if (algorithm  != null) {
			AlgoRhythm p = algorithm.clone();
			p.setName(currentBank.makeIncrementalAlgName(algorithm.getName()));
			setCurrentAlgorithm(p);
			currentBank.add(p);
			currentBank.setAlgorithmAdapterItems(algSelectAdapter);
			if (listener != null) {
				listener.algCreated(p);
			}
		}
	}
	
	public void newAlgorithm() {
		if (currentBank != null) {
			AlgoRhythm s = new AlgoRhythm(currentBank.makeIncrementalAlgName("default"));
			currentBank.add(s);
			setCurrentAlgorithm(s);
			currentBank.setAlgorithmAdapterItems(algSelectAdapter);
			if (listener != null) {
				listener.algCreated(s);
			}
		}
	}

	public void renAlgorithm() {
		if (algorithm != null) {
			algNameEditView.setText(algorithm.getName());
			algNameEditView.setVisibility(View.VISIBLE);
			algNameEditView.requestFocus();
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(algNameEditView, 0);//InputMethodManager.SHOW_FORCED
		}
	}
	
	private void setCurrentAlgorithm(AlgoRhythm s) {
		algorithm = s;
	}

	public void refreshGroupView() {
		if (groupEditor != null) {
			groupEditor.layoutSlices();
			groupEditor.invalidate();
		}
	}

	public boolean exportPattern2Algorithm() {
		if (algorithm == null) {
			newAlgorithm();
		}
		ArrayList<SliceCell> a = wreckageState.getPattern();
		ArrayList<SliceCell> b = new ArrayList<SliceCell>();
		if (algorithm != null && a != null) {
			for (SliceCell sc: a) {
				Slice s = wreckageState.getSlice(sc.getId());
				if (s == null) {
					if (listener != null) {
						listener.onError("slice not found in this pattern, index "+sc.getId());
					}
					return false;
				}
				int gind = s.getGroup();
				if (gind < 0) {
					if (listener != null) {
						listener.onError("not all pattern items are grouped. please assign them before trying to export this pattern again");
					}
					return false;
				}
				b.add(new SliceCell(gind, sc.getDuration(), sc.getGain()));
			}
			AlgoRhythm.Node n = algorithm.addNode();
			n.setCellData(b);
			if (algorithmEditor != null) {
				algorithmEditor.invalidateNode(n);
			}
		}
		return true;
		
	}

	public void refreshPatternEditor() {
		if (patternEditor != null) {
			patternEditor.layoutSlices();
			patternEditor.invalidate();
		}
	}

	public void nodeChanged(AlgoRhythm a, int lastNodeId, int currentNodeId) {
		if (algorithmEditor != null) {
			algorithmEditor.currenNodeChanged(a, lastNodeId, currentNodeId);
		}
	}

	public void cellChanged(AlgoRhythm a, int currentNodeId, int lastCellItemInd, int currentCellItemInd) {
		Log.d("algorithm", "AlgoRhythm "+(a!=null?a.name:"noname")+" node "+currentNodeId+" new cell "+currentCellItemInd);
	}

	public AlgMode getMode() {
		return mode;
	}

	public void setMode(AlgMode mode) {
		if (mode == AlgMode.PATTERN) {
			if (listener != null) {
				listener.modeSelected(PlayMode.PATTERNATOR);
			}
		} else {
			if (listener != null) {
				listener.modeSelected(PlayMode.AUTOMATOR);
			}
			if (mode == AlgMode.EDIT) {
				algorithmEditor.setLocked(false);
			} else {
				algorithmEditor.setLocked(true);
			}
		}
		this.mode = mode;
	}
	
	public void setupControlsAdapter(Context c, int tvrid, int ddrid) {
		modeAdapter  =  new ArrayAdapter<AlgMode>(c, tvrid);
		modeAdapter.setDropDownViewResource(ddrid);
		modeAdapter.add(AlgMode.EDIT);
		modeAdapter.add(AlgMode.PATTERN);
		modeAdapter.add(AlgMode.PLAY_LOOSE);
		modeAdapter.add(AlgMode.PLAY_WILD);
		modeAdapter.add(AlgMode.PLAY_FREE);
	}
}
