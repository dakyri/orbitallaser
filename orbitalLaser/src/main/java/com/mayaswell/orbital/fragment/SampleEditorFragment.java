package com.mayaswell.orbital.fragment;

import com.mayaswell.orbital.R;

import com.mayaswell.util.EventID;
import com.mayaswell.widget.InfinIntControl;
import com.mayaswell.widget.MenuControl;
import com.mayaswell.widget.MenuData;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.widget.SampleFrameControl;
import com.mayaswell.orbital.widget.OLSampleView;
import com.mayaswell.widget.InfinIntControl.InfiniteLongListener;
import com.mayaswell.widget.MenuControl.MenuControlListener;
import com.mayaswell.widget.SampleView.SampleViewListener;
import com.mayaswell.widget.FileChooserView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;

public class SampleEditorFragment extends OLFragment {
	public interface Listener {
		void onLock(boolean b);
	}

	
	public enum AnalysisMode {
		LOCK, GRID, MANUAL;
		@Override
		public String toString()
		{
			switch (this) {
			case LOCK: return "lock";
			case MANUAL: return "man";
			case GRID: return "grid";
			}
			return super.toString();
		}
		
		public static AnalysisMode parse(String s)
		{
			if (s.equals("lock")) return LOCK;
			if (s.equals("grid")) return GRID;
			if (s.equals("man")) return MANUAL;
			return GRID;
		}
	}
	protected OLSampleView sampleView = null;
	protected SampleFrameControl loopStartControl = null;
	protected SampleFrameControl loopLengthControl = null;
	protected FileChooserView fileChooser = null;
	protected TextView fileInfoView = null;
	protected InfinIntControl nbeatControl = null;
	protected InfinIntControl meterControl = null;
	protected Button analyseButton = null;
	protected MenuControl analysisModeSelect = null;
	protected InfinIntControl sliceGridControl = null;
	private AnalysisMode analysisMode = AnalysisMode.GRID;
	private Wrecker wrecker = null;
	private Listener listener = null;
	private float minBeatIncrement = 0.125f;
	private ToggleButton tempoLockToggle = null;
	
	public static ArrayAdapter<AnalysisMode> analysisModeAdapter = null;

	public SampleEditorFragment() {

	}
	
	@Override
	public void onAttach(Activity activity)
	{
		analysisModeAdapter = new ArrayAdapter<AnalysisMode>(activity, R.layout.mw_spinner_item);
		analysisModeAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		analysisModeAdapter.add(AnalysisMode.GRID);
		analysisModeAdapter.add(AnalysisMode.MANUAL);
		analysisModeAdapter.add(AnalysisMode.LOCK);
		
		super.onAttach(activity);
		layoutChildren();
	}
	
	private void layoutChildren() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		setToWrecker(ol.getWrecker());
		Log.d("wrecker", "SampleEditor::onViewCreated done");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.sample_editor_fragment, container, true);
		sampleView  =  (OLSampleView) v.findViewById(R.id.sampleView);
		loopStartControl =  (SampleFrameControl) v.findViewById(R.id.loopStartControl);
		loopLengthControl =  (SampleFrameControl) v.findViewById(R.id.loopLengthControl);
		fileChooser = (FileChooserView) v.findViewById(R.id.sampleSelector);
		fileChooser.setListener(new FileChooserView.Listener() {

			@Override
			public void chooseFile(FileChooserView self) {
				ol.launchSampleFinder(".*\\.wav", EventID.CHOOSE_FINDER);
			}

			@Override
			public void unchooseFile(FileChooserView self) {
				if (ol.getWrecker() != null) {
					ol.getWrecker().setSamplePath(null, true);
					setToWrecker(ol.getWrecker());
//					stopPad(selectedPadButton);
				}
			}
		});
		tempoLockToggle  = (ToggleButton) v.findViewById(R.id.tempoLockToggle);
		
		fileInfoView = (TextView) v.findViewById(R.id.fileInfoView);
		nbeatControl = (InfinIntControl) v.findViewById(R.id.nbeatControl);
		meterControl = (InfinIntControl) v.findViewById(R.id.meterControl);
		analyseButton = (Button) v.findViewById(R.id.analyseButton);
		analysisModeSelect = (MenuControl) v.findViewById(R.id.analysisModeSelect);
		
		analysisModeSelect.setAdapter(analysisModeAdapter);
		analysisModeSelect.setSelection(0);
		Log.d("wtf", analysisModeSelect.toString()+", "+analysisModeAdapter.getItem(analysisModeSelect.getSelectedPosition()));
		
		sliceGridControl = (InfinIntControl) v.findViewById(R.id.sliceGridControl);
		
		registerForContextMenu(v);
//		registerForContextMenu(loopStartControl);
//		registerForContextMenu(loopLengthControl);
		
		sliceGridControl.setValue(2);
		sliceGridControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				sampleView.setGridMetric(getNBeats(), getMeter(), (int) v);
				sampleView.invalidate();
			}
		});

		sampleView.setListener(new SampleViewListener() {

			@Override
			public void onRegionStartChanged(long v)
			{
			}

			@Override
			public void onRegionLengthChanged(long v)
			{
				sampleView.setGridParameters();
				wrecker.setAnalysisInfo();
				setAnalysisInfo();
			}

			@Override
			public void onMarkerChanged(int type, Object m, int ind, long fr, float v) {
				if (m != null && m instanceof Slice && wrecker != null) {
					Slice s = (Slice) m;
					if (s.type < 0) {
						Log.d("sample", "first "+wrecker.getRegionEnd()+", "+fr);
						if (wrecker.scaleRegion(fr, wrecker.getRegionEnd())) {
							ol.onMarkerChanged(wrecker, type,s, ind, fr, v);	
							loopStartControl.setValue(fr);
							sampleView.setRegionLength(wrecker.getRegionLength());
						} else {
						}
					} else if (s.type > 0) {
						Log.d("sample", "last "+wrecker.getRegionEnd()+", "+fr);
						if (wrecker.scaleRegion(wrecker.getRegionStart(), fr)) {
							ol.onMarkerChanged(wrecker, type,s, ind, fr, v);	
							loopLengthControl.setValue(fr);
						} else {
						}
					} else {
						if (ol.setSliceStart(s, fr)) {
							ol.onMarkerChanged(wrecker, type, s, ind, fr, v);
							if (analysisMode == AnalysisMode.MANUAL) {
								WreckageState ws = wrecker.getState();
								if (ws != null) {
									float tr = (float) (((double)fr-ws.regionStart())/ws.regionLength())*wrecker.nBeat;
									Slice s_1 = ws.getSlice(ind-1);
									if (s_1 != null && s_1.position >= tr) {
										tr = s_1.position + minBeatIncrement;
									}
									s.position = tr;
								}
							}
						}
					}
				}
			}

			@Override
			public void onMarkerSelected(int type, Object m, int ind) {
			}

			@Override
			public void onMarkerRegionSelected(int type, Object m, int ind) {
				if (m != null && m instanceof Slice) {
					ol.onSliceSelected(wrecker, type, (Slice) m);
				}
			}
		});
		loopStartControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				Log.d("sample", "start to "+v);
				if (wrecker != null) {
					if (wrecker.moveRegionTo(v)) {
						sampleView.setRegionStart(v);
						sampleView.setMarkerDisplay();
						sampleView.invalidate();
					} else {
					}
					loopStartControl.setValue(wrecker.getRegionStart());
				}
			}
		});
		
		loopLengthControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (wrecker != null) {
					if (wrecker.scaleRegion(wrecker.getRegionStart(), wrecker.getRegionStart()+v)) {
						wrecker.setRegionEnd(v+wrecker.getRegionStart());
						sampleView.setRegionLength(v);
						sampleView.setMarkerDisplay();
						sampleView.setGridParameters();
						sampleView.invalidate();
						setAnalysisInfo();
					} else {
						loopLengthControl.setValue(wrecker.getRegionLength());
					}
				}
			}
		});
		
		nbeatControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (wrecker != null) {
					wrecker.setNBeat((int) v);
					sampleView.setGridMetric((int) v, getMeter(), getDivision());
					setAnalysisInfo();
					sampleView.invalidate();
				}
			}
		});
		
		meterControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (wrecker != null) {
					wrecker.setMeter((int) v);
					sampleView.setGridMetric(getNBeats(), (int) v, getDivision());
					setAnalysisInfo();
					sampleView.invalidate();
				}
			}

		});
		
		analyseButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (analysisMode) {
				case GRID: {
					int nbeat = (int) nbeatControl.getValue();
					int nslice = nbeat * ((int) sliceGridControl.getValue());
					
					wrecker.setGridQuant((int) sliceGridControl.getValue());
					wrecker.setNBeat(nbeat);
					wrecker.setMeter((int) meterControl.getValue());
					wrecker.analyseGrid(nslice);
					break;
				}
				}
				setAnalysisInfo();
			}			
		});
		
		analysisModeSelect.setAdapter(analysisModeAdapter);
		analysisModeSelect.setValueListener(new MenuControlListener() {
			@Override
			public void onValueChanged(int position) {
				if (wrecker != null) {
					AnalysisMode d = analysisModeAdapter.getItem(position);
					setAnalysisMode(d);
				}
			}
		});
		
		if (tempoLockToggle != null) {
			tempoLockToggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton v, boolean b) {
					if (b) {
						ol.setTempo(wrecker.getBaseTempo());
						ol.updateTempoDisplay();
					}
				}
			});
		}
		return v;
	}
	
	
	private int getNBeats() {
		return (int) (nbeatControl != null? nbeatControl.getValue():4);
	}
	
	protected int getDivision() {
		return (int) (sliceGridControl != null? sliceGridControl.getValue():2);
	}

	protected int getMeter() {
		return (int) (meterControl != null? meterControl.getValue():4);
	}

	protected void setAnalysisMode(AnalysisMode mode) {
		analysisMode  = mode;
		switch(mode) {
		case LOCK:
			sliceGridControl.setVisibility(View.GONE);
			break;
		case MANUAL:
			sliceGridControl.setVisibility(View.VISIBLE);
			break;
		case GRID:
			sliceGridControl.setVisibility(View.VISIBLE);
			break;
		}

		if (mode == AnalysisMode.LOCK) {
			if (sampleView != null) {
				sampleView.setLocked(true);
			}
			if (listener != null) {
				listener.onLock(true);
			}
		} else {
			if (sampleView != null) {
				sampleView.setLocked(false);
			}
			if (listener != null) {
				listener.onLock(true);
			}
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = ol.getMenuInflater();
	    inflater.inflate(R.menu.sample_fragment_context_menu, menu);
/*		MenuItem miSampleTimeView = menu.findItem(R.id.option_menu_show_sample_time_beats);
		if (miSampleTimeView != null) {
			miSampleTimeView.setVisible(true);
			miSampleTimeView.setChecked(ol.showSampleTimeBeats);
		}*/
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{

		switch (item.getItemId()) {
/*		case R.id.option_menu_show_sample_time_beats: {
			ol.setShowSampleTimeBeats(!ol.showSampleTimeBeats);
			return true;
		}*/
		case R.id.option_menu_sample_help: {
			Builder d = new AlertDialog.Builder(getActivity());
			d.setTitle(getResources().getString(R.string.help_title_sample_edit));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_sample_edit)));
			d.setPositiveButton(ol.aboutButtonText(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
				}
			});
			d.show(); 
			return true;
		}
		}

		return false;
	}
	
	public void setToWrecker(Wrecker pbs)
	{
		if (getView() == null) return;
		Log.d("sampleview", "got wrecker");
		wrecker  = pbs;
		WreckageState s = null;
		if (pbs != null) {
			s = pbs.getState();
			pbs.setAnalysisInfo();
		}
		if (s != null) {
			Log.d("sampleview", "got wrecker state "+s.regionLength());
			fileChooser.setPath(s.path);
			loopLengthControl.setValue(s.regionLength());
			loopStartControl.setValue(s.regionStart());
			loopLengthControl.setCenterValue(pbs.nTotalFrames);
			loopStartControl.setCenterValue(pbs.nTotalFrames);
			nbeatControl.setValue(pbs.nBeat);
			meterControl.setValue(pbs.getMeter());
			setAnalysisInfo();
		} else {
			loopLengthControl.setValue(0);
			loopStartControl.setValue(0);
			nbeatControl.setValue(1);
			meterControl.setValue(1);
			fileInfoView.setText("");
		}
		sampleView.setToPlayer(pbs);
		Log.d("sampleview", "setToWrecker: done");
	}

	private void setAnalysisInfo() {
		if (wrecker == null) {
			fileInfoView.setText("");
			return;
		}
		if (tempoLockToggle != null && tempoLockToggle.isChecked()) {
			ol.setTempo(wrecker.getBaseTempo());
			ol.updateTempoDisplay();
		}
		fileInfoView.setText(String.format("%.3f bpm, %.3f secs", wrecker.getBaseTempo(), wrecker.getBaseDuration()));
	}

	public void setBeatMode(boolean b)
	{
		loopStartControl.setBeatMode(b);
		loopLengthControl.setBeatMode(b);
	}

	public void setSampleGfx4Pad(Wrecker pbs) {
		sampleView.setToPlayer(pbs);
	}

	public void setLoopStart(long l) {
		if (loopStartControl != null) {
			loopStartControl.setValue(l);
		}
		if (sampleView != null) {
			sampleView.setRegionStart(l);
		}
	}

	public void setLoopLength(long l) {
		if (loopStartControl != null) {
			loopLengthControl.setValue(l);
		}
		if (sampleView != null) {
			sampleView.setRegionLength(l);
		}
	}

	public void updateSampleCursor() {
		if (sampleView != null) {
			sampleView.updateCursor();
		}
	}

	public void invalidateSlice(Slice s) {
		if (sampleView != null) {
			s.label = 0;
			sampleView.checkSlice(s.ind, wrecker.countSlices(), s);
//			sampleView.invalidate();
		}
	}

	public void invalidateSampleView() {
		if (sampleView != null) {
			sampleView.invalidate();
		}
	}
	
	public void setListener(Listener l) {
		listener = l;
	}

	public AnalysisMode getAnalysisMode() {
		return analysisMode;
	}
}
