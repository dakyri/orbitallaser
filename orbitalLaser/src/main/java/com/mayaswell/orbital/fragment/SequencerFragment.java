package com.mayaswell.orbital.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;

import com.mayaswell.audio.Sequence;
import com.mayaswell.audio.Sequence.Type;
import com.mayaswell.fragment.MWFragment;
import com.mayaswell.orbital.Bank;
import com.mayaswell.orbital.Patch;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.widget.PlayButton;
import com.mayaswell.orbital.widget.SequencerStepListView;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.widget.MultiModeButton;

public class SequencerFragment extends MWFragment {
		
	private EditText sequenceNameEditView = null;
	private Spinner sequenceSelector = null;
//	private Bank currentBank = null;
	private ArrayAdapter<Sequence<Patch>> sequenceSelectAdapter = null;
	private SequencerStepListView sequenceStepView;
	private Button addStepButton;
	private Button delStepButton;
	private Sequence<Patch> currentSequence = null;
	private Listener listener = null;
	private PlayButton playButton = null;
	
	public interface Listener {
		void sequenceSelected(Sequence<Patch> p);
		void sequencerStarted();
		void sequencerStopped();
	}


	public SequencerFragment() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		layoutChildren();
	}
	
	private void layoutChildren() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.sequencer_fragment, container, true);
		playButton  = (PlayButton) v.findViewById(R.id.playButton);
		sequenceSelector = (Spinner) v.findViewById(R.id.sequenceSelector);
		sequenceNameEditView = (EditText) v.findViewById(R.id.sequenceNameEditView);
		sequenceStepView = (SequencerStepListView) v.findViewById(R.id.sequenceStepView);
		addStepButton = (Button) v.findViewById(R.id.plusButton);
		delStepButton = (Button) v.findViewById(R.id.minusButton);
		registerForContextMenu(v);

		return v;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getActivity().getMenuInflater();
	    inflater.inflate(R.menu.sample_fragment_context_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{

		switch (item.getItemId()) {
		case R.id.option_menu_sequencer_help: {
			Builder d = new AlertDialog.Builder(getActivity());
			MWBPActivity<?, ?, ?> mwbp = null;
			try {
				mwbp = (MWBPActivity<?, ?, ?>) getActivity();
			} catch (ClassCastException e) {
				
			}
			if (mwbp == null) {
				return false;
			}
			d.setTitle(getResources().getString(R.string.help_title_sequencer));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_sequencer)));
			d.setPositiveButton(mwbp.aboutButtonText(), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
				}
			});
			d.show(); 
			return true;
		}
		}

		return false;
	}
	
	public void setListener(Listener l) {
		listener = l;
	}
	
	public void setToBank(Bank pbs)
	{
		if (sequenceStepView != null) {
			sequenceStepView.setToBank(pbs);
		}
	}

	public ArrayAdapter<Sequence<Patch>> setupSequenceControls() {
		sequenceSelectAdapter = new ArrayAdapter<Sequence<Patch>>(getActivity(), R.layout.mw_spinner_item);
		sequenceSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		if (sequenceSelector != null) {
			sequenceSelector.setAdapter(sequenceSelectAdapter);
			sequenceSelector.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					renSequence();
					return true;
				}
				
			});
			
			sequenceSelector.setOnItemSelectedListener(new OnItemSelectedListener() {	
				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					Sequence<Patch> b = sequenceSelectAdapter.getItem(position);
					if (listener != null) {
						listener.sequenceSelected(b);
					}
					setCurrentSequence(b);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parentView) 
				{
				}
			});
		}
		if (sequenceNameEditView != null) {
			sequenceNameEditView.addTextChangedListener(new TextWatcher() {
				@Override	public void afterTextChanged(Editable s) { }
				@Override	public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
				@Override	public void onTextChanged(CharSequence s, int start, int before, int count) { }
			});
			sequenceNameEditView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) {				
						onSequenceNameChanged();
						return true;
					}
					return false;
				}			
			});
			sequenceNameEditView.setOnFocusChangeListener(new OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (!hasFocus) {
						onSequenceNameChanged();
					}
				}

			});
		}
		addStepButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (currentSequence == null) {
					return;
				}
				int si = sequenceStepView.getSelectedStep();
				Sequence<Patch>.Step s = currentSequence.addStep(Type.PATCH, -1, 1, si, 1);
				if (s != null) {
					sequenceStepView.addStepView(si, s);
				}
			}
			
		});
		delStepButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (currentSequence == null) {
					return;
				}
				int si = sequenceStepView.getSelectedStep();
				if (si >= 0) {
					if (currentSequence.delStep(si)) {
						sequenceStepView.delStep(si);
					}
				}
			}
			
		});
		playButton.setListener(new PlayButton.Listener() {
			@Override
			public void onStateChanged(MultiModeButton b, int mode) {
				switch (mode) {
				case MultiModeButton.ACTIVATED: {
					if (listener != null) {
						listener.sequencerStarted();
					}
					break;
				}
				case MultiModeButton.DEACTIVATED: {
					listener.sequencerStopped();
					break;
				}
				}
				
			}
		});
		
		return sequenceSelectAdapter;
	}
	
	private void onSequenceNameChanged() {
		sequenceNameEditView.setVisibility(View.GONE);
		if (sequenceNameEditView.length() > 0 && currentSequence != null) {
			
		}
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(sequenceNameEditView.getWindowToken(), 0);//InputMethodManager.HIDE_NOT_ALWAYS
		try {
			if (sequenceNameEditView.length() > 0) {
				String pnm=new String(sequenceNameEditView.getText().toString());
				currentSequence.setName(pnm);
			}
		} catch (IndexOutOfBoundsException e) {
 		
 		}
		if (currentSequence != null) {
			
		}
	}
	
	public void setSequenceSelector(Sequence<Patch> ps) {
		if (sequenceSelector != null && sequenceSelectAdapter != null) {
			for (int i=0; i<sequenceSelectAdapter.getCount(); i++) {
				Sequence<Patch> psi = sequenceSelectAdapter.getItem(i);
				if (psi == ps) {
					sequenceSelector.setSelection(i);
				}		
			}
		}
		
	}
	public void setCurrentSequence(Sequence<Patch> s) {
		currentSequence  = s;
		setSequenceSelector(s);
		sequenceStepView.setToSequence(s);
	}
	
	public void renSequence() {
		if (currentSequence != null) {
			sequenceNameEditView.setText(currentSequence.name);
			sequenceNameEditView.setVisibility(View.VISIBLE);
			sequenceNameEditView.requestFocus();
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.showSoftInput(sequenceNameEditView, 0);//InputMethodManager.SHOW_FORCED
		}
	}
	public void setSequenceStepAdapter(ArrayAdapter<Action> stepSelectAdapter) {
		sequenceStepView.setSequenceStepAdapapter(stepSelectAdapter);
	}
	
	public void setCurrentStep(int csi) {
		sequenceStepView.setCurrentStep(csi);
	}
	public void showSequencePlayState(boolean b) {
		if (playButton != null) {
			playButton.setPlayState(b);
		}
	}

}
