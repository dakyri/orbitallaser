/**
 * 
 */
package com.mayaswell.orbital.fragment;

import com.mayaswell.orbital.Bank;
import com.mayaswell.orbital.ChaosEngine;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.orbital.util.MappedProbability;
import com.mayaswell.orbital.util.RangedMP;
import com.mayaswell.orbital.widget.MappedProbabilityWidget;
import com.mayaswell.orbital.widget.RangedMPWidget;
import com.mayaswell.widget.MenuControl;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ChaosEditorFragment extends OLFragment {

	protected MappedProbabilityWidget patternShiftWidget = null;
	protected MappedProbabilityWidget groupShiftWidget = null;
	protected MappedProbabilityWidget repeatPreviousWidget = null;
	protected MappedProbabilityWidget patternGridShiftWidget = null;
	protected MappedProbabilityWidget reverseWidget = null;
	
	protected RangedMPWidget durationModWidget = null;
	protected RangedMPWidget dynamicModWidget = null;
	protected RangedMPWidget stutterModWidget = null;
	protected RangedMPWidget pitchModWidget = null;

	public interface Listener {

	}

	private ChaosEditorFragment.Listener listener = null;

	public ChaosEditorFragment() {
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.chaos_editor_fragment, container, true);
		
		patternShiftWidget = (MappedProbabilityWidget) v.findViewById(R.id.patternShiftWidget);
		groupShiftWidget = (MappedProbabilityWidget) v.findViewById(R.id.groupShiftWidget);
		repeatPreviousWidget = (MappedProbabilityWidget) v.findViewById(R.id.repeatPreviousWidget);
		patternGridShiftWidget = (MappedProbabilityWidget) v.findViewById(R.id.patternGridShiftWidget);
		reverseWidget = (MappedProbabilityWidget) v.findViewById(R.id.reverseWidget);
		
		durationModWidget = (RangedMPWidget) v.findViewById(R.id.durationModWidget);
		dynamicModWidget = (RangedMPWidget) v.findViewById(R.id.dynamicModWidget);
		stutterModWidget = (RangedMPWidget) v.findViewById(R.id.stutterModWidget);
		pitchModWidget = (RangedMPWidget) v.findViewById(R.id.pitchModWidget);
		
		return v;
	}
	public void setListener(Listener l) {
		listener = l;
	}


	public void setToBank(Bank b) {
	}

	
	public void setupControlsAdapter(Context c, int tvrid, int ddrid) {
		
	}

	public void setTo(WreckageState w) {
		if (patternShiftWidget != null) {
			patternShiftWidget.setMap(w!=null?w.patternShiftChance: null);
		}
		if (groupShiftWidget != null) {
			groupShiftWidget.setMap(w!=null?w.groupShiftChance: null);
		}
		if (patternGridShiftWidget != null) {
			patternGridShiftWidget.setMap(w!=null?w.patternGridShiftChance: null);
		}
		if (repeatPreviousWidget != null) {
			repeatPreviousWidget.setMap(w!=null?w.repeatPreviousChance: null);
		}
		if (durationModWidget != null) {
			durationModWidget.setMap(w!=null?w.durationModChance: null);
		}
		if (dynamicModWidget != null) {
			dynamicModWidget.setMap(w!=null?w.dynamicModChance: null);
		}
		if (stutterModWidget != null) {
			stutterModWidget.setMap(w!=null?w.stutterModChance: null);
		}
		if (pitchModWidget != null) {
			pitchModWidget.setMap(w!=null?w.pitchModChance: null);
		}
		if (reverseWidget != null) {
			reverseWidget.setMap(w!=null?w.reverseChance: null);
		}
		
	}

}
