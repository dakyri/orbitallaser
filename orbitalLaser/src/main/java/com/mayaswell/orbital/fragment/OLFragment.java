/**
 * 
 */
package com.mayaswell.orbital.fragment;

import com.mayaswell.fragment.MWFragment;
import com.mayaswell.orbital.OrbitalLaser;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author dak
 *
 */
public class OLFragment extends MWFragment {
	
	protected OrbitalLaser ol = null;
	
	public OLFragment() {
	}
	
	@Override
	public void onAttach(Activity activity)
	{
		ol = (OrbitalLaser) activity;
		super.onAttach(activity);
	}
}
