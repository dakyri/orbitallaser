package com.mayaswell.orbital.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.Button;
import android.widget.TextView;

import com.mayaswell.audio.Controllable;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.orbital.CControl;
import com.mayaswell.orbital.FXBusState;
import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.SliceGroups.SliceGroup;
import com.mayaswell.orbital.fragment.AlgorithmEditorFragment.Listener;
import com.mayaswell.orbital.widget.FXBusSelect;
import com.mayaswell.orbital.widget.SampleFrameControl;
import com.mayaswell.widget.InfinIntControl;
import com.mayaswell.widget.MenuControl;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.InfinIntControl.InfiniteLongListener;
import com.mayaswell.widget.MenuControl.MenuControlListener;
import com.mayaswell.widget.TeaPot.TeaPotListener;

public class SliceEditorFragment extends OLFragment {
	public interface Listener {
		void groupChanged(Slice slice, int og, int id);
	}
	
	private TextView sliceLabel = null;
	private SampleFrameControl sliceStartControl = null;
	private TeaPot sliceGainControl = null;
	private InfinIntControl stutterControl = null;
	private TeaPot sliceTuneControl = null;
	private TeaPot slicePanControl = null;
	private MenuControl sliceDirectionSelect = null;
	private TeaPot slicePositionControl = null;
	private FXBusSelect sliceBusSelect = null;
	private Button splitButton = null;
	private Button mergeButton = null;
	private MenuControl sliceGroupSelect = null;
	
	private Slice slice = null;
	private Wrecker wrecker = null;

	private Listener listener = null;

	public SliceEditorFragment() {
		
	}
	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);		
	}
	

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
		setToWrecker(ol.getWrecker());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.slice_editor_fragment, container, true);
		
		sliceLabel = (TextView) v.findViewById(R.id.sliceLabel);
		splitButton = (Button) v.findViewById(R.id.splitButton);
		mergeButton = (Button) v.findViewById(R.id.mergeButton);
		sliceStartControl = (SampleFrameControl) v.findViewById(R.id.sliceStartControl);
		slicePositionControl = (TeaPot) v.findViewById(R.id.slicePositionControl);
		sliceGainControl = (TeaPot) v.findViewById(R.id.sliceGainControl);
		slicePanControl = (TeaPot) v.findViewById(R.id.slicePanControl);
		stutterControl = (InfinIntControl) v.findViewById(R.id.stutterControl);
		sliceTuneControl = (TeaPot) v.findViewById(R.id.sliceTuneControl);
		sliceDirectionSelect = (MenuControl) v.findViewById(R.id.sliceDirectionSelect);
		sliceBusSelect  = (FXBusSelect) v.findViewById(R.id.fxBusWidget);
		sliceGroupSelect = (MenuControl) v.findViewById(R.id.cellGroupSelect);

		sliceStartControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (slice != null) {
					if (!ol.setSliceStart(slice, v)) {
						sliceStartControl.setValue(slice.startFrame);
					} else {
						ol.onMarkerChanged(wrecker, 0, slice, slice.ind, slice.startFrame, 0);
					}
				}
			}
		});
		
		slicePositionControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				if (slice != null) {
					if (!ol.setSlicePosition(slice, v)) {
						slicePositionControl.setValue(slice.position);
					} else {
//						ol.onMarkerChanged(wrecker, 0, slice, slice.ind, slice.startFrame, 0);
					}
				}
			}
		});
		
		sliceDirectionSelect.setAdapter(Wrecker.directionAdapter);
		sliceDirectionSelect.setValueListener(new MenuControlListener() {
			@Override
			public void onValueChanged(int position) {
				Log.d("main", "set sample direction "+position);
				if (wrecker != null) {
					Direction d = Wrecker.directionAdapter.getItem(position);
					wrecker.setDirection(slice, d);
					Log.d("main", "set sample direction "+d.toString());
				}
			}
		});
		
		stutterControl.setPotListener(new InfiniteLongListener() {
			@Override
			public void onValueChanged(long v) {
				if (wrecker != null) {
					wrecker.setStutter(slice, (int)v);
				}
			}
		});
		
		if (sliceGroupSelect != null) {
			sliceGroupSelect.setAdapter(Wrecker.groupAdapter);
			sliceGroupSelect.setValueListener(new MenuControlListener() {
				@Override
				public void onValueChanged(int position) {
					if (slice != null) {
						MenuData<SliceGroup> d = Wrecker.groupAdapter.getItem(position);
						int og = slice.getGroup();
						wrecker.setGroup(slice, d.getData());
						if (listener != null) {
							listener.groupChanged(slice, og, d.getData());
						}
					}
				}
			});
		}
			
		sliceGainControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				if (wrecker != null) {
					wrecker.setGain(slice, v);
//					ol.ccontrolUpdate(slice, CControl.GAIN, v, Controllable.Controller.INTERFACE);
				}
			}
			
		});
		
		slicePanControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				if (wrecker != null) {
					wrecker.setPan(slice, v);
					ol.ccontrolUpdate(slice, CControl.PAN, v, Controllable.Controller.INTERFACE);
				}
			}
			
		});
		
		sliceTuneControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				if (wrecker != null) {
					wrecker.setTune(slice, v);
//					sg.ccontrolUpdate(slice, CControl.TUNE, v, Controllable.Controller.INTERFACE);
				}
			}
			
		});
		
		sliceBusSelect.setListener(new FXBusSelect.Listener() {
			@Override
			public short useBus(short b) {
				if (wrecker != null) {
					if (wrecker.setFXBus(slice, b)) {
						;
					}
				}
				return ol.onUseBus(wrecker, b);
			}
			
			@Override
			public short displayBus(short b) {
				return ol.onDisplayBus(wrecker, b);
			}
		});
		
		splitButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (wrecker != null && slice != null) {
					if (wrecker.splitSlice(slice)) {
						setToSlice(slice);
						if (ol != null) {
							ol.onSliceSplit(wrecker, slice, slice.ind);
						}
					}
				}
				
			}
		});
		
		mergeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (wrecker != null && slice != null) {
					if (wrecker.mergeSlice(slice)) {
						setToSlice(slice);
						if (ol != null) {
							ol.onSliceMerged(wrecker, slice, slice.ind);
						}
					}
				}
			}
		});
		
		return v;
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = ol.getMenuInflater();
	    inflater.inflate(R.menu.slice_editor_context_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item)
	{

		switch (item.getItemId()) {
		}
		return false;
	}

	public void setToWrecker(Wrecker pbs)
	{
		if (getView() == null) {
			return;
		}
		wrecker = pbs;
		Slice sls = null;
		if (pbs != null) {
			WreckageState s = pbs.getState();
			if (s != null) {
				sliceBusSelect.setNameSource(s);
				if (s.sliceCount() > 0) {
					for (Slice sli: s.getSlices()) {
						if (sli.selected) {
							sls = sli;
						}
					}
				}
			}
		}
		setToSlice(sls);
	}
	public void setToSlice(Slice s)
	{
		if (getView() == null || wrecker == null) return;
		slice = s;
		if (s == null) {
			sliceLabel.setText("unselected");
			sliceStartControl.setValue(0);
			slicePositionControl.setValue(0);
			sliceGainControl.setValue(1);
			slicePanControl.setValue(0);
			stutterControl.setValue(1);
			sliceTuneControl.setValue(0);
			sliceDirectionSelect.setSelection(0);
			sliceBusSelect.setSelectedBus(0);
			sliceBusSelect.setDisplayedBus(0);
			sliceGroupSelect.setSelection(0);
		} else {
			sliceLabel.setText("Slice "+Integer.toString(s.ind+1));
			WreckageState ws = wrecker.getState();
			if (ws != null) {
				Slice prev = ws.getSlice(s.ind-1);
				Slice next = ws.getSlice(s.ind+1);
				sliceStartControl.setValueBounds(prev!=null?prev.startFrame+1:0, s.startFrame, next!=null?next.startFrame-1:wrecker.nTotalFrames);
				sliceStartControl.setValue(s.startFrame);
				slicePositionControl.setValueBounds(
						(float)(prev!=null && prev!=s? prev.position+0.01 : 0),
						(float)(next != null && next!=s? next.position-0.01:wrecker.nBeat));
				slicePositionControl.setCenterValue(s.position);
				slicePositionControl.setValue(s.position);
				sliceGainControl.setValue(s.gain);
				slicePanControl.setValue(s.pan);
				stutterControl.setValue(s.stutter);
				sliceTuneControl.setValue(s.tune);
				setDirectionSelect(s);
				setGroupSelect(s);
				sliceBusSelect.setSelectedBus(s.fxBus);
			}
		}
	}
	
	public void setDirectionSelect(Slice s) {
		if (s != slice) return;
		for (int i=0; i<Wrecker.directionAdapter.getCount(); i++) {
			Direction d = Wrecker.directionAdapter.getItem(i);
			if (d == s.direction) {
				sliceDirectionSelect.setSelection(i);
				break;
			}
		}
	}
	
	public void setGroupSelect(Slice s) {
		if (s != slice) return;
		for (int i=0; i<Wrecker.groupAdapter.getCount(); i++) {
			MenuData<SliceGroup> d = Wrecker.groupAdapter.getItem(i);
			if (d.getData() == s.getGroup() || (d.getData() < 0 && s.getGroup() < 0)) {
				sliceGroupSelect.setSelection(i);
				break;
			}
		}
	}
	
	public void displaySliceStart(Slice s, long fr) {
		if (slice == s) {
			sliceStartControl.setValue(fr);
		}
	}
	
	public void displayPosition(Slice s, float position) {
		if (slice == s) {
			slicePositionControl.setValue(position);
		}
	}

	public void updateFXBusName(FXBusState b, String s) {
		if (sliceBusSelect != null) {
			sliceBusSelect.invalidate();
		}
	}
	
	public void setListener(Listener l) {
		listener  = l;
	}
}
