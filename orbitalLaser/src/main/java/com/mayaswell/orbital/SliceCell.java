package com.mayaswell.orbital;

import com.mayaswell.util.CellDataType;

public class SliceCell extends CellDataType {
	private int which;
	private float duration;
	private float gain;
	
	public SliceCell(int sg, float duration, float gain) {
		this.which = sg;
		this.duration = duration;
		this.gain = gain;
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public int getId() {
		return which;
	}

	public void setGroup(int sg) {
		this.which = sg;
	}

	public float getGain() {
		return gain;
	}

	public void setGain(float gain) {
		this.gain = gain;
	}

	@Override
	public SliceCell clone() {
		SliceCell sc = new SliceCell(which, duration, gain);
		return sc;
	}
	
	public String toString() {
		return "group "+which+" duration "+duration+" gain"+gain;
	}
}
