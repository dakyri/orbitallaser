package com.mayaswell.orbital;

import java.util.ArrayList;

import android.util.Log;

import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.Wrecker.PlayMode;
import com.mayaswell.orbital.util.MappedProbability;
import com.mayaswell.orbital.util.RangedMP;
import com.mayaswell.orbital.widget.FXBusSelect.NameSource;

public class WreckageState implements NameSource {
	public String path = "";
	public int loopCount = 0;
	
	private ArrayList<Slice> slices = new ArrayList<Slice>();
	private ArrayList<FXBusState> fx = new ArrayList<FXBusState>();
	public int nDataBeats = 4;
	public PlayMode playMode = PlayMode.VANILLA;
	public int algorithmId = -1;
	private ArrayList<SliceCell> pattern = null;
	
	public MappedProbability patternShiftChance = new MappedProbability();
	public MappedProbability groupShiftChance = new MappedProbability();
	public MappedProbability repeatPreviousChance = new MappedProbability();
	public MappedProbability patternGridShiftChance = new MappedProbability();
	public MappedProbability reverseChance = new MappedProbability();
	
	public RangedMP durationModChance = new RangedMP(0.5f,1.5f);
	public RangedMP dynamicModChance = new RangedMP(0.5f,1.5f);
	public RangedMP stutterModChance = new RangedMP(1,10);
	public RangedMP pitchModChance = new RangedMP(-7, 7);

	public WreckageState() {
		this(true);
	}
	
	public WreckageState(boolean doInit) {
		if (doInit) {
			slices.add(new Slice());
			slices.add(new Slice());
		}
	}

	public WreckageState clone()
	{
		Log.d("wreckage state", "cloneing "+slices.size()+" slices");
		WreckageState w = new WreckageState(false);
		w.path = path;
		w.playMode = playMode;
		w.nDataBeats = nDataBeats;
		w.algorithmId = algorithmId;
		if (pattern != null) {
			w.setPattern(new ArrayList<SliceCell>());
			for (SliceCell i: pattern) {
				w.pattern.add(i);
			}
		} else {
			w.setPattern(null);
		}
		for (Slice s: slices) {
			w.slices.add(s.clone());
		}
		for (FXBusState f: fx) {
			w.fx.add(f.clone());
		}
		
		w.patternShiftChance.setTo(patternShiftChance);
		w.groupShiftChance.setTo(groupShiftChance);
		w.repeatPreviousChance.setTo(repeatPreviousChance);
		w.patternGridShiftChance.setTo(patternGridShiftChance);
		w.reverseChance.setTo(reverseChance);
		w.durationModChance.setTo(durationModChance);
		w.dynamicModChance.setTo(dynamicModChance);
		w.stutterModChance.setTo(stutterModChance);
		w.pitchModChance.setTo(pitchModChance);
		
		return w;
	}
	
	public Slice getSlice(int i) {
		if (slices == null || slices.size() == 0) {
			return null;
		} else if (i >= slices.size()) {
			i = slices.size()-1;
		} else if (i < 0) {
			i = 0;
		}
		return slices.get(i);
	}
	
	public int getSliceAt(float p) {
		if (slices == null || slices.size() == 0) {
			return -1;
		}
		for (int i=0; i<slices.size()-1; i++) {
			if (slices.get(i).position <= p && p < slices.get(i+1).position) {
				return i;
			}
		}
		return -1;
	}
	
	public Slice insSlice(int ind, Slice ns) {
		if (slices == null) {
			slices = newSliceList();
		}
		Slice s = ns;
		if (ind >= slices.size()) {
			for (int i=slices.size()-1; i <= ind; i++) {
				slices.add(i, new Slice());
			}
		} else {
			if (s == null) {
				s = new Slice();
			}
			slices.add(ind, s);
		}
		return s;
	}
	
	public Slice addSlice(int ind) {
		if (slices == null) {
			slices = newSliceList();
		}
		Slice s;
		if (ind >= slices.size()) {
			for (int i=slices.size(); i <= ind; i++) {
				slices.add(i, new Slice());
			}
		} else if ((s=slices.get(ind)) == null) {
			slices.add(ind, s);
		}
		
		return slices.get(ind);
	}
	
	private ArrayList<Slice> newSliceList() {
		ArrayList<Slice> sl = new ArrayList<Slice>();
		sl.add(new Slice());
		sl.add(new Slice());
		return sl;
	}

	public void clear()
	{
		if (slices == null || slices.size() < 2) {
			addSlice(1);
		} else {
			for (int i=1; i<slices.size(); i++) {
				slices.remove(i);
			}
		}
	}

	public void setSlice(int i, Slice s) {
		slices.set(i, s);
	}
	
	public void delSlice(int i) {
		slices.remove(i);
	}

	public int regionStart() {
		if (slices.size() < 1) return 0;
		return slices.get(0).startFrame;
	}
	public int regionLength() {
		int s = slices.size();
		if (s < 2) return 0;
		int l = slices.get(s-1).startFrame - slices.get(0).startFrame;
//		Log.d("wrecker", "regionLength "+l);
		return l;
	}

	protected Slice setSlicePosition(int i, int fr) {
		Slice s = addSlice(i);
		s.startFrame = fr;
		return s;
	}

	public ArrayList<Slice> getSlices() {
		return slices;
	}

	public Slice sliceFrameContaining(int x0, int y0) {
		int i = 0;
		for (Slice s: slices) {
			if (s.getPositionedFrameRect().contains(x0, y0)) {
				s.ind = i;
				if (i == 0) {
					s.type = -1;
				} else if (i == slices.size()-1) {
					s.type = 1;
				} else {
					s.type = 0;
				}
				return s;
			}
			i++;
		}
		return null;
	}

	public Slice sliceContaining(int x0) {
		int i = 0;
		for (i=0; i<slices.size()-1; i++) {
			Slice s = slices.get(i);
			Slice s1 = slices.get(i+1);
			if (s.frameX <= x0 && x0 <= s1.frameX) {
				s.ind = i;
				if (i == 0) {
					s.type = -1;
				} else if (i == slices.size()-1) {
					s.type = 1;
				} else {
					s.type = 0;
				}
				return s;
			}
		}
		return null;
	}
	

	
	public boolean setSliceFrame(Slice s, int fr, float fx) {
		s.startFrame = fr;
		s.frameX = fx;
		return true;
	}

	public long getRegionEnd() {
		if (slices != null && slices.size() > 0) {
			return slices.get(slices.size()-1).startFrame;
		}
		return 0;
	}

	public long getRegionStart() {
		if (slices != null && slices.size() > 0) {
			return slices.get(0).startFrame;
		}
		return 0;
	}

	public void setSlices(ArrayList<Slice> slices) {
		if (slices != null) {
			this.slices = slices;
		}
	}

	public void setRegionStart(long v) {
		addSlice(0).startFrame = (int) v;
	}

	public void setRegionEnd(long v) {
		int n = slices.size()-1;
		if (n < 1) n = 1;
		addSlice(n).startFrame = (int) v;
	}

	public void clearMarkers() {
		if (slices != null && slices.size() >= 2) {
			ArrayList<Slice> s2 = new ArrayList<Slice>();
			s2.add(slices.get(0));
			s2.add(slices.get(slices.size()-1));
			slices = s2;
		}
	}

	public void selectSlice(Slice s, boolean on, boolean union) {
		if (slices != null && !union) {
			for (Slice s2: slices) {
				s2.selected = false;
			}
		}
		if (s != null) {
			s.selected = on;
		}
	}

	public int sliceCount() {
		if (slices != null && slices.size() >= 2) {
			return slices.size()-1;
		}
		return 0;
	}

	public FXBusState getBus(int i) {
		if (i >= 0 && i<fx.size()) {
			return fx.get(i);
		}
		return null;
	}
	
	public FXBusState addBus(int i) {
		FXBusState b = new FXBusState(i, "Bus"+i);
		for (int j=fx.size(); j<i; j++) {
			fx.add(new FXBusState(j, "Bus"+j));
		}
		if (i < fx.size()) {
			fx.set(i, b);
		} else {
			fx.add(b);
		}
		return b;
	}

	@Override
	public String getBusName(int i) {
		if (fx != null) {
			if (i >= 0 && i<fx.size()) {
				FXBusState b = fx.get(i);
				if (b != null) {
					return b.name;
				}
			}
		}
		return null;
	}

	public ArrayList<FXBusState> getBuses() {
		return fx;
	}

	public void addBus(FXBusState bss) {
		for (int j=fx.size(); j<bss.id; j++) {
			fx.add(new FXBusState(j, "Bus"+j));
		}
		if (bss.id < fx.size()) {
			fx.set(bss.id, bss);
		} else {
			fx.add(bss);
		}
	}

	public Slice endMarker() {
		if (slices == null || slices.size() < 2) {
			return null;
		}
		return slices.get(slices.size()-1);
	}

	public void setPattern(ArrayList<SliceCell> p) {
		synchronized(this) {
			pattern = p;
		}
	}

	public int getPatternCount() {
		synchronized(this) {
			return pattern != null? pattern.size(): 0;
		}
	}
	
	public SliceCell getPatternSliceCell(int i) {
		synchronized(this) {
			return pattern != null && i>=0 && i<pattern.size()? pattern.get(i):null;
		}
	}
	
	public int getPatternSliceIndex(int i) {
		synchronized(this) {
			return pattern != null && i>=0 && i<pattern.size()? pattern.get(i).getId():-1;
		}
	}
	
	public Slice getPatternSlice(int g) {
		return getSlice(getPatternSliceIndex(g));
	}

	public SliceCell patternAdd(int it) {
		return patternAdd(it, 1, 1);		
	}
	
	public SliceCell patternAdd(int it, float dur, float gain) {
		SliceCell sc = null;
		synchronized(this) {
			if (pattern != null) {
				pattern.add(sc=new SliceCell(it, dur, gain));
			}
		}		
		return sc;
	}
	
	public SliceCell patternAdd(int it, float dur, float gain, int ind) {
		SliceCell sc = null;
		synchronized(this) {
			if (pattern == null) {
				pattern = new ArrayList<SliceCell>();
			}
			if (ind >= 0) {
				if (ind >= pattern.size()-1) {
					pattern.add(sc=new SliceCell(it, dur, gain));
				} else {
					pattern.add(ind, sc=new SliceCell(it, dur, gain));
				}
			}
		}		
		return sc;
	}
	public SliceCell patternAdd(int it, int ind) {
		return patternAdd(it, 1, 1, ind);
	}
	
	public SliceCell patternRem(int ind) {
		SliceCell sc = null;
		synchronized(this) {
			if (pattern != null && ind >= 0 && ind < pattern.size()) {
				sc = pattern.remove(ind);
			}
		}
		return sc;
	}
	
	public void patternMov(int ind1, int ind2) {
		synchronized(this) {
			if (pattern != null && ind1 >= 0 && ind1 < pattern.size() && ind2 >= 0) {
				if (ind1 != ind2) {
					SliceCell i = pattern.remove(ind1);
					if (ind1 < ind2) {
						ind2--;						
					}	
					if (ind2 >= pattern.size()) {
						pattern.add(i);
					} else {
						pattern.add(ind2, i);
					}
				}
			}
		}
	}

	public int findSlice(Slice s) {
		return slices.indexOf(s);
	}

	public float getPatternDuration() {
		float l=0;
		if (pattern == null) {
			return 0;
		}
		for (SliceCell sc: pattern) {
			l += sc.getDuration();
		}
		return l;
	}

	public ArrayList<SliceCell> getPattern() {
		return pattern;
	}


}
