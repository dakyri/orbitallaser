package com.mayaswell.orbital;

import java.util.ArrayList;

import android.util.Log;


public class RandomEngine extends WreckerEngine {
	protected SliceCell currentSliceCell = null;
	private ArrayList<SliceCell> currentPattern = new ArrayList<SliceCell>();
	private int currentPatternIndex=-1;
	
	public RandomEngine(Wrecker w) {
		super(w);
	}

	@Override
	public int getFirstSlice() {
		generateRandomPattern();
		currentPatternIndex = 0;
		if (currentPattern.size() > 0) {
			currentSliceCell = currentPattern.get(0);
			return currentSliceCell.getId();
		}
		return 0;
	}


	@Override
	public int getNextSlice() {
		currentPatternIndex++;
		ArrayList<SliceCell> cp = currentPattern;
		int nsi = 0;
		if (cp != null) {
			if (currentPatternIndex >= cp.size()) {
				currentPatternIndex = cp.size()-1;
			}
			if (cp.size() > 0) {
				currentSliceCell = cp.get(currentPatternIndex);
				nsi = currentSliceCell.getId();
			}
		}
		Log.d("wrecker", "next slice from pattern "+nsi);
		return nsi;
	}
	
	@Override
	public boolean isFirstSlice() {
		return currentPatternIndex == 0;
	}


	@Override
	public boolean atLoopEnd() {
		return currentPatternIndex >= currentPattern.size()-1;
	}

	@Override
	public boolean initialize() {
		generateRandomPattern();
		currentPatternIndex = 0;
		return true;
	}

	private void generateRandomPattern() {
		int cs = wrecker.countSlices();
		if (cs == 0) {
			return;
		}
		int [] trial = new int[cs];
		ArrayList<SliceCell> cp = new ArrayList<SliceCell>();
		for (int i=0; i<cs; i++) {
			trial[i] = i;
		}
		for (int i=0; i<cs; i++) {
			int pick = -1;
			int picki = (int) Math.floor(Math.random()*cs);
			for (int j=0; j<cs; j++) {
				pick = trial[picki];
				if (pick >= 0) {
					trial[picki] = -1;
					break;
				}
				picki++;
				if (picki >= cs) {
					picki = 0;
				}
			}
			
			Slice s = wrecker.getSlice(pick);
			Slice s2 = wrecker.getSlice(pick+1);
			cp.add(new SliceCell(pick, s2.position-s.position, s.gain));
		}
		currentPattern = cp;
	}
	

}
