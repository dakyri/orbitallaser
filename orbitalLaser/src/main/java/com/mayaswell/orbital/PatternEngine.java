package com.mayaswell.orbital;

import java.util.ArrayList;

import android.util.Log;

public class PatternEngine extends WreckerEngine {
	protected SliceCell currentSliceCell = null;
	private ArrayList<SliceCell> currentPattern = new ArrayList<SliceCell>();
	private int currentPatternIndex=-1;
	
	public PatternEngine(Wrecker w) {
		super(w);
	}

	@Override
	public float currentSliceGain() {
		float g = wrecker.currentSlice.gain;
		if (currentSliceCell != null) {
			g *= currentSliceCell.getGain();
		}
		return g;
	}

	@Override
	public float currentSliceBeatDuration() {
		if (currentSliceCell != null) {
			return currentSliceCell.getDuration();
		}
		return super.currentSliceBeatDuration();
	}


	@Override
	public int getFirstSlice() {
		currentPattern = loadCurrentPattern();
		currentPatternIndex = 0;
		if (currentPattern.size() > 0) {
			currentSliceCell = currentPattern.get(0);
			return currentSliceCell.getId();
		}
		return 0;
	}
	
	@Override
	public int getNextSlice() {
		currentPattern = loadCurrentPattern();
		currentPatternIndex++;
		ArrayList<SliceCell> cp = currentPattern;
		int nsi = 0;
		if (cp != null) {
			if (currentPatternIndex >= cp.size()) {
				currentPatternIndex = cp.size()-1;
			}
			if (cp.size() > 0) {
				currentSliceCell = cp.get(currentPatternIndex);
				nsi = currentSliceCell.getId();
			}
		}
		Log.d("wrecker", "next slice from pattern "+nsi);
		return nsi;
	}
		
	@Override
	public boolean isFirstSlice() {
		return currentPatternIndex == 0;
	}
	

	@Override
	public boolean atLoopEnd() {
		return currentPatternIndex >= currentPattern.size()-1;
	}

	@Override
	public boolean initialize() {
		currentPattern = loadCurrentPattern();
		currentPatternIndex = 0;
		return true;
	}

	protected ArrayList<SliceCell> loadCurrentPattern() {
		ArrayList<SliceCell> cp = new ArrayList<SliceCell>();
		if (wrecker.state.getPatternCount() > 0) {
			for (int pi=0; pi< wrecker.state.getPatternCount(); pi++) {
				int pati = wrecker.state.getPatternSliceIndex(pi);
				Slice s = wrecker.getSlice(pati);
				Slice s2 = wrecker.getSlice(pati+1);
				cp.add(new SliceCell(pati, s2.position-s.position, s.gain));
			}
		} else {
			for (int i=0; i<wrecker.countSlices(); i++) {
				Slice s = wrecker.getSlice(i);
				Slice s2 = wrecker.getSlice(i+1);
				cp.add(new SliceCell(i, s2.position-s.position, s.gain));
			}
		}
		return cp;
	}
	

}

