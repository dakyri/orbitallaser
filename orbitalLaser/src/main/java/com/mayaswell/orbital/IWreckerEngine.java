package com.mayaswell.orbital;

import com.mayaswell.audio.Sample.Direction;

public interface IWreckerEngine {
	float currentSliceGain();
	float currentSliceTune();
	float currentSliceBeatDuration();
	short currentSliceFxBus();
	Direction currentSliceDirection();
	int currentStutter();
	int getFirstSlice();
	int getNextSlice();
	boolean isFirstSlice();
	boolean initialize();
	boolean atLoopEnd();
	
	void setState(WreckageState s);
}
	
