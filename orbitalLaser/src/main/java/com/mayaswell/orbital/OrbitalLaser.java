package com.mayaswell.orbital;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import com.mayaswell.audio.file.AudioFile;
import com.mayaswell.orbital.R;
import com.mayaswell.audio.AudioMixer;
import com.mayaswell.audio.Bufferator;
import com.mayaswell.audio.Sequence;
import com.mayaswell.audio.Controllable.ControllableInfo;
import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FX.Host;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Modulator.LFWave;
import com.mayaswell.audio.Modulator.ResetMode;
import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.audio.Bufferator.SampleInfo;
import com.mayaswell.audio.SamplePlayer.Manager;
import com.mayaswell.fragment.MWFragment;
import com.mayaswell.fragment.ModControlsFragment;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.Wrecker.PlayMode;
import com.mayaswell.orbital.fragment.ChaosEditorFragment;
import com.mayaswell.orbital.fragment.FXBusFragment;
import com.mayaswell.orbital.fragment.AlgorithmEditorFragment;
import com.mayaswell.orbital.fragment.SampleEditorFragment;
import com.mayaswell.orbital.fragment.SampleEditorFragment.AnalysisMode;
import com.mayaswell.orbital.fragment.SequencerFragment;
import com.mayaswell.orbital.fragment.SliceEditorFragment;
import com.mayaswell.orbital.widget.FXBusMaster;
import com.mayaswell.orbital.widget.PlayButton;
import com.mayaswell.util.BankSanitizer;
import com.mayaswell.util.ErrorLevel;
import com.mayaswell.util.EventID;
import com.mayaswell.util.MWActivity;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.util.MWUtils;
import com.mayaswell.util.SimpleFileChooser;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.widget.FileChooserView;
import com.mayaswell.widget.IPPortView;
import com.mayaswell.widget.MenuControl;
import com.mayaswell.widget.MenuControl.MenuControlListener;
import com.mayaswell.widget.MenuData;
import com.mayaswell.widget.MultiModeButton;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.TeaPot.TeaPotListener;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.preference.PreferenceManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.AlertDialog.Builder;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import rx.Observer;
import rx.functions.Action1;

public class OrbitalLaser extends MWBPActivity<Bank, Patch, CControl> implements  SamplePlayer.Manager {
	static {
		System.loadLibrary("orbital");
	}
	
	public static final int kAlgIndexBase = 1000;
	private Bufferator bufferator = null;

	public class Global {
		public static final int LFO_PER_WRECK = 4;
		public static final int MAX_LFO_TGT = 4;
		public static final int ENV_PER_WRECK = 2;
		public static final int MAX_ENV_TGT = 4;
		public static final int MAX_BUS_PER_WRECK = 8;
		public static final int MAX_BUS_FX = 4;
		public static final int MAX_FX_PARAM = 6;
	}

	public enum ViewMode {
		MAIN, ALGORITHM, CHAOS;
		@Override
		public String toString()
		{
			switch (this) {
			case MAIN: return "main";
			case ALGORITHM: return "algorithm";
			case CHAOS: return "chaos";
			}
			return super.toString();
		}
	}
	
	public static int[] regginbow = new int[8];
	
	private SharedPreferences olPreferences = null;
	private boolean keepScreenAlive = true;

	private Wrecker wreckage = null; /* TODO for multi layers probably an array */

	static OrbitalLaser ol=null;
	protected OLAudioMixer player = null;
	
	public boolean showSampleTimeBeats=false;
	
	private SampleEditorFragment sampleEditorFragment = null;
	private SliceEditorFragment sliceEditorFragment = null;
	private ModControlsFragment<OrbitalLaser> modControlFragment = null;
	private FXBusFragment fxBusFragment = null;
	private SequencerFragment sequencerFragment = null;
	private AlgorithmEditorFragment algorithmEditorFragment = null;
	private ChaosEditorFragment chaosEditorFragment = null;

	private RelativeLayout mainLayout = null;
	private PlayButton playButton = null;
	protected TeaPot masterTempoControl = null;
	private float masterTempo = 120;

	protected MasterBus busMixer;
	private ArrayAdapter<ResetMode> rstModeAdapter;
	private ArrayAdapter<LFWave> lfoWaveAdapter;
	public static ArrayAdapter<FX> fxAdapter = null;
	private MenuControl playerSelector = null;
	private ArrayAdapter<MenuData<PlayMode>> playerSelectAdapter = null;
	private ArrayAdapter<ViewMode> viewSelectAdapter = null;
	private MenuControl viewSelector = null;
	protected TextView beatCounter = null;
	
	public static ArrayList<FX> fxList = null;
	
	private Sequence<Patch> currentSequence = null;
	private Sequence<Patch>.Step currentSequenceStep = null;
	private int currentStepCount = 0;
	private int currentSequenceStepIndex = 0;
	private boolean sequencerRunning = false;
	private ArrayAdapter<Sequence<Patch>> sequenceSelectAdapter = null;

	public OLOSCModule oscModule = null;
	private IPPortView ipView = null;
	protected ArrayAdapter<Action> sequenceStepAdapter = null;

	protected BankSanitizer bankSanitizer = null;
	private Typeface digitalDream = null;

	public OrbitalLaser() {
		super(".olx");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		regginbow[0] = getResources().getColor(R.color.ribbonBow1);
		regginbow[1] = getResources().getColor(R.color.ribbonBow2);
		regginbow[2] = getResources().getColor(R.color.ribbonBow3);
		regginbow[3] = getResources().getColor(R.color.ribbonBow4);
		regginbow[4] = getResources().getColor(R.color.ribbonBow5);
		regginbow[5] = getResources().getColor(R.color.ribbonBow6);
		regginbow[6] = getResources().getColor(R.color.ribbonBow7);
		regginbow[7] = getResources().getColor(R.color.ribbonBow8);		

		OrbitalLaser.ol = this;
		
		wreckage = new Wrecker("layer1", 1, this);
		wreckage.setListener(new Wrecker.Listener() {

			@Override
			public void playStarted(Wrecker wrecker) {// we've started it elsewhere, just want to update gfx
				if (wreckage != null) {
//				wreckage.setPlayState(true);
				}
			}

			/*
							case Wrecker.PLAY_START_SYNCED: {
// we want to start it elsewhere and we don't want a synchronization clusterfuck there is a messaging lag here though
//			player.startPad(pbs);
//			pb.setPlayState(pbs.isPlaying());
					return true;
				}

			 */
			@Override
			public void playComplete(Wrecker wrecker) {
				stopWreckage(playButton);
			}

			@Override
			public void setSampleData(Wrecker wrecker) {
				Log.d("wrecker", "setting sample data");
				if (sampleEditorFragment != null) {
					Log.d("wrecker", "setting to pad");
					sampleEditorFragment.setToWrecker(wreckage);
				}
				Log.d("wrecker", "setting sample data done");

			}

			@Override
			public void analysisComplete(Wrecker wrecker) {
				Log.d("sample", "data analysis complete");
				if (sampleEditorFragment != null) {
					Log.d("sample", "setting to pad");
					sampleEditorFragment.setToWrecker(wreckage);
				}
			}

			@Override
			public void nodeChanged(Wrecker wrecker) {
				if (algorithmEditorFragment != null) {
					algorithmEditorFragment.nodeChanged(wreckage.getAlgorithm(), wreckage.lastNodeId, wreckage.currentNodeId);
				}
			}

			@Override
			public void cellChanged(Wrecker wrecker) {
				if (algorithmEditorFragment != null) {
					algorithmEditorFragment.cellChanged(wreckage.getAlgorithm(), wreckage.currentNodeId, wreckage.lastCellItemInd, wreckage.currentCellItemInd);
				}

			}
		});
		
		wreckage.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);

		Log.d("wrecker", "oncreate global adapters ..");
		rstModeAdapter = new ArrayAdapter<ResetMode>(this, R.layout.mw_spinner_item);
		rstModeAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		rstModeAdapter.add(ResetMode.ONSLICE);		
		rstModeAdapter.add(ResetMode.ONLOOP);		
		rstModeAdapter.add(ResetMode.ONFIRE);		
		rstModeAdapter.add(ResetMode.ONATTACK);

		lfoWaveAdapter = new ArrayAdapter<LFWave>(this, R.layout.mw_spinner_item);
		lfoWaveAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		lfoWaveAdapter.add(LFWave.SIN);
		lfoWaveAdapter.add(LFWave.SQUARE);
		lfoWaveAdapter.add(LFWave.SAW);
		lfoWaveAdapter.add(LFWave.EXP);

		playerSelectAdapter = new ArrayAdapter<MenuData<PlayMode>>(this, R.layout.mw_spinner_item);
		playerSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);

		viewSelectAdapter = new ArrayAdapter<ViewMode>(this, R.layout.mw_spinner_item);
		viewSelectAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
		viewSelectAdapter.add(ViewMode.MAIN);
		viewSelectAdapter.add(ViewMode.CHAOS);
		viewSelectAdapter.add(ViewMode.ALGORITHM);

		fxList = MWUtils.defaultFX();
		if (fxAdapter == null) {
			fxAdapter = new ArrayAdapter<FX>(this, R.layout.mw_spinner_item);
			fxAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
			for (FX f: fxList) {
				fxAdapter.add(f);
			}
		}
		
		setContentView(R.layout.main);
		
		digitalDream  =Typeface.createFromAsset(getAssets(), "fonts/DigitalDreamFatNarrow.ttf"); 
		
		Log.d("wrecker", "oncreate global ui componnents ..");
		patchSelector = (Spinner) findViewById(R.id.patchSelector);
		patchNameEditView = (EditText) findViewById(R.id.patchNameEditView);
		
		sampleEditorFragment = (SampleEditorFragment) getFragmentManager().findFragmentById(R.id.sampleEditorFragment);		
		sliceEditorFragment  = (SliceEditorFragment) getFragmentManager().findFragmentById(R.id.sliceEditorFragment);		
		modControlFragment   = (ModControlsFragment<OrbitalLaser>) getFragmentManager().findFragmentById(R.id.sliceModsFragment);		
		fxBusFragment = (FXBusFragment) getFragmentManager().findFragmentById(R.id.fxBusFragment);
		sequencerFragment = (SequencerFragment) getFragmentManager().findFragmentById(R.id.sequencerFragment);
//		playerFragment = (PlayerFragment) getFragmentManager().findFragmentById(R.id.playerFragment);
		
		mainLayout  = (RelativeLayout) findViewById(R.id.mainLayout);
		
		doPreferences();
		
		Log.d("wrecker", "oncreate global init ..");
		Wrecker.globalWreckerInit();
		MasterBus.globalBmxInit();
		if (bufferator == null) {
			bufferator = Bufferator.init(this);
			bufferator.setSampleCacheSize(4);
			final MWBPActivity<?,?,?> activity = this;
			bufferator.monitorState(new Action1<Bufferator.BufferatorException>() {
				@Override
				public void call(Bufferator.BufferatorException e) {
					if (e.severity == ErrorLevel.FNF_ERROR_EVENT) {
						String si = e.getPath();
						bankSanitizer = BankSanitizer.getInstance();
						if (!BankSanitizer.isChecking()) {
							BankSanitizer.check(currentBank, activity, currentPatch, si);
						}
					} else {
						if (errorIsShowing) {
							Log.d("error", "skip formal display of error " + e.getMessage());
						} else {
							handleErrorMessage(e.getMessage(), e.severity);
						}
					}
				}
			});
			bufferator.monitorGfx(new Action1<SampleInfo>() {
				@Override
				public void call(SampleInfo sampleInfo) {
					if (wreckage != null && wreckage.hasSample(sampleInfo)) {
						if (sampleEditorFragment != null) sampleEditorFragment.setSampleGfx4Pad(wreckage);
					}
				}
			});
		}

		player = new OLAudioMixer(1);

		busMixer = MasterBus.build(1, player.getOutBufsize());
		wreckage.setBufSize(player.getOutBufsize());
		
		playButton  = (PlayButton) findViewById(R.id.playButton);
		playButton.setWrecker(wreckage);
		playButton.setListener(new PlayButton.Listener() {

			@Override
			public void onStateChanged(MultiModeButton b, int mode) {
				PlayButton pb = null;
				try {
					pb = (PlayButton) b;
				} catch (ClassCastException e2) {
					
				}
				if (pb == null) {
					return;
				}
				switch (mode) {
				case MultiModeButton.ACTIVATED: {
					if (!wreckage.isPlaying()) {
						startWreckage(pb);
					} 
					break;
				}

				case MultiModeButton.DEACTIVATED: {
					if (wreckage.isPlaying()) {
						stopWreckage(pb);
					}
					break;
				}
				}
			}
			
		});
		
		masterTempoControl = (TeaPot) findViewById(R.id.masterTempoControl);
		masterTempoControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				Patch p;
				setTempo(v);
				if ((p=getCurrentPatch()) != null) {
					p.setTempo(v);
				}
			}
		});
		masterTempoControl.setValue(masterTempo);
		
		playerSelector   = (MenuControl) findViewById(R.id.playerSelector);
		setPlayerSelectAdapter(null);
		if (playerSelector != null) {
			playerSelector.setAdapter(playerSelectAdapter);
			playerSelector.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					return true;
				}
				
			});
			
			playerSelector.setValueListener(new MenuControlListener() {	
				@Override
				public void onValueChanged(int position) {
					MenuData<PlayMode> b = playerSelectAdapter.getItem(position);
					if (b.getMode() == PlayMode.AUTOMATOR) {
						setCurrentAlgorithmInd(b.getData());
					}
					wreckage.setPlayMode(b.getMode());
					displayPlaySelectMode(b.getMode());
				}
			});
		}

		viewSelector   = (MenuControl) findViewById(R.id.viewSelector);
		if (viewSelector != null) {
			viewSelector.setAdapter(viewSelectAdapter);
			viewSelector.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					return true;
				}
				
			});
			
			viewSelector.setValueListener(new MenuControlListener() {	
				@Override
				public void onValueChanged(int position) {
					ViewMode b = viewSelectAdapter.getItem(position);
					setViewMode(b);
				}

			});
		}
		
		beatCounter = (TextView) findViewById(R.id.beatCounter);
		if (beatCounter != null) {
			beatCounter.setText("0:0.00");
			if (digitalDream != null) {
				beatCounter.setTypeface(digitalDream);
			}
		}
		
		if (sampleEditorFragment != null) {
			sampleEditorFragment.setListener(new SampleEditorFragment.Listener() {
				
				@Override
				public void onLock(boolean b) {
				}
			});
		}
		
		fxBusFragment.setFXMasterListener(new FXBusMaster.Listener() {

			@Override
			public void onGainChanged(FXBusState b, float amt) {
			}

			@Override
			public void onPanChanged(FXBusState b, float amt) {
			}

			@Override
			public void onEnableChanged(FXBusState b, boolean e) {
			}

			@Override
			public void onNameChanged(FXBusState b, String s) {
				if (sliceEditorFragment != null) {
					sliceEditorFragment.updateFXBusName(b, s);
				}
			}
			
		});
		
		sequencerFragment.setListener(new SequencerFragment.Listener() {
			
			@Override
			public void sequencerStopped() {
				stopSequencer();
			}
			
			@Override
			public void sequencerStarted() {
				startSequencer(true);
			}
			
			@Override
			public void sequenceSelected(Sequence<Patch> p) {
				currentSequence = p;
			}
		});
		
		sliceEditorFragment.setListener(new SliceEditorFragment.Listener() {	
			@Override
			public void groupChanged(Slice slice, int og, int id) {
				if (algorithmEditorFragment != null) {
					algorithmEditorFragment.refreshGroupView();
				}
			}
		});
		
		oscModule = new OLOSCModule(this);
		
		Log.d("wrecker", "oncreate banks");
		defaultBankFileName = getResources().getString(R.string.default_bank);	
		currentBank = loadBank(defaultBankFileName, true, false);
		if (currentBank == null) {
			currentBank = new Bank();
			saveBank(currentBank, defaultBankFileName, true);
		} else {
		}
		if (currentBank.numPatches() == 0) {
			currentBank.add(currentBank.newPatch());
		}
		setupPatchControls();
		
		if (currentPatchInd < 0) {
			setCurrentPatchInd(0);
		}
		if (sequencerFragment != null) {
			sequencerFragment.setSequenceStepAdapter(sequenceStepAdapter);
			sequenceSelectAdapter = sequencerFragment.setupSequenceControls();
			if (currentBank != null) {
				currentBank.setSequenceAdapterItems(sequenceSelectAdapter);
			}
			sequencerFragment.setToBank(currentBank);
		}
		
		Log.d("wrecker", "oncreate complete");
	}

	/**
	 * The activity is about to become visible.
	 */
	@Override
	protected void onStart()
	{
		Log.d("orbital", "onStart");
		super.onStart();
		startPeriodicUiUpdate();
		
		Bufferator.run();
		
		player.addWrecker(wreckage);
		player.play();
	}
	
	/** 
	 * The activity has become visible (it is now "resumed").
	 */
   @Override
   protected void onResume()
   {
		Log.d("orbital", "onResume");
		super.onResume();
		startPeriodicUiUpdate();
		Log.d("orbital", "onResume done");
   }
	
   /**
	*  Another activity is taking focus (this activity is about to be "paused").
	*/
	@Override
	protected void onPause()
	{
		Log.d("orbital", "onPause");
		super.onPause();
		stopPeriodicUiUpdate();
	}
	
	/**
	 * The activity is no longer visible (it is now "stopped")
	 */
	@Override
	protected void onStop()
	{
		Log.d("orbital", "onStop");
		super.onStop();
		stopPeriodicUiUpdate();
	}
	
	/**
	 * The activity is about to be destroyed.
	 */
	@Override
	protected void onDestroy()
	{
		Log.d("orbital", "onDestroy");
		player.stop();// which will close it and force all the cleanups
		Wrecker.globalWreckerCleanup();
//		BusMixer.globalBmxCleanup();
		Bufferator.stop();
		Bufferator.cleanup();
		super.onDestroy();
	}

	/**
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed()
	{
		bailAlog("Save current bank before exit?");
//		super.onBackPressed();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{

		switch (item.getItemId()) {

		case R.id.option_menu_new_patch: {
			newPatch();
			return true;
		}
		case R.id.option_menu_save_patch: {
			setCurrentPatchState(currentPatch);
			savePatch(currentPatchInd, currentPatch);
			return true;
		}
		case R.id.option_menu_clone_patch: {
			branchPatch();
			return true;
		}
		case R.id.option_menu_reload_patch: {
			reloadCurrentPatch();
			return true;
		}

		case R.id.option_menu_del_patch: {
			int del = currentPatchInd;
			delPatch();
			currentBank.fixSequencesOnDeletedPatch(del);
			if (sequencerFragment != null) {
				sequencerFragment.setCurrentSequence(currentSequence);
			}
			return true;
		}
		case  R.id.option_menu_ren_patch: {
			renPatch();
			return true;
		}

		case R.id.option_menu_new_sequence: {
			newSequence();
			return true;
		}
		case R.id.option_menu_del_sequence: {
			delSequence(currentSequence);
			return true;
		}
		case R.id.option_menu_branch_sequence: {
			branchSequence();
			return true;
		}
		case  R.id.option_menu_ren_sequence: {
			if (sequencerFragment != null) {
				sequencerFragment.renSequence();
			}
			return true;
		}
		
		case R.id.option_menu_new_algorithm: {
			if (algorithmEditorFragment != null) {
				algorithmEditorFragment.newAlgorithm();
			}
			return true;
		}
		case R.id.option_menu_del_algorithm: {
			if (algorithmEditorFragment != null) {
				int del = algorithmEditorFragment.delAlgorithm();
				currentBank.fixSequencesOnDeletedAlgorithm(del);
				if (del == wreckage.getState().algorithmId) {
					wreckage.setAlgorithm(-1);
				}
			}
			return true;
		}
		case R.id.option_menu_branch_algorithm: {
			if (algorithmEditorFragment != null) {
				algorithmEditorFragment.branchAlgorithm();
			}
			return true;
		}
		case  R.id.option_menu_ren_algorithm: {
			if (algorithmEditorFragment != null) {
				algorithmEditorFragment.renAlgorithm();
			}
			return true;
		}
		
		case R.id.option_menu_load_bank: {
			String [] fileFilter = {".*\\.olx"};
			String startDir = getCurrentBankBaseDir();
			Intent request =new Intent(OrbitalLaser.this, SimpleFileChooser.class);

			request.putExtra("startDir", startDir);
			request.putExtra("fileFilter", fileFilter);
			request.putExtra("showHidden", false);
			startActivityForResult(request,  R.id.option_menu_load_bank);
			return true;
		}
		case R.id.option_menu_new_bank: {
			currentBank = new Bank();
			if (currentBank.patch.size() == 0) {
				currentBank.newPatch();
			}
			currentBankFile = null;
			setCurrentPatchInd(0);
			if (sequencerFragment != null) {
				sequencerFragment.setToBank(currentBank);
			}
			setPlayerSelectAdapter(currentBank);
			loadPatchDisplayUpdate();
			return true;
		}
		case R.id.option_menu_save_bank_and_state: {
			if (currentBankFile == null) {
				saveFialog("bank.olx", null, false, true);
			} else {
				saveCurrentBank(false, true);
			}
			return true;
		}
		case R.id.option_menu_save_bank: {
			if (currentBankFile == null) {
				saveFialog("bank.olx", null, false, false);
			} else {
				saveCurrentBank(false, false);
			}
			return true;
		}
		
		case R.id.option_menu_export: {
			renderalog("loop.wav");
			break;
		}
		
		case R.id.option_menu_save_bank_as: {
			String dnm = null;
			if (currentBankFile != null) {
				dnm = currentBankFile.getName();
			} else {
				dnm = "bank.olx";
			}
			saveFialog(dnm, null, false, false);
			return true;
		}
		case R.id.option_menu_save_bank_2dflt: {
			if (currentBank != null) {
				setCurrentPatchState(getCurrentPatch());
				if (currentPatchInd >= 0 && currentPatchInd < currentBank.patch.size()) {
					currentBank.patch.set(currentPatchInd, getCurrentPatch());
				}
				saveBank(currentBank, defaultBankFileName, true);
			}
			return true;
		}
		
		case R.id.option_osc_port_and_enable: {
			ipalog();
			return true;
		}
		
		case R.id.option_menu_help: {
			showHelp();
			return true;
		}
		case R.id.option_menu_about: {
			showAbout();
			return true;
		}
		case R.id.option_menu_preferences: {
			Intent i = new Intent(this, OLPreferenceActivity.class);
			startActivityForResult(i, R.id.option_menu_preferences);
			return true;
		}
		case R.id.option_menu_exit: {
			bailAlog("Save current bank before exit?");
			return true;
		}
		
		case R.id.option_menu_export_pattern: {
			if (algorithmEditorFragment != null) {
				algorithmEditorFragment.exportPattern2Algorithm();
			}
			return true;
		}
		
		case R.id.option_menu_clear_pattern: {
			if (wreckage != null) {
				wreckage.getState().setPattern(new ArrayList<SliceCell>());
			}
			if (algorithmEditorFragment != null) {
				algorithmEditorFragment.refreshPatternEditor();
			}
			return true;
		}
		
		default: {
//			Log.d("option", "Unhandled menu option"+Integer.toString(item.getItemId()));
		}
		}
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onPrepareOptionsMenu (Menu menu)
	{
		MenuItem miDel = menu.findItem(R.id.option_menu_del_algorithm);
		MenuItem miNew = menu.findItem(R.id.option_menu_new_algorithm);
		MenuItem miBranch = menu.findItem(R.id.option_menu_branch_algorithm);
		MenuItem miRen = menu.findItem(R.id.option_menu_ren_algorithm);
		MenuItem misDel = menu.findItem(R.id.option_menu_del_sequence);
		MenuItem misNew = menu.findItem(R.id.option_menu_new_sequence);
		MenuItem misBranch = menu.findItem(R.id.option_menu_branch_sequence);
		MenuItem misRen = menu.findItem(R.id.option_menu_ren_sequence);
		MenuItem expPat = menu.findItem(R.id.option_menu_export_pattern);
		MenuItem clrPat = menu.findItem(R.id.option_menu_clear_pattern);
		if (algorithmEditorFragment != null && algorithmEditorFragment.isVisible()) {
			if (miDel != null) miDel.setVisible(true);
			if (miNew != null) miNew.setVisible(true);
			if (miBranch != null) miBranch.setVisible(true);
			if (miRen != null) miRen.setVisible(true);
			if (misDel != null) misDel.setVisible(false);
			if (misNew != null) misNew.setVisible(false);
			if (misBranch != null) misBranch.setVisible(false);
			if (misRen != null) misRen.setVisible(false);
			if (expPat != null) expPat.setVisible(true);
			if (clrPat != null) clrPat.setVisible(true);
		} else {
			if (miDel != null) miDel.setVisible(false);
			if (miNew != null) miNew.setVisible(false);
			if (miBranch != null) miBranch.setVisible(false);
			if (miRen != null) miRen.setVisible(false);
			if (misDel != null) misDel.setVisible(true);
			if (misNew != null) misNew.setVisible(true);
			if (misBranch != null) misBranch.setVisible(true);
			if (misRen != null) misRen.setVisible(true);
			if (expPat != null) expPat.setVisible(false);
			if (clrPat != null) clrPat.setVisible(false);
		}
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onLowMemory ()
	{
		super.onLowMemory();
		Log.d("orbital", String.format("on low memory"));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onTrimMemory(int level)
	{
		super.onTrimMemory(level);
		Log.d("orbital", String.format("on trim memory %d", level));
	}

	protected void setPatchSelector(int cpInd) {
		if (cpInd < 0 || cpInd >= currentBank.numPatches()) {
			cpInd = 0;
		}
		for (int i=0; i<patchSelectAdapter.getCount(); i++) {
			Action p = patchSelectAdapter.getItem(i);
			if (p.getOp() == cpInd) {
				patchSelector.setSelection(i);
				break;
			}
		}
	}
	
	public void setPlayerSelectAdapter(Bank b) {
		playerSelectAdapter.clear();
		playerSelectAdapter.add(new MenuData<PlayMode>(PlayMode.VANILLA));
		playerSelectAdapter.add(new MenuData<PlayMode>(PlayMode.PATTERNATOR));
		playerSelectAdapter.add(new MenuData<PlayMode>(PlayMode.CHAOTICOR));
		if (b != null) {
			for (int i=0; i<b.numAlgorithms(); i++) {
				AlgoRhythm a = b.getAlgorithm(i);
				playerSelectAdapter.add(new MenuData<PlayMode>(PlayMode.AUTOMATOR, i, "Alg: "+a.getName()));
			}
		}
	}
	
	public void setPlayerSelectMode(PlayMode b, int d) {
		if (playerSelector != null && playerSelectAdapter != null) {
			int sel = 0;
			for (int i=0; i<playerSelectAdapter.getCount(); i++) {
				MenuData<PlayMode> m = playerSelectAdapter.getItem(i);
				if (m.getMode() == b && (b != PlayMode.AUTOMATOR || d == m.getData())) {
					sel  = i;
					break;
				}
			}
			playerSelector.setSelection(sel);
		}
	}

	private void displayPlaySelectMode(PlayMode b) {
	}
	
	@Override
	protected void setupPatchControls() {
		super.setupPatchControls();
		sequenceStepAdapter = new ArrayAdapter<Action>(this, R.layout.mw_spinner_item);
		sequenceStepAdapter.setDropDownViewResource(R.layout.mw_spinner_dropdown_item);
	}
	
	protected void setCurrentPatchItems(Bank b) {
		super.setCurrentPatchItems(b);
		setSequenceStepAdapter();
	}

	protected void setSequenceStepAdapter() {
		if (currentBank != null && sequenceStepAdapter != null) {
			currentBank.setPatchAdapterItems(sequenceStepAdapter);
			for (int i=0; i<currentBank.numAlgorithms(); i++) {
				AlgoRhythm a = currentBank.getAlgorithm(i);
				if (a != null) {
					sequenceStepAdapter.add(new Action(i+kAlgIndexBase, a.name));
				}
			}
		}
	}

	public Patch getCurrentPatch()
	{
		return currentPatch;
	}

	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public int countSamplePlayers() {
		return 1;
	}

	@Override
	public SamplePlayer getSamplePlayer(int i) {
		return wreckage ;
	}

	@Override
	public void startSamplePlayer(SamplePlayer p, boolean b) {

	}

	@Override
	public void stopSamplePlayer(SamplePlayer p) {

	}

	@Override
	public float getSampleRate() {
		return this.getCurrentMixer().getSampleRate();
	}

	public static float getTempo() {
		return ol.masterTempo;
	}

	public static boolean getShowSampleTimeBeats() {
		return ol.showSampleTimeBeats;
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
//		Log.d("result", "got actity "+Integer.toString(requestCode));
		switch(requestCode) {
		
		case EventID.CHOOSE_FINDER: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (wreckage != null && filePath != null) {
					Log.d("result", "got actity and all ok "+filePath);
					wreckage.setSamplePath(filePath, true);
					if (sampleEditorFragment != null) sampleEditorFragment.setSampleGfx4Pad(wreckage);
					File fp = new File(filePath);
					if (fp.getParent() != null) {
						lastSampleFolder = fp.getParent();
					}
				}
			}
			break;
		}

		case EventID.SANITIZE_FINDER: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (filePath != null) {
					BankSanitizer.fileBrowserResult(filePath);
				}
			}
			break;
		}
		
		case R.id.option_menu_load_bank: {
			if(resultCode == RESULT_OK) {
				String filePath = data.getExtras().getString("filePath");
				if (filePath != null) {
					currentBankFile = new File(filePath);
					currentBank = loadBank(currentBankFile);
					if (sequencerFragment != null) {
						sequencerFragment.setToBank(currentBank);
					}
					setPlayerSelectAdapter(currentBank);
					setCurrentPatchInd(0);
					loadPatchDisplayUpdate();
				}
			}
			break;
		}

		case R.id.option_menu_preferences: {
			doPreferences();
			break;
		}
		}
		
	}
	
	private void exportLoops(File sff, int n) {
		if (wreckage != null) {
			wreckage.exportLoops(sff, n, new Observer<AudioFile>() {

				@Override
				public void onCompleted() {

				}

				@Override
				public void onError(Throwable e) {

				}

				@Override
				public void onNext(AudioFile audioFile) {

				}
			});

			if (!wreckage.isPlaying()) {
				player.startWreckage(wreckage);
				playButton.setPlayState(true);
			}
		}		
	}
	/********************************************
	 * INTERFACE AND DIALOG HOOKS AND WRAPPERS
	 ********************************************/

	@Override
	protected File getStorageBaseDir() {
		File f = new File(Environment.getExternalStorageDirectory()+"/"+"mayaswell", "orbital");
		if (!f.exists()) {
			f.mkdirs();
			// also check noe and see if we should migrate
			File orig = getExternalFilesDir(null);
			Collection<File> existingContent = FileUtils.listFiles(orig, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			for (File oldFile: existingContent) {
				try {
					FileUtils.moveToDirectory(oldFile, f, false);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			MediaScannerConnection.scanFile(this, new String[] {f.toString()}, null, null);
		}
		return f;
	}


	private void showHelp()
	{
		Builder d = new AlertDialog.Builder(this);
		if (algorithmEditorFragment != null && algorithmEditorFragment.isVisible()) {
			d.setTitle(getResources().getString(R.string.help_title_player));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_player)));
		} else if (chaosEditorFragment != null && chaosEditorFragment.isVisible()) {
			d.setTitle(getResources().getString(R.string.help_title_chaos));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_chaos)));
		} else {
			d.setTitle(getResources().getString(R.string.help_title_main));
			d.setMessage(Html.fromHtml(getResources().getString(R.string.help_text_main)));
		}
		d.setPositiveButton(aboutButtonText(), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	private void showAbout()
	{
		int pbi = (int) Math.floor(Math.random()*aboutButtonText.length);
		PackageInfo pInfo;
		String versnm = "1.0.1";
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versnm = pInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		String appnm = getResources().getString(R.string.release_name);
		String appniknm = getResources().getString(R.string.release_nickname);
		
		Builder d = new AlertDialog.Builder(this);
		d.setTitle(appnm+", "+versnm);
		d.setMessage(Html.fromHtml(getResources().getString(R.string.about_text)));
		d.setPositiveButton(aboutButtonText[pbi], new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	protected boolean cleanVerifyCutAndRun(boolean andState)
	{
		if (currentBankFile != null) {
			saveCurrentBank(true, andState);
		} else {
			saveFialog("bank.olx", null, true, andState);
		}
		return false;
	}
	
	protected void ipalog() {
		if (ipView == null) {
			ipView  = new IPPortView(this);
			/*
			mwc:label="Input"
			mwc:labelWidth="@dimen/ipLabelWidth"
			mwc:itemHeight="@dimen/subElementHeight"
			mwc:hostWidth="@dimen/ipHostViewWidth"
			mwc:portWidth="@dimen/ipPortViewWidth"
			mwc:enableWidth="@dimen/ctlLockButtonWidth"
			mwc:labelStyle="@style/TPLabelText"
			mwc:itemStyle="@style/TPInputText"
			mwc:itemBackground="@drawable/input_text_bg"
			mwc:enableStyle="@style/TPToggleText"
			mwc:enableBackground="@drawable/sg_enable_green_bmp"
			*/
			ipView.setCurrentValue("localhost", "7000");
			ipView.setMode("hostUneditable");
			ipView.setListener(new IPPortView.Listener() {
				
				@Override
				public void portChanged(String host, String port, boolean enable) {
					oscModule.setInputPort(port);
					oscModule.setInputEnabled(enable);
				}

				@Override
				public void enableChanged(boolean b) {
					oscModule.setInputEnabled(b);
				}


			});

		}

		ipView.setCurrentValue(oscModule.getHost(), Integer.toString(oscModule.getPort()));

		Builder d = new AlertDialog.Builder(this);
		d.setTitle("Set OSC port");
		d.setMessage("Set OSC port");
		ViewGroup vp = (ViewGroup) ipView.getParent();
		if (vp != null) {
			vp.removeView(ipView);
		}
		d.setView(ipView);
		d.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	
	protected void renderalog(final String fnm)
	{
		if (saveFileEditText == null) {
			saveFileEditText = new EditText(this);
		}
		
		saveFileEditText.setHint(fnm);
		saveFileEditText.setText(fnm);
	
		Builder d = new AlertDialog.Builder(this);
		d.setTitle("Export Loops to ...");
		d.setMessage("Enter file name to save to");
		ViewGroup vp = (ViewGroup) saveFileEditText.getParent();
		if (vp != null) {
			vp.removeView(saveFileEditText);
		}
		d.setView(saveFileEditText);
		d.setPositiveButton("Export", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String tfnm = saveFileEditText.getText().toString();
				if (!tfnm.matches(".*\\.wav")) {
					tfnm = tfnm + ".wav";
				}
				String pnm = getCurrentBankBaseDir();
				File sffd = new File(pnm, "Rendered");
				if (!sffd.exists()) {
					sffd.mkdir();
				}
				File sff = new File(sffd, tfnm);
				dialog.dismiss();
				if (sff.exists()) {
					srslyRenderalog(sff, 1);
				} else {
					exportLoops(sff, 1);
				}
			}

		});
		d.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		});
		d.show(); 
	}

	protected void srslyRenderalog(final File sff, final int count)
	{
		Builder d = new AlertDialog.Builder(this);
		d.setTitle("Are you sure about that?");
		d.setMessage("'"+sff.getName()+"'"+" exists. Overwrite?");
		d.setPositiveButton("Totally", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				exportLoops(sff, count);
			}
		});
		d.setNegativeButton("Ooops", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton)
			{
				dialog.dismiss();
			}
		});
		d.show(); 
	}
	
	protected void saveCurrentBank(final boolean andQuit, final boolean andPatch)
	{
		if (currentBankFile != null && currentBank != null) {
			if (andPatch) {
				setCurrentPatchState(currentBank.get(currentPatchInd));
			}
			saveBank(currentBank, currentBankFile);
			if (andQuit) finish();
		}
	}

	public Bank loadBank(String fnm, boolean internal, boolean qq)
	{
		FileInputStream fp;
		Bank b = null;
		try {
			if (internal) {
				fp = openFileInput(fnm);
				b = new Bank();
				b.load(fp);
				fp.close();
				BankSanitizer.finish();
				setCurrentPatchItems(b);
				if (sequencerFragment != null) {
					b.setSequenceAdapterItems(sequenceSelectAdapter);
					sequencerFragment.setSequenceSelector(b.getSequence(0));
				}
			} else {
			}
		} catch (FileNotFoundException e) {
			if (qq) {
				e.printStackTrace();
				mafFalog("File exception while loading bank "+e.getMessage());
			}
		} catch (IOException e) {
			if (qq) {
				e.printStackTrace();
				mafFalog("IO exception while loading bank "+e.getMessage());
			}
		}
		return b;
	}
	
	public Bank loadBank(File file)
	{
		FileInputStream fp;
		Bank b = currentBank;
		try {
			fp = new FileInputStream(file);
			b = new Bank(false);
			b.load(fp);
			fp.close();
			
			BankSanitizer.finish();
			setCurrentPatchItems(b);
			if (sequencerFragment != null) {
				b.setSequenceAdapterItems(sequenceSelectAdapter);
				sequencerFragment.setSequenceSelector(b.getSequence(0));
			}
			if (algorithmEditorFragment != null) {
				algorithmEditorFragment.setToBank(b);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			mafFalog("File exception while loading bank "+e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			mafFalog("IO exception while loading bank "+e.getMessage());
		}
		return b;
	}
	
	@Override
	protected void setCurrentPatchState(Patch p)
	{
		if (p == null) {
			Log.d("wrecker", "current patch state is null");
			return;
		}
		p.tempo = masterTempo;
		p.state = wreckage.getState();
		Log.d("wrecker", "setpatchstate "+p.state.getSlices().size()+" slices");
		/*
		int i = 0;
		for (i=0; i<padSample.size(); i++) {
			PadSample ps = padSample.get(i);
			if (i>=p.padState.size()) {
				p.padState.add(ps.state);
			} else {
				if (p.padState.get(i) != ps.state) {
					p.padState.remove(i);
					p.padState.add(i, ps.state);
				}
			}
		}
		for (i=0; i<busMixer.bus.size(); i++) {
			Log.d("set patch state", String.format("bus %d", i));
			Bus b = busMixer.bus.get(i);
			if (i>=p.busState.size()) {
				p.busState.add(b.state);
			} else {
				if (p.busState.get(i) != b.state) {
					p.busState.remove(i);
					p.busState.add(i, b.state);
				}
			}
		}*/
	}
	
	private void doPreferences()
	{
		olPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		keepScreenAlive  = olPreferences.getBoolean("screenKeepAlive",true);
		defaultSampleSearchPath = olPreferences.getString("defaultSampleSearchPath",null);
		if (keepScreenAlive) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
	}

	/***************************************
	 * EVENT HANDLING
	 ***************************************/
	protected void onPeriodicUiUpdate() {
		if (sampleEditorFragment.isVisible() && wreckage != null && wreckage.isPlaying()) {
			sampleEditorFragment.updateSampleCursor();
		}
		
		if (beatCounter != null && wreckage != null && wreckage.isPlaying()) {
			beatCounter.setText(wreckage.getBeatCounterString());
		}
	}

	protected boolean startWreckage(PlayButton pb)
	{
//		Wreckage p = pb.getSample();
//		if (p.shouldStartPaused()) {
//			p.firePaused();
//			pb.setPausedState(p.isPaused());
//			return false;
//		} else {
			Log.d("orbital", "starting wrecker");
			player.startWreckage(wreckage);
			pb.setPlayState(wreckage.isPlaying());
			return true;
//		}
	}
	
	protected boolean stopWreckage(PlayButton pb)
	{
		player.stopWreckage(wreckage);
		if (pb != null) {
			pb.setPlayState(false);
		}
		wreckage.stop(false);
		return true;
	}

	public Wrecker getWrecker() {
		return wreckage;
	}

	public void setShowSampleTimeBeats(boolean b)
	{
		showSampleTimeBeats = b;
		if (sampleEditorFragment != null) sampleEditorFragment.setBeatMode(b);
	}
	
	protected void setPatch(Patch p)
	{
		if (p == null) {
			Log.d("orbital", String.format("set patch null"));
			setSubPanels(wreckage, null);
			return;
		}
		wreckage.setState(p.state);
		setTempo(p.tempo);
		final Patch fp = p;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				setSubPanels(wreckage, fp);
				masterTempoControl.setValue(fp.tempo);
				loadPatchDisplayUpdate();
			}
		});
	}

	private void setSubPanels(Wrecker w, Patch p) {
		Log.d("wrecker", "setting sub panels");
		if (sampleEditorFragment != null) {
			sampleEditorFragment.setToWrecker(p != null? w: null);
		}
		if (sliceEditorFragment != null) {
			sliceEditorFragment.setToWrecker(p != null? w: null);
		}
		if (algorithmEditorFragment != null) {
			algorithmEditorFragment.setToWrecker(p != null? w: null);
			algorithmEditorFragment.setToBank(currentBank);
		}
		if (chaosEditorFragment != null) {
			chaosEditorFragment.setTo(p!=null?p.state:null);
			chaosEditorFragment.setToBank(currentBank);
		}
		setPlayerSelectAdapter(currentBank);
		if (fxBusFragment != null) {
			fxBusFragment.setToWrecker(w);
		}
		Log.d("wrecker", "set sub panels");
	}

	private void loadPatchDisplayUpdate() {
		setPlayerSelectMode(currentPatch.state.playMode, currentPatch.state.algorithmId);
		/*
		if (padButton != null) {
			for (PadButton pb: padButton) {
				pb.invalidate();
			}
		}
		if (ribbonController != null) {
			ribbonController.invalidate();
		}
		refreshTouchPad();
		if (selectedPadButton != null) {
			selectPad(selectedPadButton);
		}
		checkStartStopSensors();
		*/
	}

	public void setTempo(float t)
	{
		masterTempo  = t;
		if (wreckage != null) {
			wreckage.setTempo(t);
		}
	}
	
	public void updateTempoDisplay()
	{
		if (masterTempoControl != null) {
			masterTempoControl.setValue(masterTempo);
		}
	}

	public void ccontrolUpdate(Slice currentSlice, int pan, float v, int from) {
		// TODO Auto-generated method stub
		
	}

	public void onMarkerChanged(Wrecker wrecker, int type, Slice s, int ind, long fr, float v) {
		if (sampleEditorFragment != null) {
			sampleEditorFragment.invalidateSlice(s);
			if (sliceEditorFragment != null && sampleEditorFragment.getAnalysisMode() == AnalysisMode.MANUAL) {
				sliceEditorFragment.displayPosition(s, s.position);
			}
		}
		if (sliceEditorFragment != null) {
			sliceEditorFragment.displaySliceStart(s, fr);
		}
		
	}

	public void onSliceSelected(Wrecker wrecker, int type, Slice m) {
		if (sliceEditorFragment != null) {
			sliceEditorFragment.setToSlice(m);
		}
		if (modControlFragment != null && wrecker != null) {
			wrecker.setLastSelectedSlice(m);
			modControlFragment.setEnvHost(wrecker);
			modControlFragment.setLFOHost(wrecker);
		}
	}


	public void onSliceSplit(Wrecker wrecker, Slice s, int ind) {
		if (sampleEditorFragment != null) {
			int n = wrecker.getState().getSlices().size();
			for (int i=0; i<n; i++) {
				if (i >= ind) {
					sampleEditorFragment.invalidateSlice(wrecker.getSlice(i));
				}
			}
			sampleEditorFragment.invalidateSampleView();
		}
	}

	public void onSliceMerged(Wrecker wrecker, Slice s, int ind) {
		if (sampleEditorFragment != null) {
			int n = wrecker.getState().getSlices().size();
			for (int i=0; i<n; i++) {
				if (i >= ind) {
					sampleEditorFragment.invalidateSlice(wrecker.getSlice(i));
				}
			}
			sampleEditorFragment.invalidateSampleView();
		}
	}
	
	public boolean setSliceStart(Slice slice, long v) {
		if (wreckage == null) {
			return false;
		}
		return wreckage.setSliceStart(slice, v);
	}

	public boolean setSlicePosition(Slice slice, float v) {
		if (wreckage == null) {
			return false;
		}
		return wreckage.setSlicePosition(slice, v);
	}

	public double getFramesPerBeat() {
		return 60.0*OLAudioMixer.sampleRate/masterTempo;
	}

	@Override
	public int lfoRateId(int xyci) {
		return CControl.lfoRateId(xyci);
	}

	@Override
	public int lfoPhaseId(int xyci) {
		return CControl.lfoPhaseId(xyci);
	}

	@Override
	public int lfoTargetDepthId(int xyci, int tid) {
		return CControl.lfoTargetDepthId(xyci, tid);
	}

	@Override
	public int envAttackTimeId(int xyci) {
		return CControl.envAttackTimeId(xyci);
	}

	@Override
	public int envDecayTimeId(int xyci) {
		return CControl.envDecayTimeId(xyci);
	}

	@Override
	public int envReleaseTimeId(int xyci) {
		return CControl.envReleaseTimeId(xyci);
	}

	@Override
	public int envSustainTimeId(int xyci) {
		return CControl.envSustainTimeId(xyci);
	}

	@Override
	public int envSustainLevelId(int xyci) {
		return CControl.envSustainLevelId(xyci);
	}

	@Override
	public int envTargetDepthId(int xyci, int tid) {
		return CControl.envTargetDepthId(xyci, tid);
	}

	@Override
	public ArrayAdapter<ResetMode> getLfoRstModeAdapter(LFO.Host h) {
		return rstModeAdapter;
	}

	@Override
	public ArrayAdapter<ResetMode> getEnvRstModeAdapter(Envelope.Host h) {
		return rstModeAdapter;
	}

	@Override
	public ArrayAdapter<LFWave> getLfoWaveAdapter(LFO.Host h) {
		return lfoWaveAdapter;
	}

	@Override
	public Patch clonePatch(Patch p) {
		return p != null? p.clone():null;
	}

	public AudioMixer getCurrentMixer() {
		return this.player;
	}

	@Override
	public ArrayAdapter<FX> getFXAdapter(Host h) {
		return fxAdapter;
	}

	public short onDisplayBus(Wrecker w, short b) {
		if (w != null) {
			w.setDisplayedBus(b);
		}
		if (fxBusFragment != null) {
			if (w != null) {
				fxBusFragment.setFXBusState(b, w.getDisplayedBusState());
			} else {
				fxBusFragment.setFXBusState(-1, null);
			}
		}
		return b;
	}

	public short onUseBus(Wrecker w, short b) {
		return b;
	}
	
	@Override
	public boolean alwaysTempoLock()
	{
		return true;
	}
	
	@Override
	public LFO newLFO()
	{
		LFO l = new LFO();
		l.tempoLock = true;
		l.trigger = ResetMode.ONSLICE;
		return l;
	}
	
	@Override
	public Envelope newEnv()
	{
		Envelope e = new Envelope();
		e.tempoLock = true;
		e.trigger = ResetMode.ONSLICE;
		return e;
	}

	public static OrbitalLaser getOL() {
		return ol;
	}
	
	public void setSequence(Sequence<Patch> p) {
		setCurrentSequence(p);
		currentSequenceStepIndex = 0;
	}
	
	public void startSequencer(boolean fromStart)
	{
		if (!sequencerRunning) {
			sequencerRunning = true;
			if (fromStart) {
				currentSequenceStepIndex = 0;
			}
			setSequenceStep();
			if (sequencerFragment != null) {
				sequencerFragment.showSequencePlayState(true);
			}
		}
	}
	
	public void stopSequencer()
	{
		if (sequencerRunning) {
			sequencerRunning = false;
			if (sequencerFragment != null) {
				sequencerFragment.showSequencePlayState(false);
			}
		}
	}
	
	public void onLoopEnd(Wrecker wrecker)
	{
		if (sequencerRunning && getCurrentSequence() != null) {
			if (currentSequenceStep == null || ++currentStepCount >= currentSequenceStep.count) {
				if (++currentSequenceStepIndex >= getCurrentSequence().countSteps()) {
					currentSequenceStepIndex = 0;
				}
				setSequenceStep();
			}			
		}
	}

	protected void setSequenceStep() {
		currentSequenceStep = getCurrentSequence().getStep(currentSequenceStepIndex);
		currentStepCount = 0;
		Log.d("sequencer", "got current count "+currentStepCount+" ... "+currentSequenceStepIndex);
		if (sequencerFragment != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					sequencerFragment.setCurrentStep(currentSequenceStepIndex);
				}
			});
		}
		if (currentSequenceStep != null) {
			if (currentSequenceStep.patchIdx >= 0) {
//				setPatch(currentSequenceStep.patch);
				if (currentSequenceStep.type == Sequence.Type.PATCH) {
					setCurrentPatchInd(currentSequenceStep.patchIdx);
				} else {
					setCurrentAlgorithmInd(currentSequenceStep.patchIdx);
				}
			}
		}
	}
	
	protected void delSequence(Sequence<Patch> s) {
		if (currentBank != null && s != null) {
			if (currentBank.numSequences() > 1) {
				int csi = currentBank.getSequenceIndex(s);
				if (csi < 0) {
					return;
				}
				csi = currentBank.deleteSequence(csi);
				if (csi >= 0) {
					setCurrentSequence(currentBank.getSequence(csi));
					if (sequencerFragment != null) {
						currentBank.setSequenceAdapterItems(sequenceSelectAdapter);
						sequencerFragment.setSequenceSelector(currentSequence);
					}
				}
			}
		}
	}

/**
 * clone: copies the working sequence, saves the current one, and selects the new one
 * !! the working sequence is cloned from the bank, not a reference to it
 */
	protected void branchSequence() {
		if (currentSequence != null) {
			Sequence<Patch> p = currentSequence.clone();
			p.name = currentBank.makeIncrementalSeqName(currentSequence.name);
			setCurrentSequence(p);
			currentBank.add(p);
			if (sequencerFragment != null) {
				currentBank.setSequenceAdapterItems(sequenceSelectAdapter);
				sequencerFragment.setSequenceSelector(currentSequence);
			}
			setCurrentSequence(p);
		}
	}
	
	protected void newSequence() {
		if (currentBank != null) {
			Sequence<Patch> s = new Sequence<Patch>(currentBank.makeIncrementalSeqName("default"));
			currentBank.add(s);
			if (s != null) {
				if (sequencerFragment != null) {
					currentBank.setSequenceAdapterItems(sequenceSelectAdapter);
					sequencerFragment.setSequenceSelector(currentSequence);
				}
				setCurrentSequence(s);
			}
		}
	}
	
	public void refreshSeqAdapterItems(Sequence<Patch> cs) {
		;
		;
	}
	
	public Sequence<Patch> getCurrentSequence() {
		return currentSequence;
	}
	
	public void setCurrentSequence(Sequence<Patch> s) {
		currentSequence = s;
		if (sequencerFragment != null) {
			sequencerFragment.setSequenceSelector(currentSequence);
		}
	}

	@Override
	public ArrayAdapter<ControllableInfo> getControllableAdapter() {
		return null;
	}
	
	private void setViewMode(ViewMode mode) {

		switch (mode) {
		case MAIN:
			showMainEditor();
			break;
		case ALGORITHM:
			showAlgorithmEditor();
			break;
		case CHAOS:
			showChaosEditor();
			break;
		}
		
	}

	public void showAlgorithmEditor()
	{
		if (algorithmEditorFragment == null) {
			algorithmEditorFragment = new AlgorithmEditorFragment();
			algorithmEditorFragment.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
			algorithmEditorFragment.setListener(new AlgorithmEditorFragment.Listener() {
				
				@Override
				public void patternChanged(ArrayList<SliceCell> p) {
					wreckage.setPattern(p);
				}
				
				@Override
				public void algSelected(AlgoRhythm p) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void algChanged(AlgoRhythm p) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void editorClosed() {
					showMainEditor();
				}

				@Override
				public void algDeleted(AlgoRhythm s) {
					setPlayerSelectAdapter(currentBank);
					setSequenceStepAdapter();
				}

				@Override
				public void algCreated(AlgoRhythm s) {
					setPlayerSelectAdapter(currentBank);
				}

				@Override
				public void algNameChanged(AlgoRhythm s) {
					setPlayerSelectAdapter(currentBank);
				}

				@Override
				public void groupChanged(Slice slice, int og, int id) {
					if (sliceEditorFragment != null) {
						sliceEditorFragment.setGroupSelect(slice);
					}
				}

				@Override
				public void onError(String msg) {
					mafFalog(msg);
				}

				@Override
				public void modeSelected(PlayMode p) {
					wreckage.setPlayMode(p);
					setPlayerSelectMode(p, wreckage.getCurrentAlgorithmId());
				}
			});
		}
		showFragment(algorithmEditorFragment);
		hideFragment(sliceEditorFragment);
		hideFragment(modControlFragment);
		hideFragment(fxBusFragment);
		hideFragment(sequencerFragment);
		hideFragment(chaosEditorFragment);
		
		RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		rl.addRule(RelativeLayout.BELOW, sampleEditorFragment.getView().getId());
		algorithmEditorFragment.getView().setLayoutParams(rl);

		algorithmEditorFragment.setToWrecker(wreckage);
		algorithmEditorFragment.setToBank(currentBank);
	}
	
	private void showChaosEditor() {
		if (chaosEditorFragment == null) {
			chaosEditorFragment = new ChaosEditorFragment();
			chaosEditorFragment.setupControlsAdapter(this, R.layout.mw_spinner_item, R.layout.mw_spinner_dropdown_item);
			chaosEditorFragment.setListener(new ChaosEditorFragment.Listener() {
			
			});
		}
		showFragment(chaosEditorFragment);
		hideFragment(sliceEditorFragment);
		hideFragment(modControlFragment);
		hideFragment(fxBusFragment);
		hideFragment(sequencerFragment);
		hideFragment(algorithmEditorFragment);
		
		RelativeLayout.LayoutParams rl = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		rl.addRule(RelativeLayout.BELOW, sampleEditorFragment.getView().getId());
		chaosEditorFragment.getView().setLayoutParams(rl);

		chaosEditorFragment.setTo(wreckage!=null? wreckage.state: null);
		chaosEditorFragment.setToBank(currentBank);
	}

	public void showMainEditor()
	{
		if (algorithmEditorFragment != null) {
			hideFragment(algorithmEditorFragment);
		}
		if (chaosEditorFragment != null) {
			hideFragment(chaosEditorFragment);
		}
		showFragment(sliceEditorFragment);
		showFragment(modControlFragment);
		showFragment(fxBusFragment);
		showFragment(sequencerFragment);
//		showFragment(playerFragment);
	}
	
	private int currentFragMainViewId = 0;
	public void showFragment(MWFragment f) {
		if (f == null) {
			return;
		}
		View v = f.getView();
		FragmentManager fm = getFragmentManager();
		if (v == null) {
			fm.beginTransaction().add(f, "fragment").commit();
			fm.executePendingTransactions();
			v = f.getView();
			v.setId(currentFragMainViewId ++);
		}
		if (v != null && v.getParent() == null) {
			mainLayout.addView(v);
		}
	}
	
	public void hideFragment(Fragment f)
	{
		if (f == null) {
			return;
		}
		View v = null;
		if ((v=f.getView()) == null) {
			return;
		}
		mainLayout.removeView(v);
	}

	public static boolean inCellEditMode() {
		return ol.algorithmEditorFragment != null && ol.algorithmEditorFragment.isVisible();
	}
	
	private void setCurrentAlgorithmInd(int a) {
		wreckage.setAlgorithm(a);
	}
}
