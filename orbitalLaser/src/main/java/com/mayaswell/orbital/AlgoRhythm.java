package com.mayaswell.orbital;

import com.mayaswell.util.Automaton;
import com.mayaswell.util.Automaton.Listener;
import com.mayaswell.util.Automaton.Node;

public class AlgoRhythm extends Automaton<SliceCell> {
	public String name;
	public AlgoRhythm(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
	public AlgoRhythm clone() {
		AlgoRhythm a = new AlgoRhythm("");
		copyInto(a);
		return a;
	}
	
	public void setCurrentNode4Id(int nid) {
		Node n = findNode(nid);
		if (n != null) {
			setCurrentNode(n);
		}
	}
	
	public void setNextNode4Id(int nid) {
		Node n = findNode(nid);
		if (n != null) {
			setNextNode(n);
		}
	}
}
