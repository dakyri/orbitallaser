package com.mayaswell.orbital.util;

public class FloatPair {
	public FloatPair(float f, float s) {
		first = f;
		second = s;
	}
	public float first;
	public float second;
}
