package com.mayaswell.orbital.util;

import android.util.Log;

public class MappedProbability extends DurationMap {
	private static final long serialVersionUID = 1L;
	private float baseProbability = 0;
	public static final float kMaxSlotValue = 99; // slots values for time mapping are in range [0, kMaxSlotValue]
	public static final float kDfltSlotValue = 50; // slots values for time mapping are in range [0, kMaxSlotValue]
	public static final float kDfltSlotDur = 0.5f; // slots values for time mapping are in range [0, kMaxSlotValue]
	
	public MappedProbability()
	{
		super();
	}
	
	public float getPr(float pos) {
		float sum2 = max();//sum();
		if (sum2 == 0) {
			return baseProbability;
		}
		return baseProbability*findValue(pos)/sum2;
	}

	public float getBaseProbability() {
		return baseProbability;
	}

	public void setBaseProbability(float baseProbability) {
		if (baseProbability < 0) baseProbability = 0;
		else if (baseProbability > 1) baseProbability = 1;
		this.baseProbability = baseProbability;
	}

	public void setTo(MappedProbability p) {
		setBaseProbability(p.baseProbability);
		clear();
		for (FloatPair fp: p) {
			add(new FloatPair(fp.first, fp.second));
		}
	}

}
