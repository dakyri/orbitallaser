package com.mayaswell.orbital.util;

import java.util.ArrayList;

import android.util.Log;


/**
 * maps a floating value against a position. array list of float pairs, first is (position+duration) of a region, second is the value of that region
 */
public class DurationMap extends ArrayList<FloatPair> {
	private static final long serialVersionUID = 1L;
	
	public static final float kMaxDurValue = 32;
	public static final float kMinDurValue = 0.1f;

	public DurationMap() {		
		super();
	}
	
	public synchronized void clear() {
		super.clear();
	}
	
	/**
	 * 
	 * @param p position to place it at
	 * @param v value represented for the range
	 * @param d duration
	 * @return true if success
	 */
	public synchronized boolean add(float p, float v, float d) {
		if (d <= 0 || p < 0) return false;
		float pd = p + d;
		if (size() == 0) {
			add(new FloatPair(pd, v));
			return true;
		}
		FloatPair fpt = get(size()-1);
		if (fpt.first <= p) {
			add(new FloatPair(pd, v));
			fpt.first = p;
			return true;
		}
		int i=0;
		int ins=-1;
		float lastFp = 0;
		float newpd = -1;
		float newv = -1;
		for (FloatPair fp: this ) {
			if (p < fp.first) { /* we insert here */
				if (p == lastFp) {
					if (pd == fp.first) {
						fp.second = v;
						return true;
					}
					ins = i; /* i is the insert index */
				} else { /* p > lastFP ... position i stays the same */
					if (pd < fp.first) {
						// splitting this one
						newpd = fp.first;
						newv = fp.second;
					}
					fp.first = p;
					ins = i+1;
				}
				break; 
			}
			lastFp = fp.first;
			i++;
		}
		if (ins < 0) {
			return false;
		}
		if (ins >= size()) {
			add(new FloatPair(pd, v));
		} else {
			add(ins, new FloatPair(pd, v));
		}
		if (newpd >= 0) {
			if (ins+1 >= size()) {
				add(new FloatPair(newpd, newv));
			} else {
				add(ins+1, new FloatPair(newpd, newv));				
			}
		}
		FloatPair fp = null;
		lastFp = 0;
		i = ins;
		while (i < size()) {
			fp = get(i);
			if (fp.first < lastFp) {
				remove(i);
			} else {
				lastFp = fp.first;
				i++;
			}
		}
		return false;
	}
	
	public synchronized int addAt(int pos, float v, float d) {
		FloatPair fp = new FloatPair(d, v);
		if (size() == 0) {
			super.add(fp);
			return 0;
		}
		if (pos < 0) {
			pos = 0;
		} else if (pos > size()) {
			pos = size();
		}
		FloatPair gp = (pos > 0)?super.get(pos-1):null;
		if (gp != null) {
			fp.first += gp.first;
		}
		for (int i=pos; i<size(); i++) {
			gp = super.get(i);
			if (gp != null) {
				gp.first += d;
			}
		}
		super.add(pos, fp);
		return pos;
	}

	public synchronized boolean remove4Pos(float p) {
		int i = findIndex4Pos(p);
		if (i < 0 || i >= size()) {
			return false;
		}
		remove(i);
		return true;
	}
	
	public synchronized FloatPair remove(int i) {
		if (i < 0 || i >= size()) {
			return null;
		}
		FloatPair p = super.remove(i);
		float d = p.first;
		if (i > 0 && size() > 0) {
			FloatPair q = super.get(i-1);
			d -= q.first;
		}
		for (int j=i; j<super.size(); j++) {
			FloatPair q = super.get(j);
			q.first -= d;
		}
		return p;
	}
	
	public synchronized boolean setVal(int i, float v) {
		FloatPair fp = get(i);
		if (fp == null) {
			return false;
		}
		fp.second = v;
		return true;
	}
	
	public synchronized boolean setDur(int i, float v) {
		if (v <= 0) {
			return false;
		}
		FloatPair fp = get(i);
		if (fp == null) {
			return false;
		}
		if (v < kMinDurValue) {
			return false;
		}
		FloatPair gp = get(i-1);
		if (gp != null) { // last entry should have zero length ... it's a null boundary entry
			if (v < gp.first + kMinDurValue) {
				return false;
			}
		}
		gp = get(i+1);
		if (gp != null) { // last entry should have zero length ... it's a null boundary entry
			if (v > gp.first - kMinDurValue) {
				return false;
			}
		}
		fp.first = v;
		/*
		float d0 = fp.first - gp.first;
		for (int j=i; i<super.size(); i++) {
			gp = get(j);
			if (gp != null) {
				gp.first += d0;
			}
		}
		*/
		return true;
	}
	
	public float getDur(int slot) {
		FloatPair fp = get(slot);
		if (fp == null) {
			return 0;
		}
		return fp.first;
	}

	
	private int findIndex4Pos(float position) {
		if (size() == 0) {
			return -1;
		}
		FloatPair fpt = get(size()-1);
		float le = fpt.first;
		if (le <= 0) {
			return -1;
		}
		if (position >= le) {
			position %= le;
		}
		int i=0;
		for (FloatPair fp: this ) {
			if (position < fp.first) {
				return i;
			}
			i++;
		}
		return -1; // shouldn't get here anyway
	}
	
	public synchronized float findValue(float position) {
		int i = findIndex4Pos(position);
		if (i < 0) {
			return 1;
		}
		return get(i).second;
	}

	public synchronized float sum() {
		float s = 0;
		for (FloatPair f: this) {
			s += f.second;
		}
		return s;
	}
	
	public synchronized float max() {
		float s = 0;
		for (FloatPair f: this) {
			if (f.second > s) {
				s = f.second;
			}
		}
		return s;
	}
	
	public synchronized FloatPair get(int i) {
		if (i<0 || i>=super.size()) {
			return null;
		}
		return super.get(i);
	}
	
	public synchronized int size() {
		return super.size();
	}
}
