package com.mayaswell.orbital.util;

public class RangedMP extends MappedProbability {

	private static final long serialVersionUID = 1L;

	private float rangeBase;
	private float rangeScope;
	private float rangeMin;
	private float rangeMax;
	private float c;
	private float w;
	
	public RangedMP(float rangeMin, float rangeMax) {
		super();
		rangeBase = 0.5f;
		rangeScope = 0.5f;
		this.rangeMax = rangeMax;
		this.rangeMin = rangeMin;
		setParameters();
	}
	
	private void setParameters() {
		w = rangeMax - rangeMin;
		c = rangeMin + rangeBase*w*(1-rangeScope);
	}

	public float getAdjustment() {
		return (float) (c + Math.random()*rangeScope*w);
	}

	public float getRangeBase() {
		return rangeBase;
	}

	public void setRangeBase(float rangeBase) {
		this.rangeBase = rangeBase;
		setParameters();
	}

	public float getRangeScope() {
		return rangeScope;
	}

	public void setRangeScope(float rangeScope) {
		this.rangeScope = rangeScope;
		setParameters();
	}
	
	public void setTo(RangedMP p) {
		super.setTo(p);
		rangeBase = p.rangeBase;
		rangeScope = p.rangeScope;
		rangeMax = p.rangeMax;
		rangeMin = p.rangeMin;
	}

}
