package com.mayaswell.orbital.widget;

import java.util.Timer;
import java.util.TimerTask;

import com.mayaswell.audio.SamplePlayer;
import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.orbital.Slice;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class OLSampleView extends com.mayaswell.widget.SampleView implements com.mayaswell.orbital.widget.IDragSource {
	protected int sliceMarkerColor = 0x000000ff;
	protected int selectedSliceColor = 0x881111ff;
	protected Paint sliceMarkerBrush = null;
	protected Paint selectedSliceBrush = null;
	private float sliceRectW = 50;
	private float sliceRectH = 50;
	private Paint sliceRegionBrush = null;
	private Slice tweakedSlice;
	private float sliceClassMarkH = 20;
	private Timer longTouchTimer = null;
	private final int longpressTimeDownBegin = 500;
	private int lastx0 = 0;
	private int lasty0 = 0;
	
	private int nBeatsPerRegion = 4;
	
	public OLSampleView(Context context) {
		super(context);
		setup(context, null, 0);
	}

	public OLSampleView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, 0);
	}

	public OLSampleView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		Log.d("sampleview", "setup ol");
		sliceMarkerBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		sliceMarkerBrush.setStyle(Paint.Style.STROKE);
		sliceMarkerBrush.setStrokeWidth(2);
		
		sliceRegionBrush  = new Paint(Paint.ANTI_ALIAS_FLAG);
		sliceRegionBrush.setStyle(Paint.Style.FILL);

		selectedSliceBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectedSliceBrush.setStyle(Paint.Style.FILL);
		
		if (attrs != null) {
			int id = attrs.getAttributeResourceValue(schemaName, "sv_selectedSliceColor", -1);
			if (id >= 0) {
				selectedSliceColor = getResources().getColor(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "sv_sliceMarkerColor", -1);
			if (id >= 0) {
				sliceMarkerColor = getResources().getColor(id);
			}
		}
		
		sliceMarkerBrush.setColor(sliceMarkerColor);
		sliceRegionBrush.setColor(sliceMarkerColor);
		selectedSliceBrush.setColor(selectedSliceColor);
		
		setGridMetric(4, 4, 2);
	}
	
	public boolean setToPlayer(SamplePlayer p)
	{
		boolean b = super.setToPlayer(p);
		setMarkerDisplay();
		return b;
	}
	
	public void setGridMetric(int nb, int maj, int min) {
		gridMajorSubdivision = maj; // beats per bar
		gridMinorSubdivision = min; // subdivisions per beat
		nBeatsPerRegion  = nb;
		setGridParameters();
	}
	
	@Override
	public void setGridParameters() {
		if (nBeatsPerRegion == 0 || gridMinorSubdivision == 0) {
			gridWidth = 0;
			return;
		}
		float bw = dur2w(regionLength)/nBeatsPerRegion;
		gridWidth = bw / gridMinorSubdivision;		
	}
	
	@Override
	protected void onDraw (Canvas canvas)
	{
//		Log.d("OLSampleView", "sv on draw");
		if (currentPlayer != null) {
			if (gridWidth > 0 && showGrid) {
				drawGrid(canvas);
			}
			if (pxRs < svW && pxRe > 0) {
//				if (pxRs < 0) pxRs = 0;
//				if (pxRe > svW) pxRe = svW;
				canvas.drawRect(pxRs, 0, pxRe, svH-scrollerHeight, regionBrush);
			}

			if (sampleBitmap != null) {
				canvas.drawBitmap(sampleBitmap, sampleBitmapSrc, svRect, null);
			}
			
			// draw selection
			WreckageState w = getWreckageState();
			if (w != null) {
				int i=0;
				for (Slice s: w.getSlices()) {
					drawSlice(canvas, s, i++);
				}
			}
			
			
			if (crsX >= 0 && crsX <= svW) {
				canvas.drawLine(crsX, 0, crsX, svH, cursorBrush);
			}
		}
		scrollerBack.setBounds(scrollerBackRect);	
		scrollerBack.draw(canvas);
		
		scrollerKnob.setBounds(scrollerKnobRect);	
		scrollerKnob.draw(canvas);
//		Log.d("OLSampleView", "sv on draw done");
	}
	
	private void drawSlice(Canvas canvas, Slice s, int i) {
		if (s == null || canvas == null) {
			return;
		}

		canvas.drawLine(s.frameX, 0, s.frameX, svH, sliceMarkerBrush);
		RectF r = s.getPositionedFrameRect();
		canvas.drawRect(r, sliceRegionBrush);
		canvas.drawText(Integer.toString(s.label), (r.left+r.right)/2, (r.top+r.bottom)/2, textBrush);
		
		if (s.selected) {
			WreckageState w = getWreckageState();
			Slice s2;
			if (w != null && (s2=w.getSlice(i+1)) != null) {
				canvas.drawRect(new RectF(s.frameX, 0, s2.frameX, svH), selectedSliceBrush);
			}	
		}
	}

	public void updateMarkerDisplay() {
		WreckageState w = getWreckageState();
		if (w != null) {
			int i=0;
			int n=w.getSlices().size();
			for (Slice s: w.getSlices()) {
				checkSlice(i++, n, s);
			}
		}
	}

	public void setMarkerDisplay() {
		WreckageState w = getWreckageState();
		if (w != null) {
			int i=0;
			int n=w.getSlices().size();
			for (Slice s: w.getSlices()) {
				setFrameRect(i++, n, s, frame2px(s.startFrame));
			}
		}
	}

	public void checkSlice(int i, int n, Slice s) {
		if (s.label <= 0) {
			s.label = i+1;
			float px = frame2px(s.startFrame);
			setFrameRect(i, n, s, px);
			invalidateSliceMarker(s);
		} else {
			float px = frame2px(s.startFrame);
			if (s.frameX != px) {
				invalidateSliceMarker(s);	
			} else {
				
			}
		}
	}

	private void setFrameRect(int i, int n, Slice s, float px) {
		s.frameX = px;
		s.label = i+1;
		if (i == 0) {
			s.setFrameRect(0, 0, sliceRectW, sliceRectH);
		} else if (i >= n-1) {
			s.setFrameRect(-sliceRectW, 0, 0, sliceRectH);
		} else {
			s.setFrameRect(-sliceRectW/2, 0, sliceRectW/2, sliceRectH);
		}
	}

	private void invalidateSliceMarker(Slice s) {
		RectF r = s.getFrameRect();
		float px = s.frameX;
		invalidate((int)(px+r.left-2), (int)(r.top-2), (int)(px+r.right+2), (int)(r.bottom+2));
		invalidate((int)px-2, (int)0, (int)px+2, (int)svH);
	}

	private WreckageState getWreckageState() {
		Wrecker wr = (Wrecker) currentPlayer;
		if (wr == null) {
			return null;
		}
		return wr.getState();
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		int action = e.getActionMasked();
		int x0 = (int) e.getX();
		int y0 = (int) e.getY();
		lastx0 = x0;
		lasty0 = y0;
		if (action == MotionEvent.ACTION_DOWN) {
			if (scrollerBackRect.contains(x0, y0)) {
				if (Math.abs(scrollerKnobRect.right-x0) < kScrollerRightEdgeMargin) {
					setScaling(true);
				} else {
					touchOffset = x0 - scrollerKnobRect.left;
					setScrolling(true);					
				}
			} else {
				Slice s = null;			
				if ((s=sliceStartRect(x0, y0)) != null) {
					tweakedSlice = s;
					touchOffset = (int) (x0-frame2px(s.startFrame));
					setSelecting(true);
				} else if ((s=sliceRegionRect(x0, y0)) != null) {
					selectSlice(s, e);
					startLongTouch(s);
					invalidate();
				}
			}
		} else if (action == MotionEvent.ACTION_POINTER_DOWN) {
			if (scrollerBackRect.contains(x0, y0)) {
				
			} else {
				Slice s = null;
				if ((s=sliceRegionRect(x0, y0)) != null) {
					selectSlice(s, e);
					invalidate();
				}
			}
		} else if (action == MotionEvent.ACTION_UP) {
			cancelLongTouch();
		}
		
		if (isSelecting) {
			int fr0=0;
			int fr1=0;
			float px0 = e.getX()-touchOffset;
			fr0 = (int) px2frame(px0);
			
			if (setSliceFrame(tweakedSlice, fr0)) {
			}
		} else if (isScrolling) {
			if (isScaling) {
				if (x0 > scrollerKnobRect.left+kMinimumScrollerWidth) {
					setScrollerL(getLenForScrollerW((int) (x0-scrollerKnobRect.left)));
					setMarkerDisplay();
				}
			} else {
				setScrollerX(getStartForScrollerX(x0-touchOffset));
				if (e.getPointerCount() > 1) {
					int x1 = (int) e.getX(1);
					if (x1 > x0) {
						setScrollerL(getLenForScrollerW(x1-x0));
						setMarkerDisplay();
					}
				} else {
					setMarkerDisplay();
				}
			}
			invalidate();
		}
		/*
		for (int i=0; i<e.getPointerCount(); i++) {
			float pnx = (((float)e.getX(i))/svW);
			float pny = 1-((float)e.getY(i))/svH;
			if (pnx < 0) pnx = 0; else if (pnx > 1) pnx = 1;
			if (pny < 0) pny = 0; else if (pny > 1) pny = 1;
		}
		*/
		if (action == MotionEvent.ACTION_UP) {
			if (isScrolling) {
				setScrolling(false);
				invalidate();
			}
			if (isSelecting) {
				setSelecting(false);
			}
			if (isScaling) {
				setScaling(false);
			}
		} else {
//			clearXYCATrack(tracked);
		}
		return true;
	}
	
	private OLSampleView self = this;

	private void startLongTouch(final Slice s) {
		if (!OrbitalLaser.inCellEditMode()) {
			return;
		}
		longTouchTimer = new Timer();
		longTouchTimer.schedule(new TimerTask(){

			@Override
			public void run() {
				if (sliceRegionRect(lastx0, lasty0) == s) {
					ClipData.Item item = new ClipData.Item(Integer.toString(s.ind));
					ClipData data = new ClipData("Slice "+Integer.toString(s.label), new String[] {"object/slice"}, item);
					SliceShadowBuilder shadowBuilder = new SliceShadowBuilder(s, densityMultiplier);
					startDrag(data, shadowBuilder, self, 0);
				}
			}
		}, longpressTimeDownBegin);
	}

	private void cancelLongTouch() {
		Timer t = longTouchTimer;
		longTouchTimer = null;
		if (t != null) {
			t.cancel();
		}
	}

	private void selectSlice(Slice s, MotionEvent e) {
		if (s == null) {
			return;
		}
		WreckageState ws = getWreckageState();
		if (ws == null) {
			return;
		}
		if (e.getPointerCount() <= 1) {
			ws.selectSlice(s, true, false);
			if (listener != null) {
				listener.onMarkerRegionSelected(s.type, s, s.ind);
			}
		} else {
			int i = e.getActionIndex();
			float xp = e.getX(i);
			float yp = e.getY(i);
			Slice si = sliceRegionRect((int)xp, (int)yp);
			if (si != null) {
				ws.selectSlice(si, !si.selected, true);
			}
		}
	}

	private boolean setSliceFrame(Slice s, int fr) {
		if (currentPlayer == null) {
			return false;
		}
		WreckageState ws = getWreckageState();
		if (fr < 0) {
			fr = 0;
		} else if (fr >= currentPlayer.nTotalFrames) {
			fr = currentPlayer.nTotalFrames;
		}
		int fx = (int) frame2px(fr);
		if (ws != null) {
			invalidateSliceMarker(tweakedSlice);
			if (tweakedSlice.type < 0) {
				setRegionStart(fr);
				if (listener != null) {
					listener.onMarkerChanged(0, tweakedSlice, 0, fr, 1);
					setMarkerDisplay();
					listener.onRegionStartChanged(fr);
					listener.onRegionLengthChanged(ws.getRegionEnd()-fr);
				}
			} else if (tweakedSlice.type > 0) {
				listener.onMarkerChanged(0, tweakedSlice, tweakedSlice.ind, fr, 1);
				setMarkerDisplay();
				long l = fr-ws.getRegionStart();
				setRegionLength(l);
				if (listener != null) {
					listener.onRegionLengthChanged(l);
				}
			} else {
				listener.onMarkerChanged(0, tweakedSlice, tweakedSlice.ind, fr, 1);
				setMarkerDisplay();
			}
			invalidate();
			return true;
		}
		return false;
	}

	private Slice sliceStartRect(int x0, int y0) {
		WreckageState ws = getWreckageState();
		if (ws != null) {
			return ws.sliceFrameContaining(x0, y0);
		}
		return null;
	}
	
	private Slice sliceRegionRect(int x0, int y0) {
		WreckageState ws = getWreckageState();
		if (ws != null) {
			return ws.sliceContaining(x0);
		}
		return null;
	}

	public long getRegionStart() {
		return regionStart;
	}

	public long getRegionLength() {
		return regionLength;
	}

	@Override
	public Object getObject4Tag(ClipData clip) {
		if (clip == null) return null;
		Wrecker wr = (Wrecker) currentPlayer;
		if (wr == null) return null;
		WreckageState ws = wr.getState();
		if (ws == null) return null;
		if (clip.getItemCount() == 0) return null;
		int ind = Integer.parseInt(clip.getItemAt(0).getText().toString());
		return ws.getSlice(ind);
	}

	@Override
	public Object getObject4Tag(ClipData clipData, String mimetype) {
		return getObject4Tag(clipData);
	}

	public void setLocked(boolean locked) {
	}

	public boolean isLocked() {
		return false;
	}

}
