package com.mayaswell.orbital.widget;

import com.mayaswell.orbital.FXBusState;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.widget.FXBusMaster.Listener;
import com.mayaswell.widget.BaseControl;
import com.mayaswell.widget.StringControl;
import com.mayaswell.widget.TeaPot;
import com.mayaswell.widget.StringControl.StringControlListener;
import com.mayaswell.widget.TeaPot.TeaPotListener;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

public class FXBusMaster extends BaseControl {

	public interface Listener
	{
		public void onGainChanged(FXBusState b, float amt);
		public void onPanChanged(FXBusState b, float amt);
		public void onEnableChanged(FXBusState b, boolean e);
		public void onNameChanged(FXBusState b, String s);
	}

	private StringControl busLabel = null;
	private TeaPot busGainControl = null;
	private TeaPot busPanControl = null;
	
	public FXBusMaster(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}
	
	public FXBusMaster(Context context, AttributeSet attrs) {
		super(context, attrs, -1);
		setup(context, attrs, -1);
	}
	
	public FXBusMaster(Context context) {
		super(context, null, -1);
		setup(context, null, -1);
	}
	
	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (R.layout.ol_fx_bus_master, this, true);
		busLabel  = (StringControl) findViewById(R.id.labelControl);
		busGainControl = (TeaPot) findViewById(R.id.gainControl);
		busPanControl = (TeaPot) findViewById(R.id.panControl);
		busLabel.setValueListener(new StringControlListener() {
			@Override
			public void onValueChanged(String v) {
				onSetBusName(v);
			}
		});
		busGainControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				onSetBusGain(v);
			}
		});
		busPanControl.setPotListener(new TeaPotListener() {
			@Override
			public void onValueChanged(float v) {
				onSetBusPan(v);
			}
		});
		
	}
	
	Listener listener = null;
	private FXBusState fxState = null;
	private int fxBusInd = -1;
	
	
	public boolean showingBus(int bus) {
		return bus == fxBusInd;
	}
	
	public void onSetBusPan(float v) {
		if (fxState != null) {
			fxState.pan = v;
		}
		if (listener != null) {
			listener.onPanChanged(fxState,  v);
		}
	}
	
	public void onSetBusGain(float v) {
		if (fxState != null) {
			fxState.gain = v;
		}
		if (listener != null) {
			listener.onGainChanged(fxState,  v);
		}
	}
	
	public void onSetBusName(String v) {
		if (fxState != null) {
			fxState.name = v;
		}
		if (listener != null) {
			listener.onNameChanged(fxState,  v);
		}
	}

	public void setBusPan(float v) {
		busPanControl.setValue(v);
	}
	
	public void setBusName(String v) {
		busLabel.setValue(v);
	}
	
	public void setBusGain(float v) {
		busGainControl.setValue(v);
	}
	
	public void setBusState(int i, FXBusState fxs) {
		fxBusInd  = i;
		fxState  = fxs;
		if (fxs != null) {
			setBusPan(fxs.pan);
			setBusGain(fxs.gain);
			setBusName(fxs.name);
		} else {
			setBusPan(0.0f);
			setBusGain(1.0f);
			setBusName(i>=0? "Bus "+i : "");
		}
	}
	
	public void setListener(Listener l) {
		listener = l;
	}

}
