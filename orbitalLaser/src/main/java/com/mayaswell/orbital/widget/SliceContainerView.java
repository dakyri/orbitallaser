package com.mayaswell.orbital.widget;

import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.Slice;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class SliceContainerView extends View {

	protected float sliceRectW = 40;
	protected float sliceRectH = 60;
	protected float densityMultiplier = 1;
	private Paint textBrush;
	private float textScaledPx;
	private Paint dataBrush;

	public SliceContainerView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public SliceContainerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public SliceContainerView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		densityMultiplier = context.getResources().getDisplayMetrics().density;
		textScaledPx = densityMultiplier * 10;
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		textBrush.setColor(0xff000000);
	
		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);
	}

	protected void drawSlice(Canvas c, RectF r, int gind, int label, float alpha) {
		int col = 0xff444444;
		if (gind >= 0) {
			if (gind >= OrbitalLaser.regginbow.length) {
				gind = OrbitalLaser.regginbow.length-1;
			}
			col = OrbitalLaser.regginbow[gind];
		}
		col = (col & 0xffffff) | ((((int)(255*alpha) & 0xff)) << 24);
		dataBrush.setColor(col);
		textBrush.setColor((col&0xffffff) ^ 0xffffffff);
		c.drawRect(r, dataBrush);
		c.drawText(Integer.toString(label), (r.left+r.right)/2, r.top+20, textBrush);
	}

	protected void drawSlice(Canvas c, RectF r, Slice s) {
		drawSlice(c, r, s.getGroup(), s.label, 1);
	}
}