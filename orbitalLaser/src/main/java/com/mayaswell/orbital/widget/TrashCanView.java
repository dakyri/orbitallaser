package com.mayaswell.orbital.widget;

import com.mayaswell.orbital.AlgoRhythm;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.SliceCell;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.SliceGroups.SliceGroup;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.widget.AutomatonView;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class TrashCanView extends View {

	private Paint dataBrush;
	private int inactiveColor;
	private int activeColor;
	private boolean isActive;
	private Bitmap icon=null;

	public TrashCanView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public TrashCanView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public TrashCanView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}
	
	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);
		inactiveColor = 0xff888888;
		activeColor = 0xfffd8f00;
		isActive = false;
		icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_action_discard);
		ri.set(0, 0, icon.getWidth(), icon.getHeight());
	}

	Rect rc = new Rect();
	Rect ri = new Rect();
	private Wrecker wrecker;
	@Override
	protected void onDraw(Canvas c) {
		if (isActive) {
			dataBrush.setColor(activeColor);
		} else {
			dataBrush.setColor(inactiveColor);
		}
		rc.set(0, 0, c.getHeight(), c.getHeight());
		c.drawRect(rc, dataBrush);
		rc.left = rc.top = 2;
		rc.right -= 2;
		rc.bottom -= 2;
		c.drawBitmap(icon, ri, rc, null);
	}
	
	@Override
	public boolean onDragEvent(DragEvent event) {
		int action = event.getAction();
		
		Object state = event.getLocalState();

		ClipDescription d = event.getClipDescription();
		switch (action) {
		
		case DragEvent.ACTION_DRAG_STARTED:
			break;
			
		case DragEvent.ACTION_DRAG_LOCATION:
		case DragEvent.ACTION_DRAG_ENTERED: {
			isActive = true;
			invalidate();
			break;
		}
		
		case DragEvent.ACTION_DRAG_ENDED:
		case DragEvent.ACTION_DRAG_EXITED: {
			isActive = false;
			invalidate();
			break;
		}
		
		case DragEvent.ACTION_DROP: {
			ClipData clipData = event.getClipData();
			if (state instanceof IDragSource) {
				if (state instanceof GroupView) {
					Object dragged = ((GroupView)state).getObject4Tag(clipData);
					if (dragged instanceof Slice) {
						Slice s = (Slice) dragged;
						wrecker.setGroup(s, -1);
						((GroupView) state).layoutSlices();
						((GroupView) state).invalidate();
						return true;
					}
				} else if (state instanceof PatternView) {
					Object dragged = ((PatternView)state).getObject4Tag(clipData, "object/index");
					if (dragged instanceof Integer) {
						((PatternView) state).deleteFromPattern(((Integer)dragged).intValue());
						return true;
					}
				} else if (state instanceof AlgoRhythmView) {
					AlgoRhythmView av = (AlgoRhythmView) state;
					AlgoRhythm a = av.getAlgoRythm();
					if (d != null) {
						if (d.hasMimeType("object/cellslot")) {
							Object draggedn = av.getObject4Tag(clipData, "object/node/index");
							Object draggedc = av.getObject4Tag(clipData, "object/cellslot/index");
							int nodei=-1;
							int cdi=-1;
							if (draggedn instanceof Integer) {
								nodei = ((Integer) draggedn).intValue();
							}
							if (draggedc instanceof Integer) {
								cdi = ((Integer) draggedc).intValue();
							}
							AlgoRhythm.Node n = a.getNode(nodei);
							if (n != null && cdi >= 0) {
								n.cellDataRem(cdi);
								av.forceAutomatonLayout();
								av.invalidate();
								return true;
							}
						} else if (d.hasMimeType("object/node")) {
							Object draggedn = av.getObject4Tag(clipData, "object/node/index");
							int nodei=-1;
							if (draggedn instanceof Integer) {
								nodei = ((Integer) draggedn).intValue();
							}
							AlgoRhythm.Node n = a.getNode(nodei);
							if (n != null && nodei >= 0) {
								a.removeNode(n);
								av.forceAutomatonLayout();
								av.invalidate();
								return true;
							}
							
						}
					}
				}
			}
			Log.d("trash can", "unknown drop item");
			break;
		}
		
		default:
			break;
		}
		return true;
	}
	
	public void setTo(Wrecker w) {
		wrecker = w;
		
		requestLayout();
		invalidate();
	}
	

}
