package com.mayaswell.orbital.widget;

import android.content.ClipData;

public interface IDragSource {
	public final boolean locked = false;
	Object getObject4Tag(ClipData clipData);
	public abstract void setLocked(boolean locked);
	public abstract boolean isLocked();
	Object getObject4Tag(ClipData clipData, String mimetype);
}
