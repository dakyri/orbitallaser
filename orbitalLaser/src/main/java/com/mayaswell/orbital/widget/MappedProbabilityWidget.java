package com.mayaswell.orbital.widget;

import com.mayaswell.audio.Sequence;
import com.mayaswell.audio.Sequence.Type;
import com.mayaswell.orbital.Patch;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.util.FloatPair;
import com.mayaswell.orbital.util.MappedProbability;
import com.mayaswell.orbital.widget.MappedProbabilityWidget.Listener;
import com.mayaswell.widget.LongTouchEditText;
import com.mayaswell.widget.RotaryPotView;
import com.mayaswell.widget.TeaPot.TeaPotListener;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.RelativeLayout.LayoutParams;

public class MappedProbabilityWidget extends RelativeLayout {

	public interface Listener {
		void onProbChanged(float f);
		void onSlotDurChanged(int slot, float v);
		void onSlotProbChanged(int slot, float v);
	}
	
    private final String schemaName = "http://schemas.android.com/apk/res-auto";

	private TextView label = null;
    private RotaryPotView globPrPot = null;
	private LongTouchEditText globPrView = null;
	private DurationMapView mapView = null;
    private RotaryPotView prPot = null;
	private LongTouchEditText prView = null;
    private RotaryPotView durPot = null;
	private LongTouchEditText durView = null;
	
	private Button addStepButton = null;
	private Button delStepButton = null;
	
	private MappedProbability map = null;
	private int selectedSlot = -1;
	
	protected String prValueDisplayFormat = "%1.0f";
	protected String valueDisplayFormat = "%1.2f";
	private boolean notifyTextChange = true;

	private Listener listener = null;
	
	protected int layoutResource = R.layout.mapped_probability_widget;
	
	public MappedProbabilityWidget(Context context) {
		super(context);
		setup(context, null);
	}

	public MappedProbabilityWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs);
	}

	public MappedProbabilityWidget(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs);
	}

	protected void setup(Context context, AttributeSet attrs) {
		LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
		li.inflate (layoutResource, this, true);
		
		label = (TextView)findViewById (R.id.mwCtlLabel);
		globPrView = (LongTouchEditText)findViewById (R.id.mwCtlEditText);
		globPrPot = (RotaryPotView)findViewById (R.id.mwCtlPot);
		
		mapView = (DurationMapView)findViewById(R.id.mwCtlMap);
		
		prView = (LongTouchEditText)findViewById (R.id.mwCtlEditText2);
		prPot = (RotaryPotView)findViewById (R.id.mwCtlPot2);	
		durView = (LongTouchEditText)findViewById (R.id.mwCtlEditText3);
		durPot = (RotaryPotView)findViewById (R.id.mwCtlPot3);
		
		addStepButton = (Button) findViewById(R.id.plusButton);
		delStepButton = (Button) findViewById(R.id.minusButton);
		
		if (mapView != null) {
			mapView.setListener(new DurationMapView.Listener() {
				@Override
				public void fpSelected(int g, FloatPair f) {
					showSelectedCell(g);
				}				
			});
		}
		
		if (globPrView != null) {
			globPrView.addTextChangedListener(new TextWatcher() {
				@Override
				public void afterTextChanged(Editable tv) {
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try {
						if (s.length() > 0) {
							float v = Float.parseFloat(s.toString());
							if (v < globPrPot.getMinVal()) v = globPrPot.getMinVal();
							else if (v > globPrPot.getMaxVal()) v = globPrPot.getMaxVal();
							globPrPot.setValue(v);
							onProbChanged(v);
						}
					} catch (NumberFormatException e) {
					}
				}
			});
		}
		if (globPrPot != null) {
			globPrPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
					globPrView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
					onProbChanged(v);
				}
			});
		}
		if (prPot != null) {
			prPot.setValueBounds(0, MappedProbability.kMaxSlotValue);
			prPot.setCenterValue(MappedProbability.kDfltSlotValue);
		}
		if (prView != null) {
			prView.addTextChangedListener(new TextWatcher() {
				@Override
				public void afterTextChanged(Editable tv) {
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try {
						if (s.length() > 0) {
							float v = Float.parseFloat(s.toString());
							if (v < prPot.getMinVal()) v = prPot.getMinVal();
							else if (v > prPot.getMaxVal()) v = prPot.getMaxVal();
							prPot.setValue(v);
							onSlotProbChanged(selectedSlot, v);
						}
					} catch (NumberFormatException e) {
					}
				}
			});
		}
		if (prPot != null) {
			prPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
					prView.setText(prValueDisplayFormat!=null?String.format(prValueDisplayFormat, v):Float.toString(v));
					onSlotProbChanged(selectedSlot, v);
				}
			});
		}
		if (durPot != null) {
			durPot.setValueBounds(0, MappedProbability.kMaxDurValue);
			durPot.setCenterValue(MappedProbability.kDfltSlotValue);
		}
		if (durView != null) {
			durView.addTextChangedListener(new TextWatcher() {

				@Override
				public void afterTextChanged(Editable tv) {
				}
	
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}
	
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try {
						if (s.length() > 0) {
							float v = Float.parseFloat(s.toString());
							if (v < durPot.getMinVal()) v = durPot.getMinVal();
							else if (v > durPot.getMaxVal()) v = durPot.getMaxVal();
							durPot.setValue(v);
							if (notifyTextChange) onSlotDurChanged(selectedSlot, v);
						}
					} catch (NumberFormatException e) {
					}
				}
			});
		}
		if (durPot != null) {
			durPot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
					durView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
					onSlotDurChanged(selectedSlot, v);
				}
			});
		}
		if (addStepButton != null) {
			addStepButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (map != null) {
						selectedSlot = map.addAt(selectedSlot, MappedProbability.kDfltSlotValue, MappedProbability.kDfltSlotDur);
						mapView.layoutSlices();
						mapView.requestLayout();
						mapView.invalidate();
					}
				}
			});
		}
		if (delStepButton != null) {
			delStepButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (map != null) {
						FloatPair p = map.remove(selectedSlot);
						mapView.layoutSlices();
						mapView.requestLayout();
						mapView.invalidate();
					}
				}
			});
		}
		if (attrs != null) {
			// ... possible extras are ..
			//   componentHeight
			//   potHeight
			//   potAmbit
			
			String lbl = attrs.getAttributeValue(schemaName, "label");
			if (lbl != null) {
				if (label != null) {
					label.setText(lbl);
				}
			}
			String displayPrecision = attrs.getAttributeValue(schemaName, "displayPrecision");
			if (displayPrecision != null) {
				setValueDisplayPrecision(Integer.parseInt(displayPrecision));
			}
			/*
			String valueBounds = attrs.getAttributeValue(schemaName, "valueBounds");
			if (valueBounds != null) {
				String [] valueBoundsA = valueBounds.split(",");
				if (valueBoundsA.length >=2) {
					float floatBoundsA[] = new float[valueBoundsA.length];
					int i=0;
					for (String s: valueBoundsA) {
						floatBoundsA[i++] = Float.parseFloat(s);
					}
					setValueBounds(floatBoundsA[0], floatBoundsA[floatBoundsA.length-1]);
					setCenterValue(floatBoundsA[floatBoundsA.length-2]);
				}
			}*/
			int px = -1;
			int lwr = attrs.getAttributeResourceValue(schemaName, "labelWidth", -1);
			if (lwr >= 0) {
				px = getResources().getDimensionPixelSize(lwr);
			} else {
				String labelWidth = attrs.getAttributeValue(schemaName, "labelWidth");
				if (labelWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0 && label != null) {
				LayoutParams params = (LayoutParams) label.getLayoutParams();
				params.width = px;
			}
			
			/*
			int vwr = attrs.getAttributeResourceValue(schemaName, "viewWidth", -1);
			if (vwr >= 0) {
				px = getResources().getDimensionPixelSize(vwr);
			} else {
				px = 0;
				String viewWidth = attrs.getAttributeValue(schemaName, "viewWidth");
				if (viewWidth != null) { // if we have to do this we've probably set things up wrongly
				}
			}
			if (px > 0 && view != null) {
				LayoutParams params = (LayoutParams) view.getLayoutParams();
				params.width = px;
			}*/
		}
	}

	protected void showSelectedCell(int g) {
		selectedSlot = g;
		if (mapView != null) {
			mapView.invalidate();
		}
		FloatPair fp = map.get(selectedSlot);
		if (fp != null) {
			setPrValue(fp.second);
			setDurValue(fp.first);
		}
	}

	public MappedProbability getMap() {
		return map;
	}

	public void setMap(MappedProbability map) {
		this.map = map;
		if (map != null) {
			setGlobPrVal(map.getBaseProbability());
		} else {
			setGlobPrVal(0);
		}
		mapView.setTo(map);
	}
	
	/**
	 *  sets value for both pot and value viewer ... expects to be called from UI thread
	 * @param v
	 */
	public void setGlobPrVal(float v)
	{
		if (globPrPot != null) {
			if (v < globPrPot.getMinVal()) v = globPrPot.getMinVal();
			else if (v > globPrPot.getMaxVal()) v = globPrPot.getMaxVal();
			globPrPot.setValue(v);
		}
		if (globPrView != null) {
			notifyTextChange  = false;
			globPrView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
			notifyTextChange = true;
		}
	}
	
	private void setDurValue(float v) {
		if (durPot != null) {
			if (v < durPot.getMinVal()) v = durPot.getMinVal();
			else if (v > durPot.getMaxVal()) v = durPot.getMaxVal();
			durPot.setValue(v);	
		}
		if (durView != null) {
			notifyTextChange  = false;
			durView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
			notifyTextChange = true;
		}
	}

	private void setPrValue(float v) {
		if (prPot != null) {
			if (v < prPot.getMinVal()) v = prPot.getMinVal();
			else if (v > prPot.getMaxVal()) v = prPot.getMaxVal();
			prPot.setValue(v);
		}
		if (prView != null) {
			notifyTextChange  = false;
			prView.setText(valueDisplayFormat!=null?String.format(valueDisplayFormat, v):Float.toString(v));
			notifyTextChange = true;
		}
	}

   public void setListener(Listener l )
    {
    	listener  = l;
    }
    
	protected void onSlotProbChanged(int slot, float v) {
		if (map != null) {
			if (notifyTextChange && listener != null) {
				listener.onSlotProbChanged(slot, v);
			}
			map.setVal(slot, v);
			mapView.invalidate();
		}
	}

	protected boolean onSlotDurChanged(int slot, float v) {
		if (map != null) {
			if (map.setDur(slot, v)) {
				if (notifyTextChange && listener != null) {
					listener.onSlotDurChanged(slot, v);
				}
				mapView.layoutSlices();
				mapView.invalidate();
				return true;
			} else {
//				setDurValue(map.getDur(slot));
			}
		}
		return false;
	}

	private void onProbChanged(float v)
	{
		if (map != null) {
			if (notifyTextChange && listener != null) {
				listener.onProbChanged(v);
			}
			map.setBaseProbability(v);
		}
	}
	
	public void setValueDisplayPrecision(int i)
	{
		if (i < 1) i = 1; else if (i > 5) i = 5;
		valueDisplayFormat = "%1."+Integer.toString(i)+"f";
	}
	

}
