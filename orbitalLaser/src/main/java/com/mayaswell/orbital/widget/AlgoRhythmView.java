package com.mayaswell.orbital.widget;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.mayaswell.orbital.AlgoRhythm;
import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.SliceCell;
import com.mayaswell.orbital.SliceGroups;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.SliceGroups.SliceGroup;
import com.mayaswell.orbital.Wrecker;
import com.mayaswell.util.Automaton;
import com.mayaswell.util.Automaton.Node;
import com.mayaswell.util.Automaton.NodeConnection;
import com.mayaswell.widget.AutomatonView;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

public class AlgoRhythmView extends AutomatonView<SliceCell> implements IDragSource {

	private Wrecker wrecker = null;
	private float baseCellItemWidth = 60;
	private float minCellItemWidth = 15;
	private float maxCellItemWidth = 100;
	protected float cellItemHeight = 90;
	private Listener listener = null;
	private int highlightCellSlotId = -1;
	
	private int cellOutlineColPossible = 0xffff0000;
	private int cellOutlineCol = 0xff000000;
	
	protected float densityMultiplier = 1;
	private Timer longTouchTimer = null;
	private final int longpressTimeDownBegin = 500;
//	private float thisX0 = 0;
//	private float thisY0 = 0;
	private int lastNodeId = -1;
	private int lastCellSlotId = -1;
	private float dfltInsertLength = 0.5f;
	private int touchNodeId = -1;
	private int touchCellSlotId = -1;
	private Paint sliceLabelBrush = null;
	private TrashCanView trashView = null;
	private Rect trashRect = new Rect(0,0,0,0); // location of trash can in screen coords
	
	public interface Listener extends AutomatonView.Listener {
		AlgoRhythm getCurrentAlgorithm();
	}

	public AlgoRhythmView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public AlgoRhythmView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public AlgoRhythmView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}
	
	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		densityMultiplier = context.getResources().getDisplayMetrics().density;
		sliceLabelBrush  = new Paint(Paint.ANTI_ALIAS_FLAG);
		sliceLabelBrush.setTextSize(textScaledPx);
		sliceLabelBrush.setTextAlign(Align.CENTER);
		sliceLabelBrush.setColor(textCol);
	}
	
	protected void setupTrashCan() {
		try {
			RelativeLayout v = (RelativeLayout) getParent();
			if (v != null) {
				trashView  = (TrashCanView) v.findViewById(R.id.trashCan);
				if (trashView != null) {
					int li[] = new int[2];
					trashView.getLocationOnScreen(li);
					trashRect.set(li[0], li[1], li[0]+trashView.getMeasuredWidth(), li[1]+trashView.getMeasuredHeight());
				}
			}
			
		} catch (ClassCastException ex) {
			
		}
	}

	@Override
	protected void drawCell(Canvas canvas, Automaton<SliceCell>.Node n) {
		ArrayList<SliceCell> cells = n.getPattern();
		int i=0;
		float beats = 0;
		for (SliceCell s: cells) { // XXX on syncronization
			int gind = s.getId();
			beats += s.getDuration();
			fillBrush.setColor((OrbitalLaser.regginbow[gind] & 0xffffff) | ((((int)(255*s.getGain()) & 0xff)) << 24) );
			if (i < n.cellItemRect.size()) {
				RectF r = n.cellItemRect.get(i);
				if (r != null) {
					canvas.drawRect(r, fillBrush);
					if (n.getId() >= 0 && n.getId() == highlightNodeId && i == highlightCellSlotId) {
						outlineBrush.setColor(cellOutlineColPossible);
						outlineBrush.setStrokeWidth(2);
					} else {
						outlineBrush.setStrokeWidth(1);
						outlineBrush.setColor(cellOutlineCol);
					}
					canvas.drawRect(r, outlineBrush);
					canvas.drawText(Integer.toString(gind), (r.left+r.right)/2, r.top+20, sliceLabelBrush);
				}
			}
			i++;
		}
		if (n != automaton.getRoot()) {
			float xt0 = n.nodeRect.left+cellLabelWidth/2;
			float yt0 = n.nodeRect.bottom-5;
			canvas.rotate(90, xt0, yt0);
			canvas.drawText(String.format("%.3f", beats), xt0, yt0, textBrush);
			canvas.rotate(-90, xt0, yt0);
		}
	}

	@Override
	protected void allocateCellRect(Automaton<SliceCell>.Node n, float x, float y) {
		float cellX = x+cellMarginX+cellLabelWidth;
		float cellY = y+cellMarginY;
		float cx = cellX;
		float cy = cellY;
		n.cnxRect.clear();
		n.cellItemRect.clear();
		ArrayList<SliceCell> cells = n.getPattern();
		if (cells.size() > 0) {
			for (SliceCell s: cells) { // XXX on syncronization
				float ciw = s.getDuration() * baseCellItemWidth ;
				if (ciw < minCellItemWidth) ciw = minCellItemWidth;
				if (ciw > maxCellItemWidth) ciw = maxCellItemWidth;
				RectF r = new RectF(cx, cy, cx+ciw, cy+cellItemHeight);
				n.cellItemRect.add(r);
//				Log.d("allocate", "cell "+r.toShortString());
				cx += ciw;
			}
			n.cellRect.set(cellX, cellY, cx, cy+cellItemHeight);
			cy += cellItemHeight+cellMarginY;
		}
		float cx2 = cellX;
		for (int i=0; i<n.countCnx(); i++) {
			Automaton<SliceCell>.NodeConnection cnx = n.getCnx(i);
			if (cnx != null) {
				RectF r = new RectF(cx2 ,cy, cx2+cnxWidth, cy+cnxHeight);
				n.cnxRect.add(r);
//				Log.d("allocate", "cnx "+r.toShortString());
				cx2 += cnxWidth;
			}
		}
		RectF r = new RectF(cx2 ,cy, cx2+cnxWidth, cy+cnxHeight);
		n.cnxRect.add(r);
//		Log.d("allocate", "cnx 0 "+r.toShortString());
		cx2 += cnxWidth;
		n.nodeRect.set(x, y, Math.max(cx, cx2)+cellMarginX, cy+cnxHeight);
//		Log.d("allocate", "node "+n.nodeRect.toShortString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean onDragEvent(DragEvent event) {
		int action = event.getAction();
		float x = event.getX()+getScrollX();
		float y = event.getY()+getScrollY();
		
		int nodeId = -1;
		int cellSlotId = -1;
		
		nodeId = getNodeId4(x, y);
		cellSlotId = getCellDataId4(nodeId, x, y);
		Object state = event.getLocalState();

		boolean isSliceGroup;
		ClipDescription d = event.getClipDescription();
		if (d == null) {
			isSliceGroup = false;
		} else {
			isSliceGroup = (d.hasMimeType("object/slice") ||  d.hasMimeType("object/group") || d.hasMimeType("object/cellslot"));
		}
		switch (action) {
		
		case DragEvent.ACTION_DRAG_STARTED:
			break;
			
		case DragEvent.ACTION_DRAG_LOCATION:
		case DragEvent.ACTION_DRAG_ENTERED: {
			if (isSliceGroup) {
				highlightNodeId = nodeId;
				highlightCellSlotId = cellSlotId;
//				Log.d("algview", "location is in "+nodeId+", "+cellSlotId);
			} else {
				highlightNodeId = -1;
				highlightCellSlotId = -1;
			}
			invalidate();
			break;
		}
		
		case DragEvent.ACTION_DRAG_ENDED:
		case DragEvent.ACTION_DRAG_EXITED: {
			highlightNodeId  = -1;
			highlightCellSlotId  = -1;
			invalidate();
			break;
		}
		
		case DragEvent.ACTION_DROP: {
			SliceGroup sg = null;
			Slice s = null;
			SliceCell sc = null;
			AlgoRhythm.Node n=null;
//			Log.d("alg view", String.format("drop %d %d", nodeId, cellSlotId));
			if (state instanceof IDragSource) {
				Object dragged = ((IDragSource)state).getObject4Tag(event.getClipData());
				if (dragged instanceof SliceGroup) {
					sg = (SliceGroup) dragged;
				} else if (dragged instanceof Slice) {
					s = (Slice) dragged;
				} else if (dragged instanceof SliceCell) {
					sc = (SliceCell) dragged;
				} else if (dragged instanceof Automaton.Node) {
					n = (AlgoRhythm.Node) dragged;
				}
			}
			if (wrecker == null) return true;
			if (s != null) {
				sg = wrecker.getGroup(s.getGroup());
			}
			if (sg != null) {
				Log.d("algorithm", "drop "+sg.getId());
				if (automaton == null) {
					if (listener != null) {
						setTo(listener.getCurrentAlgorithm());
					}
					if (automaton == null) {
						return true;
					}
				}
				if (nodeId >= 0) {			
					if ((n = automaton.getNode(nodeId)) == null) {
						n = automaton.addNode();
					}
					int addId = cellSlotId+1;
					if (addId >= 0) {
						n.addCellData(addId, new SliceCell(sg.getId(), dfltInsertLength, 1));
					} else {
						n.addCellData(new SliceCell(sg.getId(), dfltInsertLength, 1));
					}
				} else {
					n = automaton.addNode();
					n.addCellData(new SliceCell(sg.getId(), dfltInsertLength, 1));
				}
				Log.d("algorithm", "handled drop, nodeID "+nodeId);
				layoutAutomaton(automaton);
				invalidate();
			} else if (sc != null) {
				int addId = cellSlotId+1;
				if (nodeId >= 0 && addId >= 0) {
					Object draggedn = ((IDragSource)state).getObject4Tag(event.getClipData(), "object/node/index");
					Object draggedc = ((IDragSource)state).getObject4Tag(event.getClipData(), "object/cellslot/index");
					Object draggedm = ((IDragSource)state).getObject4Tag(event.getClipData(), "object/op");
					int dropNodeId=-1;
					int dropCellSlotId=-1;
					boolean isCopyOp = false;
					if (draggedn instanceof Integer) {
						dropNodeId = ((Integer) draggedn).intValue();
					}
					if (draggedc instanceof Integer) {
						dropCellSlotId = ((Integer) draggedc).intValue();
					}
					if (draggedm instanceof String) {
						isCopyOp = ((String) draggedm).equals("copy");
					}
					if (nodeId == dropNodeId) {
						if ((n = automaton.getNode(nodeId)) == null) {
							return true;
						}
						if (isCopyOp) {
							Log.d("automaton", String.format("doing a copy %d > %d", dropCellSlotId, cellSlotId));
							n.addCellData(addId, sc.clone());
						} else {
							Log.d("automaton", String.format("doing a move %d > %d", dropCellSlotId, cellSlotId));
							n.cellDataMov(dropCellSlotId, addId);
						}
						layoutAutomaton(automaton);
						invalidate();
					} else {
						Log.d("automaton", String.format("failed drop, no match node id %d", cellSlotId));
					}
				}
			} else if (n != null) {
				boolean isCopyOp = false;
				Object draggedm = ((IDragSource)state).getObject4Tag(event.getClipData(), "object/op");
				if (draggedm != null && draggedm instanceof String) {
					isCopyOp = ((String) draggedm).equals("copy");
				}
				if (isCopyOp) {
					AlgoRhythm.Node nn = n.clone();
					nn.setId(automaton.countNodes());
					automaton.addNode(nn);
					layoutAutomaton(automaton);
					invalidate();
				}
			} else {
				Log.d("automaton", String.format("failed drop, null sc %d", cellSlotId));
			}

			break;
		}
		
		default:
			break;
		}
		return true;
	}

	protected int getCellDataId4(int g, float x, float y) {
		if (automaton == null) return -1;
		if (g == -1) return -1;
		Automaton<SliceCell>.Node n = automaton.getNode(g);
		if (n != null) {
			for (int i=0; i<n.cellItemRect.size(); i++) {
				RectF r = n.cellItemRect.get(i);
				if (r.contains(x, y)) {
					return i;
				}
			}
		} else {
			Log.d("alg view", "non existent node");
		}
		return -1;
	}

	protected boolean checkConnectionDelete(MotionEvent e) {
		float thisX0 = e.getRawX();
		float thisY0 = e.getRawY();
		int action = e.getActionMasked();
		if (trashView == null) {
			setupTrashCan();
		}
//		Log.d("alg", "check connection delete "+thisX0+", "+thisY0+" / "+trashRect.toShortString());
		if (action == MotionEvent.ACTION_UP && trashRect != null && trashRect.contains((int)thisX0, (int)thisY0)) {
			return true;
		}
		return false;
	}
	
	protected boolean onTouchCellData(int nodeId, MotionEvent e, boolean clear) {
		if (clear) {
			cancelLongTouch();
		}
		if (trashView == null) {
			setupTrashCan();
		}
		float thisX0 = e.getX() + getScrollX();
		float thisY0 = e.getY() + getScrollY();
		
		lastNodeId = nodeId;
		lastCellSlotId = getCellDataId4(nodeId, thisX0, thisY0);
		int action = e.getActionMasked();
		if (lastNodeId >= 0) {
			if (listener != null) listener.nodeSelected(nodeId);
			if(lastCellSlotId >= 0) {
				if (listener != null) listener.cellSelected(nodeId, lastCellSlotId);
			}
		}
		if (action == MotionEvent.ACTION_DOWN) {
			touchNodeId = lastNodeId;
			touchCellSlotId = lastCellSlotId;
			if (lastNodeId >= 0) {
				startLongTouch(lastNodeId, lastCellSlotId);
			}
			Log.d("algorithm", String.format("onTouch() down %d %d %d %d", touchNodeId, lastNodeId, touchCellSlotId, lastCellSlotId));
		} else if (action == MotionEvent.ACTION_UP) {
			if (isMakingConnection) {
				
			}
			touchNodeId = -1;
			touchCellSlotId = -1;
			cancelLongTouch();
		} else if (action == MotionEvent.ACTION_MOVE) {
			if (isMakingConnection) {
				if (trashView != null) {
					if (trashRect.contains((int)e.getRawX(), (int)e.getRawY())) {
						
					}
				}
			} else {
				if (touchCellSlotId >= 0 && touchCellSlotId != lastCellSlotId) {
					Automaton<SliceCell>.Node s = automaton.getNode(nodeId);
					if (s == null) return true;
					final SliceCell sc = s.getCellData(touchCellSlotId);
					if (sc == null) return true;
					Log.d("algorithm", String.format("slot move %d %d %d %d", touchNodeId, lastNodeId, touchCellSlotId, lastCellSlotId));
					startDragCell(touchNodeId, touchCellSlotId, sc, "move");
					cancelLongTouch();
				} else if (touchNodeId >= 0 && touchNodeId != lastNodeId) {
					Log.d("automaton", String.format("%b %d %b move node", isMakingConnection, nodeId, cnxFromNode == null));
					if (!isMakingConnection && cnxFromNode == null) {
						startDragNode(touchNodeId, "move");
					}
					cancelLongTouch();
				}
			}
		}
		return false;
	}
	public void setTo(Wrecker w) {
		wrecker = w;
		dfltInsertLength  = (wrecker == null || wrecker.gridQuant <= 0)? 0.5f: (1/((float)wrecker.gridQuant));
	}
	
	public void setTo(AlgoRhythm w) {
		automaton = w;
		layoutAutomaton(automaton);
		invalidate();
	}

	public void setListener(Listener listener) {
		this.listener = listener;
		super.setListener(listener);
	}

	private AlgoRhythmView self = this;
//	private int dropCellSlotId = 0;
//	private int dropNodeId = 0;
	
	private void startLongTouch(final int nodeId, final int cellSlotId) {
		if (nodeId >= 0) {
			Log.d("algorithm", "long touch on node");
			if (automaton == null) {
				return;
			}
			Automaton<SliceCell>.Node s = automaton.getNode(nodeId);
			if (s == null) return;
			if (cellSlotId >= 0) {
				final SliceCell sc = s.getCellData(cellSlotId);
				if (sc == null) return;
				longTouchTimer = new Timer();
				longTouchTimer.schedule(new TimerTask(){
					@Override
					public void run() {
						if (lastNodeId == nodeId && lastCellSlotId == cellSlotId) {
							startDragCell(nodeId, cellSlotId, sc, "copy");
						}
					}

				}, longpressTimeDownBegin);
			} else {
				longTouchTimer = new Timer();
				longTouchTimer.schedule(new TimerTask(){
					@Override
					public void run() {
						if (lastNodeId == nodeId) {
							startDragNode(nodeId, "copy");
						}
					}

				}, longpressTimeDownBegin);
			}
		} else {
		}
	}
	
	protected void startDragCell(final int nodeId, final int cellSlotId, final SliceCell sc, final String op) {
		String itemContent = "cellslot/"+nodeId+"/"+cellSlotId;
		if (op != null) {
			itemContent += "/" + op;
		}
		ClipData.Item item = new ClipData.Item(itemContent);
		ClipData data = new ClipData("Cell"+" "+Integer.toString(cellSlotId), new String[] {"object/cellslot"}, item);
		TextnumShadowBuilder shadowBuilder = new TextnumShadowBuilder(op, cellSlotId, SliceGroups.getGroupColor(sc.getId()), densityMultiplier);
		startDrag(data, shadowBuilder, self, 0);
	}

	protected void startDragNode(final int nodeId, final String op) {
		String itemContent = "node/"+nodeId;
		if (op != null) {
			itemContent += "/" + op;
		}
		ClipData.Item item = new ClipData.Item(itemContent);
		ClipData data = new ClipData("Node "+Integer.toString(nodeId), new String[] {"object/node"}, item);
		TextnumShadowBuilder shadowBuilder = new TextnumShadowBuilder(op, nodeId, 0xffbbbbbb, densityMultiplier);
		startDrag(data, shadowBuilder, self, 0);
	}

	private void cancelLongTouch() {
		Timer t = longTouchTimer;
		longTouchTimer = null;
		if (t != null) {
			t.cancel();
		}
	}
	
	protected int getCellSlotId(ClipData clip) {
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length >= 3 && sa[0].equals("cellslot")) {
			return Integer.parseInt(sa[2]);
		}
		return -1;
	}
	
	protected int getNodeId(ClipData clip) {
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length >= 3 && sa[0].equals("cellslot")) {
			return Integer.parseInt(sa[1]);
		}
		if (sa.length >= 2 && sa[0].equals("node")) {
			return  Integer.parseInt(sa[1]);
		}
		return -1;
	}
	
	protected String getClipDataOp(ClipData clip) {
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length >= 4 && sa[0].equals("cellslot")) {
			return sa[3];
		}
		if (sa.length >= 3 && sa[0].equals("node")) {
			return sa[2];
		}
		return "move";
	}
	
	@Override
	public Object getObject4Tag(ClipData clip) {
		if (clip == null) return null;
		Automaton<SliceCell> a = automaton;
		if (a == null) return null;
		if (clip.getItemCount() == 0) return null;
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length >= 3 && sa[0].equals("cellslot")) {
			int clipNodeId = Integer.parseInt(sa[1]);
			int clipCellSlotId = Integer.parseInt(sa[2]);
			Automaton<SliceCell>.Node n = a.getNode(clipNodeId);
			if (n == null) return null;
			return n.getCellData(clipCellSlotId);
		}
		if (sa.length >= 2 && sa[0].equals("node")) {
			int clipNodeId = Integer.parseInt(sa[1]);
			return a.getNode(clipNodeId);
		}
		return null;
	}

	@Override
	public Object getObject4Tag(ClipData clipData, String mimetype) {
		if (mimetype == null) return getObject4Tag(clipData);
		if (mimetype.equals("object/cellslot/index")) return getCellSlotId(clipData);
		if (mimetype.equals("object/node/index")) return getNodeId(clipData);
		if (mimetype.equals("object/op")) return getClipDataOp(clipData);
		return getObject4Tag(clipData);
	}

	public AlgoRhythm getAlgoRythm() {
		return (AlgoRhythm) automaton;
	}

	public void currenNodeChanged(AlgoRhythm a, int lastNodeId, int currentNodeId) {
		if (automaton != null && automaton == a) {
			redrawNode(a.findNode(lastNodeId));
			redrawNode(a.findNode(currentNodeId));
		}
		
	}

	public boolean isConnecting() {
		return isMakingConnection;
	}


}
