package com.mayaswell.orbital.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Align;
import android.view.View.DragShadowBuilder;

import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.Slice;

public class SliceShadowBuilder extends DragShadowBuilder {

	private Slice slice;
	private Paint textBrush;
	private float textScaledPx;
	private Paint dataBrush;

	public SliceShadowBuilder(Slice s, float dm) {
		super();
		slice  = s;
		textScaledPx = dm * 10;
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		textBrush.setColor(0xff000000);

		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);
	}

	public void onDrawShadow (Canvas canvas) {
		int gind = slice.getGroup();
		int c = 0xff444444;
		if (gind >= 0) {
			if (gind >= OrbitalLaser.regginbow.length) {
				gind = OrbitalLaser.regginbow.length-1;
			}
			c = OrbitalLaser.regginbow[gind];
		}
		dataBrush.setColor(c);
		textBrush.setColor((c ^ 0xffffff)|0xff000000);
		canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), dataBrush);
		canvas.drawText("Slice "+slice.label, 20, 20, textBrush);
	}
	
	public void onProvideShadowMetrics (Point shadowSize, Point shadowTouchPoint) {
		shadowSize.x = 70;
		shadowSize.y = 140;
		shadowTouchPoint.x = 35;
		shadowTouchPoint.y = 140;
	}
}

