package com.mayaswell.orbital.widget;

import com.mayaswell.orbital.Slice;

import android.content.ClipData;
import android.content.Intent;
import android.content.ClipData.Item;
import android.net.Uri;

/**
 * unfortunately, this approach doesn't work so well :((((( ClipData gets Parceled and the default ClipData constructor won't know about
 * any clever overrides ....
 * @author dak
 *
 */
public class SliceClipItem extends Item {

	Slice slice;
	public SliceClipItem(Slice s) {
		super(Integer.toString(s.label));
		slice = s;
	}
	
	public static Slice get(ClipData d) {
		if (d == null) {
			return null;
		}
		for (int i=0; i<d.getItemCount(); i++) {
			if (d.getItemAt(i) instanceof SliceClipItem) {
				return ((SliceClipItem)d.getItemAt(i)).slice;
			}
 		}
		return null;
	}

}
