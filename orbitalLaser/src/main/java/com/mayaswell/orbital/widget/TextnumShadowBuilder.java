package com.mayaswell.orbital.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Align;
import android.view.View.DragShadowBuilder;

public class TextnumShadowBuilder extends DragShadowBuilder {

	private int index;
	private String label;
	private Paint textBrush;
	private float textScaledPx;
	private Paint dataBrush;
	private int color;

	public TextnumShadowBuilder(String lbl, int gid, int col, float dm) {
		super();
		label = lbl;
		index  = gid;
		color = col;
		textScaledPx = dm * 10;
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.CENTER);
		textBrush.setColor(0xff000000);

		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);
	}

	public void onDrawShadow (Canvas canvas) {
		int c = color;
		dataBrush.setColor(c);
		textBrush.setColor((c ^ 0xffffff)|0xff000000);
		canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), dataBrush);
		canvas.drawText(label+index, 20, 20, textBrush);
	}
	
	public void onProvideShadowMetrics (Point shadowSize, Point shadowTouchPoint) {
		shadowSize.x = 70;
		shadowSize.y = 140;
		shadowTouchPoint.x = 35;
		shadowTouchPoint.y = 140;
	}
}

