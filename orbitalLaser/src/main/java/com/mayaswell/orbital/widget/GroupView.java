package com.mayaswell.orbital.widget;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.SliceGroups;
import com.mayaswell.orbital.SliceGroups.SliceGroup;
import com.mayaswell.orbital.fragment.AlgorithmEditorFragment.Listener;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.Wrecker;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;

public class GroupView extends SliceContainerView implements IDragSource {
	public interface Listener {
		void groupChanged(Slice slice, int og, int id);
	}
	
	private Listener listener = null;
	private int nGroups = 8;
	private Wrecker wrecker = null;
	private float[] columns = null;
	private float minColumnWidth = 50;
	private float minRowHeight = 50;
	private float gvh = minRowHeight;
	private ArrayList<RectF> rects = null;
	private float [] xpos = null;
	private float [] ypos = null;
	
	private Timer longTouchTimer = null;
	private final int longpressTimeDownBegin = 500;
	private int lastx0 = 0;
	private int lasty0 = 0;

	private float columnWidth = minColumnWidth;
	Paint gridBrush;
	Paint baseBrush;
	private int highlightGroup = -1;
	private float margin = 5;
	
	public GroupView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public GroupView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public GroupView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		minRowHeight = sliceRectH+2*margin;
		
		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setStyle(Paint.Style.STROKE);
		gridBrush.setColor(0xff000000);
		
		baseBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		baseBrush.setStyle(Paint.Style.FILL);
		baseBrush.setColor(0xffffffff);
		
		requestLayout();
	}
	
	protected void setDefaultColumns() {
		if (columns == null) {
			columns = new float[nGroups+1];
		}
		if (xpos == null) {
			xpos = new float[nGroups];
		}
		if (ypos == null) {
			ypos = new float[nGroups];
		}
		if (rects == null) {
			rects = new ArrayList<RectF>();
		}
		columnWidth = getMeasuredWidth()/nGroups;
		float x = 0;
		for (int i=0; i<nGroups; i++) {
			columns[i] = x;
			x += columnWidth;
		}
		columns[nGroups] = x;
	}
	
	public void setTo(Wrecker w) {
		wrecker = w;
		requestLayout();
		setDefaultColumns();
		layoutSlices();
	}
	
	public void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		requestLayout();
	}
	
	
	public void layoutSlices() {
		WreckageState state = wrecker.getState();
		if (state == null) {
			return;
		}
		layoutSlices(state);
//	    Log.d("groups", "do layout "+gvh);
	}

	protected void layoutSlices(WreckageState state) {
		rects.clear();
		for (int i=0; i<nGroups; i++) {
			xpos[i] = columns[i]+margin;
			ypos[i] = margin;
		}
		float gvho = gvh;
//		Log.d("group thing", "start of layout !!"+gvh+", "+" wide "+getMeasuredWidth());
		gvh = minRowHeight;
		for (Slice s: state.getSlices()) {
			int g;
			if ((g=s.getGroup()) >= 0 && g<nGroups) {
				if (xpos[g] + sliceRectW > columns[g+1]) {
					xpos[g] = columns[g] + margin;
					ypos[g] += sliceRectH + margin ;
					if (ypos[g] + sliceRectH + margin> gvh) {
//						Log.d("group thing", "start of layout !!"+gvh+", "+" wide "+getMeasuredWidth()+" g "+g);
						gvh = ypos[g] + sliceRectH + margin;
					}
				}
				RectF r = new RectF(xpos[g], ypos[g], xpos[g]+sliceRectW, ypos[g]+sliceRectH);
				xpos[g] += sliceRectW + margin;
				rects.add(r);
			} else {
				rects.add(null);
			}
		}
		if (gvh != gvho) {
//			Log.d("group thing", "changed height !!"+gvh+", "+gvho);
			requestLayout();
		}
	}
	
	@Override
	protected void onLayout (boolean changed, int left, int top, int right, int bottom) { 
		if (changed) {
			setDefaultColumns();
			layoutSlices();
		}
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		for (int i=0; i<nGroups; i++) {
			if (i != highlightGroup) {
				baseBrush.setColor((OrbitalLaser.regginbow[i] & 0xffffff) | 0x44000000);
			} else {
				baseBrush.setColor((OrbitalLaser.regginbow[i] & 0xffffff) | 0xaa000000);
			}
			canvas.drawRect(columns[i], 0, columns[i+1], gvh, baseBrush);
			if (i > 0) {
				gridBrush.setColor(0xff000000);
				canvas.drawLine(columns[i], 0, columns[i], gvh, gridBrush);
			}
		}
		if (wrecker != null) {
			WreckageState state = wrecker.getState();
			if (state != null) {
				for (int i=0; i<state.sliceCount(); i++) {
					Slice s = state.getSlice(i);
					RectF r = i<rects.size()?rects.get(i):null;
					if (r != null && s != null) {
						drawSlice(canvas, r, s);
					}
				}
			}
		}
	}
	
	@Override
	public boolean onDragEvent(DragEvent event) {
		int action = event.getAction();
		float x = event.getX();
		float y = event.getY();
		
		int g = getGroup4(x, y);
		
		ClipDescription d = event.getClipDescription();
		boolean isSlice;
		if (d == null) {
			isSlice = false;
		} else {
			isSlice = d.hasMimeType("object/slice");
		}
		Log.d("group", "drag "+g+", "+x+", "+y+" act "+action);
		switch (action) {
		case DragEvent.ACTION_DRAG_STARTED:
			break;
		case DragEvent.ACTION_DRAG_LOCATION:
		case DragEvent.ACTION_DRAG_ENTERED: {
			if (isSlice) {
				highlightGroup = g;
			} else {
				highlightGroup = -1;
			}
			invalidate();
			break;
		}
		case DragEvent.ACTION_DRAG_ENDED:
		case DragEvent.ACTION_DRAG_EXITED: {
			highlightGroup  = -1;
			invalidate();
			break;
		}
		case DragEvent.ACTION_DROP: {
			Object state = event.getLocalState();
			Slice s = null;
			if (state instanceof IDragSource) {
				Object dragged = ((IDragSource)state).getObject4Tag(event.getClipData());
				if (dragged instanceof Slice) {
					s = (Slice) dragged;
				}
			}
			if (s != null && wrecker != null) {
				int og = s.getGroup();
				wrecker.setGroup(s, g);
				if (listener != null) {
					listener.groupChanged(s, og, s.getGroup());
				}
				layoutSlices();
			}
			break;
		}
		default:
			break;
		}
		return true;
	}

	private int getGroup4(float x, float y) {
		int t = (int) Math.floor(x / columnWidth);
		if (t < 0) t = 0;
		else if (t >= nGroups) t = nGroups - 1;
		return t;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int action = e.getActionMasked();
		int x0 = (int) e.getX();
		int y0 = (int) e.getY();
		lastx0 = x0;
		lasty0 = y0;
		if (action == MotionEvent.ACTION_DOWN) {
			startLongTouch(x0, y0);
		} else if (action == MotionEvent.ACTION_POINTER_DOWN) {
		} else if (action == MotionEvent.ACTION_UP) {
			cancelLongTouch();
		}
		return super.onTouchEvent(e);
	}
	
	private GroupView self = this;
	
	private void startLongTouch(int x, int y) {
		if (!OrbitalLaser.inCellEditMode()) {
			return;
		}
		final Slice s = slice4Rect(x, y);
		if (s != null) {
			longTouchTimer = new Timer();
			longTouchTimer.schedule(new TimerTask(){
				@Override
				public void run() {
					if (slice4Rect(lastx0, lasty0) == s) {
						ClipData.Item item = new ClipData.Item("slice/"+Integer.toString(s.ind));
						ClipData data = new ClipData("Slice "+Integer.toString(s.label), new String[] {"object/slice"}, item);
						SliceShadowBuilder shadowBuilder = new SliceShadowBuilder(s, densityMultiplier);
						startDrag(data, shadowBuilder, self, 0);
					}
				}
	
			}, longpressTimeDownBegin);
		} else {
			final int gid = getGroup4(x, y);
			if (gid >= 0) {
				longTouchTimer = new Timer();
				longTouchTimer.schedule(new TimerTask(){
					@Override
					public void run() {
						if (getGroup4(lastx0, lasty0) == gid) {
							ClipData.Item item = new ClipData.Item("group/"+Integer.toString(gid));
							ClipData data = new ClipData("Group "+Integer.toString(gid), new String[] {"object/group"}, item);
							TextnumShadowBuilder shadowBuilder = new TextnumShadowBuilder("Group ", gid, SliceGroups.getGroupColor(gid), densityMultiplier);
							startDrag(data, shadowBuilder, self, 0);
						}
					}
		
				}, longpressTimeDownBegin);
			}
		}
	}

	private Slice slice4Rect(int lastx0, int lasty0) {
		int i=0;
		for (RectF r: rects) {
			if (r != null && r.contains(lastx0, lasty0)) {
				return wrecker != null? wrecker.getSlice(i):null;
			}
			i++;
		}
		return null;
	}
	
	private void cancelLongTouch() {
		Timer t = longTouchTimer;
		longTouchTimer = null;
		if (t != null) {
			t.cancel();
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);
	    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
	    int heightSize = MeasureSpec.getSize(heightMeasureSpec);
//	    Log.d("groups", "do measure "+gvh);
	    heightMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) gvh, View.MeasureSpec.EXACTLY);
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setDefaultColumns();
		layoutSlices();
	}

	@Override
	public Object getObject4Tag(ClipData clip) {
		if (clip == null) return null;
		if (wrecker == null) return null;
		if (clip.getItemCount() == 0) return null;
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length != 2) return null;
		int ind = Integer.parseInt(sa[1]);
		if (sa[0].equals("slice")) {
			WreckageState ws = wrecker.getState();
			if (ws == null) return null;
			return ws.getSlice(ind);
		} else if (sa[0].equals("group")) {
			return wrecker.getGroup(ind);
		}
		return null;
	}

	@Override
	public Object getObject4Tag(ClipData clipData, String mimetype) {
		return getObject4Tag(clipData);
	}

	public void setListener(Listener l) {
		listener  = l;
	}

	public void setLocked(boolean locked) {
	}

	public boolean isLocked() {
		return false;
	}

}
