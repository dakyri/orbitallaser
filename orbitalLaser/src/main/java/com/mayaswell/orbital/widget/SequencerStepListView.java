package com.mayaswell.orbital.widget;
import java.util.ArrayList;

import com.mayaswell.audio.Sequence;
import com.mayaswell.audio.Sequence.Step;
import com.mayaswell.audio.Sequence.Type;
import com.mayaswell.orbital.Bank;
import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.R;
import com.mayaswell.orbital.Patch;
import com.mayaswell.util.AbstractPatch.Action;
import com.mayaswell.util.MWBPActivity;
import com.mayaswell.widget.InfinIntControl;
import com.mayaswell.widget.InfinIntControl.InfiniteLongListener;
import com.mayaswell.widget.RotaryPotView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


public class SequencerStepListView extends LinearLayout {

	private static final int[] STATE_CURRENT = {R.attr.state_current};

	public class SequencerStepView extends RelativeLayout {

		private boolean isCurrent=false;
		private Spinner patchSelector = null;
		private InfinIntControl countControl = null;
		private Sequence<Patch>.Step step = null;
		private TextView stepLabel;
		private int index;

		public SequencerStepView(Context context) {
			super(context);
			setup(context, null, -1);
		}

		public SequencerStepView(Context context, AttributeSet attrs) {
			super(context, attrs);
			setup(context, attrs, -1);
		}

		public SequencerStepView(Context context, AttributeSet attrs, int defStyleAttr) {
			super(context, attrs, defStyleAttr);
			setup(context, attrs, defStyleAttr);
		}

		private void setup(Context context, AttributeSet attrs, int defStyleAttr) {
			LayoutInflater li = (LayoutInflater)context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
			li.inflate (R.layout.sequencer_step_view, this, true);
			RelativeLayout.LayoutParams lp;
			
			index = 0;
			
			stepLabel = (TextView) findViewById(R.id.mwCtlLabel);
			
			patchSelector = (Spinner) findViewById(R.id.patchSelector);
			patchSelector.setAdapter(sequenceStepAdapter);
			int pxh = context.getResources().getDimensionPixelSize(R.dimen.stepSpinnerHeight);
			int pxw = context.getResources().getDimensionPixelSize(R.dimen.patchSelectorWidth);
			lp = new RelativeLayout.LayoutParams(pxw, pxh);
			lp.addRule(RelativeLayout.RIGHT_OF, stepLabel.getId());
			patchSelector.setLayoutParams(lp);
			
			countControl = (InfinIntControl) findViewById(R.id.countControl);
			lp = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			lp.addRule(RelativeLayout.RIGHT_OF, patchSelector.getId());
			countControl.setLayoutParams(lp);
			
			stepLabel.setOnTouchListener(new OnTouchListener() {
				private float lastX = 0;
				private float lastY = 0;

				@Override
				public boolean onTouch(View v, MotionEvent e) {
					int action = e.getActionMasked();
					float x0 = e.getRawX();
					float y0 = e.getRawY();
					switch (action) {
					case MotionEvent.ACTION_DOWN:
						lastX = x0;
						lastY = y0;
						selectStep(true);
						break;
					case MotionEvent.ACTION_MOVE:
						float y = thisListView.getScrollY();
						float h = thisListView.getMeasuredHeight();
						float ch = thisListView.getContentHeight();
						float dy = (lastY-y0);
						if (y+dy < 0) {
							dy = -y;
						} else if (y+dy > ch-h) {
							dy = ch-h-y;
						}
						thisListView.scrollBy(0, (int) dy);
						lastX = x0;
						lastY = y0;
						break;
					}
					return true;
				}
			});
			
			patchSelector.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
					Action b = sequenceStepAdapter.getItem(position);
					if (b != null) {
						int pind = b.getOp();
						MWBPActivity<?, ?, ?> a = null;
						try {
							a = (MWBPActivity<?, ?, ?>) getContext();
						} catch (ClassCastException e) {
							
						}
						if (pind >= OrbitalLaser.kAlgIndexBase) {
							step.patchIdx = pind - OrbitalLaser.kAlgIndexBase;
							step.type = Sequence.Type.ALGORITHM;
						} else {
							step.patchIdx = pind;
							step.type = Sequence.Type.PATCH;
						}
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> parentView) {
				}
				
			});
			
			countControl.setPotListener(new InfiniteLongListener() {

				@Override
				public void onValueChanged(long v) {
					step.count = (int) v;
				}
				
			});
			
			setBackgroundResource(R.drawable.seq_step_bg);
		}
		
		protected void selectStep(boolean b) {
			thisListView.selectStep(index, b);
		}
		
		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		    heightMeasureSpec = MeasureSpec.UNSPECIFIED;
		    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
		
		protected void setPatchSelector(Type type, int cpInd) {
			if (type == Sequence.Type.PATCH) {
				if (cpInd < 0 || cpInd >= currentBank.numPatches()) {
					cpInd = 0;
				}
				setPatchSelectorInd(cpInd);
			} else if (type == Sequence.Type.ALGORITHM) {
				if (cpInd < 0 || cpInd >= currentBank.numAlgorithms()) {
					cpInd = 0;
				}
				setPatchSelectorInd(cpInd+OrbitalLaser.kAlgIndexBase);
			}
		}

		public void setTo(Sequence<Patch>.Step p) {
			step = p;
			setPatchSelector(p.type, p.patchIdx);
			countControl.setValue(p.count);
		}
		
		public void setPatchSelector(Patch p) {
			if (patchSelector != null && currentBank != null && p != null) {
				int ind = currentBank.find(p);
				if (ind >= 0) {
					setPatchSelectorInd(ind);
				}
			}
		}

		protected void setPatchSelectorInd(int ind) {
			if (sequenceStepAdapter == null) {
				return;
			}
			for (int i=0; i<sequenceStepAdapter.getCount(); i++) {
				Action a = sequenceStepAdapter.getItem(i);
				if (a.getOp() == ind) {
					patchSelector.setSelection(i);
					break;
				}
			}
		}
		/** 
		 * @see android.widget.TextView#onCreateDrawableState(int)
		 */
		@Override
		protected int[] onCreateDrawableState(int extraSpace)
		{
			final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
			if (isCurrent) {
				mergeDrawableStates(drawableState, STATE_CURRENT);
			}
			return drawableState;
		}
		
		/** 
		 * @see android.view.View#drawableStateChanged(int)
		 */
		@Override
		protected void drawableStateChanged ()
		{
			super.drawableStateChanged();
		}
		
		/**
		 * @param s
		 */
		public void setCurrentState(boolean s)
		{
			isCurrent = s;
			refreshDrawableState();
		}
		
		public boolean getCurrentState()
		{
			return isCurrent;
		}

		public void setLabel(String s) {
			stepLabel.setText(s);
		}


	}

	private ArrayAdapter<Action> sequenceStepAdapter;
	private Sequence<Patch> currentSequence;
	private ArrayList<SequencerStepView> stepViews;
	private Bank currentBank;
	private SequencerStepListView thisListView;
	private int selectedStep = -1;

	public SequencerStepListView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public SequencerStepListView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public SequencerStepListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setup(context, attrs, defStyle);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle) {
		setOrientation(VERTICAL);
		stepViews = new ArrayList<SequencerStepView>();
		currentBank = null;
		thisListView = this;
	}
	
	public void selectStep(int si, boolean b) {
		if (b) {
			setSelectedStep(si);
		} else {
			setSelectedStep(-1);
		}
		for (SequencerStepView ssv: stepViews) {
			if (ssv.index != si) {
				if (ssv.isSelected()) {
					ssv.setSelected(false);
				}
			} else {
				if (!ssv.isSelected()) {
					ssv.setSelected(b);
				}
			}
			ssv.refreshDrawableState();
		}
	}

	public void setCurrentStep(int si) {
		int i=0;
		for (SequencerStepView ssv: stepViews) {
			if (i != si) {
				ssv.setCurrentState(false);
			} else {
				ssv.setCurrentState(true);
			}
			i++;
		}
	}

	protected float getContentHeight() {
		float h = 0;
		for (SequencerStepView ssv: stepViews) {
			h += ssv.getMeasuredHeight();
		}
		return h;
	}

	public void setSequenceStepAdapapter(ArrayAdapter<Action> a)
	{
		sequenceStepAdapter = a;
	}
	

	public void setToSequence(Sequence<Patch> s)
	{
		currentSequence = s;
		int i=0;
		for (; i<s.countSteps(); i++ ) {
			Sequence<Patch>.Step ss = s.getStep(i);
			SequencerStepView sv;
			if (i >= stepViews.size()) {
				sv = new SequencerStepView(getContext());
			} else {
				sv = stepViews.get(i);
			}
			sv.setTo(ss);
		}
	}

	public void setToBank(Bank pbs) {
		currentBank = pbs;
	}

	public void select(int i) {
		for (SequencerStepView v: stepViews) {
			v.setSelected(false);
			
		}
		if (i >= 0 && i < stepViews.size()) {
			stepViews.get(i).setSelected(true);
		}
	}

	public void delStep(int i) {
		if (i >= 0 && i < stepViews.size()) {
			stepViews.remove(i);
			removeViewAt(i);
			renumberStepViews();
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    heightMeasureSpec = MeasureSpec.makeMeasureSpec(350, MeasureSpec.AT_MOST);
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	public void addStepView(int i, Sequence<Patch>.Step ss) {
		SequencerStepView v =  new SequencerStepView(getContext());
		v.setTo(ss);
		if (i >= 0 && i <= stepViews.size()) {
			stepViews.add(i, v);
			addView(v,i);
		} else {
			stepViews.add(v);
			addView(v);
		}
		renumberStepViews();
	}
	
	private void renumberStepViews()
	{
		int i=1;
		for (SequencerStepView s: stepViews) {
			s.index = i;
			s.setLabel(Integer.toString(i++));
		}
	}

	public int getSelectedStep() {
		return selectedStep;
	}

	public void setSelectedStep(int selectedStep) {
		this.selectedStep = selectedStep;
	}
	
}
