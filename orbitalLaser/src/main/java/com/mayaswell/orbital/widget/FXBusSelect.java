package com.mayaswell.orbital.widget;

import com.mayaswell.orbital.FXBusState;
import com.mayaswell.orbital.Patch;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class FXBusSelect extends View {
	
	public interface NameSource {
		String getBusName(int i);
	}

	public interface Listener {
		short useBus(short b);
		short displayBus(short b);
	}

    protected final String schemaName = "http://schemas.android.com/apk/res-auto";

	protected int textColor = 0xffffffff;
	protected int gridColor = 0xffffffff;
	protected int selectRegionColorUp = 0xff000000;
	protected int selectRegionColorDn = 0xffaaaaaa;
	protected int displayRegionColorUp = 0xff441111;
	protected int displayRegionColorDn = 0xffaa4444;
	
	protected Paint selectRegionBrush = null;
	protected Paint displayRegionBrush = null;
	protected Paint textBrush = null;	
	protected Paint gridBrush = null;
	
	protected int maxFX = 8;
	protected int activeBus = 0;
	protected int displayedBus = 0;
	protected int selectedBus = 0;
	
	protected int internalPadding = 10;

	float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
	float textScaledPx = 10 * densityMultiplier;

	private float cX;
	private float cY;

	private float outerRadius;
	private float innerRadius;
	private float edgeW;
	private RectF outerRect;
	private RectF innerRect;
	
	private Listener listener = null;

	private NameSource busNameList = null;

	public FXBusSelect(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public FXBusSelect(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public FXBusSelect(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}

	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		densityMultiplier = context.getResources().getDisplayMetrics().density;
		textScaledPx = 10 * densityMultiplier;
		
		textColor = 0xffffffff;
		gridColor = 0xffffffff;
		selectRegionColorUp = 0xff000000;
		selectRegionColorDn = 0xff444444;
		displayRegionColorUp = 0xff441111;
		displayRegionColorDn = 0xffff1111;
		
		edgeW = 30;
		setupShapes(getWidth(), getHeight());		
		
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.LEFT);
		
		gridBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		gridBrush.setStyle(Paint.Style.STROKE);
		
		selectRegionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		selectRegionBrush.setStyle(Paint.Style.FILL);
		
		displayRegionBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		displayRegionBrush.setStyle(Paint.Style.FILL);
		
		if (attrs != null) {
			int id = attrs.getAttributeResourceValue(schemaName, "fx_textColor", -1);
			if (id >= 0) {
				textColor  = getResources().getColor(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "fx_gridColor", -1);
			if (id >= 0) {
				gridColor  = getResources().getColor(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "fx_selectRegionColorUp", -1);
			if (id >= 0) {
				selectRegionColorUp = getResources().getColor(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "fx_selectRegionColorDn", -1);
			if (id >= 0) {
				selectRegionColorDn = getResources().getColor(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "fx_displayRegionColorUp", -1);
			if (id >= 0) {
				displayRegionColorUp = getResources().getColor(id);
			}
			id = attrs.getAttributeResourceValue(schemaName, "fx_displayRegionColorDn", -1);
			if (id >= 0) {
				displayRegionColorDn = getResources().getColor(id);
			}
		}
		gridBrush.setColor(gridColor);
		textBrush.setColor(textColor);
		displayRegionBrush.setColor(displayRegionColorUp);
		Log.d("buser", "setup done");
	}
	/**
	 * @see android.view.View#onSizeChanged(int, int, int, int)
	 */
	@Override
	protected void onSizeChanged (int w, int h, int oldw, int oldh)
	{
		Log.d("buser", "size changed "+w+","+h);
		super.onSizeChanged(w, h, oldw, oldh);
		setupShapes(w, h);
	}

	protected void setupShapes(int w, int h) {
		Log.d("buser", "setup "+w+", "+h);
		outerRadius = (float) (Math.min(w,  h)/2)-internalPadding;
		innerRadius = outerRadius - edgeW;
		cX = w/2;
		cY = h/2;
		outerRect = new RectF(cX-outerRadius, cY-outerRadius, cX+outerRadius, cY+outerRadius);
		innerRect = new RectF(cX-innerRadius, cY-innerRadius, cX+innerRadius, cY+innerRadius);
	}

	/**
	 * @see android.view.View#onDraw(android.graphics.Canvas)
	 */
	@Override
	protected void onDraw (Canvas canvas)
	{
		textBrush.setColor(textColor);
	    super.onDraw(canvas);         

		float arcWid = 360/maxFX;
		float arc = 0;
		int csi = selectRegionColorUp;
		int cdi = displayRegionColorUp;
		for (int i=0; i<maxFX; i++, arc+=arcWid) {
			selectRegionBrush.setColor(i == selectedBus?selectRegionColorDn:csi);
			displayRegionBrush.setColor(i == displayedBus ?displayRegionColorDn:cdi);
			if (i > maxFX/2) {
				csi += 0x000800;
				cdi += 0x000008;
			} else {
				csi += 0x000800;
				cdi += 0x000008;
			}
			if (i == activeBus) {
				cdi += 0x00101010;
			}
			canvas.drawArc(outerRect, arc, arcWid, true, displayRegionBrush);
			canvas.drawArc(innerRect, arc, arcWid, true, selectRegionBrush);
		}
		arc = 0;
		for (int i=0; i<maxFX; i++, arc+=arcWid) {
			canvas.drawArc(outerRect, arc, arcWid, false, gridBrush);
			canvas.drawArc(innerRect, arc, arcWid, false, gridBrush);
			canvas.drawLine(cX, cY,
					(float)(cX+innerRadius*Math.cos(Math.toRadians(arc))),
					(float)(cY+innerRadius*Math.sin(Math.toRadians(arc))), gridBrush);
		}
		canvas.save();
		arc = 0;
		canvas.rotate((float)(-arcWid*.333), cX, cY);
		boolean d = true;
		textBrush.setTextAlign(Align.RIGHT);
		for (int i=0; i<maxFX; i++, arc+=arcWid) {
			canvas.rotate(d?arcWid:arcWid, cX, cY);
			if (arc >= 90) {
				arc -= 180;
				d = !d;
				textBrush.setTextAlign(d?Align.RIGHT:Align.LEFT);
				canvas.rotate(-180, cX, cY);
			}
			String nm = "Bus "+i;
			if (busNameList != null) {
				String f = busNameList.getBusName(i);
				if (f != null) {
					nm = f;
				}
			}
			canvas.drawText(nm, cX+(d?innerRadius-10:10-innerRadius), cY+(d?0:+10), textBrush);
		}
		canvas.restore();
	}
	
	/**
	 * @see android.view.View#onTouchEvent(android.view.MotionEvent)
	 */
	@Override
	public boolean onTouchEvent (MotionEvent e)
	{
		int action = e.getActionMasked();
		float x0 = e.getX()-cX;
		float y0 = e.getY()-cY;
		float r0 = (float) Math.sqrt(x0*x0 + y0*y0);
		float th0 = (float) (Math.atan2(x0, -y0));
		th0 = (float) (th0*360/(2*Math.PI))-90;
		if (th0 < 0) th0 += 360;
		if (action == MotionEvent.ACTION_DOWN) {
			float arcWid = 360/maxFX;
			short nb = (short) (((int)th0) / ((int)arcWid));
			if (r0 < innerRadius) {
				if (listener != null) {
					nb = listener.useBus(nb);
				}
				setSelectedBus(nb);
			} else {
				if (listener != null) {
					nb = listener.displayBus(nb);
				}
				setDisplayedBus(nb);
			}
		}
		return false;
	}
	
	public void setNameSource(NameSource p)
	{
		busNameList  = p;
		invalidate();
	}
	
	public void setDisplayedBus(int nb) {
		displayedBus = nb;
		Log.d("bus", "display "+nb);
		invalidate();
	}

	public void setSelectedBus(int nb) {
		selectedBus = nb;
		Log.d("bus", "select "+nb);
		invalidate();
	}

	public void setActiveBus(int nb) {
		activeBus = nb;
		invalidate();
	}

	public Listener getListener() {
		return listener;
	}

	public void setListener(Listener listener) {
		this.listener = listener;
	}


}
