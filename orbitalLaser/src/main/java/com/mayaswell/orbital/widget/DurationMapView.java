package com.mayaswell.orbital.widget;

import java.util.ArrayList;

import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.WreckageState;
import com.mayaswell.orbital.util.DurationMap;
import com.mayaswell.orbital.util.FloatPair;
import com.mayaswell.orbital.util.MappedProbability;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DurationMapView extends View {

	public interface Listener {
		void fpSelected(int g, FloatPair f);
	}
	
	private float margin = 0;
	private Listener listener = null;
	protected float sliceRectW = 80;
	protected float sliceRectH = 60;
	protected float densityMultiplier = 1;
	private Paint textBrush;
	private Paint outlineBrush;
	private float textScaledPx;
	private Paint dataBrush;
	private DurationMap myMap=null;
	
	private int touchPosition = -1;
	private int highlightPosition = -1;
	
	private float minItemWidth = 35;
	private float maxItemWidth = 100;
	private float minRowHeight = 50;
	
	private float pvh = minRowHeight;

	private ArrayList<RectF> rects = new ArrayList<RectF>();

	public DurationMapView(Context context) {
		super(context);
		setup(context, null);
	}

	public DurationMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs);
	}

	public DurationMapView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs);
	}

	private void setup(Context context, AttributeSet attrs) {
		densityMultiplier = context.getResources().getDisplayMetrics().density;
		textScaledPx = densityMultiplier * 10;
		textBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		textBrush.setTextSize(textScaledPx);
		textBrush.setTextAlign(Align.RIGHT);
		textBrush.setColor(0xff000000);
	
		dataBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		dataBrush.setStyle(Paint.Style.FILL);
		
		outlineBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		outlineBrush.setStyle(Paint.Style.STROKE);
		outlineBrush.setStrokeWidth(2.0f);
		outlineBrush.setColor(0xff0000ff);
	}

	
	@Override
	protected void onDraw(Canvas canvas) {
		if (myMap != null) {
			float su = myMap.sum();
			if (su == 0)
				su = 1;
 			for (int i=0; i<myMap.size(); i++) {
				FloatPair sc = myMap.get(i);
				RectF r = (i>=0 && i<rects.size())?rects.get(i):null;
				if (r != null && sc != null) {
					drawMapItem(canvas, r, String.format("%.2f", sc.first), sc.second/MappedProbability.kMaxSlotValue, i%2);
				}
			}
		}
		if (highlightPosition >= 0) {
			float x=2;
			float y1=2;
			float y2=sliceRectH+3;
			if (rects.size() > 0) {
				if (highlightPosition >= rects.size()) {
					RectF r = rects.get(rects.size()-1);
					x=r.right+2;
					y1=r.top-2;
					y2=r.bottom+2;
				} else {
					RectF r = rects.get(highlightPosition);
					x=r.left-2;
					y1=r.top-2;
					y2=r.bottom+2;
				}
			}
			outlineBrush.setColor(0xff00ff00);
			canvas.drawLine(x, y1, x, y2, outlineBrush);
		}
	}

	protected void drawMapItem(Canvas c, RectF r, String label, float alpha, int offi) {
		int col = 0xffff4444;
		col = (col & 0xffffff) | ((((int)(255*alpha) & 0xff)) << 24);
		dataBrush.setColor(col);
		textBrush.setColor(0xff000000);//(col&0xffffff) ^ 0xffffffff);
		outlineBrush.setColor(0xff0000ff);
		c.drawRect(r, dataBrush);
		c.drawRect(r, outlineBrush);
		c.drawText(label, /* (r.left+r.right)/2 */r.right-2, r.top+(offi+1)*20, textBrush);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int action = e.getActionMasked();
		float x0 = e.getX();
		float y0 = e.getY();
//		lastx0 = x0;
//		lasty0 = y0;
		int g = getPosition4(x0, y0);
		if (action == MotionEvent.ACTION_DOWN) {
			touchPosition  = g;
			if (listener != null && myMap != null) {
				FloatPair sp = myMap.get(g);
				listener.fpSelected(g, sp);
			}
			if (touchPosition >= 0) {
//				startLongTouch(g);
				return true;
			}
		} else if (action == MotionEvent.ACTION_MOVE) {
		} else if (action == MotionEvent.ACTION_POINTER_DOWN) {
		} else if (action == MotionEvent.ACTION_UP) {
//			cancelLongTouch();
			setHilight(touchPosition);
			return true;
		}
		return super.onTouchEvent(e);
	}
	
	private int getPosition4(float x, float y) {
		if (rects == null) return -1;
		for (int i=0; i<rects.size(); i++) {
			RectF r = rects.get(i);
			if (x >= r.left-margin && x <= r.right+margin && y >= r.top-margin && y <= r.bottom+margin) {
				return i;
			}
		}
		return rects.size()+1;
	}
	
	public void setListener(Listener l) {
		listener = l;
	}
	
	public void setTo(DurationMap m) {
		if (myMap != m && listener != null) {
			if (m != null) {
//				listener.patternSet(myMap);
			}
		}
		myMap = m;
		layoutSlices();
		requestLayout();
		invalidate();
	}	
	
	@Override
	protected void onLayout (boolean changed, int left, int top, int right, int bottom) { 
		if (changed) {
			layoutSlices();
		}
	}
	
	public void layoutSlices() {
		rects.clear();
		float pvho = pvh;
		pvh = minRowHeight;
		float xpos = margin;
		float ypos = margin;
		if (myMap != null) {
			float lastFirst = 0;
			for (int i=0; i<myMap.size(); i++) {
				FloatPair s = myMap.get(i);
				if (s != null) { // because we can mod a synchronously getter and setter is sync'd but might return null
					float w = s.first - lastFirst;
					lastFirst = s.first;
					float ciw = w * sliceRectW ;
					if (ciw < minItemWidth) ciw = minItemWidth;
					if (ciw > maxItemWidth) ciw = maxItemWidth;
					/*
					if (xpos + ciw> getMeasuredWidth()) {
						xpos = margin;
						ypos += sliceRectH + margin;
						if (ypos > pvh) {
							pvh = ypos;
						}
					}*/
					RectF r = new RectF(xpos, ypos, xpos+ciw, ypos+sliceRectH);
					xpos += ciw + margin;
					rects.add(r);
				}
			}
		}
		if (pvh != pvho) {
			requestLayout();
		}
	}

	public int getSelection() {
		return highlightPosition;
	}

	public void setHilight(int selection) {
		this.highlightPosition = selection;
	}


}
