package com.mayaswell.orbital.widget;

import com.mayaswell.orbital.R;
import com.mayaswell.orbital.util.MappedProbability;
import com.mayaswell.orbital.util.RangedMP;
import com.mayaswell.widget.RotaryPotView;

import android.content.Context;
import android.util.AttributeSet;

public class RangedMPWidget extends MappedProbabilityWidget {

	protected RotaryPotView rangeBasePot;
	protected RotaryPotView rangeScopePot;
	private RangedMP rangedMap;

	public RangedMPWidget(Context context) {
		super(context);
	}

	public RangedMPWidget(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RangedMPWidget(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
	
	protected void setup(Context context, AttributeSet attrs) {
		layoutResource = R.layout.ranged_mapped_probability_widget;
		super.setup(context, attrs);

		rangedMap = null;
		rangeBasePot  = (RotaryPotView)findViewById (R.id.mwCtlRangePotBase);
		rangeScopePot   = (RotaryPotView)findViewById (R.id.mwCtlRangePotScope);
		if (rangeBasePot != null) {
			rangeBasePot.setValueBounds(0, 1);
			rangeBasePot.setCenterValue(0);
			rangeBasePot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
//					prView.setText(prValueDisplayFormat!=null?String.format(prValueDisplayFormat, v):Float.toString(v));
//					onSlotProbChanged(selectedSlot, v);
					if (rangedMap != null) {
						rangedMap.setRangeBase(v);
					}
				}
			});
		}
		if (rangeScopePot != null) {
			rangeScopePot.setValueBounds(0,1);
			rangeScopePot.setCenterValue(0.5f);
			rangeScopePot.setKnobListener(new RotaryPotView.RotaryKnobListener() {
				@Override
				public void onKnobChanged(float v) {
//					prView.setText(prValueDisplayFormat!=null?String.format(prValueDisplayFormat, v):Float.toString(v));
//					onSlotProbChanged(selectedSlot, v);
					if (rangedMap != null) {
						rangedMap.setRangeScope(v);
					}
				}
			});
		}


	}
	
	public void setMap(RangedMP map) {
		rangedMap = map;
		if (map != null) {
			if (rangeBasePot != null) rangeBasePot.setValue(map.getRangeBase());
			if (rangeScopePot != null) rangeScopePot.setValue(map.getRangeScope());
		} else {
			if (rangeBasePot != null) rangeBasePot.setValue(0.5f);
			if (rangeScopePot != null) rangeScopePot.setValue(0.5f);
		}
		super.setMap(map);
	}
}
