package com.mayaswell.orbital.widget;

import android.content.Context;
import android.util.AttributeSet;
import com.mayaswell.orbital.Wrecker;

public class PlayButton extends com.mayaswell.widget.PlayButton {
	
	protected Wrecker wrecker=null;

	public PlayButton(Context context) {
		super(context);
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public PlayButton(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public PlayButton(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}
	
	public void setWrecker(Wrecker pbs)
	{
		wrecker = pbs;
	}
	
	public Wrecker getWrecker()
	{
		return wrecker;
	}

}
