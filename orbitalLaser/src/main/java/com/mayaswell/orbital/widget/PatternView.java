package com.mayaswell.orbital.widget;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.mayaswell.orbital.OrbitalLaser;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.SliceCell;
import com.mayaswell.orbital.SliceGroups;
import com.mayaswell.orbital.WreckageState;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

public class PatternView  extends SliceContainerView implements IDragSource {
	
	public interface Listener {
		void cellSelected(int g, SliceCell patternSliceCell);
		void cellAdded(SliceCell sc);
		void cellDeleted(SliceCell sc);
		void patternSet(ArrayList<SliceCell> sca);
	}

	private float minRowHeight = 50;
	private float pvh = minRowHeight;
	private ArrayList<RectF> rects = new ArrayList<RectF>();
	private WreckageState wreckageState=null;
	private int highlightPosition = -1;
	private float margin = 5;
	private Timer longTouchTimer = null;
	private final int longpressTimeDownBegin = 1000;
	private float lastx0 = 0;
	private float lasty0 = 0;
	private Paint outlineBrush;
	
	private float minCellItemWidth = 15;
	private float maxCellItemWidth = 100;
	
	private Listener listener = null;
	private int touchPosition = -1;
	private int currentPosition;
	
	public PatternView(Context context) {
		super(context);
		setup(context, null, -1);
	}

	public PatternView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setup(context, attrs, -1);
	}

	public PatternView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setup(context, attrs, defStyleAttr);
	}
	
	private void setup(Context context, AttributeSet attrs, int defStyle)
	{
		sliceRectW = 80;
		minRowHeight = sliceRectH+2*margin;
		outlineBrush = new Paint(Paint.ANTI_ALIAS_FLAG);
		outlineBrush.setStyle(Paint.Style.STROKE);
		outlineBrush.setStrokeWidth(2.0f);
		outlineBrush.setColor(0xffff0000);
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if (wreckageState != null) {
			for (int i=0; i<wreckageState.getPatternCount(); i++) {
				SliceCell sc = wreckageState.getPatternSliceCell(i);
				Slice s = wreckageState.getSlice(sc.getId());
				RectF r = (i>=0 && i<rects.size())?rects.get(i):null;
				if (r != null && s != null) {
					drawSlice(canvas, r, s.getGroup(), s.label, sc.getGain());
				}
			}
		}
		if (highlightPosition >= 0) {
			float x=2;
			float y1=2;
			float y2=sliceRectH+3;
			if (rects.size() > 0) {
				if (highlightPosition >= rects.size()) {
					RectF r = rects.get(rects.size()-1);
					x=r.right+2;
					y1=r.top-2;
					y2=r.bottom+2;
				} else {
					RectF r = rects.get(highlightPosition);
					x=r.left-2;
					y1=r.top-2;
					y2=r.bottom+2;
				}
			}
			canvas.drawLine(x, y1, x, y2, outlineBrush);
		}
	}
	

	public void layoutSlices() {
		for (int i=0; i<rects.size(); i++) {
			rects.clear();
		}
		float pvho = pvh;
		pvh = minRowHeight;
		float xpos = margin;
		float ypos = margin;
		for (int i=0; i<wreckageState.getPatternCount(); i++) {
			SliceCell s = wreckageState.getPatternSliceCell(i);
			float ciw = s.getDuration() * sliceRectW ;
			if (ciw < minCellItemWidth) ciw = minCellItemWidth;
			if (ciw > maxCellItemWidth) ciw = maxCellItemWidth;
			if (xpos + ciw> getMeasuredWidth()) {
				xpos = margin;
				ypos += sliceRectH + margin;
				if (ypos > pvh) {
					pvh = ypos;
				}
			}
			RectF r = new RectF(xpos, ypos, xpos+ciw, ypos+sliceRectH);
			xpos += ciw + margin;
			rects.add(r);
		}
		if (pvh != pvho) {
			requestLayout();
		}
	}


	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent e) {
		int action = e.getActionMasked();
		float x0 = e.getX();
		float y0 = e.getY();
		lastx0 = x0;
		lasty0 = y0;
		int g = getPosition4(x0, y0);
		currentPosition = g;
//		Log.d("pattern", "action touch "+action+", "+currentPosition);
		if (action == MotionEvent.ACTION_DOWN) {
			touchPosition  = g;
			if (listener != null && wreckageState != null) {
				listener.cellSelected(g, wreckageState.getPatternSliceCell(g));
			}
			if (touchPosition >= 0) {
				startLongTouch(g);
				return true;
			}
		} else if (action == MotionEvent.ACTION_MOVE) {
//			Log.d("pattern", "move "+touchPosition+", "+currentPosition);
			if (touchPosition >= 0 && touchPosition != currentPosition && wreckageState != null) {
//				Log.d("pattern", "doing drag "+touchPosition+", "+currentPosition);
				Slice sc = wreckageState.getPatternSlice(touchPosition);
				startDragCell("Move "+sc.label+"/"+touchPosition, touchPosition, sc, "move");
				cancelLongTouch();
			}
			return true;
		} else if (action == MotionEvent.ACTION_POINTER_DOWN) {
		} else if (action == MotionEvent.ACTION_UP) {
			cancelLongTouch();
			return true;
		}
		return super.onTouchEvent(e);
	}

	@Override
	public boolean onDragEvent(DragEvent event) {
		int action = event.getAction();
		float x = event.getX();
		float y = event.getY();
		
		int g = getPosition4(x, y);
		
		ClipDescription d = event.getClipDescription();
		boolean isSlice;
		if (d == null) {
			isSlice = false;
		} else {
			isSlice = d.hasMimeType("object/slice");
		}
//		Log.d("pattern", "drag "+g+", "+x+", "+y+" act "+action);
		switch (action) {
		case DragEvent.ACTION_DRAG_STARTED:
			break;
		case DragEvent.ACTION_DRAG_LOCATION:
		case DragEvent.ACTION_DRAG_ENTERED: {
			if (isSlice) {
				highlightPosition = g;
			} else {
				highlightPosition = -1;
			}
			invalidate();
			break;
		}
		case DragEvent.ACTION_DRAG_ENDED:
		case DragEvent.ACTION_DRAG_EXITED: {
			highlightPosition  = -1;
			invalidate();
			break;
		}
		case DragEvent.ACTION_DROP: {
			Object state = event.getLocalState();
			WreckageState w = wreckageState;
			if (isSlice) {
				highlightPosition = g;
			} else {
				highlightPosition = -1;
			}
			if (state != null && w != null) {
				if (state instanceof PatternView) {
					PatternView pv = (PatternView)state;
					if (pv == this) {
						Object dragged = pv.getObject4Tag(event.getClipData(), "object/index");
						Object draggedm = ((IDragSource)state).getObject4Tag(event.getClipData(), "object/op");
						if (dragged instanceof Integer) {
							if (draggedm instanceof String && ((String) draggedm).equals("copy")) {
								SliceCell sc = w.getPatternSliceCell(((Integer)dragged).intValue());
								int si = sc.getId();
								SliceCell sc2 = w.patternAdd(si, sc.getDuration(), sc.getGain(), g);						
								if (listener != null) {
									listener.cellAdded(sc2);
								}
							} else {
								w.patternMov(((Integer)dragged).intValue(), g);						
							}
						}
					} else {
						Object dragged = pv.getObject4Tag(event.getClipData());
						if (dragged instanceof Slice) {
							int si = ((Slice)dragged).ind;
							Slice s = w.getSlice(si);
							Slice s2 = w.getSlice(si+1);
							SliceCell sc = w.patternAdd(si, s2.position-s.position, 1, g);						
							if (listener != null) {
								listener.cellAdded(sc);
							}
						}
					}
				} else {
					if (state instanceof IDragSource) {
						Object dragged = ((IDragSource)state).getObject4Tag(event.getClipData());
						if (dragged instanceof Slice) {
							int si = ((Slice)dragged).ind;
							Slice s = w.getSlice(si);
							Slice s2 = w.getSlice(si+1);
							SliceCell sc = w.patternAdd(si, s2.position-s.position, 1, g);						
							if (listener != null) {
								listener.cellAdded(sc);
							}
						}
					}
				}
				layoutSlices();
			}
			break;
		}
		default:
			break;
		}
		return true;
	}

	
	private PatternView self = this;
	
	private void startLongTouch(final int g) {
		if (!OrbitalLaser.inCellEditMode()) {
			return;
		}
		if (wreckageState != null) {
			Log.d("pattern", "getPosition4 "+g+"/"+wreckageState.getPatternCount());
			if (g >= wreckageState.getPatternCount()) {
				return;
			}
			longTouchTimer = new Timer();
			longTouchTimer.schedule(new TimerTask(){
				@Override
				public void run() {
					Slice s;
					Log.d("pattern", "long touch tripped ");
					if (getPosition4(lastx0, lasty0) == g && (s=wreckageState.getPatternSlice(g)) != null) {
						startDragCell("Copy "+s.label+"/"+g, g, s, "copy");
					}
				}

			}, longpressTimeDownBegin);
		}
	}
	
	protected void startDragCell(String lbl, int g, Slice s, final String op) {
		String itemContent = "slice/"+g;
		if (op != null) {
			itemContent += "/" + op;
		}
		ClipData.Item item = new ClipData.Item(itemContent);
		ClipData data = new ClipData(lbl, new String[] {"object/slice", "object/index"}, item);
		TextnumShadowBuilder shadowBuilder = new TextnumShadowBuilder(op, g, SliceGroups.getGroupColor(s.getGroup()), densityMultiplier);
		startDrag(data, shadowBuilder, self, 0);
	}
	
	private void cancelLongTouch() {
		Timer t = longTouchTimer;
		longTouchTimer = null;
		if (t != null) {
			t.cancel();
		}
	}

	private int getPosition4(float x, float y) {
		if (rects == null) return -1;
		for (int i=0; i<rects.size(); i++) {
			RectF r = rects.get(i);
			if (x >= r.left-margin && x <= r.right+margin && y >= r.top-margin && y <= r.bottom+margin) {
				return i;
			}
		}
		return rects.size()+1;
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//	    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
//	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);
//	    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//	    int heightSize = MeasureSpec.getSize(heightMeasureSpec);
	    
	    heightMeasureSpec = View.MeasureSpec.makeMeasureSpec((int) pvh, View.MeasureSpec.EXACTLY);
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		layoutSlices();
	}

	@Override
	public Object getObject4Tag(ClipData clip) {
		int ind = getPatternIndex(clip);
		WreckageState ws = wreckageState;
		if (ws == null) return null;
		int item = ws.getPatternSliceIndex(ind);
		return ws.getSlice(item);
	}

	protected int getPatternIndex(ClipData clip) {
		if (clip == null) return -1;
		if (clip.getItemCount() == 0) return -1;
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length < 2) return -1;
		int ind = Integer.parseInt(sa[1]);
		if (sa[0].equals("slice")) {
			WreckageState ws = wreckageState;
			if (ws == null) return -1;
			return ind;
		}
		return -1;
	}

	protected String getClipDataOp(ClipData clip) {
		if (clip == null) return "move";
		if (clip.getItemCount() == 0) return "move";
		String s = clip.getItemAt(0).getText().toString();
		String [] sa = s.split("/");
		if (sa.length < 3) return "move";
		return sa[2];
	}


	@Override
	public Object getObject4Tag(ClipData clip, String mimetype) {
		if (mimetype.equals("object/index")) return Integer.valueOf((getPatternIndex(clip)));
		if (mimetype.equals("object/op")) return getClipDataOp(clip);
		return getObject4Tag(clip);
	}

	public void setTo(WreckageState w) {
		if (wreckageState != w && listener != null) {
			if (w != null) {
				listener.patternSet(w.getPattern());
			}
		}
		wreckageState = w;
		requestLayout();
		invalidate();
	}
	
	@Override
	protected void onLayout (boolean changed, int left, int top, int right, int bottom) { 
		if (changed) {
			layoutSlices();
		}
	}
	
	public void setListener(Listener l) {
		listener = l;
	}

	public void invalidateCell() {
		layoutSlices();
		invalidate();
	}

	public float getDuration() {
		return wreckageState != null? wreckageState.getPatternDuration(): 0;
	}

	public void deleteFromPattern(int ind) {
		if (wreckageState == null) return;
		SliceCell sc = wreckageState.patternRem(ind);
		layoutSlices();
		invalidate();
		if (listener != null) {
			listener.cellDeleted(sc);
		}
	}

	public void setLocked(boolean locked) {
	}

	public boolean isLocked() {
		return false;
	}
	

}
