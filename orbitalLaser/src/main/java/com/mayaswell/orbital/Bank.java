package com.mayaswell.orbital;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;
import android.widget.ArrayAdapter;

import com.mayaswell.audio.Envelope;
import com.mayaswell.audio.FX;
import com.mayaswell.audio.FXState;
import com.mayaswell.audio.LFO;
import com.mayaswell.audio.Sample;
import com.mayaswell.audio.Sequence;
import com.mayaswell.audio.Controllable.Filter;
import com.mayaswell.audio.Sample.Direction;
import com.mayaswell.audio.Sequence.Type;
import com.mayaswell.orbital.Slice;
import com.mayaswell.orbital.Wrecker.PlayMode;
import com.mayaswell.orbital.util.FloatPair;
import com.mayaswell.orbital.util.MappedProbability;
import com.mayaswell.orbital.util.RangedMP;
import com.mayaswell.util.AbstractBank;
import com.mayaswell.util.Automaton.Node;

/**
 * @author dak
 *
 */
public class Bank extends AbstractBank<Patch, CControl> {
	
	protected ArrayList<Sequence<Patch>> sequence;
	protected ArrayList<AlgoRhythm> algorithm;
	
	public Bank()
	{
		this(true);
	}
	
	public Bank(boolean doInit)
	{ 
		sequence = new ArrayList<Sequence<Patch>>();
		algorithm = new ArrayList<AlgoRhythm>();
		if (doInit) {
			patch.add(new Patch("default", true));
			sequence.add(new Sequence<Patch>("default"));
			algorithm.add(new AlgoRhythm("default"));
		}
	}

	@Override
	public Patch newPatch() {
		return new Patch();
	}
	
	@Override
	public boolean save(FileOutputStream fp) {
		XmlSerializer xml = Xml.newSerializer();
		try {
			FileWriter fw = new FileWriter(fp.getFD());
			xml.setOutput(fw);
			xml.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			xml.startDocument("UTF-8", true);
			xs(xml, "bank");
			sa(xml, "source", "orbital");
			for (Patch p: patch) {
				xs(xml, "patch");
				sa(xml, "name", p.name);
				fa(xml, "tempo", p.tempo);
				la(xml, "nDataBeats", p.state.nDataBeats);
				if (p.state.playMode != PlayMode.VANILLA) {
					String s = p.state.playMode.toString();
					if (p.state.playMode == PlayMode.AUTOMATOR) {
						s += ":"+p.state.algorithmId;
					}
					sa(xml, "playMode", s);
				}
				putSlices(xml, p.state.getSlices(), p.state.path);
				if (p.state.getPatternCount() > 0) {
					xs(xml, "pattern");
					for (int pi=0; pi<p.state.getPatternCount(); pi++) {
						SliceCell pati = p.state.getPatternSliceCell(pi);
						xs(xml, "cell");
						la(xml, "id", pati.getId());
						fa(xml, "dur", pati.getDuration());
						if (pati.getGain() != 1.0) fa(xml, "id", pati.getGain());
						xe(xml, "cell");
					}
					xe(xml, "pattern");
				}
				ArrayList<FXBusState> fxbus = p.state.getBuses();
				for (FXBusState fxb: fxbus) {
					xs(xml, "bus");
					sa(xml, "name", fxb.name);
					la(xml, "id", fxb.id);
					fa(xml, "gain", fxb.gain);
					fa(xml, "pan", fxb.pan);
					ba(xml, "enable", fxb.enable);
					
					for (FXState f: fxb.fx) {
						putFX(xml, f);
					}
					xe(xml, "bus");
				}
				putChaosParameters(xml, p.state);
				
				xe(xml, "patch");
			}
			for (Sequence<Patch> ps: sequence) {
				xs(xml, "sequence");
				sa(xml, "name", ps.name);
				for (Sequence<Patch>.Step pss: ps.getSteps()) {
					xs(xml, "step");
					switch (pss.type) {
					case ALGORITHM:
						la(xml, "alg", pss.patchIdx);
						if (pss.count != 1) {
							la(xml, "cnt", pss.count);
						}
						if (pss.probability != 1) {
							fa(xml, "pr", pss.probability);
						}
						break;
					case PATCH:
					default:
						la(xml, "pat", pss.patchIdx);
						if (pss.count != 1) {
							la(xml, "cnt", pss.count);
						}
						if (pss.probability != 1) {
							fa(xml, "pr", pss.probability);
						}
						break;
					}
					xe(xml, "step");
				}
				xe(xml, "sequence");
			}
			for (AlgoRhythm a: algorithm) {
				xs(xml, "algorithm");
				sa(xml, "name", a.name);
				xs(xml, "node");
				sa(xml, "id", "root");
				for (int j=0; j<a.getRoot().countCnx(); j++) {
					AlgoRhythm.NodeConnection nc = a.getRoot().getCnx(j);
					xs(xml, "cnx");
					la(xml, "weight", nc.getWeight());
					la(xml, "node", nc.getNode().getId());
					xe(xml, "cnx");
				}
				xe(xml, "node");
				for (int i=0; i<a.countNodes(); i++) {
					AlgoRhythm.Node n = a.getNode(i);
					xs(xml, "node");
					la(xml, "id", n.getId());
					for (int j=0; j<n.countCellData(); j++) {
						SliceCell sc = n.getCellData(j);
						xs(xml, "cell");
						la(xml, "group", sc.getId());
						fa(xml, "dur", sc.getDuration());
						if (sc.getGain() != 1.0) fa(xml, "gain", sc.getGain());
						xe(xml, "cell");
					}
					for (int j=0; j<n.countCnx(); j++) {
						AlgoRhythm.NodeConnection nc = n.getCnx(j);
						xs(xml, "cnx");
						la(xml, "weight", nc.getWeight());
						la(xml, "node", nc.getNode().getId());
						xe(xml, "cnx");
					}
					xe(xml, "node");
				}
				xe(xml, "algorithm");
			}
			xe(xml, "bank");
			xml.endDocument();
			fw.close();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void putChaosParameters(XmlSerializer xml, WreckageState state) throws IllegalArgumentException, IllegalStateException, IOException {
		putChaos(xml, state.patternShiftChance, "patternShiftChance");
		putChaos(xml, state.patternGridShiftChance, "patternGridShiftChance");
		putChaos(xml, state.groupShiftChance, "groupShiftChance");
		putChaos(xml, state.repeatPreviousChance, "repeatPreviousChance");
		putChaos(xml, state.reverseChance, "reverseChance");
		putChaos(xml, state.dynamicModChance, "dynamicModChance");
		putChaos(xml, state.durationModChance, "durationModChance");
		putChaos(xml, state.stutterModChance, "stutterModChance");
		putChaos(xml, state.pitchModChance, "pitchModChance");
	}

	private void putChaos(XmlSerializer xml, RangedMP pr, String nm) throws IllegalArgumentException, IllegalStateException, IOException {
		if (pr.getBaseProbability() == 0 && pr.size() == 0) {
			return;
		}
		xs(xml, "prob");
		sa(xml, "dst", nm);
		fa(xml, "base", pr.getBaseProbability());
		fa(xml, "raBase", pr.getRangeBase());
		fa(xml, "raScope", pr.getRangeScope());
		for (FloatPair fp: pr) {
			xs(xml, "val");
			fa(xml, "d", fp.first);
			fa(xml, "v", fp.second);
			xe(xml, "val");
		}
		xe(xml, "prob");
		
	}

	private void putChaos(XmlSerializer xml, MappedProbability pr, String nm) throws IllegalArgumentException, IllegalStateException, IOException {
		if (pr.getBaseProbability() == 0 && pr.size() == 0) {
			return;
		}
		xs(xml, "prob");
		sa(xml, "dst", nm);
		fa(xml, "base", pr.getBaseProbability());
		for (FloatPair fp: pr) {
			xs(xml, "val");
			fa(xml, "d", fp.first);
			fa(xml, "v", fp.second);
			xe(xml, "val");
		}
		xe(xml, "prob");
		
	}

	protected void putSlices(XmlSerializer xml, ArrayList<Slice> slices, String src) throws IOException {
		if (slices == null || src == null) {
			return;
		}
		xs(xml, "slices");
		sa(xml, "src", src);
		int i=0;
		for (Slice s: slices) {
			xs(xml, "slice");
			la(xml, "ind", s.ind);
			fa(xml, "position", s.position);
			la(xml, "startFrame", s.startFrame);
			int gid = s.getGroup();
			if (gid >= 0) {
				la(xml, "group", gid);
			}
			if (i < slices.size()-1 && s.hasNonDefaultParams()) {
				xs(xml, "slicep");
				if (s.direction != Direction.FORWARD) sa(xml, "direction", s.direction.toString());
				if (s.pan != 0) fa(xml, "pan", s.pan);
				if (s.gain != 1) fa(xml, "gain", s.gain);
				if (s.tune != 0) fa(xml, "tune", s.tune);
				if (s.stutter != 1) la(xml, "stutter", s.stutter);
				if (s.fxBus != 0) la(xml, "fxBus", s.fxBus);
				
				for (LFO psslf: s.lfo) {
					putLFO(xml, psslf);
				}
				for (Envelope psse: s.envelope) {
					putEnv(xml, psse);
				}
				xe(xml, "slicep");
//					impulse
			}
			xe(xml, "slice");
			i++;
		}
		xe(xml, "slices");
	}
	
	
	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private boolean doSliceParams(XmlPullParser xpp, Slice slice) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("slicep")) return false;
		
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("gain")) {
				slice.gain = Float.parseFloat(v);
			} else if (n.equals("pan")) {
				slice.pan = Float.parseFloat(v);
			} else if (n.equals("tune")) {
				slice.tune = Float.parseFloat(v);
			} else if (n.equals("stutter")) {
				slice.stutter = Integer.parseInt(v);
			} else if (n.equals("fxBus")) {
				slice.fxBus = Short.parseShort(v);
			} else if (n.equals("direction")) {
				slice.direction = Sample.Direction.parse(v);
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		int eventType = xpp.next();//xpp.getEventType();
		Filter fs = null;
		LFO lfo = null;
		Envelope env = null;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((lfo=doLFO(xpp)) != null) {
					slice.lfo.add(lfo);
					if (lfo.target.size() > 0) {
						Log.d("lfo target", "got "+lfo.target.get(0).target.ccontrolName());
					}
				} else if ((env=doEnvelope(xpp)) != null) {
					slice.envelope.add(env);
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("slicep")) { // tag consumed by doPatch()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'pad'");
	}
	
	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private Slice doSlice(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("slice")) return null;
		
		Slice pss = new Slice();
		
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("ind")){
				pss.ind = Integer.parseInt(v);
			} else if (n.equals("position")) {
				pss.position = Float.parseFloat(v);
			} else if (n.equals("startFrame")) {
				pss.startFrame = Integer.parseInt(v);
			} else if (n.equals("group")) {
				pss.setGroup(Integer.parseInt(v));
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		Log.d("load", "slices for "+pss.position);
		if (xpp.isEmptyElementTag()) {
			return pss;
		}
		int eventType = xpp.next();//xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if (doSliceParams(xpp, pss)) {
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("slice")) { // tag consumed by doPatch()
					return pss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'pad'");
	}
	
	private boolean doMappedProbability(XmlPullParser xpp, WreckageState state) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("prob")) return false;
		
		int na=xpp.getAttributeCount();
		MappedProbability pss = null;
		RangedMP rss = null;
		float prob = 0;
		float base = 0.5f;
		float scope = 0.5f;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("dst")) {
				if (v.equals("patternShiftChance")) {
					pss = state.patternShiftChance;
				} else if (v.equals("patternGridShiftChance")) {
					pss = state.patternGridShiftChance;
				} else if (v.equals("groupShiftChance")) {
					pss = state.groupShiftChance;
				} else if (v.equals("repeatPreviousChance")) {
					pss = state.repeatPreviousChance;
				} else if (v.equals("reverseChance")) {
					pss = state.reverseChance;
				} else if (v.equals("dynamicModChance")) {
					rss = state.dynamicModChance;
					pss = rss;
				} else if (v.equals("durationModChance")) {
					rss = state.durationModChance;
					pss = rss;
				} else if (v.equals("stutterModChance")) {
					rss = state.stutterModChance;
					pss = rss;
				} else if (v.equals("pitchModChance")) {
					rss = state.pitchModChance;
					pss = rss;
				} else {				
				}
			} else if (n.equals("base")) {
				prob = Float.valueOf(v);
			} else if (n.equals("raBase")) {
				base = Float.valueOf(v);
			} else if (n.equals("raScope")) {
				scope = Float.valueOf(v);
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		if (pss != null) {
			pss.setBaseProbability(prob);
		}
		if (rss != null) {
			rss.setRangeBase(base);
			rss.setRangeScope(scope);
		}
		
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		int eventType = xpp.next();//xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				FloatPair fp = null;
				if ((fp=doMapVal(xpp, (pss.size()+1)*0.5f, 50)) != null) {
					pss.add(fp);
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("prob")) { // tag consumed by doPatch()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'slices'");
	}

	private FloatPair doMapVal(XmlPullParser xpp, float fdd, float fvd) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("val")) return null;
		int na=xpp.getAttributeCount();
		float fd = fdd;
		float fv = fvd;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("v")) {
				fv = Float.parseFloat(v);
			} else if (n.equals("d")) {
				fd = Float.parseFloat(v);
			}
		}
		FloatPair fp = new FloatPair(fd, fv);
		if (xpp.isEmptyElementTag()) {
			return fp;
		}
		int eventType = xpp.next();//xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				doSkipUnexpected(xpp);
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("val")) { // tag consumed by doPatch()
					doSkipUnexpected(xpp);
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		return fp;
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private boolean doSlices(XmlPullParser xpp, WreckageState s) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("slices")) return false;
		
		ArrayList<Slice> pss = new ArrayList<Slice>();
		s.setSlices(pss);
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("src")) {
				s.path = v;
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		Log.d("load", "slices for "+s.path);
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		int eventType = xpp.next();//xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				Slice slice = null;
				if ((slice=doSlice(xpp)) != null) {
					pss.add(slice);
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("slices")) { // tag consumed by doPatch()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'slices'");
	}
	
	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	private Patch doPatch(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		String tag = xpp.getName();
		if (tag == null || !tag.equals("patch")) return null;
		
		Patch p = new Patch();
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				p.name = v;
			} else if (n.equals("tempo")){
				p.tempo = Float.parseFloat(v);
			} else if (n.equals("playMode")){
				String [] sa = v.split(":");
				p.state.playMode = PlayMode.parse(sa[0]);
				if (sa.length > 1) {
					try {
						p.state.algorithmId = Integer.parseInt(sa[1]);
					} catch (NumberFormatException e) {
						p.state.algorithmId = -1;
					}
				}
			} else if (n.equals("nDataBeats")){
				p.state.nDataBeats = Integer.parseInt(v);
			} else {
			}
		}
		if (xpp.isEmptyElementTag()) {
			return p;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				FXBusState bss;
				ArrayList<SliceCell> pss;
				if (doSlices(xpp, p.state)) {
				} else if (doMappedProbability(xpp, p.state)) {
				} else if ((bss=doBus(xpp)) != null) {
					p.state.addBus(bss);
				} else if ((pss=doPattern(xpp)) != null) {
					p.state.setPattern(pss);
				}
				/* else if ((xss=doController(xpp)) != null) {
					p.add(xss);
				} else if ((sss=doSensor(xpp)) != null) {
					p.add(sss);
				} else if ((rrr=doRibbon(xpp)) != null) {
					p.add(rrr);
				}
				*/
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("patch")) { // tag consumed in loop of doBank()
					return p;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'patch'");
	}
	
	private ArrayList<SliceCell> doPattern(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("pattern")) return null;
		if (xpp.isEmptyElementTag()) {
			return null;
		}
		
		int eventType = xpp.next();
		ArrayList<SliceCell> p = new ArrayList<SliceCell>();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				SliceCell sc = doCell(xpp);
				if (sc == null) {
					throw new XmlPullParserException("Unexpected start tag in xml for 'pattern'");
				}
				p.add(sc);
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("pattern")) {
					return p;
				}
			} else if(eventType == XmlPullParser.TEXT) {
				String s = xpp.getText();
				if (s != null) {
					for (String patis: s.split("\\s")) {
						p.add(new SliceCell(Integer.parseInt(patis), 1, 1));
					}
				}
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'pattern'");
	}

	private AlgoRhythm doAlgorithm(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("algorithm")) return null;
		
		AlgoRhythm p = new AlgoRhythm("");
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				p.name = v;
			} else {
			}
		}
		
		if (xpp.isEmptyElementTag()) {
			return p;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if (doNode(xpp, p)) {
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("algorithm")) { // tag consumed in loop of doBank()
					return p;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'algorithm'");
	}

	private boolean doNode(XmlPullParser xpp, AlgoRhythm p) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("node")) return false;
		
		int id = -1;
		boolean isRoot = false;
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("id")){
				if (v.equals("root")) {
					isRoot = true;
				} else {
					id = Integer.parseInt(v);
				}
			}
		}
		AlgoRhythm.Node ni;
		if (isRoot) {
			ni = p.getRoot();
		} else if (id >= 0) {
			ni = p.findNode(id);
			if (ni == null) {
				ni = p.addNode();
				ni.setId(id);
			}
		} else {
			ni = p.addNode();
		}
		
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				SliceCell sc = null;
				if (!isRoot && (sc=doCell(xpp)) != null) {
					ni.addCellData(sc);
				} else if (doCnx(xpp, p, ni)) {
					
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("node")) { // tag consumed in loop of doBank()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'node'");
	}

	private boolean doCnx(XmlPullParser xpp, AlgoRhythm a, AlgoRhythm.Node ni) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("cnx")) return false;
		
		int weight = 1; 
		int nodeid = -1; 
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("weight")){
				weight = Integer.parseInt(v);
			} else if (n.equals("node")){
				nodeid = Integer.parseInt(v);
			}
		}
		AlgoRhythm.Node ni2;
		if (nodeid >= 0) {
			ni2 = a.findNode(nodeid);
			if (ni2 == null) {
				ni2 = a.addNode();
				ni2.setId(nodeid);
			}
		} else {
			return false;
		}
		AlgoRhythm.NodeConnection nc = ni.addCnx(ni2);
		nc.setWeight(weight);
		
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("cnx")) { // tag consumed in loop of doBank()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'cnx'");
	}

	private SliceCell doCell(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		SliceCell sc = null;
		if (tag == null || !tag.equals("cell")) return sc;
		int na=xpp.getAttributeCount();
		float gain = 1;
		float dur = 1;
		int gr = -1;
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("group")){
				gr = Integer.parseInt(v);
			} else if (n.equals("id")) {
				gr = Integer.parseInt(v);
			} else if (n.equals("dur")) {
				dur = Float.parseFloat(v);
			} else if (n.equals("gain")) {
				gain = Float.parseFloat(v);
			}
		}
		sc = new SliceCell(gr, dur, gain);
		
		if (xpp.isEmptyElementTag()) {
			return sc;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("cell")) { // tag consumed in loop of doBank()
					return sc;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'cell'");
	}

	private Sequence<Patch> doSequence(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("sequence")) return null;
		
		Sequence<Patch> p = new Sequence<Patch>("");
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				p.name = v;
			} else {
			}
		}
		
		if (xpp.isEmptyElementTag()) {
			return p;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if (doStep(xpp, p)) {
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("sequence")) { // tag consumed in loop of doBank()
					return p;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'sequence'");
	}

	
	private boolean doStep(XmlPullParser xpp, Sequence<Patch> ps) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("step")) return false;
		
		Type type = Type.JUMP;
		float pr = 1;
		int cnt = 1;
		int alg = -1;
		int ind = -1;
		int patch = -1; 
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("pat")){
				patch = Integer.parseInt(v);
				type = Type.PATCH;
			} else if (n.equals("alg")) {
				type = Type.ALGORITHM;
				alg = Integer.parseInt(v);
			} else if (n.equals("cnt")) {
				cnt = Integer.parseInt(v);
			} else if (n.equals("pr")) {
				pr = Float.parseFloat(v);
			}
		}
	
		if (patch >= 0) {
			ps.addStep(Type.PATCH, patch, cnt, ind, pr);
		} else if (alg >= 0) {
			ps.addStep(Type.ALGORITHM, alg, cnt, ind, pr);
		} else {
			ps.addStep(type, patch, cnt, ind, pr);
		}
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("step")) { // tag consumed in loop of doBank()
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'step'");
	}

	private FXBusState doBus(XmlPullParser xpp) throws XmlPullParserException, IOException {
		String tag = xpp.getName();
		if (tag == null || !tag.equals("bus")) return null;
		
		FXBusState bss = new FXBusState(0, "");
		
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("name")){
				bss.name = v;
			} else if (n.equals("id")) {
				bss.id = Integer.parseInt(v);
			} else if (n.equals("gain")) {
				bss.gain = Float.parseFloat(v);
			} else if (n.equals("pan")) {
				bss.pan = Float.parseFloat(v);
			} else if (n.equals("enable")) {
				bss.enable = Boolean.parseBoolean(v);
			} else {
//				Log.d("bank error", "Unknown attribute in 'pad' element, "+n);
//				throw new XmlPullParserException("Unknown attribute in pad element, "+n);
			}
		}
		if (xpp.isEmptyElementTag()) {
			return bss;
		}
		int eventType = xpp.next();//xpp.getEventType();
		FXState fs = null;
		LFO lfo = null;
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((fs=doFX(xpp)) != null) {
					bss.fx.add(fs);
				} else {
					doSkipUnexpected(xpp);
//					throw new XmlPullParserException("Unexpected start tag "+xpp.getName());
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("bus")) { // tag consumed by doPatch()
					return bss;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'bus'");
	}

	/**
	 * @param xpp
	 * @return
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	@Override
	protected boolean doBank(XmlPullParser xpp) throws XmlPullParserException, IOException
	{
		patch = new ArrayList<Patch>();
		sequence = new ArrayList<Sequence<Patch>>();
		algorithm = new ArrayList<AlgoRhythm>();
		
		String tag = xpp.getName();
		if (tag == null || !tag.equals("bank")) return false;
		
		if (xpp.isEmptyElementTag()) {
			return true;
		}
		
		String source = null;
		int na=xpp.getAttributeCount();
		for (int i=0; i<na; i++) {
			String n = xpp.getAttributeName(i);
			String v = xpp.getAttributeValue(i);
			if (n.equals("source")){
				source = v;
			}
		}
		if (source == null || !source.equals("orbital")) {
			return false;
		}
		Patch p = null;
		Sequence<Patch> sp = null;
		AlgoRhythm ap = null;
		int eventType = xpp.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG) {
				if ((p=doPatch(xpp)) != null) {
					patch.add(p);
				} else if ((sp=doSequence(xpp)) != null) {
					sequence.add(sp);
				} else if ((ap=doAlgorithm(xpp)) != null) {
					algorithm.add(ap);
				}
			} else if(eventType == XmlPullParser.END_TAG) {
				tag = xpp.getName();
				if (tag.equals("bank")) { // this will be consumed by the 'next()' call in the current iteration of the loop of load() 
					return true;
				}
			} else if(eventType == XmlPullParser.TEXT) {
			}
			eventType = xpp.next();
		}
		throw new XmlPullParserException("Unexpected document end in xml for 'bank'");
	}

	@Override
	public CControl createControl(String s) {
		CControl cc = new CControl(s);
		return cc;
	}
	@Override
	protected ArrayList<FX> fxList() {
		return OrbitalLaser.fxList;
	}

	@Override
	public Patch clonePatch(int i) {
		Patch p = get(i);
		return (p!=null)? p.clone(): null;
	}

	@Override
	public Patch saveCopy(int i, Patch p) {
		return set(i, p.clone());
	}

	public void setSequenceAdapterItems(ArrayAdapter<Sequence<Patch>> aa)
	{
		if (aa == null) {
			return;
		}
		aa.clear();
		for (int i=0; i<sequence.size(); i++) {
			Sequence<Patch> p = sequence.get(i);
			aa.add(p);
		}
	}

	public int numSequences() {
		return sequence != null? sequence.size(): 0;
	}

	public String makeIncrementalSeqName(String name) {
		Pattern p = Pattern.compile("(\\D*)(\\d*)");
		Matcher m = p.matcher(name);
		if (m.matches()) {
			String base = "default";
			int n = 0;
			if (m.group(1) != null && m.group(1).length() > 0) {
				base = m.group(1);
			}
			if (m.group(2) != null && m.group(2).length() > 0) {
				n = Integer.parseInt(m.group(2));
			}
			boolean found;
			String nnm;
			do {
				n++;
				nnm = base+Integer.toString(n);
				found = false;
				for (Sequence<Patch> pp: sequence) {
					if (pp.name.equals(nnm)) {
						found = true;
						break;
					}
				}
			} while (found);
			return nnm;
		} else {
			return "default";
		}
	}

	public String makeIncrementalAlgName(String name) {
		Pattern p = Pattern.compile("(\\D*)(\\d*)");
		Matcher m = p.matcher(name);
		if (m.matches()) {
			String base = "default";
			int n = 0;
			if (m.group(1) != null && m.group(1).length() > 0) {
				base = m.group(1);
			}
			if (m.group(2) != null && m.group(2).length() > 0) {
				n = Integer.parseInt(m.group(2));
			}
			boolean found;
			String nnm;
			do {
				n++;
				nnm = base+Integer.toString(n);
				found = false;
				for (AlgoRhythm pp: algorithm) {
					if (pp.getName().equals(nnm)) {
						found = true;
						break;
					}
				}
			} while (found);
			return nnm;
		} else {
			return "default";
		}
	}


	public void add(AlgoRhythm s) {
		if (s != null) {
			algorithm.add(s);
		}
	}
	
	public int deleteAlgorithm(int i) {
		if (i >= 0 && i < algorithm.size()) {
			algorithm.remove(i);
			return algorithm.size() == 0? -1: i >= algorithm.size()? algorithm.size()-1: i; 
		}
		return -1;
	}

	public AlgoRhythm getAlgorithm(int csi) {
		if (algorithm == null || csi < 0 || csi >= algorithm.size()) {
			return null;
		}
		return algorithm.get(csi);
	}

	public int find(AlgoRhythm csi) {
		if (algorithm == null || csi == null) {
			return -1;
		}
		return algorithm.indexOf(csi);
	}
	
	public int numAlgorithms() {
		return algorithm.size();
	}
	
	public void add(Sequence<Patch> s) {
		if (s != null) {
			sequence.add(s);
		}
	}

	public int getSequenceIndex(Sequence<Patch> currentSequence) {
		if (sequence == null) return -1;
		return sequence.indexOf(currentSequence);
	}

	public int deleteSequence(int csi) {
		if (sequence == null && csi < 0 && csi >= sequence.size()) 
			return -1;
		sequence.remove(csi);
		return csi < sequence.size()? csi: sequence.size();
	}

	public Sequence<Patch> getSequence(int csi) {
		if (sequence == null || csi < 0 || csi >= sequence.size()) 
			return null;
		return sequence.get(csi);
	}

	public int find(Sequence<Patch> csi) {
		if (sequence == null || csi == null) {
			return -1;
		}
		return sequence.indexOf(csi);
	}
	
	public void fixSequencesOnDeletedPatch(int delPatchIdx) {
		for (Sequence<Patch> p: sequence) {
			for (Sequence<Patch>.Step ps: p.getSteps()) {
				if (ps.type == Type.PATCH) {
					if (ps.patchIdx == delPatchIdx) {
						ps.patchIdx = -1;
					} else if (ps.patchIdx > delPatchIdx) {
						ps.patchIdx--;
					}
				}
			}
		}
	}

	public void fixSequencesOnDeletedAlgorithm(int delAlgIdx) {
		for (Sequence<Patch> p: sequence) {
			for (Sequence<Patch>.Step ps: p.getSteps()) {
				if (ps.type == Type.ALGORITHM) {
					if (ps.patchIdx == delAlgIdx) {
						ps.patchIdx = -1;
					} else if (ps.patchIdx > delAlgIdx) {
						ps.patchIdx--;
					}
				}
			}
		}
		for (Patch p: patch) {
			if (p.state.algorithmId == delAlgIdx) {
				p.state.algorithmId = -1;
			} else if (p.state.algorithmId > delAlgIdx) {
				p.state.algorithmId--;
			}
		}
	}

	public void setAlgorithmAdapterItems(ArrayAdapter<AlgoRhythm> algSelectAdapter) {
		algSelectAdapter.clear();
		for (AlgoRhythm p: algorithm) {
			algSelectAdapter.add(p);
		}
	}

}
