package com.mayaswell.orbital;

import com.mayaswell.audio.Sample.Direction;

/**
 * base slice selection engine for the wrecker ... currently this is an implentation of the linear engine
 *
 */
public class WreckerEngine implements IWreckerEngine {
	protected Wrecker wrecker;
	protected WreckageState state = null;

	public WreckerEngine(Wrecker w) {
		wrecker = w;
	}

	@Override
	public float currentSliceGain() {
		return wrecker.currentSlice.gain;
	}

	@Override
	public float currentSliceTune() {
		return wrecker.currentSlice.tune;
	}

	@Override
	public short currentSliceFxBus() {
		return wrecker.currentSlice.fxBus;
	}

	@Override
	public Direction currentSliceDirection() {
		return wrecker.currentSlice.direction;
	}

	@Override
	public int currentStutter() {
		return wrecker.currentSlice.stutter;
	}

	@Override
	public int getFirstSlice() {
		return 0;
	}

	@Override
	public int getNextSlice() {
		return wrecker.currentSliceIndex+1;
	}

	@Override
	public boolean isFirstSlice() {
		return wrecker.currentSliceIndex == 0;
	}

	@Override
	public boolean initialize() {
		return true;
	}

	@Override
	public boolean atLoopEnd() {
		return wrecker.currentSliceIndex >= wrecker.state.sliceCount()-1;
	}

	@Override
	public float currentSliceBeatDuration() {
		return wrecker.state.getSlice(wrecker.currentSliceIndex+1).position-wrecker.currentSlice.position;
	}

	public void setState(WreckageState state) {
		this.state = state;
	}
}
