/*
 * Wrecker.cpp
 *
 *  Created on: Oct 19, 2014
 *      Author: dak
 */

#include <android/log.h>

#include "Wrecker.h"
#undef NOFX
Wrecker::Wrecker(int _id, int maxEnv, int maxEnvTgt, int maxLfo, int maxLfoTgt, int maxFXChain, int maxFXPerChain) {
	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "creating %d, %d %d %d %d, %d %d", _id, maxEnv, maxEnvTgt, maxLfo, maxLfoTgt, maxFXChain, maxFXPerChain);
	id = _id;
	buffer = NULL;
	setBufsize(4096);
	gain = 1;
	pan = 0;
	realTune = tune = 0;
	psi = 1;

	leftCycleBuffer = new float[framesPerControlCycle];
	rightCycleBuffer = new float[framesPerControlCycle];
	preFXBuffer = new float[framesPerControlCycle*2];

	for (int i=0; i<maxEnv; i++) {
		env.push_back(Envelope(maxEnvTgt, *this));
	}
	for (int i=0; i<maxLfo; i++) {
		lfo.push_back(LFO(maxLfoTgt, *this));
	}
#ifndef NOFX
	for (int i=0; i<maxFXChain; i++) {
		__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "push fx %d", i);
		fxBus.push_back(new OBus(i, maxFXPerChain, com_mayaswell_orbital_OrbitalLaser_Global_MAX_FX_PARAM, framesPerControlCycle, *this));
	}
#endif
	nActiveEnv = 0;
	nActiveLFO = 0;

	lfoOffset = std::vector<float>(fxIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_PER_WRECK, 0), 0);
	envOffset = std::vector<float>(fxIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_PER_WRECK, 0), 1);
	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "created: offset array %d", lfoOffset.size());
	controlFrame = 0;
	lastFraction = 0;
	nextDataFrame = 0;

	setControlRate(framesPerControlCycle/signalSampleRate);
}

Wrecker::~Wrecker() {
	if (buffer != NULL) {
		delete [] buffer;
	}
	if (leftCycleBuffer != NULL) {
		delete [] leftCycleBuffer;
	}
	if (rightCycleBuffer != NULL) {
		delete [] rightCycleBuffer;
	}
	if (preFXBuffer != NULL) {
		delete [] preFXBuffer;
	}
#ifndef NOFX
	for (OBus *bp: fxBus) {
		delete bp;
	}
#endif
}

bool
Wrecker::setTempo(float t)
{
	TimeSource::setTempo(t);
	for (int i=0; i<nActiveLFO; i++) {
		lfo[i].resetRate();
	}
	return true;
}

void
Wrecker::setBufsize(int bufsize)
{
	if (buffer != NULL) {
		delete [] buffer;
	}
	buffer = new float[bufsize];
}

void
Wrecker::setSampleInfo(long ntf, short nc)
{
	nTotalFrames = ntf;
	nChannels = nc;
}

void
Wrecker::fire()
{

}

void
Wrecker::stop()
{

}

void
Wrecker::reloop()
{

}

void
Wrecker::setGain(float g)
{
	gain = g;
//	__android_log_print(ANDROID_LOG_DEBUG, "CWreck", "set gain %g\n", g);
}

void
Wrecker::setPan(float p)
{
	pan = p;
}

void
Wrecker::setTune(float t, float e, float l)
{
	float rt = (t/12)+e+l;
	tune = t;
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set tune %g mods %g %g psi %g\n", t, e, l, psi);
	if (realTune != rt) {
		realTune = rt;
		psi = exp2_tune_psi[rt];//exp2(t/12);
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set tune rt %g psi %g\n", rt, psi);
	}
}


inline void
Wrecker::calculateControlsStereo(float &l, float &r)
{
	float p=pan;
	float g=gain;

	bool hasPanEnv = false;
	bool hasGainEnv = false;
	hasTuneMod = false;
	std::vector<bool> hasLFORateMod(com_mayaswell_orbital_OrbitalLaser_Global_LFO_PER_WRECK, false);
	std::vector<bool> hasLFORateEnv(com_mayaswell_orbital_OrbitalLaser_Global_LFO_PER_WRECK, false);
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "calculateControlsStereo %d %d", nActiveLFO, nActiveEnv);

	for (int i=0; i<lfoOffset.size(); i++) {
		lfoOffset[i] = 0;
	}
	std::unordered_set<int> wasModBus;
	std::unordered_set<int> wasModFx;
	wasModBus = modBus;
	wasModFx = modFx;
	modBus.clear();
	modFx.clear();

	for (int i=0; i<nActiveLFO; i++) {
		LFO &lf = lfo[i];

		float a = lf();
		for (int j=0; j<lf.nActiveSends; j++) {
			ModulationTarget&mt = lf.target[j];
			if (mt.target > 0 && mt.target <lfoOffset.size()) {
				float am = a*(mt.amount*envOffset[lfoTargetDepthId(i,j)]);
//				__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "lfo %d %g", mt.target, am);
				if (mt.target == com_mayaswell_orbital_CControl_TUNE) {
					hasTuneMod = true;
				} else if (isLFOControl(mt.target)) {
					int l = lfo4Id(mt.target);
					int p = lfoParam4Id(mt.target);
					if (p == 0) hasLFORateMod[l] = true;
				} else if (isEnvControl(mt.target)) {
				} else if (isBusControl(mt.target)) {
					int bid = bus4Id(mt.target);
					modBus.insert(bid);
					wasModBus.erase(bid);
				} else if (isFXControl(mt.target)) {
					int bfid = bfx4Id(mt.target);
					modFx.insert(bfid);
					wasModFx.erase(bfid);
				}
				lfoOffset[mt.target] += am;

			}
		}
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "wrecker", "done lfo %d %d", nActiveLFO, nActiveEnv);

	for (int i=0; i<envOffset.size(); i++) {
		envOffset[i] = 1;
	}
	for (int i=0; i<nActiveEnv; i++) {
		Envelope & e = env[i];
		float a = e();
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "envelope %d %g %d", i, a, e.nActiveSends);
		for (int j=0; j<e.nActiveSends; j++) {
			ModulationTarget&mt = e.target[j];
				float mta = mt.amount+lfoOffset[envTargetDepthId(i,j)];
				if (mt.target > 0 && mt.target <envOffset.size()) {
					envOffset[mt.target] = (mt.amount < 0)?((a-1)*mta):(a*mta);
//					__android_log_print(ANDROID_LOG_DEBUG, "wrecker", "have env target %d total %g lock %d a %g val %g amt %g lf %g",  mt.target, envOffset[mt.target], e.lock, a, mta, mt.amount, lfoOffset[envTargetDepthId(i,j)]);
					if (mt.target == com_mayaswell_orbital_CControl_TUNE) {
						envOffset[mt.target] -= mt.amount;
						hasTuneMod = true;
					} else if (mt.target == com_mayaswell_orbital_CControl_PAN) {
						hasPanEnv = true;
					} else if (mt.target == com_mayaswell_orbital_CControl_GAIN) {
						hasGainEnv = true;
					} else if (isLFOControl(mt.target)) {
						int l = lfo4Id(mt.target);
						int p = lfoParam4Id(mt.target);
						if (p == 0) hasLFORateEnv[l] = true;
					} else if (isEnvControl(mt.target)) {
					} else if (isBusControl(mt.target)) {
						int bid = bus4Id(mt.target);
						modBus.insert(bid);
						wasModBus.erase(bid);
					} else if (isFXControl(mt.target)) {
						int bfid = bfx4Id(mt.target);
						modFx.insert(bfid);
						wasModFx.erase(bfid);
					}
				}
		}
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "wrecker", "done env %d %d", nActiveLFO, nActiveEnv);

	if (hasTuneMod) {
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "tune envelope %g %g %g", tune, envOffset[com_mayaswell_orbital_CControl_TUNE], lfoOffset[com_mayaswell_orbital_CControl_TUNE]);
		setTune(tune, envOffset[com_mayaswell_orbital_CControl_TUNE], lfoOffset[com_mayaswell_orbital_CControl_TUNE]);
	}

	for (int i=0; i<nActiveLFO; i++) {
		if (hasLFORateMod[i] || hasLFORateEnv[i]) {
			float mod = 0;
			if (hasLFORateEnv[i]) {
				mod = LFO::logRmin * (1-envOffset[lfoRateId(i)]);
			}
			mod += lfoOffset[lfoRateId(i)];
			float emod = pow2_6[mod];//pow(2, mod);

//			__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "lfo mod %g %g %g", mod, emod, pow(2, mod));
			lfo[i].modulate(emod);
		}
	}

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "adjusted mods %d %d", nActiveLFO, nActiveEnv);

	for (auto bid: modBus) {
		OBus *b = fxBus[bid];
		b->setGain(b->getGain(), envOffset[busGainId(bid)], lfoOffset[busGainId(bid)]);
		b->setPan(b->getPan(), envOffset[busPanId(bid)], lfoOffset[busPanId(bid)]);
	}
	for (auto bfid: modFx) {
		int bid = bfid/com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX;
		int fid = bfid%com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX;
		OBus *b = fxBus[bid];
		int nc = b->nParams(fid);
//		__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "adjusted mods %d %d %d nc %d", bfid, bid, fid, nc);
		for (int i=0; i<nc; i++) {
			b->setFXParam(fid, i, b->getParam(fid, i)*envOffset[fxParamId(bid, fid, i)] + lfoOffset[fxParamId(bid, fid, i)], false);
//		__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "adjusted param %d %g %g %g", i, b->getParam(fid, i), envOffset[fxParamId(bid, fid, i)], lfoOffset[fxParamId(bid, fid, i)]);
		}
	}
	for (auto bid: wasModBus) {
		OBus *b = fxBus[bid];
		b->setGain(b->getGain(), 1, 0);
		b->setPan(b->getPan(), 1, 0);
	}
	for (auto bfid: wasModFx) {
		int bid = bfid/com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX;
		int fid = bfid%com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX;
		OBus *b = fxBus[bid];
		int nc = b->nParams(fid);
		for (int i=0; i<nc; i++) {
			b->setFXParam(fid, i, b->getParam(fid, i), true);
		}
	}

	if (hasPanEnv) {
		p = pan*envOffset[com_mayaswell_orbital_CControl_PAN] + lfoOffset[com_mayaswell_orbital_CControl_PAN];
	} else {
		p = pan + lfoOffset[com_mayaswell_orbital_CControl_PAN];
	}
	if (hasGainEnv) {
		g = gain*envOffset[com_mayaswell_orbital_CControl_GAIN] + lfoOffset[com_mayaswell_orbital_CControl_GAIN];
	} else {
		g = gain + lfoOffset[com_mayaswell_orbital_CControl_GAIN];
	}
	if (p < -1) p = -1; else if (p > 1) p = 1;
	if (g < 0) g = 0; else if (g > 1) g = 1;
	r = rAmp = (g)*(1+p)/2;
	l = lAmp = (g)*(1-p)/2;
}

/**
 * I'm not expecting this to get called ... most of the lf modulation stuff will work ... fill this sometime later for completeness,
 * but ignore until then
 * todo
 */
inline void
Wrecker::calculateControlsMono(float &l)
{
	l = lAmp = gain;
}


/**
 *
 *
 * @param buff output buffer
 * @param nRequestedFrames optimal number of frames to play.
 * @param chunkData data etc for the chunk
 * @param chunkStartFrame
 * @param chunkNFrames
 * @param nextBufferStartL start of 'next' buffer
 * @param nextBufferStartR
 * @param direction >= 0 for forwards, else backwards
 */
inline int
Wrecker::playChunkReplacing(
		float *buff, int nRequestedFrames, int nDataFramesAvailable, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStartFrame, int chunkNFrames, float nextBufferStartL, float nextBufferStartR, short direction, short fxBusI)
{
	int currentBufferOffset = 0;
//	int nDataFramesAvailable = 0;
	if (chunkNFrames > 0) {
		currentBufferOffset = currentDataFrame-chunkStartFrame; // currentBufferOffest initially in frames
		if (currentBufferOffset < 0) {
			currentBufferOffset = 0;
		}
		if (direction >= 0) {
			if (nDataFramesAvailable > chunkNFrames - currentBufferOffset) {
				nDataFramesAvailable = chunkNFrames - currentBufferOffset;
			}
		} else {
			if (nDataFramesAvailable > currentBufferOffset+1) {
				nDataFramesAvailable = currentBufferOffset+1;
			}
		}
		if (nDataFramesAvailable < 0) {
			nDataFramesAvailable = 0;
		}
		currentBufferOffset = currentBufferOffset*nChannels; // currentBufferOffest now an index into buffer
	} else {
		nDataFramesAvailable = 0;
	}

	float *lSig = leftCycleBuffer;
	float *rSig = rightCycleBuffer;
	unsigned int buffI = 0;
	unsigned int chunkI = currentBufferOffset;
	unsigned int nCycleFrames;
	unsigned int nControlChunkFrames;
	unsigned int nOutputFrames = 0;
	float lRaw1 = 0;
	float rRaw1 = 0;
	float lRaw2 = 0;
	float rRaw2 = 0;
	long lframe=-1;
	long frame=-1;
	long nextBufferStartFrame = (direction >= 0)?(chunkStartFrame+chunkNFrames):(chunkStartFrame-1);

	bool pitchShifting = (tune != 0 || hasTuneMod);
	double phs=0;
	if (currentDataFrame != nextDataFrame) {
		phs = currentDataFrame;
	} else {
		phs = currentDataFrame+lastFraction;
	}

//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "wrecker bus %d fr %d %d %d %d chunk %d %d ", fxBusI, nRequestedFrames, (int)nDataFramesAvailable, (int)nextBufferStartFrame, (int)currentBufferOffset, (int)chunkStartFrame, (int)chunkNFrames);
	if (nOutChannels == 2) {
		float crAmp=rAmp;
		float clAmp=lAmp;
		if (nChannels == 2) {
			nCycleFrames = (nDataFramesAvailable > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
//			__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "stereo loop0 %d ", nCycleFrames);
			while (nCycleFrames > 0) {
//				__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "stereo loop1 %d %d %d %d chunk %d %d ", nRequestedFrames, (int)nDataFramesAvailable, (int)nextBufferStartFrame, (int)currentBufferOffset, (int)chunkStartFrame, (int)chunkNFrames);
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}

//					__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop2 cycle %d %d %d ", nControlChunkFrames, nCycleFrames, nOutputFrames);

					if (direction >= 0) { // forwards, un pitch shifted, stereo sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI++];
							rSig[i] = chunkData[chunkI++];
						}
					} else { // backwards, un pitch shifted, stereo sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI--];
							rSig[i] = chunkData[chunkI--];
						}
					}
//					__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop3 set data %g %g", lSig[0], rSig[0]);
				} else {
//					__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop2 pitch shifting");
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					if (direction >= 0) { // forwards, pitch shifted, stereo sample data into a stereo out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI+2];
										rRaw2 = chunkData[chunkI+3];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							rSig[i] = interpolate(rRaw1, rRaw2, frac);
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, stereo sample data into a stereo out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI-2];
										rRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							rSig[i] = interpolate(rRaw1, rRaw2, frac);
							phs += rpsi;
						}
					}
				}
//				lFilter.apply(lSig, nControlChunkFrames);
//				rFilter.apply(rSig, nControlChunkFrames);
//				__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop data %d %d %d %d chunk %d %d ", nRequestedFrames, (int)nDataFramesAvailable, (int)nextBufferStartFrame, (int)currentBufferOffset, (int)chunkStartFrame, (int)chunkNFrames);
#ifdef NOFX
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*rSig[i];
				}
#else
				for (unsigned int i=0, j=0; i<nControlChunkFrames; i++, j+=2) {
					preFXBuffer[j] = clAmp*lSig[i];
					preFXBuffer[j+1] = crAmp*rSig[i];
				}
				applyFXStereo(preFXBuffer, nControlChunkFrames, fxBusI, buff+buffI);
				buffI+=2*nControlChunkFrames;
#endif
//				__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop3 set data %g %g", lSig[0], rSig[0]);
//				__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop3 set into %g %g %g %g %d", buff[0], buff[1], clAmp, crAmp, buffI);
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}

//			__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop5 %d ", nOutputFrames);
			//(!pitchShifting?currentDataFrame + nOutputFrames:floor(phs))>=nTotalFrames || nDataFramesAvailable <= 0
			if (!pitchShifting) {
				nextDataFrame = currentDataFrame + (direction >= 0?nOutputFrames:-nOutputFrames);
				lastFraction = 0;
			} else {
				nextDataFrame = floor(phs);
				lastFraction = phs-nextDataFrame;
			}
// or 	XXX		if (nDataFramesAvailable <= nOutputFrames)
			if (nOutputFrames < nRequestedFrames) {
				nCycleFrames = nRequestedFrames-nOutputFrames;
				while (nCycleFrames > 0) {
					if (controlFrame == 0) {
						calculateControlsStereo(clAmp, crAmp);
					}
					nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
/*
					memset(rSig, 0, nControlChunkFrames*sizeof(float));
					memset(lSig, 0, nControlChunkFrames*sizeof(float));
//					lFilter.apply(lSig, nControlChunkFrames);
//					rFilter.apply(rSig, nControlChunkFrames);
*/
//					__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop null %d %d %d %d chunk %d %d ", nRequestedFrames, (int)nDataFramesAvailable, (int)nextBufferStartFrame, (int)currentBufferOffset, (int)chunkStartFrame, (int)chunkNFrames);
#ifdef NOFX
					for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
						buff[buffI] = clAmp*lSig[i];
						buff[buffI+1] = crAmp*rSig[i];
					}
#else
					memset(preFXBuffer, 0, 2*nControlChunkFrames*sizeof(float));
					applyFXStereo(preFXBuffer, nControlChunkFrames, fxBusI, buff+buffI);
					buffI+=2*nControlChunkFrames;
#endif
					if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
						controlFrame = 0;
					}
					nCycleFrames -= nControlChunkFrames;
					nOutputFrames += nControlChunkFrames;
//					float rpsi = (direction>=0)?psi:-psi; // ????
//					phs += rpsi*nControlChunkFrames;
				}
			}
//			__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "loop6 %d ", nOutputFrames);
		} else if (nChannels == 1) {
			nCycleFrames = (nDataFramesAvailable > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
//			__android_log_print(ANDROID_LOG_DEBUG, "Wrecker", "mono loop1 %d %d %d %d chunk %d %d ", nRequestedFrames, (int)nDataFramesAvailable, (int)nextBufferStartFrame, (int)currentBufferOffset, (int)chunkStartFrame, (int)chunkNFrames);
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsStereo(clAmp, crAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, mono sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI++];
						}
					} else  {  // backwards, un pitch shifted, mono sample data into a stereo out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI--];
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					if (direction >= 0) { // forwards, pitch shifted, single channel sample data into a stereo out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI+1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, single channel sample data into a stereo out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					}
				}
//				lFilter.apply(lSig, nControlChunkFrames);
#ifdef NOFX
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
					buff[buffI] = clAmp*lSig[i];
					buff[buffI+1] = crAmp*lSig[i];
				}
#else
				for (unsigned int i=0, j=0; i<nControlChunkFrames; i++, j+=2) {
					preFXBuffer[j] = clAmp*lSig[i];
					preFXBuffer[j+1] = crAmp*lSig[i];
				}
				applyFXStereo(preFXBuffer, nControlChunkFrames, fxBusI, buff+buffI);
				buffI+=2*nControlChunkFrames;
#endif
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0; // -= framesPerControlCycle ... if it's > we have a mess where control cycle is skipped. so line above, nControlChunkFrames = .... etc
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
			if (!pitchShifting) {
				nextDataFrame = currentDataFrame + (direction >= 0?nOutputFrames:-nOutputFrames);
				lastFraction = 0;
			} else {
				nextDataFrame = floor(phs);
				lastFraction = phs-nextDataFrame;
			}
			// ((!pitchShifting)?currentDataFrame + nOutputFrames:floor(phs))>=nTotalFrames || nDataFramesAvailable <= 0
			if (nDataFramesAvailable <= nOutputFrames) {
				nCycleFrames = nRequestedFrames-nOutputFrames;
				while (nCycleFrames > 0) {
					if (controlFrame == 0) {
						calculateControlsStereo(clAmp, crAmp);
					}
					nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
#ifdef NOFX
					memset(lSig, 0, nControlChunkFrames*sizeof(float));
//					lFilter.apply(lSig, nControlChunkFrames);
					for (unsigned int i=0; i<nControlChunkFrames; i++, buffI+=2) {
						buff[buffI] = clAmp*lSig[i];
						buff[buffI+1] = crAmp*lSig[i];
					}
#else
					memset(preFXBuffer, 0, 2*nControlChunkFrames*sizeof(float));
					applyFXStereo(preFXBuffer, nControlChunkFrames, fxBusI, buff+buffI);
					buffI+=2*nControlChunkFrames;

					if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
						controlFrame = 0;
					}
#endif
					nCycleFrames -= nControlChunkFrames;
					nOutputFrames += nControlChunkFrames;
//					float rpsi = (direction>=0)?psi:-psi; // XXX as above
//					phs += rpsi*nControlChunkFrames;
				}
			}
		}
	} else if (nOutChannels == 1) { // mono output ... an edge case we hope not to see
		float clAmp = lAmp;
		nCycleFrames = (nDataFramesAvailable > 0)?nRequestedFrames:0; // we just check this egde case ... there isn't a 1:1 correspondence between requirements and data consumed
		if (nChannels == 2) {
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, stereo sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = (chunkData[chunkI++]+chunkData[chunkI++])/2;
						}
					} else {  // backwards, un pitch shifted, stereo sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = (chunkData[chunkI--]+chunkData[chunkI--])/2;
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					if (direction >= 0) { // forwards, pitch shifted, stereo sample data into a mono out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI+2];
										rRaw2 = chunkData[chunkI+3];
									}
								}
							}
							lSig[i] = (interpolate(lRaw1, lRaw2, frac)+interpolate(rRaw1, rRaw2, frac))/2;
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, stereo sample data into a mono out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = 2*(frame-chunkStartFrame);
									lRaw1 = chunkData[chunkI];
									rRaw1 = chunkData[chunkI+1];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
										rRaw2 = nextBufferStartR;
									} else {
										lRaw2 = chunkData[chunkI-2];
										rRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = (interpolate(lRaw1, lRaw2, frac)+interpolate(rRaw1, rRaw2, frac))/2;
							phs += rpsi;
						}
					}
				}
//				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
		} else if (nChannels == 1) { // this is near identical to the mono -> stereo case except for calculateControlsMono
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				if (!pitchShifting) {
					if (nOutputFrames + nControlChunkFrames > nDataFramesAvailable) {
						nCycleFrames = nControlChunkFrames = nDataFramesAvailable-nOutputFrames;
					}
					if (direction >= 0) { // forwards, un pitch shifted, single channel sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI++];
						}
					} else { // backwards, un pitch shifted, single channel sample data into a mono out
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lSig[i] = chunkData[chunkI--];
						}
					}
				} else {
					lframe = frame = -1;
					lRaw1 = rRaw1 = lRaw2 = rRaw2 = 0;
					float rpsi = (direction>=0)?psi:-psi;
					if (direction >= 0) { // forwards, pitch shifted, single channel sample data into a mono out
						float rpsi = psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame >= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame >= nextBufferStartFrame-1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI+1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					} else { // backwards, pitch shifted, single channel sample data into a mono out
						float rpsi = -psi;
						for (unsigned int i=0; i<nControlChunkFrames; i++) {
							lframe = frame;
							frame = (long)floor(phs);
							float frac = phs-frame;
							if (frame != lframe) {
								if (frame <= nextBufferStartFrame) { // hit the end of data
									nCycleFrames = nControlChunkFrames = i;
									break;
								} else {
									chunkI = frame-chunkStartFrame;
									lRaw1 = chunkData[chunkI];
									if (frame <= nextBufferStartFrame+1) {
										lRaw2 = nextBufferStartL;
									} else {
										lRaw2 = chunkData[chunkI-1];
									}
								}
							}
							lSig[i] = interpolate(lRaw1, lRaw2, frac);
							phs += rpsi;
						}
					}
				}
//				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
			}
		}
		if (!pitchShifting) {
			nextDataFrame = currentDataFrame + (direction >= 0?nOutputFrames:-nOutputFrames);
			lastFraction = 0;
		} else {
			nextDataFrame = floor(phs);
			lastFraction = phs-nextDataFrame;
		}
		if (((!pitchShifting)?currentDataFrame + nOutputFrames:floor(phs))>=nTotalFrames || nDataFramesAvailable <= 0) {
			nCycleFrames = nRequestedFrames-nOutputFrames;
			while (nCycleFrames > 0) {
				if (controlFrame == 0) {
					calculateControlsMono(clAmp);
				}
				nControlChunkFrames = (controlFrame+nCycleFrames > framesPerControlCycle)? (framesPerControlCycle-controlFrame):nCycleFrames;
				memset(lSig, 0, nControlChunkFrames*sizeof(float));
//				lFilter.apply(lSig, nControlChunkFrames);
				for (unsigned int i=0; i<nControlChunkFrames; i++, buffI++) {
					buff[buffI] = clAmp*lSig[i];
				}
				if ((controlFrame += nControlChunkFrames) >= framesPerControlCycle) {
					controlFrame = 0;
				}
				nCycleFrames -= nControlChunkFrames;
				nOutputFrames += nControlChunkFrames;
				float rpsi = (direction>=0)?psi:-psi;
				phs += psi*nControlChunkFrames;
			}
		}
	} else {
		// we would be exceedingly ambitious for a phone ;D
	}
	return nOutputFrames;
}


void
Wrecker::applyFXStereo(float *in, int nControlChunkFrames, short fxBusi, float *out)
{
	memset(out, 0, 2*nControlChunkFrames*sizeof(float));
	for (int i=0; i<fxBus.size(); i++) {
		OBus *b = fxBus[i];
		if (b->hasTail || i==fxBusi) {
			b->zero(nControlChunkFrames);
		}
	}
	for (int i=0; i<fxBusi; i++) {
		OBus *b = fxBus[i];
		if (b->hasTail) {
			b->apply(nControlChunkFrames);
			b->addto(out, nControlChunkFrames);
		}
	}
	fxBus[fxBusi]->apply(in, nControlChunkFrames);
	fxBus[fxBusi]->addto(out, nControlChunkFrames);
	for (int i=fxBusi+1; i<fxBus.size(); i++) {
		OBus *b = fxBus[i];
		if (b->hasTail) {
			b->apply(nControlChunkFrames);
			b->addto(out, nControlChunkFrames);
		}
	}
}

int
Wrecker::playChunkMixer(
		MasterBus *master, int buffInd, int nIterSliceFrames, int nIterDataFrames, short nOutChannels, int currentDataFrame,
		float *chunkData, int chunkStart, int chunkLen, float nextBufferStartL, float nextBufferStartR, short direction, short fxi)
{
	nIterSliceFrames = playChunkReplacing(
			buffer, nIterSliceFrames, nIterDataFrames, nOutChannels, currentDataFrame, chunkData, chunkStart, chunkLen,
			nextBufferStartL, nextBufferStartR, direction, fxi);
	master->mxn(buffer, buffInd, nIterSliceFrames, 1);
	/*
	if (send.size() == 0) {

	} else {
		for (int i=0; i<nActiveSend; i++) {
			Send &s = send[i];
			if (s.bus != NULL && s.gain > 0 && !s.mute) {
				s.bus->mxn(buffer, buffInd, nIterSliceFrames, s.gain);
			}
		}
	}*/
	return nIterSliceFrames;
}

bool
Wrecker::setEnvelopeTarget(int which, int whichn, int cc, float amt)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set env target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= env.size()) {
		return false;
	}
	if (whichn >= env[which].nActiveSends || whichn < 0 || whichn >= env[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING)
			return false;
	}

	int was = env[which].target[whichn].target;

	env[which].target[whichn].target = cc;
	env[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set env target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	if (was == com_mayaswell_orbital_CControl_TUNE) {
		if (hasLFO(com_mayaswell_orbital_CControl_TUNE)<0 && hasEnv(com_mayaswell_orbital_CControl_TUNE)<0) {
			setTune(tune);
		}
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
//		__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set env target %d %d to %d %g was %d %d %d", which, whichn, cc, amt, was, lw, pw);
		if (pw == 0) {
			if (hasLFO(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}
	return true;
}

bool
Wrecker::setLFOTarget(int which, int whichn, int cc, float amt)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (which < 0 || which >= lfo.size()) {
		return false;
	}
	if (whichn < 0 || whichn >= lfo[which].target.size()) {
		if (cc != com_mayaswell_audio_Control_NOTHING) {
			return false;
		}
	}
	int was = lfo[which].target[whichn].target;

	lfo[which].target[whichn].target = cc;
	lfo[which].target[whichn].amount = amt;

//	__android_log_print(ANDROID_LOG_DEBUG, "CPadSample", "done set lfo target %d %d %d %g", which, whichn, cc, amt);
	if (was == cc) {
		return true;
	}
	if (was == com_mayaswell_orbital_CControl_TUNE) {
		if (hasLFO(com_mayaswell_orbital_CControl_TUNE)<0 && hasEnv(com_mayaswell_orbital_CControl_TUNE)<0) {
			setTune(tune);
		}
	} else if (isLFOControl(was)) {
		int lw = lfo4Id(was);
		int pw = lfoParam4Id(was);
		if (pw == 0) {
			if (hasLFO(was) < 0 && hasEnv(was) < 0) {
				lfo[lw].resetRate();
			}
		}
	}
	return true;
}


bool
Wrecker::setBusGain(int whichBus, float g)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	b->setGain(g);
	return true;
}

bool
Wrecker::setBusPan(int whichBus, float p)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	b->setPan(p);
	return true;
}

bool
Wrecker::setBusEnable(int whichBus, bool en)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	b->setEnable(en);
	return true;
}

bool
Wrecker::setFXType(int whichBus, int whichFX, int type, int nParams, float *params)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "::setFXType %d %d type %d par %d %g", whichBus, whichFX, type, nParams, nParams>0?params[0]:0);
	return b->setFXType(whichFX, type, nParams, params);
}

bool
Wrecker::setFXEnable(int whichBus, int whichFX, bool en)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	return b->setFXEnable(whichFX, en);
}

bool
Wrecker::setFXParam(int whichBus, int whichFX, int whichParam, float val)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	return b->setFXParam(whichFX, whichParam, val);
}

bool
Wrecker::setFXParams(int whichBus, int whichFX, int nParams, float *params)
{
	if (whichBus < 0 || whichBus >= fxBus.size()) {
		return false;
	}
	OBus *b = fxBus[whichBus];
	return b->setFXParams(whichFX, nParams, params);
}

