/*
 * Bus.cpp
 *
 *  Created on: Jan 14, 2016
 *      Author: dak
 */

#include "OBus.h"

OBus::OBus(short _id, int maxFX, int maxFXparam, int maxFrame, TimeSource &t)
	: FXChain(id, maxFX, maxFXparam, t, true)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::Bus %d %d %d %d", _id, maxFX, maxFXparam, maxFrame);
	buf = new float[2*maxFrame];
	setPan(0);
	setGain(1);
}

OBus::~OBus()
{
	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::~Bus");
	if (buf) {
		delete [] buf;
	}
}

void
OBus::setGain(float g, float e, float l)
{
	gain = g;
	gAct = g*e+l;
	setLRAmp(gAct,pAct);
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::setGain %g %g %g", g, lAmp, rAmp);
}

void
OBus::setPan(float p, float e, float l)
{
	pan = p;
	pAct = p*e+l;
	setLRAmp(gAct, pAct);
}

void
OBus::setLRAmp(float ga, float pa)
{
	lAmp = ga*(1-pa)/2;
	rAmp = ga*(1+pa)/2;
}

void
OBus::setEnable(bool e)
{
	enable = e;
}

void
OBus::zero(int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::zero");
	memset(buf, 0, n*2*sizeof(float));
}

void
OBus::addto(float *b, int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::addto");
	for (int i=0; i<2*n; i+=2) {
		*b++ += lAmp*buf[i];
		*b++ += rAmp*buf[i+1];
	}
}

void
OBus::apply(float *b, int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::apply in");
	memcpy(buf, b, n*2*sizeof(float));
	applyFX(buf, n);
}

void
OBus::apply(int n)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "Bus::apply");
	applyFX(buf, n);
}




