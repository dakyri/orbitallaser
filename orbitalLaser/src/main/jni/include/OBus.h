/*
 * Bus.h
 *
 *  Created on: Nov 14, 2013
 *      Author: dak
 */

#ifndef BUS_H_
#define BUS_H_

#include <vector>
#include "TimeSource.h"
#include "FXChain.h"
#include "LFO.h"

class OBus: public FXChain
{
public:
	OBus(short _id, int maxFX, int maxFXparam, int maxFrame, TimeSource &t);
	virtual ~OBus();

	void zero(int n);
	void addto(float *b, int n);
	void apply(float *b, int n);
	void apply(int n);
	void setGain(float g, float e=1, float m=0);
	void setPan(float p, float e=1, float m=0);
	void setLRAmp(float ga, float pa);
	void setEnable(bool en);
	float getGain() { return gain; }
	float getPan() { return pan; }

protected:
	float gain;
	float pan;
	bool enable;

	float gAct;
	float pAct;

	float lAmp;
	float rAmp;

	float *buf;
};


#endif /* BUS_H_ */
