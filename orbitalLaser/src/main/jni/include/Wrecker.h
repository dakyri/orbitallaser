/*
 * Wrecker.h
 *
 *  Created on: Oct 19, 2014
 *      Author: dak
 */

#ifndef WRECKER_H_
#define WRECKER_H_

#include <vector>
#include <unordered_set>

#include "OBus.h"
#include "MasterBus.h"
#include "Envelope.h"
#include "LFO.h"

#include "com_mayaswell_audio_Control.h"
#include "com_mayaswell_orbital_CControl.h"
#include "com_mayaswell_audio_Controllable.h"
#include "com_mayaswell_orbital_OrbitalLaser_Global.h"

#include "ControlDefs.h"

class Wrecker: public TimeSource, public LFOHost, public EnvelopeHost {
public:
	Wrecker(int id, int maxEnv, int maxEnvTgt, int maxLfo, int maxLfoTgt, int maxFXChain, int maxFXPerChain);
	virtual ~Wrecker();

	void setBufsize(int bufsize);

	void setSampleInfo(long ntf, short nc);

	bool setTempo(float t);

	void fire();
	void stop();
	void reloop();

	void setGain(float g);
	void setPan(float p);
	void setTune(float t, float e=0, float l=0);

	bool setLFOTarget(int which, int whichn, int cc, float amt);
	bool setEnvelopeTarget(int which, int whichn, int cc, float amt);

	bool setBusGain(int whichBus, float g);
	bool setBusPan(int whichBus, float p);
	bool setBusEnable(int whichBus, bool en);
	bool setFXType(int whichBus, int whichFX, int type, int nParams, float *params);
	bool setFXEnable(int whichBus, int whichFX, bool en);
	bool setFXParam(int whichBus, int whichFX, int whichParam, float val);
	bool setFXParams(int whichBus, int whichFX, int nParams, float *params);

	int playChunkMixer(
			MasterBus *mixer, int buffInd, int nIterSliceFrames, int nIterDataFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, float nextBufferStartL, float nextBufferStartR, short direction, short fxBus);

	short id;

	float gain;
	float pan;

	float tune;
	float realTune;
	float psi;
	bool hasTuneMod;

	float lastFraction;
	long nextDataFrame;

	long nTotalFrames;
	short nChannels;

protected:
	int playChunkReplacing(
			float *buff, int nIterSliceFrames, int nIterDataFrames, short nOutChannels, int currentDataFrame,
			float *chunkData, int chunkStart, int chunkLen, float nextBufferStartL, float nextBufferStartR, short direction, short fxBus);
	void calculateControlsMono(float &l);
	void calculateControlsStereo(float &l, float &r);
	void applyFXStereo(float *in, int nControlChunkFrames, short fxBus, float *out);

	unsigned int controlFrame;

	float lAmp;
	float rAmp;

	std::vector<float> lfoOffset;
	std::vector<float> envOffset;

	std::unordered_set<int> modBus;
	std::unordered_set<int> modFx;

	std::vector<OBus*> fxBus;

	float *buffer;
	float *leftCycleBuffer;
	float *rightCycleBuffer;
	float *preFXBuffer;
};

extern BoundedLookup pow2_6;
extern BoundedLookup exp2_tune_psi;

#endif /* WRECKER_H_ */
