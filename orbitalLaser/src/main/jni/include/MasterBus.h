/*
 * BusArray.h
 *
 *  Created on: Oct 21, 2014
 *      Author: dak
 */

#ifndef MASTERBUS_H_
#define MASTERBUS_H_

#include <stdlib.h>
#include <vector>

class MasterBus {
public:
	MasterBus(int bufSize);
	virtual ~MasterBus();

	unsigned int mxn(float *buffer, int offset, int nFrame, float gain);
	void mix(int nActiveBus, short *outbufC, int nFramePerBuf, short nOutChannels);
	void zeroBuffers(int nFramePerBuf);

	void setBufsize(int bufsize);

protected:
	float 				*buffer;
	long				currentBufSize;
};

#endif /* MASTERBUS_H_ */
