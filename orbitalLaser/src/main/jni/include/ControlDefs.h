/*
 * ControlDefs.h
 *
 *  Created on: Oct 23, 2013
 *      Author: dak
 */

#ifndef CONTROLDEFS_H_
#define CONTROLDEFS_H_

#include "com_mayaswell_audio_Control.h"
#include "com_mayaswell_orbital_CControl.h"
#include "com_mayaswell_orbital_OrbitalLaser_Global.h"

// OrbitalLaser.ENV_PER_PAD *
//		attackT
//		decayT
//		sustainT
//		sustainL
//		releaseT
// 		+ OrbitalLaser.MAX_ENV_TGT * target
		inline int envParam4Id(int i) { return (i-com_mayaswell_orbital_CControl_ENV_BASE)%(com_mayaswell_orbital_OrbitalLaser_Global_MAX_ENV_TGT+5); }
		inline int env4Id(int i) { return (i-com_mayaswell_orbital_CControl_ENV_BASE)/(com_mayaswell_orbital_OrbitalLaser_Global_MAX_ENV_TGT+5); }
		inline int envIdBase(int i) { return com_mayaswell_orbital_CControl_ENV_BASE+i*(com_mayaswell_orbital_OrbitalLaser_Global_MAX_ENV_TGT+5); }
		inline int envAttackTimeId(int i) { return 0+envIdBase(i); }
		inline int envDecayTimeId(int i) { return 1+envIdBase(i); }
		inline int envSustainTimeId(int i) { return 2+envIdBase(i); }
		inline int envSustainLevelId(int i) { return 3+envIdBase(i); }
		inline int envReleaseTimeId(int i) { return 4+envIdBase(i); }
		inline int envTargetDepthId(int i, int j) { return j+5+envIdBase(i); }
// OrbitalLaser.LFO_PER_PAD *
//		rate
// 		+ OrbitalLaser.MAX_LFO_TGT * target
		inline int lfoParam4Id(int i) { return (i-envIdBase(com_mayaswell_orbital_OrbitalLaser_Global_ENV_PER_WRECK))%(com_mayaswell_orbital_OrbitalLaser_Global_MAX_LFO_TGT+2); }
		inline int lfo4Id(int i) { return (i-envIdBase(com_mayaswell_orbital_OrbitalLaser_Global_ENV_PER_WRECK))/(com_mayaswell_orbital_OrbitalLaser_Global_MAX_LFO_TGT+2); }
		inline int lfoIdBase(int i) { return envIdBase(com_mayaswell_orbital_OrbitalLaser_Global_ENV_PER_WRECK)+i*(com_mayaswell_orbital_OrbitalLaser_Global_MAX_LFO_TGT+2); }
		inline int lfoRateId(int i) { return 0+lfoIdBase(i); }
		inline int lfoPhaseId(int i) { return 1+lfoIdBase(i); }
		inline int lfoTargetDepthId(int i, int j) { return j+2+lfoIdBase(i); }

//OrbitalLaser.MAX_BUS_PER_WRECK *
//	gain + pan + enable
		inline int busIdBase(int i) { return (lfoIdBase(com_mayaswell_orbital_OrbitalLaser_Global_LFO_PER_WRECK) + 3*i); }
		inline int busGainId(int i) { return (busIdBase(i) + 0); }
		inline int busPanId(int i) { return (busIdBase(i) + 1); }
		inline int busEnableId(int i) { return (busIdBase(i) + 2); }
		inline int bus4Id(int id) { return (id-busIdBase(0))/3; }
		inline int busParam4Id(int id) { return (id-busIdBase(0))%3; }

//OrbitalLaser.MAX_BUS_PER_WRECK * OrbitalLaser.MAX_BUS_FX *
//	 enable + param 0-MAX_FX_PARAM
	//params are 0 enable, 1-6 floating point parameters
		inline int fxIdBase(int b, int i) {
			return busIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_PER_WRECK)+
					(b*com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX+i)*
						(com_mayaswell_orbital_OrbitalLaser_Global_MAX_FX_PARAM+1); }
		inline int fxEnableId(int b, int i) { return 0+fxIdBase(b, i); }
		inline int fxParamId(int b, int i, int j) { return 1+j+fxIdBase(b, i); }
		inline int param4fxId(int id) {
			return ((id-busIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_PER_WRECK))
					% (com_mayaswell_orbital_OrbitalLaser_Global_MAX_FX_PARAM+1)); }
		inline int bfx4Id(int id) {
			return (id-busIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_PER_WRECK))
					/ (com_mayaswell_orbital_OrbitalLaser_Global_MAX_FX_PARAM+1); }
		inline int bus4fxId(int id) {
			return bfx4Id(id) / com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX; }
		inline int fx4fxId(int id) {
			return bfx4Id(id) % com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX; }


		inline bool isEnvControl(int typeCode) {
			return typeCode >= envIdBase(0) && typeCode < envIdBase(com_mayaswell_orbital_OrbitalLaser_Global_ENV_PER_WRECK);
		}
		inline bool isLFOControl(int typeCode) {
			return typeCode >= lfoIdBase(0) && typeCode < lfoIdBase(com_mayaswell_orbital_OrbitalLaser_Global_LFO_PER_WRECK);
		}
		inline bool isBusControl(int typeCode) {
			return typeCode >= busIdBase(0) && typeCode < busIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX);
		}
		inline bool isFXControl(int typeCode) {
			return typeCode >= fxIdBase(0,0) && typeCode < fxIdBase(com_mayaswell_orbital_OrbitalLaser_Global_MAX_BUS_FX,0);
		}

#endif /* CONTROLDEFS_H_ */
