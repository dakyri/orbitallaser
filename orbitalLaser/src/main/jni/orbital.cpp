/*
 * orbital.cpp
 *
 *  Created on: Oct 19, 2014
 *      Author: dak
 */


#include <stdlib.h>

#include "com_mayaswell_audio_Controllable.h"
#include "com_mayaswell_orbital_Wrecker.h"
#include "com_mayaswell_orbital_MasterBus.h"

#include "Wrecker.h"
#include "MasterBus.h"

#include <android/log.h>

const float A_E	= 2.0;
const float pow2Range = 6;
const float pad2Octs = (com_mayaswell_audio_Controllable_TUNE_RANGE_SEMITONES/12);

FunctionTable 		pow2Wavetable(wavetableLength);
FunctionTable 		exp2TuneWavetable(wavetableLength);

BoundedLookup		pow2_6(&pow2Wavetable, -pow2Range, pow2Range);
BoundedLookup		exp2_tune_psi(&exp2TuneWavetable, -pad2Octs, pad2Octs);

JNIEXPORT jlong JNICALL Java_com_mayaswell_orbital_Wrecker_allocWrecker(JNIEnv *env, jclass obj, jint id,
		jint nEnv, jint nEnvTgt, jint nLfo, jint nLfoTgt,
		jint maxFXChain, jint maxFXPerChain)
{
	return (jlong)(new Wrecker(id, nEnv, nEnvTgt, nLfo, nLfoTgt,  maxFXChain, maxFXPerChain));
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_deleteWrecker(JNIEnv *env, jclass obj, jlong wreckerPointer)
{
	if (wreckerPointer == 0) return;
	delete ((Wrecker *)wreckerPointer);

}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerBufsize(JNIEnv *env, jclass obj, jlong wreckerPointer, jint bufsize)
{
	if (wreckerPointer == 0) return;
	((Wrecker *)wreckerPointer)->setBufsize(bufsize);

}


JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_globalWreckerInit(JNIEnv *env, jclass obj)
{
	int i;
	for(i=0;i<=wavetableLength;i++) {
		sineWavetable[i] = sin(2*M_PI*((float)i/(float)wavetableLength));
		negSawWavetable[i] = ((float)(wavetableLength-2*i)/(float)wavetableLength);
		posSawWavetable[i] = ((float)(2*i-wavetableLength)/(float)wavetableLength);
		negExpWavetable[i] =
			(2*exp(A_E* ((float)(wavetableLength-2*i)/(float)wavetableLength))/exp(A_E)) - 1;
		posExpWavetable[i] =
			(2*exp(A_E* ((float)(2*i-wavetableLength)/(float)wavetableLength))/exp(A_E)) - 1;
		squareWavetable[i] = (i<(wavetableLength/2))?1:-1;

		pow2Wavetable[i] = pow(2, pow2Range*((float)(2*i-wavetableLength))/wavetableLength);
		exp2TuneWavetable[i] = pow(2, pad2Octs*((float)(2*i-wavetableLength))/wavetableLength);
	}
}


JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_globalWreckerCleanup(JNIEnv *env, jclass obj)
{

}


JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerSampleInfo(JNIEnv *env, jclass obj, jlong wreckerPointer, jlong ntf, jshort nc)
{
	if (wreckerPointer == 0) return;
	((Wrecker *)wreckerPointer)->setSampleInfo(ntf, nc);

}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerGain(JNIEnv *env, jclass obj, jlong wreckerPointer, jfloat v)
{
	if (wreckerPointer == 0) return;
	((Wrecker*)wreckerPointer)->setGain(v);
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerPan(JNIEnv *env, jclass obj, jlong wreckerPointer, jfloat v)
{
	if (wreckerPointer == 0) return;
	((Wrecker*)wreckerPointer)->setPan(v);
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerTune(JNIEnv *env, jclass obj, jlong wreckerPointer, jfloat v)
{
	if (wreckerPointer == 0) return;
	((Wrecker*)wreckerPointer)->setTune(v);
}

JNIEXPORT jint JNICALL Java_com_mayaswell_orbital_Wrecker_playChunkMixer(JNIEnv *env, jclass obj, jlong wreckerPointer, jlong busArrayPointer,
		jint buffInd, jint nIterSliceFrames, jint nIterDataFrames, jshort nOutChannels, jint currentDataFrame, jfloatArray chunkData, jint chunkStart, jint chunkLen,
		jfloat nextL, jfloat nextR, jint direction, jshort fxBus)
{
	if (wreckerPointer == 0 || busArrayPointer == 0) return (jint)0;
	unsigned char isCopyChunk;

	jfloat *chunkDataC;
	if (chunkData != NULL) {
		chunkDataC = env->GetFloatArrayElements(chunkData, &isCopyChunk);
		if (chunkDataC == NULL) return (jint)0;
	} else {
		chunkDataC = NULL;
		chunkLen = 0;
		chunkStart = 0;
	}

	nIterSliceFrames = ((Wrecker *)wreckerPointer)->playChunkMixer(
			((MasterBus *)busArrayPointer), buffInd, nIterSliceFrames, nIterDataFrames, nOutChannels, currentDataFrame,
			chunkDataC, chunkStart, chunkLen, nextL, nextR, direction, fxBus);

	if (chunkDataC != NULL) env->ReleaseFloatArrayElements(chunkData, chunkDataC, 0);
	return (jint)nIterSliceFrames;
}


JNIEXPORT jint JNICALL Java_com_mayaswell_orbital_Wrecker_getNextDataFrame(JNIEnv *env, jclass obj, jlong wreckerPointer)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->nextDataFrame;
}


JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerNLFO (JNIEnv *env, jclass obj, jlong wreckerPointer, jint nmx)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setNLFO(nmx);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerLFO(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi,
		jbyte wf, jbyte rst, jboolean lock, jfloat rate, jfloat phs)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setLFO(mxi, wf, rst, lock, rate, phs);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerLFOTgt(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi, jint txi, jint cc, jfloat v)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setLFOTarget(mxi, txi, cc, v);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerNLFOTgt(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi, jint nmxt)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setNLFOTarget(mxi, nmxt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_resetWreckerLFO(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi)
{
	if (wreckerPointer == 0) return (jint)0;
	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "resetLFO %d", mxi);
	return ((Wrecker *)wreckerPointer)->resetLFO(mxi);
}
JNIEXPORT jfloat JNICALL Java_com_mayaswell_orbital_Wrecker_getWreckerLFO(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->getLFO(mxi);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerNEnv(JNIEnv *env, jclass obj, jlong wreckerPointer, jint nmx)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setNEnvelope(nmx);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerEnv(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi,
		jbyte rst, jboolean lock, jfloat attT, jfloat decayT, jfloat susT, jfloat susL, jfloat relT)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setEnvelope(mxi, rst, lock, attT, decayT, susT, susL, relT);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerEnvTgt(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi, jint txi, jint cc, jfloat v)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setEnvelopeTarget(mxi, txi, cc, v);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerNEnvTgt(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi, jint nmxt)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->setNEnvelopeTarget(mxi, nmxt);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_resetWreckerEnv(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->resetEnvelope(mxi);
}
JNIEXPORT jfloat JNICALL Java_com_mayaswell_orbital_Wrecker_getWreckerEnv(JNIEnv *env, jclass obj, jlong wreckerPointer, jint mxi)
{
	if (wreckerPointer == 0) return (jint)0;
	return ((Wrecker *)wreckerPointer)->getEnvelope(mxi);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setWreckerTempo(JNIEnv *env, jclass obj, jlong wreckerPointer, jfloat t)
{
	if (wreckerPointer == 0) return false;
	return ((Wrecker *)wreckerPointer)->setTempo(t);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setBusGain(JNIEnv *env, jclass obj, jlong wreckerPointer, jint whichBus, jfloat g)
{
	if (wreckerPointer == 0) return false;
	return ((Wrecker *)wreckerPointer)->setBusGain(whichBus, g);
}
JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setBusPan(JNIEnv *env, jclass obj, jlong wreckerPointer, jint whichBus, jfloat p)
{
	if (wreckerPointer == 0) return false;
	return ((Wrecker *)wreckerPointer)->setBusPan(whichBus, p);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setBusEnable(JNIEnv *env, jclass obj, jlong wreckerPointer, jint whichBus, jboolean en)
{
	if (wreckerPointer == 0) return false;
	return ((Wrecker *)wreckerPointer)->setBusEnable(whichBus, en);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setFXType(JNIEnv *env, jclass obj, jlong wreckerPointer,
		jint whichBus, jint whichFX, jint type, jint np, jfloatArray paramsJ)
{
	if (wreckerPointer == 0) return false;

	unsigned char isCopyBuf;
	jfloat *paramsC = NULL;

	if (paramsJ != NULL) {
		paramsC = env->GetFloatArrayElements(paramsJ, &isCopyBuf);
		if (paramsC == NULL) return false;
	}
	return ((Wrecker *)wreckerPointer)->setFXType(whichBus, whichFX, type, np, paramsC);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setFXEnable(JNIEnv *env, jclass obj, jlong wreckerPointer, jint whichBus, jint whichFX, jboolean en)
{
	if (wreckerPointer == 0) return false;
	return ((Wrecker *)wreckerPointer)->setFXEnable(whichBus, whichFX, en);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setFXParam(JNIEnv *env, jclass obj, jlong wreckerPointer, jint whichBus, jint whichFX, jint whichParam, jfloat v)
{
	if (wreckerPointer == 0) return false;
	return ((Wrecker *)wreckerPointer)->setFXParam(whichBus, whichFX, whichParam, v);
}

JNIEXPORT jboolean JNICALL Java_com_mayaswell_orbital_Wrecker_setFXParams(JNIEnv *env, jclass obj, jlong wreckerPointer, jint whichFX, jint type, jint np, jfloatArray paramsJ)
{
	if (wreckerPointer == 0) return false;

	unsigned char isCopyBuf;
	jfloat *paramsC = NULL;

	if (paramsJ != NULL) {
		paramsC = env->GetFloatArrayElements(paramsJ, &isCopyBuf);
		if (paramsC == NULL) return false;
	}
	return ((Wrecker *)wreckerPointer)->setFXParams(whichFX, type, np, paramsC);
}


JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_reloopWrecker(JNIEnv *env, jclass obj, jlong wreckerPointer)
{
	if (wreckerPointer == 0) return;
	((Wrecker *)wreckerPointer)->reloop();
}


JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_stopWrecker(JNIEnv *env, jclass obj, jlong wreckerPointer)
{
	if (wreckerPointer == 0) return;
	((Wrecker *)wreckerPointer)->stop();

}


JNIEXPORT void JNICALL Java_com_mayaswell_orbital_Wrecker_fireWrecker(JNIEnv *env, jclass obj, jlong wreckerPointer)
{
	if (wreckerPointer == 0) return;
	((Wrecker *)wreckerPointer)->fire();

}

/**
 * BusArray
 */

JNIEXPORT jlong JNICALL Java_com_mayaswell_orbital_MasterBus_allocBmx(JNIEnv *env, jclass obj, jint bufSize)
{
	return (jlong)(new MasterBus(bufSize));
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_MasterBus_deleteBmx(JNIEnv *env, jclass obj, jlong bmxPointer)
{
	if (bmxPointer == 0) return;
	delete ((MasterBus *)bmxPointer);
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_MasterBus_setBmxBufsize(JNIEnv *env, jclass obj, jlong bmxPointer, jint bufSize)
{
	if (bmxPointer == 0) return;
	((MasterBus *)bmxPointer)->setBufsize(bufSize);
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_MasterBus_globalBmxInit(JNIEnv *env, jclass obj)
{
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_MasterBus_globalBmxCleanup(JNIEnv *env, jclass obj)
{

}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_MasterBus_zeroBmxBuffers(JNIEnv *env, jclass obj, jlong bmxPointer, jint nFramePerBuf)
{
	if (bmxPointer == 0) return;
	((MasterBus *)bmxPointer)->zeroBuffers(nFramePerBuf);
}

JNIEXPORT void JNICALL Java_com_mayaswell_orbital_MasterBus_bmxMix(
		JNIEnv *env, jclass obj, jlong bmxPointer, jint nActiveBus, jshortArray outBuffer, jint nFramePerBuf, jshort nOutChannels)
{
	if (bmxPointer == 0) return;
	unsigned char isCopyBuf;

	jshort *outbufC;

	if (outBuffer != NULL) {
		outbufC = env->GetShortArrayElements(outBuffer, &isCopyBuf);
		if (outbufC == NULL) return;
	}
	((MasterBus *)bmxPointer)->mix(nActiveBus, outbufC, nFramePerBuf, nOutChannels);
	if (outbufC != NULL) env->ReleaseShortArrayElements(outBuffer, outbufC, 0);
	return;
}
