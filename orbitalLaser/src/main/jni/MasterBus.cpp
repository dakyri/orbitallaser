/*
 * BusArray.cpp
 *
 *  Created on: Oct 21, 2014
 *      Author: dak
 */
#include <limits.h>
#include <android/log.h>

#include "MasterBus.h"

MasterBus::MasterBus(int bufSize) {
	currentBufSize = 0;
	buffer = NULL;
	setBufsize(bufSize);
}

MasterBus::~MasterBus() {
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "MasterBus::~MasterBus");
	if (buffer) {
		delete [] buffer;
	}
}

void
MasterBus::setBufsize(int bufsize)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "MasterBus::setBufSize %d", bufsize);
	currentBufSize = bufsize;
	if (buffer != NULL) {
		delete [] buffer;
	}
	buffer = new float[bufsize];
}

void
MasterBus::mix(int nActiveBus, short *outBuffer, int nFramePerBuf, short nOutChannels)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "MasterBus::mix %d", nFramePerBuf);
	int ns = nFramePerBuf*nOutChannels;
	for(int i=0; i < ns; i++) {
		float b=buffer[i];
		outBuffer[i] = (short)(SHRT_MAX*(b<-1.0f?-1.0f:(b>1.0f?1.0f:b)));
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "mixer", "mix %d %g %g %d %d", nFramePerBuf, buffer[0], buffer[1], outBuffer[0], outBuffer[1]);
}

unsigned int
MasterBus::mxn(float *dta, int offset, int nFrame, float gain)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "MasterBus::mxn %d", nFrame);
	int nb = nFrame * 2;
	if (nb > currentBufSize-offset) nb = currentBufSize-offset;
	float *b = buffer+offset;
	for (int i=0; i<nb; i++, b++, dta++) {
		*b += *dta * gain;
	}
//	__android_log_print(ANDROID_LOG_DEBUG, "mixer", "mxn %d %g %g %g", offset, buffer[offset], buffer[offset+1], gain);
	return nFrame;
}

void
MasterBus::zeroBuffers(int nFramePerBuf)
{
//	__android_log_print(ANDROID_LOG_DEBUG, "CWrecker", "MasterBus::zero %d", nFramePerBuf);
	memset(buffer, 0, 2*nFramePerBuf*sizeof(float));
}
